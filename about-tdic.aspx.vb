﻿
Partial Class about_tdic
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, Link, MapImage, GalleryID, SecondText from List_Saadiyat where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 4
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                lblDetails.Text = ManipulateListing(reader("BigDetails").ToString()) & Utility.showEditButton(Request, "/Admin/A-Saadiyat/ItemEdit.aspx?lid=" & hdnID.Value)
                title = reader("Title").ToString()
                '  galid = reader("GalleryID")
                lblSub.Text = FormateTitle(reader("SubTitle").ToString())
                link1 = reader("Link").ToString()
                DynamicSEO.PageType = "List_Saadiyat"
                DynamicSEO.PageID = hdnID.Value
            End While
            conn.Close()

            styledtitle = FormateTitle(title)
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2) - 1
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = "<span>" & firstpart & "</span>" & secondpart
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where MasterID not in (5,6,7,8) and Lang=@Lang and Status=1 order by  SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"

            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If


            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle dbluebg"">")
        Return bigText
    End Function
End Class
