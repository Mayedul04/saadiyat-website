﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class news
    Inherits System.Web.UI.Page
    Public PageList As String
    Public fbImglnk As String
    Public fbTitle As String
    Public fbUrl As String
    Public fbDesc As String
    Public Title As String = "", htmlmasterid As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""

        If Page.RouteData.Values("category") = "press-release" Then
            htmlmasterid = "25"
            pnlSocial.Visible = False
            '  pnlNewsSubs.Visible = False
        ElseIf Page.RouteData.Values("category") = "features" Then
            htmlmasterid = "26"
            pnlSocial.Visible = False
            pnlNewsSubs.Visible = False
        Else
            htmlmasterid = "27"
        End If
        If Session("lang") = "ar" Then
            btnSubmit.Text = Language.Read("Subscribe", Page.RouteData.Values("lang"))
        End If
        HTML(htmlmasterid, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        hdnID.Value = HTMLID

        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function socialShare(ByVal Sharetitle As String, ByVal ShareDesc As String, ByVal ShareImage As String) As String
        Dim M As String = ""

        Dim title As String = Utility.EncodeTitle(Sharetitle, "-")
        Dim imgUrl As String = Session("domainName") & "Admin/" & ShareImage
        Dim desc As String = ShareDesc
        Dim lnk As String = Request.Url.ToString '"http://" & Request.Url.Host & "/e-reportdetail.aspx?rid=" & Request.QueryString("rid") & "&t=" & Utility.EncodeTitle(Sharetitle, "-")
        Dim twiturl As String = "https://twitter.com/share?url=" & lnk & "&text=" & title
        Dim googleurl As String = "https://plus.google.com/share?url=" & lnk





        fbTitle = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:title"" content=""" & title & """/>"
        fbImglnk = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:image"" content=""" & imgUrl & """/>"
        fbDesc = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:description"" content=""" & desc & """/>"
        fbUrl = "<meta prefix=""og: http://ogp.me/ns#"" property=""og:url"" content=""" & lnk & """/>"


        Page.Header.Title = title
        Dim meta As HtmlMeta = New HtmlMeta
        meta.Name = "description"
        meta.Content = desc
        Me.Page.Header.Controls.Add(meta)





        M += " <ul class=""socialIconsListings right"">"
        M += "  <li>"
        M += "   <a href=""#"" onclick=""compshare('" & title & "','" & imgUrl & "','" & desc & "','" & lnk & "'); return false;""> <img src=""" & Session("domainName") & "ui/media/dist/icons/fb-share.png""> </a>"
        M += "  </li>                       "
        M += "    <li>"
        M += "  <a target=""_blank"" href=""javascript:;"" rel=""nofollow""  onclick=""MM_openBrWindow('" & twiturl & "','TEXTEDITOR','scrollbars=yes,resizable=yes,width=700,height=300')"" > <img src=""" & Session("domainName") & "ui/media/dist/icons/tweet-count.png""> </a>"

        M += "  </li>"

        M += "  <li>"
        M += "   <a href=""" & googleurl & """ onclick=""javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;""> <img src=""" & Session("domainName") & "ui/media/dist/icons/gplus-icon.png""> </a>"
        M += "  </li>"




        M += " </ul>"

        Return M

    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftNav(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang,MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li>"
            retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """>"
            If Request.Url.AbsoluteUri.Contains(link) Then
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            Else
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            End If
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""
            ltSocialBar.Text = socialShare(reader("Title").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), reader("SmallDetails").ToString.Replace("""", "-").Replace("'", "").Replace("’", "").Replace("‘", "").Replace("@", "-at-").Replace("&", "-").Replace("(", "-").Replace(")", "-").Replace(",", "").Replace(":", "").Replace("/", "").Replace("\", "").Replace("?", "").Replace("!", "").Replace("؟", ""), If(reader("SmallImage").ToString = "" Or IsDBNull(reader("SmallImage").ToString), "Content/Noimages.jpg", reader("SmallImage").ToString))
        End If
        conn.Close()
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim secondvalue As Integer = 0
        Dim selectString1 As String = ""
        Dim sConn As String
        If Page.RouteData.Values("category") = "new" Then
            If ddsType.SelectedValue <> "" Then
                selectString1 = "Select COUNT(0) from List_News where Category=@Category  and Lang=@Lang and NewsType=@NewsType and Status=1"
            Else
                selectString1 = "Select COUNT(0) from List_News where Category=@Category  and Lang=@Lang  and Status=1"
            End If

        Else
            selectString1 = "Select COUNT(0) from List_News where Category=@Category  and Lang=@Lang  and Status=1"
        End If
        If ddlMonth.SelectedValue <> "" Then
            selectString1 += " and DATEPART(MM, ReportDate)=@ReportMonth"
        End If
        If ddlYear.SelectedValue <> "" Then
            selectString1 += " and DATEPART(yy, ReportDate)=@ReportYear"
        End If
        '  Dim selectString2 As String = "SELECT COUNT(0)  FROM [dbo].[CommonGallery] where Status=1 GROUP BY TableName,TableMasterID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        If Page.RouteData.Values("category") = "press-release" Then
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "Press"
            ' cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "NULL"
        ElseIf Page.RouteData.Values("category") = "features" Then
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "Featured"
            ' cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "NULL"
        Else
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "News"
            '  cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "test 1"
        End If
        If ddlMonth.SelectedValue <> "" Then
            cmd.Parameters.Add("ReportMonth", Data.SqlDbType.Int).Value = ddlMonth.SelectedValue
        End If
        If ddlYear.SelectedValue <> "" Then
            cmd.Parameters.Add("ReportYear", Data.SqlDbType.Int).Value = ddlYear.SelectedValue
        End If
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang").ToString()
        If Page.RouteData.Values("category") = "new" Then
            If ddsType.SelectedValue <> "" Then
                cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 50).Value = ddsType.SelectedValue
            End If

        End If

        totalRows = Math.Ceiling((cmd.ExecuteScalar) / 5)
        cn.Close()
        Return totalRows
    End Function
    Public Function NewsGallery() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = Session("domainName").ToString() & Session("lang").ToString() & "/news/" & Page.RouteData.Values("category") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim retstr As String = "<ul class=""csrListing mediaListings"">"
        Dim link1 As String = ""
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = ""




        '  selectString1 += "  (Select Gallery.GalleryId as MasterID, Gallery.Title, Gallery.ArTitle  , Gallery.SmallImage , Gallery.LastUpdated , 'Gallery' as TName, ParentGalleryID from Gallery where Status=1 and ParentGalleryID=@PGalleryID)"
        If Page.RouteData.Values("category") = "new" Then
            If ddsType.SelectedValue <> "" Then
                selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 5; Select * from ( select  ROW_NUMBER()  over(ORDER BY List_News.Title ASC) AS RowNum  , NewsID, Title , MasterID, SmallDetails, SmallImage, NewsType, Publisher,FileUploaded, ReportDate, LastUpdated from List_News where  Category=@Category and Lang=@Lang and NewsType=@NewsType and Status=1 "
            Else
                selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 5; Select * from ( select  ROW_NUMBER()  over(ORDER BY List_News.Title ASC) AS RowNum  , NewsID, Title , MasterID, SmallDetails, SmallImage, NewsType, Publisher,FileUploaded, ReportDate, LastUpdated from List_News where  Category=@Category and Lang=@Lang  and Status=1 "
            End If

        Else
            selectString = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 5; Select * from ( select  ROW_NUMBER()  over(ORDER BY List_News.Title ASC) AS RowNum  , NewsID, Title , MasterID, SmallDetails, SmallImage, NewsType, Publisher,FileUploaded, ReportDate, LastUpdated from List_News where  Category=@Category and Lang=@Lang  and Status=1 "
        End If
        If ddlMonth.SelectedValue <> "" Then
            selectString += " and DATEPART(MM, ReportDate)=@ReportMonth"
        End If
        If ddlYear.SelectedValue <> "" Then
            selectString += " and DATEPART(yy, ReportDate)=@ReportYear"
        End If
        selectString += ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        If Page.RouteData.Values("category") = "press-release" Then
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "Press"
            '  cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "NULL"
        ElseIf Page.RouteData.Values("category") = "features" Then
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "Featured"
            '   cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "NULL"
        Else
            cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = "News"
            ' cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 150).Value = "12"
        End If
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang").ToString()
        If Page.RouteData.Values("category") = "new" Then
            If ddsType.SelectedValue <> "" Then
                cmd.Parameters.Add("NewsType", Data.SqlDbType.NVarChar, 50).Value = ddsType.SelectedValue
            End If

        End If
        If ddlMonth.SelectedValue <> "" Then
            cmd.Parameters.Add("ReportMonth", Data.SqlDbType.Int).Value = ddlMonth.SelectedValue
        End If
        If ddlYear.SelectedValue <> "" Then
            cmd.Parameters.Add("ReportYear", Data.SqlDbType.Int).Value = ddlYear.SelectedValue
        End If
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read
                If Page.RouteData.Values("category") = "press-release" Then
                    link1 = "<a class=""clickHereButton"" target=""_blank"" href=""" & Session("domainName") & "Admin/" & reader("FileUploaded").ToString() & """>View press release <span>"
                ElseIf Page.RouteData.Values("category") = "features" Then
                    link1 = "<a class=""clickHereButton"" target=""_blank"" href=""" & Session("domainName") & "Admin/" & reader("FileUploaded").ToString() & """>View Features <span>"
                Else
                    link1 = "<a class=""clickHereButton"" href=""" & Session("domainName").ToString() & Session("lang") & "/news-details/" & Page.RouteData.Values("category") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>More Information <span>"
                End If



                retstr += "<li>"
                retstr += "<div class=""csrContentSection"">"
                retstr += " <div class=""contentPart"">"
                If IsDBNull(reader("SmallImage")) = False Then
                    retstr += "<div class=""imgHold"">"
                    retstr += "<img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                End If



                retstr += "<h2>" & reader("Title").ToString() & "</h2>"
                retstr += "<ul class=""categoryNdate"">"
                If IsDBNull(reader("NewsType")) = False Then
                    retstr += "<li class=""right"">" & reader("NewsType") & "</li>"
                End If
                retstr += "<li>" & CDate(reader("ReportDate").ToString()).ToString("dd MMM, yyyy") & "</li></ul>"
                retstr += "<p>" & reader("SmallDetails").ToString() & "</p>"

                retstr += link1
                retstr += "<img src=""" & Session("domainName").ToString() & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                retstr += Utility.showEditButton(Request, Session("domainName").ToString() & "Admin/A-News/NewsEdit.aspx?nid=" & reader("NewsID") & "&Small=1&Big=0&Footer=0") & "</li>"


            End While
            retstr = retstr + "</ul>"
        Else

            retstr = "Sorry there is no news(s) to show."

            retstr += Utility.showAddButton(Request, Session("domainName").ToString() & "Admin/A-News/NewsEdit.aspx")

        End If

        cn.Close()
        If PageList = "" Then
            pnlPageination.Visible = False
        End If


        Return retstr
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""paginationList"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
    Function WRequest(URL As String, method As String, POSTdata As String) As String
        Dim responseData As String = ""
        Try
            Dim cookieJar As New Net.CookieContainer()
            Dim hwrequest As HttpWebRequest = CType(WebRequest.Create(URL), HttpWebRequest)
            'Dim hwrequest As Net.HttpWebRequest = Net.WebRequest.Create(URL)
            hwrequest.CookieContainer = cookieJar
            hwrequest.Accept = "*/*"
            hwrequest.AllowAutoRedirect = True
            hwrequest.UserAgent = "http_requester/1.1"
            hwrequest.Timeout = 60000
            hwrequest.Method = method
            If hwrequest.Method = "POST" Then
                hwrequest.ContentType = "application/x-www-form-urlencoded"
                Dim encoding As New Text.ASCIIEncoding() 'Use UTF8Encoding for XML requests
                Dim postByteArray() As Byte = encoding.GetBytes(POSTdata)
                hwrequest.ContentLength = postByteArray.Length
                Dim postStream As Stream = hwrequest.GetRequestStream()
                postStream.Write(postByteArray, 0, postByteArray.Length)
                postStream.Close()
            End If

            Dim hwresponse As WebResponse = hwrequest.GetResponse()
            responseData = New StreamReader(hwresponse.GetResponseStream()).ReadToEnd()
            'If hwresponse.StatusCode = Net.HttpStatusCode.OK Then
            '    Dim responseStream As IO.StreamReader = _
            '      New IO.StreamReader(hwresponse.GetResponseStream())
            '    responseData = responseStream.ReadToEnd()
            'End If
            hwresponse.Close()
        Catch e As Exception
            responseData = "An error occurred: " & e.Message
        End Try
        Return responseData
    End Function
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Dim _htmlRegex As New Regex("http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", RegexOptions.Singleline Or RegexOptions.IgnoreCase)
        Dim pattern As String = "((https?|ftp)\:\/\/)?" ' SCHEME
        pattern += "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?" ' // User and Pass
        pattern += "([a-z0-9-.]*)\.([a-z]{2,4})" '; // Host or IP
        pattern += "(\:[0-9]{2,5})?" '; // Port 
        pattern += "(\/([a-z0-9+\$_-]\.?)+)*\/?" '; // Path
        pattern += "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?" '; // GET Query
        pattern += "(#[a-z_.-][a-z0-9+\$_.-]*)?" '; // Anchor
        Dim _htmlRegex1 As New Regex(pattern, RegexOptions.Singleline Or RegexOptions.IgnoreCase)

        If Not _htmlRegex1.IsMatch(txtFirstName.Text) Then
            hdnContactDatetime.Value = DateTime.Now

            txtFirstName.Text = _htmlRegex1.Replace(txtFirstName.Text, "")
            txtLastName.Text = _htmlRegex1.Replace(txtLastName.Text, "")

            If sdsSubscribtion.Insert > 0 Then

                Dim m_ToAddress As String = "info@tdic.ae"
                Dim m_CCAddress As String = ""
                Dim m_BCCAddress As String = "debashis.chowdhury@wvss.net"
                Dim m_EmailFromName As [String] = txtFirstName.Text & " " & txtLastName.Text
                Dim m_EmailSubject As [String] = "Saadiyat: Subscribe Message"
                Dim m_EmailBody As [String] = GetRegistrationInfoMessage()

                Dim postData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactClass=1&Salutation=194787&FirstName=" & HttpUtility.UrlEncode(txtFirstName.Text) & "&FamilyName=" & HttpUtility.UrlEncode(txtLastName.Text) & "&Remarks=&MobileCountryCode=&MobileAreaCode=&MobileNumber=&TelephoneCountryCode=&TelephoneAreaCode=&Telephone=&EmailAddress1=" & HttpUtility.UrlEncode(txtEmail.Text) & "&EmailAddress2=&DirectMarketing=194577~1&NationalityID=&DateofBirth=&Gender=&MaritalStatus=&ResidenceCountryID=&PassportNumber=&PassportIssueDate=&PassportExpiryDate=&IDTypeID=&IDNumber=&"
                Dim wData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/InsertContactWithoutLead?", "POST", postData)
                '  MsgBox(wData)
                If wData.Contains("Success") Then
                    Utility.SendMail(m_EmailFromName, txtEmail.Text, m_ToAddress, m_CCAddress, m_BCCAddress, m_EmailSubject, m_EmailBody)
                    Response.Redirect("http://" & Request.Url.Host & "/" & Page.RouteData.Values("lang") & "/news-subscribtion-thankyou")
                Else

                    Dim doc = New XmlDocument()
                    doc.LoadXml(wData)
                    '  doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText
                    Dim contactId As String = Right(doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText, 6).Replace(".", "")
                    Dim secondpostData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactID=" & contactId & "&ContactEmail=" & HttpUtility.UrlEncode(txtEmail.Text) & "&UpdateValues=194577~1&"

                    Dim secondwData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/UpdateDirectMarketing?", "POST", secondpostData)

                    If secondwData.Contains("successfully") Then
                        Utility.SendMail(m_EmailFromName, txtEmail.Text, m_ToAddress, m_CCAddress, m_BCCAddress, m_EmailSubject, m_EmailBody)
                        Response.Redirect("http://" & Request.Url.Host & "/" & Page.RouteData.Values("lang") & "/news-subscribtion-thankyou")
                    End If
                End If

            End If
        Else
            txtFirstName.Text = _htmlRegex1.Replace(txtFirstName.Text, "")
            txtLastName.Text = _htmlRegex1.Replace(txtLastName.Text, "")
        End If


    End Sub
    Private Function GetRegistrationInfoMessage() As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
"<html lang='en'>" & _
"<head>" & _
"    <meta charset='utf-8'>" & _
"    <title>Royal Rose</title>" & _
"    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" & _
"    <body style='margin:0px; padding:0px;'>" & _
"        <table bgcolor='#ac7e14' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>" & _
"            <tr>" & _
"                <td style='padding:2px 0;'>" & _
"                    <table bgcolor='#fff' width='610' cellspacing='0' cellpadding='0' border='0' align='center' style='background:#ffffff;'>" & _
"                        <tr>" & _
"                            <td align='center'>" & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>&nbsp;</p>" & _
"                                <table width='90%' cellspacing='0' cellpadding='0' align='center' border='1' style='border:1px solid #eee; border-collapse: collapse; margin:0 0 20px;'>" & _
"                                    <tr bgcolor='#eee'>" & _
"                                        <td colspan='2'>" & _
"                          <div style=""text-align: center;""><a href=""http://" & Request.Url.Host & """><img src=""http://" & Request.Url.Host & "/ui/media/dist/elements/color-logo.png"" width=""165px""  alt=''></a></div>" & _
"                                            <h3 style='font-family:arial; color:#490109; font-size:24px; text-align:center; margin:10px 0;'>Subscribe Details</h3>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtFirstName.Text & " " & txtLastName.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Email</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtEmail.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                </table>                                " & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>© Copyright " & Date.Now.Year & ". All rights reserved by ~<a href='http://" & Request.Url.Host & "'>Saadiyat</a>~.</p>" & _
"                            </td>" & _
"                        </tr>  " & _
"                    </table>" & _
"                </td>" & _
"            </tr>" & _
"        </table>" & _
"    </body>" & _
"</html>"


        '"                        <tr>" & _
        '"                            <td align='center' style='padding:15px 0 7px;'>" & _
        '"                                <a href=' http://Site-URL.com '><img src='http://" & Request.Url.Host & "/ui/media/dist/email/hilton_logo2.png' width='200' alt=''></a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        '"                        <tr>" & _
        '"                            <td align='center' style='padding:0 0 10px;'>" & _
        '"                                <a style='font-family:arial; color:#490109; text-decoration:; font-size:14px;' href='http://Site-URL.com' target='_blank'>~Site Name~</a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        Return retVal
    End Function
End Class
