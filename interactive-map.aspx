﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="interactive-map.aspx.vb" Inherits="interactive_map" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="/ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="/ui/js/dist/jquery.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5rbpOaNw6fM6KxajU52byKxUr_Nnmsyc"></script> -->

    <script type="text/javascript">
        /**
         * Data for the markers consisting of a name, a LatLng and a zIndex for
         * the order in which these markers should display on top of each
         * other.
         */
        var markerData = [<%= retstr %>];



        var map;
        var markerArray = [];
        var overlay;
        var infowindow = new google.maps.InfoWindow;
        var rendermap = '/ui/media/dist/map/rendered-map2.png';
        var srcImage = '/ui/media/dist/images/transparent.png';

        USGSOverlay.prototype = new google.maps.OverlayView();

        // To center the map
        var myMapCenterLatlng = new google.maps.LatLng(24.5403768, 54.43546770);

        function initialize(display) {
            var styles = [
                // we will add the style rules here.  
                {
                    featureType: 'water',
                    elementType: 'geometry.fill',
                    stylers: [{
                        color: '#07405b'
                    }]
                },

                {
                    "featureType": "road",
                    "elementType": "labels",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }, {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{
                        "visibility": "off"
                    }]
                }

            ];


            // To display default value
            var display = display || "all";

            var mapOptions = {
                mapTypeControlOptions: {
                    mapTypeIds: ['Styled']
                },
                mapTypeId: 'Styled',
                zoom: 13,
                center: myMapCenterLatlng
            };


            map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            var styledMapType = new google.maps.StyledMapType(styles, {
                name: 'Styled'
            });
            map.mapTypes.set('Styled', styledMapType);


            // To set Marker
            // We can pass display string
            // Eg : setMarkers(map, markerData, 'education');
            setMarkers(map, markerData, display);

            // seeting image overlay at the top 

            var swBound = new google.maps.LatLng(24.486679898796446, 54.389190673828125); // it is south west / bottom left corner of the image
            var neBound = new google.maps.LatLng(24.589900050435183, 54.483089447021484); // it is north east /top right corner of the image
            var bounds = new google.maps.LatLngBounds(swBound, neBound);
            overlay = new USGSOverlay(bounds, srcImage, map);

        }



        /**
         * [setMarkers description]
         * @param {[object]} map
         * @param {[array]} location
         * @param {[string]} display
         */
        function setMarkers(map, locations, display) {



            for (var i = 0; i < locations.length; i++) {


                var loc = locations[i];
                var latlng = loc.position.split(',');
                var lat = latlng[0];
                var lng = latlng[1];
                var visiblebool = display === loc.type || display === 'all' ? true : false

                var myLatLng = new google.maps.LatLng(lat, lng);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    icon: "http://saadiyat.nexadesigns.com/ui/media/dist/icons/map-marker-yellow.png",
                    // shape: shape,
                    title: loc.title,
                    type: loc.type,
                    visible: visiblebool
                });

                markerArray.push(marker);

                var contentHtml = "<div class='map-popup'><h3>" + loc.title + "</h3>" + "<div class='imagethumbnail'><img src='" + loc.image + "'></div>" + "<div class='description'><p>" + loc.description + "</p>" + "<a href='" + loc.readmore + "' class='btnlink' target='_parent'>More Information</a>" + "<a href='https://maps.google.com/maps?saddr=Current+Location&daddr=" + loc.position + "' class='btnlink' target='_blank'>Get Directions</a></div></div>";

                // To open Individual INFOWINDOW
                bindInfoWindow(marker, contentHtml, infowindow);

            }


            function bindInfoWindow(marker, contentString, infowindow) {
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(contentString);
                    infowindow.open(map, marker);
                });

            }


        };



        //google.maps.event.addDomListener(window, 'load', initialize);

        $(document).on("click", ".mapcategory a", function () {
            $this = $(this);
            $display = $this.closest("li").attr("id");

            $display = $display || 'all';

            // To set Marker
            // We can pass display string
            // Eg : setMarkers(map, markerData, 'education');
            //initialize($display);
            console.log(markerArray);
            markArr($display)
        });

        $(document).on("click", "#googlemap", function () {
            $(this).toggleClass('active');

            if ($(this).hasClass('active')) {
                srcImage = '/ui/media/dist/images/transparent.png';
                initialize('all');
                $(this).text('Saadiyat Map');
            } else {
                srcImage = rendermap;
                initialize('all');
                $(this).text('Google Map');
            }


        });

        function markArr($display) {
            $.each(markerArray, function () {
                if (this.type == $display || $display == 'all') {
                    this.visible = true;
                    this.setMap(map);
                } else {
                    this.setMap(null);
                }
            });
        }

        function USGSOverlay(bounds, image, map) {
            $this = $(this);
            // Now initialize all properties.
            this.bounds_ = bounds;
            this.image_ = image;
            this.map_ = map;

            // Define a property to hold the image's div. We'll
            // actually create this div upon receipt of the onAdd()
            // method so we'll leave it null for now.
            this.div_ = null;

            // Explicitly call setMap on this overlay
            this.setMap(map);
        }

        /**
         * onAdd is called when the map's panes are ready and the overlay has been
         * added to the map.
         */
        USGSOverlay.prototype.onAdd = function () {

            var div = document.createElement('div');
            div.style.border = 'none';
            div.style.borderWidth = '0px';
            div.style.position = 'absolute';

            // Create the img element and attach it to the div.
            var img = document.createElement('img');
            img.src = this.image_;
            img.style.width = '100%';
            img.style.height = '100%';
            div.appendChild(img);

            this.div_ = div;

            // Add the element to the "overlayImage" pane.
            var panes = this.getPanes();
            panes.mapPane.appendChild(this.div_);
        };

        USGSOverlay.prototype.draw = function () {

            // We use the south-west and north-east
            // coordinates of the overlay to peg it to the correct position and size.
            // To do this, we need to retrieve the projection from the overlay.
            var overlayProjection = this.getProjection();

            // Retrieve the south-west and north-east coordinates of this overlay
            // in LatLngs and convert them to pixel coordinates.
            // We'll use these coordinates to resize the div.
            var sw = overlayProjection.fromLatLngToDivPixel(this.bounds_.getSouthWest());
            var ne = overlayProjection.fromLatLngToDivPixel(this.bounds_.getNorthEast());

            // Resize the image's div to fit the indicated dimensions.
            var div = this.div_;
            div.style.left = sw.x + 'px';
            div.style.top = ne.y + 'px';
            div.style.width = (ne.x - sw.x) + 'px';
            div.style.height = (sw.y - ne.y) + 'px';
        };

        USGSOverlay.prototype.onRemove = function () {
            this.div_.parentNode.removeChild(this.div_);
        };

        // Set the visibility to 'hidden' or 'visible'.
        USGSOverlay.prototype.hide = function () {
            if (this.div_) {
                // The visibility property must be a string enclosed in quotes.
                this.div_.style.visibility = 'hidden';
            }
        };

        USGSOverlay.prototype.show = function () {
            if (this.div_) {
                this.div_.style.visibility = 'visible';
            }
        };

        USGSOverlay.prototype.toggle = function () {
            if (this.div_) {
                if (this.div_.style.visibility == 'hidden') {
                    this.show();
                } else {
                    this.hide();
                }
            }
        };

        // Detach the map from the DOM via toggleDOM().
        // Note that if we later reattach the map, it will be visible again,
        // because the containing <div> is recreated in the overlay's onAdd() method.
        USGSOverlay.prototype.toggleDOM = function () {
            if (this.getMap()) {
                // Note: setMap(null) calls OverlayView.onRemove()
                this.setMap(null);
            } else {
                this.setMap(this.map_);
            }
        };


    </script>
    <style>
    #map-canvas {
        width: 100%;
        height: 500px;
    }
    #locs {
        margin-top: 0px;
        padding-top: 0px;
        margin-left: 10px;
        float: left;
        height: 500px;
        overflow-y: scroll;
    }
    .loc {
        border-style: solid;
        border-width: thin;
        width: 300px;
        padding: 5px;
        cursor: pointer;
        margin-top: 0px;
    }
    #form {
        margin-top: 0px;
        padding-top: 0px;
        margin-left: 10px;
        height: 60px;
    }
    label {
        width: 200px;
        padding: 5px;
    }
    </style>


</head>

<body onLoad="initialize()">

    <form id="form1" runat ="server">
        
    <%--<asp:Label ID="Label1"  runat="server" Text="S"></asp:Label>--%>
    <div id="locs"></div>
    <div id="map">

        <div id="map-canvas"></div>
    </div>

    <div class="mapnavigation">
        <h4>Select By Category</h4>
        <ul class="list-unstyled mapcategory">
            <li id="residentials">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map/1,2,3,17,18"%>'>Residentials</a></span>
            </li>
            <li id="resorts">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map/4,15"%>'>Hotels & Resorts</a></span>
            </li>
            <li id="leisure">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map/5,6,7,8"%>'>Leisure</a></span>
            </li>
            <li id="cultural">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map/9,10,11,12"%>'>Cultural</a></span>
            </li>

            <li id="education">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map/13,14,19"%>'>Education</a></span>
            </li>
            <li id="all">
                <span class="label"><a href='<%= Session("domainName") & "/en/interactive-map"%>'>All</a></span>
            </li>
            <li>
                <span class="label"><a href="javascript:;"  id="googlemap" class="active">Saadiyat Map</a></span>
            </li>
        </ul>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


</form>
</body>

</html>

