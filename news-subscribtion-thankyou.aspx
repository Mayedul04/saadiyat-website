﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="news-subscribtion-thankyou.aspx.vb" Inherits="thanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
     <!-- Main Content Section -->
            <div class="main-content-area">

                
                <div class="thankYouContent">
                    
                    <h2>Thank You</h2>

                  <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
                    

                    <a href='<%= Session("domainName").ToString() & Session("lang") & "/news/new"%>' class="clickHereButton">
                        Go To News <span><img alt="" src="/ui/media/dist/icons/readmore-arrow.png"></span>
                    </a>

                </div>


            </div>
            <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

