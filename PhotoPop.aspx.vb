﻿Imports System.Data.SqlClient

Partial Class PhotoPop
    Inherits System.Web.UI.Page
    Public PageList As String
    Public section As String
    Public shareurl As String = ""
    Public imageurl As String = ""
   
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        ' If IsPostBack = False Then

        LoadContent(Request.QueryString("id"))
        If Not Request.Cookies("RegUser") Is Nothing Then
            pnlMedia.Visible = True
            pnlPrint.Visible = False
        End If
        '  End If
    End Sub
    Public Sub LoadContent(ByVal id As Integer)
        Dim sConn As String
        Dim selectString1 As String = ""
        If Not Request.QueryString("src") Is Nothing Then
            selectString1 = "SELECT * FROM InstagramImage WHERE ImageID=@ImageID"
        Else
            selectString1 = "SELECT * FROM GalleryItem WHERE GalleryItemID=@GalleryItemID and Status=1 and isnull( ItemType,'') <>'Uploaded Video' "
        End If


        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)

        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        If Not Request.QueryString("src") Is Nothing Then
            cmd.Parameters.Add("ImageID", Data.SqlDbType.Int).Value = id
        Else
            cmd.Parameters.Add("GalleryItemID", Data.SqlDbType.Int).Value = id
        End If

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                Title = reader("Title").ToString()
                lblTitle.Text = reader("Title").ToString()
                imageurl = Session("domainName") & "Admin/" & reader("BigImage").ToString()
                ImgBig.ImageUrl = Session("domainName") & "Admin/" & reader("BigImage").ToString()
                shareurl = Request.QueryString("url") 'Session("domainName") & Session("lang") & "Photos/Gallery/" & reader("GalleryItemID") & "/" & Title
                'lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

            End While
       
        End If
        cn.Close()
    End Sub

    Protected Sub chkSelect_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelect.CheckedChanged
        If chkSelect.Checked = True Then
            If Not Request.Cookies("SaadiyatSelections") Is Nothing Then
                Dim tempids As String = Request.Cookies("SaadiyatSelections").Value
                Response.Cookies("SaadiyatSelections").Value = tempids & "," & Request.QueryString("id")
                Response.Cookies("SaadiyatSelections").Expires = Date.Now.AddHours(+5)
            Else
                HttpContext.Current.Response.AddHeader("p3p", "CP=""IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT""")
                Response.Cookies.Add(New HttpCookie("SaadiyatSelections"))
                Response.Cookies("SaadiyatSelections").Value = Request.QueryString("id")
                Response.Cookies("SaadiyatSelections").Expires = Date.Now.AddHours(+5)
            End If
        End If
        

    End Sub
End Class
