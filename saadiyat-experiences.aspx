﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="saadiyat-experiences.aspx.vb" Inherits="saadiyat_experiences" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider landingSlider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">Your  <span>Experience</span>
            </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'>Home</a></li>
                <li class="active">Your  Experience </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">

        <div class="intro-block">

            <h2 class="maintitle"><span>Your  </span>Experience</h2>

            <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
            <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
        </div>
        <asp:Literal ID="ltrsaadiyat" runat="server"></asp:Literal>
        <asp:Literal ID="ltrinspiration" runat="server"></asp:Literal>
        <asp:Literal ID="ltrleisure" runat="server"></asp:Literal>
        <asp:Literal ID="ltrhome" runat="server"></asp:Literal>
        <asp:Literal ID="ltreducation" runat="server"></asp:Literal>
        <asp:Literal ID="ltrstay" runat="server"></asp:Literal>
        <asp:Literal ID="ltrenvironment" runat="server"></asp:Literal>
        <asp:Literal ID="ltrEvents" runat="server"></asp:Literal>
        <asp:Literal ID="ltrBuy" runat="server"></asp:Literal>
        <asp:Literal ID="ltrInvest" runat="server"></asp:Literal> 
        

        


    </div>
    <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

