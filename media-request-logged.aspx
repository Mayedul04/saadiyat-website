﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="media-request-logged.aspx.vb" Inherits="media_request_logged" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
      <!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                   <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        Media <span> Library </span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") %>'>Home</a></li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/media-center" %>'>Media Centre</a></li>
                        <li class="active"> Media Image Request </li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                
                

                <h2 class="subtitle">
                   <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </h2>

                <div class="downloadTable">
                    
                    <table class="table table-bordered">
                      
                      <tbody>
                        <tr>
                          <td>Module</td>
                          <td>Album </td>
                          <td>Image Title</td>
                           <td>Image</td>
                        </tr>
                          <%= LoadMedia() %>
                      
                      </tbody>

                    </table>
              <asp:Button ID="btnRequest" runat="server" Text="Request to Admin" />
                </div>


            </div>
            <!-- Main Content Section -->
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

