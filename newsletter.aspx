﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="newsletter.aspx.vb" Inherits="newsletter" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">
     <div class=" fadeInLeft animated home innerdetail">
            <div class="heading">
                <span>Media <br /><b>Centre</b></span>
            </div>
            <!-- -- heading ends here -- -->
            <a href="javascript:;" class="scrollers"><img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
            </a>
            <div class="panel-content nano">
                <div class="scroller nano-content">
                    <ul class="list-unstyled">
                       <%= getLeftNav("25", "news/press-release")%>
                    <%= getLeftNav("26", "news/features")%>
                    <%= getLeftNav("27", "news/new")%>
                    <%= getLeftNav("28", "media-image-request")%>
                    <%= getLeftNav("29", "newsletter")%>
                    </ul>
                </div>
            </div>
            <!-- -- panel-content ends here -->
         <asp:HiddenField ID="hdnID" runat="server" />
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
       <!-- Main Banner Section -->
            <div class="section-slider flexslider">
                <ul class="list-unstyled slides">
                      <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <span> Newsletter </span>
                    </h1>
                    <ol class="breadcrumb">
                       <li><a href='<%= Session("domainName") %>'>Home</a>
                </li>
                        <li class="active"> <%= Title %> </li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">
                <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
               

                <!-- Register Section -->
                <div class="checkAvailabityWidget mediaRequestForm newsLetter">

                    <h2 class="subtitle">
                        Newsletter Signup
                    </h2>
                    
                    <div class="row formRowMargin">
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">First Name
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="form1" ControlToValidate="txtFName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </label>
                                 <asp:TextBox ID="txtFName" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Last Name
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form1" ControlToValidate="txtLName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </label>
                                <asp:TextBox ID="txtLName" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Email
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form1" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form1" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </label>
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                            </div>
                        </div>

                    </div>
        

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed et, rerum eaque expedita amet quae, pariatur, molestias itaque nam totam ipsum.</p>
                     <asp:Button ID="btnSend" runat="server" ValidationGroup="form1" class="submitButton" Text="Sign Up" />
                    <asp:HiddenField ID="hdnDate" runat="server" />
                   <asp:SqlDataSource ID="sdsNewsLetter" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                       InsertCommand="INSERT INTO [NewsletterSubscription] ([FirstName], [LastName], [Email], [SubscriptionDate]) VALUES (@FirstName, @LastName, @Email, @SubscriptionDate)" 
                      >
                      
                       <InsertParameters>
                           <asp:ControlParameter ControlID="txtFName" Name="FirstName" PropertyName="Text" Type="String" />
                           <asp:ControlParameter ControlID="txtLName" Name="LastName" PropertyName="Text" Type="String" />
                           <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                           <asp:ControlParameter ControlID="hdnDate" Name="SubscriptionDate" PropertyName="Value" Type="DateTime" />
                       </InsertParameters>
                       
                    </asp:SqlDataSource>


                </div>
                <!-- Register Section -->

            </div>
            <!-- Main Content Section -->
    <uc1:dynamicseo runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

