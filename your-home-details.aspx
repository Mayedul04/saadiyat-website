﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="your-home-details.aspx.vb" Inherits="hour_home_details" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagName="DynamicSEO" TagPrefix="uc1" %>

<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagName="UserGalleryControl" TagPrefix="uc2" %>

<%@ Register Src="~/CustomControl/RegisterNow.ascx" TagName="RegisterNow" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">

    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span>Your
                <br />
                <b>Home</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src="/ui/media/dist/elements/down.png" alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <asp:Literal ID="ltrList" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">


    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/your-home" %>'>Your Home</a>
                </li>
                <li class="active">
                    <asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <div class="extralinks">
        <asp:Literal ID="ltrExtraLinks" runat="server"></asp:Literal>
    </div>


    <!-- Main Content Section -->
    <div class="main-content-area relativeDiv">

        <!-- Call To Action Buttons -->
        <asp:Literal ID="ltrBadge" runat="server"></asp:Literal>
        <%--<ul data-wow-iteration="100" class="badgeInner"><li>Ready to move In</li></ul>--%>
        <!-- Call To Action Buttons -->



        <!-- About The Designs Section -->
        <div class="aboutDesignBox">

            <h2 class="maintitle">
                <asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></h2>
            <div class="imgHold">
                <asp:Image ID="imgBigImage" runat="server" />
            </div>


            <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

            <!-- Misinary TCab Boxes -->
            <ul class="misinaryBoxesListings csrMisinaryListing tabBoxes">
                <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>


            </ul>
            <!-- Misinary TCab Boxes -->

        </div>
        <!-- About The Designs Section -->

        <!-- About the life style & Testimonials -->
        <%--<div class="row">--%>
            <asp:Literal ID="ltrAbtTest" runat="server"></asp:Literal>
          

       <%-- </div>--%>
        <!-- About the life style & Testimonials -->

        <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>

        <!-- Download List Container -->


        <!-- Featured Bullet Listings -->
        <asp:Literal ID="ltrFeature" runat="server"></asp:Literal>
        <!-- Featured Bullet Listings -->


        <!-- Enquire Now and Testimonials Section -->
        <uc3:RegisterNow ID="RegisterNow1" runat="server" />
        <asp:Literal ID="lblRegister" runat="server"></asp:Literal>
        <!-- Enquire Now and Testimonials Section -->
        <!-- Map & Gallery -->
        <div class="row">

            <div class="col-sm-6">
                <div class="gallerybox">
                    <h2 class="subtitle yellow">Images and <span>Video</span><asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc2:UserGalleryControl ID="UserGalleryControl1" runat="server" Gallery_Relevency="Gallery" />
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Video Slider Section -->
            <asp:Literal ID="ltrVideo" runat="server"></asp:Literal>
            <!-- Video Slider Section -->


        </div>
        <!-- Map & Gallery -->


        <!-- contact Info & Video Section -->
        <div class="row marginBtm20">

            <!-- EnQuire Now Box -->
            <div class="col-sm-6">
                <h2 class="subtitle">Quick <span>Contact</span>
                </h2>

                <!-- Contact Detail Section -->
                <div class="contactDetailsSection">

                    <ul class="contactListings">


                        <asp:Literal ID="ltrContact" runat="server"></asp:Literal>

                    </ul>



                </div>
                <!-- Contact Detail Section -->

            </div>
            <!-- EnQuire Now Box -->

            <!-- Testimonial Section -->
            <div class="col-sm-6">
                <h2 class="subtitle yellow">Interactive  <span>Map</span>
                </h2>
                <div class="imap">
                    <a href='<%= Session("domainName") & Session("lang") & "/interactive-map/residentials/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")  %>' class="mappop" data-fancybox-type="iframe">
                        <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                    </a>
                </div>

            </div>


            <!-- Testimonial Section -->

        </div>
        <!-- contact Info & Video Section -->

        <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
    </div>
    <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">

    <script src="<%= Session("domainName") %>ui/js/dist/jquery.BlackAndWhite.js"></script>
    <script src="<%= Session("domainName") %>ui/js/dist/masonry.pkgd.min.js"></script>

</asp:Content>

