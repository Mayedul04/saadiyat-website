﻿
Partial Class interactive_map
    Inherits System.Web.UI.Page
    Public retstr As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim selectString = ""
            Dim link1 As String = ""
            
            If Page.RouteData.Values("module") = "education" Then
                link1 = Session("domainName") & "en/education-details/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")
            ElseIf Page.RouteData.Values("module") = "cultural" Then
                link1 = Session("domainName") & "en/inspiration-details/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")
            ElseIf Page.RouteData.Values("module") = "residentials" Then
                '' http://saadiyat.nexadesigns.com/en/your-home-details/4/Saadiyat-Beach-Villas
                link1 = Session("domainName") & "en/your-home-details/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")
            ElseIf Page.RouteData.Values("module") = "resorts" Then
                ''http://saadiyat.nexadesigns.com/en/stay-details/2/Park-Hyatt-Abu-Dhabi-Hotel-and-Villas
                link1 = Session("domainName") & "en/stay-details/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")
            ElseIf Page.RouteData.Values("module") = "leisure" Then
                'http://saadiyat.nexadesigns.com/en/your-leisure-details/1/Saadiyat-Beach-Club
                link1 = Session("domainName") & "en/your-leisure-details/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title")
                If Page.RouteData.Values("id") = "7" Then
                    link1 = Session("domainName") & "en/your-leisure-details/7/The-District"
                End If
            End If
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            
            If Page.RouteData.Values("module") Is Nothing Then
                If Not Page.RouteData.Values("ids") Is Nothing Then
                    selectString = "SELECT * from MapLocations where Status=1 and LocationID in (" & Page.RouteData.Values("ids") & ")"
                Else
                    selectString = "SELECT * from MapLocations where Status=1 "
                End If
            Else
                If Not Page.RouteData.Values("ids") Is Nothing Then
                    selectString = "SELECT * from MapLocations where Status=1 and LocationID in (" & Page.RouteData.Values("ids") & ")"
                Else
                    selectString = "SELECT * from MapLocations where Status=1 and Category=@Category and Link=@Link"
                End If

            End If

                Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            If Not Page.RouteData.Values("module") Is Nothing Then
                
                    cmd.Parameters.Add("Category", Data.SqlDbType.NVarChar, 150).Value = Page.RouteData.Values("module")
                cmd.Parameters.Add("Link", Data.SqlDbType.NVarChar, 250).Value = link1


            End If

            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                retstr += "{title: '" & reader("Title").ToString() & "',"
                retstr += "type: '" & reader("Category").ToString() & "',"
                retstr += "position: '" & reader("Coordinates").ToString() & "',"
                retstr += "description: '" & reader("Details").ToString().Trim() & "',"
                If IsDBNull(reader("Link")) = False Then
                    retstr += " readmore: '" & reader("Link").ToString() & "',"
                End If
                retstr += "image: '" & "http://saadiyat.ae/Admin/" & reader("SmallImage").ToString() & "'},"

            End While
            conn.Close()

        End If


    End Sub
    
End Class
