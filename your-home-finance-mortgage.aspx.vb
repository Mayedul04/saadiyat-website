﻿
Partial Class your_home_finance_mortgage
    Inherits System.Web.UI.Page


    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About]  FROM  [dbo].[List_Home] where  Lang=@Lang and Status=1 order by SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()
                If reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-home-advantage-card"
                ElseIf reader("MasterID") = "7" Then
                    link = Session("domainName") & Session("lang") & "/your-home-bank-partners"
                Else
                    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & link & """>" & _
                       "         <div class=""" & If(reader("MasterID") = "7", "thumbnail-box activeBW", "thumbnail-box") & """ >" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = "7" Then
                    currentListID = reader("ListID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()
                    ltrBigDetails.Text = reader("BigDetails").ToString & Utility.showEditButton(Request, "/Admin/A-YourHome/YourHomeEdit.aspx?lid=" & reader("ListID"))

                    currentGalleryID = reader("GalleryID").ToString()



                    ltrSubTitle.Text = (reader("SubTitle").ToString())

                    With DynamicSEO1
                        .PageType = "List_Home"
                        .PageID = currentListID
                    End With


                End If

                i = i + 1
            End While
            conn.Close()


            ltrAccordianContent.Text = Contents("List_Home", currentListID)
            ltrBanner.Text = getBanners("List_Home", currentListID)
        End If
    End Sub


    Private Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText,Link from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            retstr &= "<li><div class=""row"">" & _
                      "          <div class=""col-sm-3"">" & _
                      "              <div class=""imgHold"">" & _
                      "                  <a href=""" & reader("Link") & """ target='_blank'><img src=""/admin/" & reader("Image") & """ alt=""" & reader("Title") & """></a>" & _
                      "              </div>" & _
                      "          </div>" & _
                      "          <div class=""col-sm-9"">" & _
                      "              <h3><a href=""" & reader("Link") & """ target='_blank'>" & reader("Title") & "</a></h3>" & _
                      "              <p>" & reader("DetailText") & "</p>" & _
                      "               <a href=""" & reader("Link") & """  target='_blank' class=""clickHereButton"" data-wow-iteration=""100"">View Website <span><img src=""/ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>" & _
                      "          </div>" & _
                      Utility.showEditButton(Request, "/admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=215&ImageHeight=171&Title=1&Image=1&Text=1&link=1") & _
                      "      </div></li>"

            'retstr &= "<li >" & _
            '         "       <div class=""imgHold"">" & _
            '         "           <a href=""" & reader("Link") & """ ><img src=""/Admin/" & reader("Image") & """ alt=""" & reader("Title") & """></a>" & _
            '         "       </div>" & _
            '         Utility.showEditButton(Request, "/admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=232&ImageHeight=154&Title=1&Image=1&Text=0&link=1") & _
            '         "   </li>"
            counter += 1
        End While

        conn.Close()
        '
        retstr = Utility.showAddButton(Request, "/admin/A-Content/ContentEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=215&ImageHeight=171&Title=1&Image=1&Text=1&link=1") & "<ul class=""list-unstyled row"">" & retstr & "</ul>"
        Return retstr
    End Function


    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function


End Class
