﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Collections.Generic

Public Class Language

    Public Shared Lang As Dictionary(Of String, String)

    Public Shared Function Read(ByVal word As String, ByVal ln As String) As String
        Dim retVal As String = ""
        If Lang Is Nothing Then

            Lang = New Dictionary(Of String, String)
            If ln = "" Then
                ln = "en"
            End If
            Using sr As New StreamReader(AppDomain.CurrentDomain.BaseDirectory & "\lang\" & ln & ".txt")
                Dim line As String
                line = sr.ReadToEnd
                Dim lines As String() = line.Split(New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
                For Each l As String In lines
                    Try
                        Dim words As String() = l.Split(New String() {":="}, StringSplitOptions.RemoveEmptyEntries)
                        Lang.Add(words(0).ToLower().Trim, words(1).Trim)
                    Catch ex As Exception

                    End Try

                Next


            End Using


        End If
        Try
            'If ln = "ar" Then
            retVal = Lang(word.ToLower().Trim)
            'Else
            'retVal = word.Trim
            'End If
        Catch ex As Exception

            retVal = word.Trim '& ex.Message
        End Try


        Return retVal
    End Function

End Class
