﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="Investment.aspx.vb" Inherits="Investment" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<%@ Register Src="~/CustomControl/RegisterNow.ascx" TagPrefix="uc1" TagName="RegisterNow" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
    <!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        Investment <span> Opportunities </span>
                    </h1>
                    <ol class="breadcrumb">
                         <li><a href='<%= Session("domainName") %>'>Home</a></li>
                        <li class="active">Investment Opportunities</li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                
                  <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                <%--<span class="clickHereButton downLoadListTrigger" data-wow-iteration="100">Downloadable Documents<span><img src="/ui/media/dist/icons/readmore-arrow.png" alt=""></span></span>

                <div class="downloadListContainer">

                    <div class="contentSection">


                        <asp:Literal ID="lblAddFile" runat="server"></asp:Literal>
                        <ul class="listings">

                           <%= getDownloadableFiles() %>
                        </ul>

                    </div>
                </div>--%>
                <!-- Download List Container -->
               
                <ul class="csrListing mediaListings">
                     <%= Contents("HTML",77) %>
                   

                </ul>

                
             


                <!-- Enquire Now and Testimonials Section -->
               <uc1:RegisterNow runat="server" ID="RegisterNow"  TableName="HTML" TableID="77"/>
                <!-- Enquire Now and Testimonials Section -->
                

                <!-- contact Info & Video Section -->
                <div class="row marginBtm20">
                    <asp:Panel ID="pnlVideo" runat="server">
                <div class="col-sm-6">
                    <h2 class="subtitle">Video <span>Section</span>
                    </h2>
                    <!-- Video Slider Section -->
                    <div class="detailVideoSlider video-flexsliders">
                        <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & 100) %>
                        <ul class="slides">
                            <asp:Literal ID="lblVgal" runat="server"></asp:Literal>
                        </ul>

                    </div>
                    <!-- Video Slider Section -->
                </div>
                <!-- Testimonial Section -->
            </asp:Panel>
                  

                    <asp:Literal ID="lblMap" runat="server"></asp:Literal>

                </div>
                <!-- contact Info & Video Section -->

                <uc1:DynamicSEO runat="server" ID="DynamicSEO" />

            </div>
            <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

