﻿
Partial Class stay_details
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, BigImage, Link, MapImage,TripAdvisorCode, GalleryID, BookingLink from List_Stay where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                lblTitle.Text = reader("SubTitle").ToString()
                hdnID.Value = reader("ListID").ToString()
                ddlHotels.SelectedValue = Page.RouteData.Values("id")
                lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, "/Admin/A-Stay/ResortEdit.aspx?lid=" & hdnID.Value)
                link1 = reader("Link").ToString()
                title = reader("Title").ToString()
                galid = reader("GalleryID")
                '  hdnBookingLink.Value = reader("BookingLink").ToString()
                lblTripadvisorcode.Text = reader("TripAdvisorCode").ToString()

            End While
            conn.Close()
            If galid <> 0 Then

                UserGalleryControl1.Gallery_ID = galid
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If
            styledtitle = FormateTitle(title)
        End If
        ' If IsPostBack = False Then
        
        
        hdnBookingLinkPk.Value = getBookingLink(2)
        hdnBookingLinkSt.Value = getBookingLink(1)
        If txtCheckIn.Text <> "" Then
            Dim diff As TimeSpan = CDate(txtCheckOut.Text) - CDate(txtCheckIn.Text)
            txtNight.Text = diff.Days
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Stay"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getResortThumbList() As String
        Dim retstr As String = ""
        Dim selectString As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        selectString = "SELECT ListID, Title, SmallImage, SmallDetails,MasterID from List_Stay where  Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If hdnID.Value = reader("ListID") Then
                retstr += "<li>"
            Else
                retstr += "<li>"
            End If
            retstr += "<a href=""" & Session("domainName") & Session("lang") & "/stay-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>"
            If reader("MasterID") = Page.RouteData.Values("id") Then
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If

            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        reader.Close()
        selectString = "SELECT Title, SmallImage  from HTML where MasterID=@MasterID and Lang=@Lang"
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd1.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 4
        cmd1.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()

        While reader1.Read()
            retstr += "<li><a href=""" & Session("domainName") & Session("lang") & "/book-your-stay"">"
            
            retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader1("SmallImage").ToString() & """ alt=""" & reader1("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader1("Title").ToString()) & "</h2></a></li>"

        End While
        reader1.Close()
        conn.Close()
        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">" & title & " at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Stay"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Stay"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = Math.Ceiling(total / 2) Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr


      
    End Function
    Public Function getAddresses() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email, Details from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Stay"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += reader("Details").ToString() & Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=List_Stay&TID=" & hdnID.Value & "&t=" & Server.UrlEncode(title)) & retstr
        Return retstr
    End Function

    Public Function GetFeaturedVideos() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
                'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
                'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

                retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""250""></iframe></div>"
                retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
            End While

        End If

        conn.Close()
        Return retstr
    End Function
    Public Function getBookingLink(ByVal hotelid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  BookingLink from List_Stay where  MasterID=@MasterID and Lang='en'"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = hotelid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += reader("BookingLink").ToString()



        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub btnCheckAvaibility_Click(sender As Object, e As EventArgs) Handles btnCheckAvaibility.Click
        Dim monthyear1, monthyear2, day1, day2 As String
        monthyear1 = CDate(txtCheckIn.Text).Month.ToString() + "%" + CDate(txtCheckIn.Text).Year.ToString()
        day1 = CDate(txtCheckIn.Text).Day.ToString()
        monthyear2 = CDate(txtCheckOut.Text).Month.ToString() + "%" + CDate(txtCheckOut.Text).Year.ToString()
        day2 = CDate(txtCheckOut.Text).Day.ToString()
        Dim night As Integer = CInt(day2) - CInt(day1)
        txtNight.Text = night
        Dim returl As String = ""
        If ddlHotels.SelectedValue = 1 Then
            returl = hdnBookingLinkSt.Value & "&departureDate=" & Date.Parse(txtCheckOut.Text).ToString("yyyy-MM-dd") & "&arrivalDate=" & Date.Parse(txtCheckIn.Text).ToString("yyyy-MM-dd") & "&lengthOfStay=" & txtNight.Text & "&numberOfRooms=1&numberOfAdults=1&numberOfChildren=0&language=en_US&ES=LPS_3400_EN_ST_BOOKWIDGET_ME_EAME"
        Else
            returl = hdnBookingLinkPk.Value & "&adults=2&kids=0&rooms=1&monthyear1=" + monthyear1 + "&day1=" + day1 + "&monthyear2=" + monthyear2 + "&day2=" + day2 + "&gpnum=&srcd=dayprop&Lang=en&src=saadiyat_roombooking"
            '  txtNight.Text = returl
        End If

        ' Else
        '
        '  End If

        Response.Redirect(returl)
    End Sub
    Public Function getfirstFooter() As String
        Dim retstr As String = ""
        Dim videos As String = GetFeaturedVideos()
        If videos <> "" Then
            retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">Video <span>Section</span></h2><div class=""detailVideoSlider video-flexsliders"">"
            retstr += "<ul class=""slides"">" & videos & "</ul></div></div>"
        Else
            retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/resorts/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        End If

        Return retstr
    End Function
    Public Function getSecondFooter() As String
        Dim retstr As String = ""
        Dim addr As String = getAddresses()
        
        'If Page.RouteData.Values("id") = 2 Then

        '    retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/resorts"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        'Else
        retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">Quick <span>Contact</span></h2><div class=""contactDetailsSection"">"
        retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/resorts/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        ' End If

        Return retstr
    End Function

    
End Class
