﻿
Partial Class about_districts
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, Link, MapImage, GalleryID, SecondText from List_Saadiyat where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                lblTitle.Text = "<h2 class=""maintitle tagline"">" & reader("SubTitle").ToString() & "</h2>"
                lblDetails.Text = ManipulateListing(reader("BigDetails").ToString()) & Utility.showEditButton(Request, "/Admin/A-Saadiyat/ItemEdit.aspx?lid=" & hdnID.Value)
                title = reader("Title").ToString()
                galid = reader("GalleryID")
                If IsDBNull(reader("Link")) = False Then
                    lblink.Text = "<a target=""_blank"" href=""" & reader("Link").ToString() & """ class=""clickHereButton"">View Website <span><img alt="""" src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png""></span></a>"
                End If
                link1 = reader("Link").ToString()
            End While
            conn.Close()
            If galid <> 0 Then

                UserGalleryControl.Gallery_ID = galid
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If
            If Page.RouteData.Values("id") = 5 Then
                lblmap.Text = "<div class=""col-sm-6 bottomBoxesMinHeight""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/9,10,11,12,13"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            ElseIf Page.RouteData.Values("id") = 7 Then
                lblmap.Text = "<div class=""col-sm-6 bottomBoxesMinHeight""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/14"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            Else
                lblmap.Text = "<div class=""col-sm-6 bottomBoxesMinHeight""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/1,2,3,4,5,6,7,8,15,18"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            End If

            If Page.RouteData.Values("id") = 6 Then
                pnlQC.Visible = False
                pnlVideo.Visible = False
                ' lblmap.Text = "<a href=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-cultural-map-big.jpg"" class=""fancybox-thumb""><img src=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-cultural-map.jpg"" alt=""""></a>"
            ElseIf Page.RouteData.Values("id") = 7 Then
                pnlQC.Visible = False
                ' lblmap.Text = "<a href=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-beach-map-big.jpg"" class=""fancybox-thumb""><img src=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-beach-map.jpg"" alt=""""></a>"
            Else
                ' lblmap.Text = "<a href=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-marina-map-big.jpg"" class=""fancybox-thumb""><img src=""" & Session("domainName") & "ui/media/dist/saadiyat/saadiyat-marina-map.jpg"" alt=""""></a>"
            End If

            styledtitle = FormateTitle(title)
            lblVideos.Text = GetFeaturedVideos()
            DynamicSEO.PageType = "List_Saadiyat"
            DynamicSEO.PageID = hdnID.Value
            'If lblVideos.Text = "" Then
            '    pnlVideo.Visible = False
            'End If
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where MasterID not in (5,6,7,8) and Lang=@Lang and Status=1 order by  SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"

            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If


            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">" & title & " at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = total / 2 Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr
    End Function
    
    Public Function GetFeaturedVideos() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""230""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle dbluebg"">")
        Return bigText
    End Function
    Public Function getAddresses() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email, WebSite from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><p>" & reader("Heading").ToString() & "</p>"
            If IsDBNull(reader("Phone")) = False Then
                retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            End If
            If IsDBNull(reader("IntPhone")) = False Then
                retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>"
            End If
            If IsDBNull(reader("Email")) = False Then
                retstr += "<h5>Email : <a href=""mailto:" & reader("Email").ToString() & """ target=""_blank"">" & reader("Email").ToString() & "</h5>"
            End If
            If IsDBNull(reader("WebSite")) = False Then
                retstr += "<h5>Web : <a href=""" & reader("WebSite").ToString() & """ target=""_blank"">" & reader("WebSite").ToString() & "</h5>"
            End If
            retstr += Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=List_Saadiyat&TID=" & hdnID.Value & "&t=" & Server.UrlEncode(title)) & retstr
        Return retstr
    End Function
End Class
