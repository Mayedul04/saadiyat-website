﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Session.Add("domainName", ConfigurationManager.AppSettings("RedirectUrl").ToString)
        Session.Add("lang", If(Page.RouteData().Values("lang") IsNot Nothing, Page.RouteData().Values("lang"), "en"))
    End Sub
    Public Function LoadModules(ByVal modulename As String, ByVal tname As String, ByVal detailslink As String, ByVal htmlmasterid As String, heading As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT HTMLID, Title, SmallImage, MasterID from HTML where MasterID=@MasterID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retstr += "<div class=""column fadeInLeft animated " & modulename.ToLower() & """>"
            retstr += " <div class=""heading""><a href=""" & Session("domainName") & Session("lang") & "/your-" & modulename.ToLower() & """><span>" & heading & "</span></a></div>"
            retstr += "<a href=""javascript:;"" class=""scrollers ""><img src=""" & Session("domainName") & "ui/media/dist/elements/down.png"" alt=""""></a>"
            retstr += "<div class=""panel-content animated  nano""><div class=""scroller nano-content"">"
            retstr += "<ul class=""list-unstyled"">"
            If modulename = "Saadiyat" Then
                retstr += SaadiyatThumbList() & "</ul></div></div>"
            Else
                retstr += ModuleThumbList(tname, detailslink) & "</ul></div></div>"
            End If

            retstr += "<div class=""big-thumbnail animated""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            retstr += "<div class=""panel-overlay animated ""></div></div>"
        End If

        Return retstr
    End Function
    Public Function ModuleThumbList(ByVal tablename As String, ByVal detailslink As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ListID, Title, SmallImage, SmallDetails, MasterID from " & tablename & " where  Lang=@Lang and status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()


        While reader.Read()
            Dim link As String = Session("domainName") & Session("lang") & "/" & detailslink & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")

            If tablename = "List_Home" Then
                If reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-home-advantage-card"
                ElseIf reader("MasterID") = "7" Then
                    link = Session("domainName") & Session("lang") & "/your-home-bank-partners"
                Else
                    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
            ElseIf tablename = "List_Leisure" Then
                If reader("MasterID") = "4" Or reader("MasterID") = "5" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-content/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                ElseIf reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-retail"
                Else
                    link = Session("domainName") & Session("lang") & "/your-leisure-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
            End If
            retstr += "<li><a href=""" & link & """>"
            retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        reader.Close()
        If tablename = "List_Stay" Then
            retstr += AddHtmlThumb(4)  '4 for Book-your-Stay
        End If
        conn.Close()
        Return retstr
    End Function
    Public Function SaadiyatThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where  Lang=@Lang and MasterID not in (5,6,7,8) and Status=1 order by  SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            
            retstr += "<li>"
            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                retstr += "<h2 class=""title"">About " & "<span>" & "Abu Dhabi" & "</span>" & "</h2></a></li>"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
                retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"
            End If

            

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function AddHtmlThumb(ByVal htmlmasterid As String) As String
        Dim retstr As String = ""
        Dim selectString As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        selectString = "SELECT Title, SmallImage  from HTML where MasterID=@MasterID and Lang=@Lang"
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd1.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = htmlmasterid
        cmd1.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()
        While reader1.Read()
            retstr += "<li><a href=""" & Session("domainName") & Session("lang") & "/book-your-stay"">"
            retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader1("SmallImage").ToString() & """ alt=""" & reader1("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader1("Title").ToString()) & "</h2></a></li>"

        End While
        reader1.Close()
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            If titlearray(i).ToLower = "abu" Then
                
            Else
                If titlearray(i).ToLower = "dhabi" Then
                    firstpart += "Abu " & titlearray(i) & " "
                Else
                    firstpart += titlearray(i) & " "
                End If
            End If

        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            If titlearray(i).ToLower = "dhabi" Then
                secondpart += "Abu " & titlearray(i) & " "
            Else
                secondpart += titlearray(i) & " "
            End If

        Next
        If secondpart <> "" Then
            retstr = firstpart & "<span>" & secondpart & "</span>"
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
End Class
