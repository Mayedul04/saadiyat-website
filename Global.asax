﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        RegisterRoute(Routing.RouteTable.Routes)
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
    
    Sub RegisterRoute(ByVal routes As Routing.RouteCollection)
        routes.MapPageRoute("Index", "", "~/intro.aspx")
        routes.MapPageRoute("Default-Home", "default", "~/Default.aspx")
        routes.MapPageRoute("Default-Home-lang", "{lang}/home", "~/Default.aspx")
        routes.MapPageRoute("your-Education", "{lang}/your-education", "~/your-education.aspx")
        routes.MapPageRoute("your-Education-details", "{lang}/education-details/{id}/{title}", "~/education-details.aspx")
        routes.MapPageRoute("your-environment", "{lang}/your-environment", "~/your-environment.aspx")
        routes.MapPageRoute("your-environment-details", "{lang}/environment-details/{id}/{title}", "~/environment-details.aspx")
        routes.MapPageRoute("your-stay", "{lang}/your-stay", "~/your-stay.aspx")
        routes.MapPageRoute("your-stay-details", "{lang}/stay-details/{id}/{title}", "~/stay-details.aspx")
        routes.MapPageRoute("your-stay-details1", "{lang}/book-your-stay", "~/book-your-stay.aspx")
        routes.MapPageRoute("your-inspiration", "{lang}/your-inspiration", "~/your-inspiration.aspx")
        routes.MapPageRoute("your-inspiration-details", "{lang}/inspiration-details/{id}/{title}", "~/inspiration-details.aspx")
        routes.MapPageRoute("your-home.aspx", "{lang}/your-home", "~/your-home.aspx")
        routes.MapPageRoute("your-home-details.aspx", "{lang}/your-home-details/{id}/{title}", "~/your-home-details.aspx")
        routes.MapPageRoute("your-home-advantage-card.aspx", "{lang}/your-home-advantage-card", "~/your-home-advantage-card.aspx")
        routes.MapPageRoute("your-home-finance-mortgage.aspx", "{lang}/your-home-bank-partners", "~/your-home-finance-mortgage.aspx")
        routes.MapPageRoute("photo-pop", "{lang}/photo-popup/{id}/{title}/{url}", "~/PhotoPop.aspx")
        routes.MapPageRoute("your-saadiyat", "{lang}/your-saadiyat", "~/your-saadiyat.aspx")
        routes.MapPageRoute("your-saadiyat-details1", "{lang}/about-saadiyat", "~/saadiyat-details.aspx")
        routes.MapPageRoute("your-saadiyat-details4", "{lang}/Corporate-Social-Responsibility", "~/saadiyat-csr.aspx")
        routes.MapPageRoute("your-saadiyat-details2", "{lang}/our-partners", "~/our-partners.aspx")
        routes.MapPageRoute("your-saadiyat-details3", "{lang}/about-tdic", "~/about-tdic.aspx")
          routes.MapPageRoute("your-saadiyat-details8", "{lang}/about-abudhabi", "~/about-abudhabi.aspx")
        routes.MapPageRoute("your-saadiyat-details53", "{lang}/about-districts/{id}/{title}", "~/about-districts.aspx")
          routes.MapPageRoute("your-saadiyat-details6", "{lang}/worker-welfare", "~/worker-welfare.aspx")
        routes.MapPageRoute("your-leisure.aspx", "{lang}/your-leisure", "~/your-leisure.aspx")
        routes.MapPageRoute("your-leisure-details.aspx", "{lang}/your-leisure-details/{id}/{title}", "~/your-leisure-details.aspx")
        routes.MapPageRoute("your-leisure-retail.aspx", "{lang}/your-leisure-retail", "~/your-leisure-retail.aspx")
        routes.MapPageRoute("your-leisure-content.aspx", "{lang}/your-leisure-content/{id}/{title}", "~/your-leisure-content.aspx")
        
        
        routes.MapPageRoute("Gallery", "{lang}/gallery", "~/gallery.aspx")
        routes.MapPageRoute("Experiences","{lang}/experiences", "~/experiences.aspx")
        routes.MapPageRoute("Gallery1", "{lang}/gallery/{type}", "~/photo-gallery.aspx")
        routes.MapPageRoute("Gallery3", "{lang}/gallery/{type}/{catid}/{module}", "~/album-category.aspx")
         routes.MapPageRoute("Gallery5", "{lang}/gallery/{type}/{catid}/{module}/{page}", "~/album-category.aspx")
        routes.MapPageRoute("Gallery7", "{lang}/gallery/{type}/{catid}/{module}/{galid}/{title}", "~/gallery-items.aspx")
        routes.MapPageRoute("Gallery8", "{lang}/gallery/{type}/{catid}/{module}/{galid}/{title}/{page}", "~/gallery-items.aspx")
        
        routes.MapPageRoute("media", "{lang}/media-center", "~/media-center.aspx")
        routes.MapPageRoute("media1", "{lang}/news/{category}", "~/news.aspx")
         routes.MapPageRoute("media12", "{lang}/news/{category}/{page}", "~/news.aspx")
        routes.MapPageRoute("media2", "{lang}/news-details/{category}/{id}/{title}", "~/news-details.aspx")
        routes.MapPageRoute("media3", "{lang}/media-image-request", "~/media-request.aspx")
        routes.MapPageRoute("media4", "{lang}/media-image-request-logged", "~/media-request-logged.aspx")
        routes.MapPageRoute("media5", "{lang}/social-details", "~/social1.aspx")
        routes.MapPageRoute("Saadiyat-Exp", "{lang}/saadiyat-experience/{id}/{title}", "~/saadiyat-experiences.aspx")
        routes.MapPageRoute("Investment", "{lang}/investment-opportunities", "~/Investment.aspx")
        
        routes.MapPageRoute("ContactUs", "{lang}/Contact-us", "~/contact-us.aspx")
        routes.MapPageRoute("AboutUs", "{lang}/AboutUs", "~/AboutUs.aspx")
        routes.MapPageRoute("Terms", "{lang}/terms-conditions", "~/terms.aspx")
        routes.MapPageRoute("Privacy", "{lang}/privacy-policy", "~/privacy.aspx")
        routes.MapPageRoute("Social", "{lang}/social", "~/social.aspx")
        routes.MapPageRoute("Thanks", "{lang}/thank-you", "~/thanks.aspx")
        routes.MapPageRoute("Thanks1", "{lang}/thank-you/{subject}", "~/thanks.aspx")
        routes.MapPageRoute("interactive-map", "{lang}/interactive-map", "~/interactive-map.aspx")
         routes.MapPageRoute("interactive-map2", "{lang}/interactive-map/{ids}", "~/interactive-map.aspx")
         routes.MapPageRoute("interactive-map1", "{lang}/interactive-map/{module}/{id}/{title}", "~/interactive-map.aspx")
        routes.MapPageRoute("event-details", "{lang}/event/{id}/{title}", "~/event-details.aspx")
        routes.MapPageRoute("promotion-details", "{lang}/promotion/{id}/{title}", "~/promotion-details.aspx")
        routes.MapPageRoute("buy-lease-properties", "{lang}/buy-lease-properties", "~/buy-lease-properties.aspx")
        routes.MapPageRoute("buy-lease-property-list.aspx", "{lang}/buy-lease-property-list/{id}/{title}", "~/buy-lease-property-list.aspx")
        routes.MapPageRoute("buy-lease-property-details", "{lang}/property/{pid}/{id}/{ptitle}/{title}", "~/buy-lease-property-details.aspx")
        routes.MapPageRoute("buy-lease-property-details-Residential-Plots", "{lang}/property/{pid}/{ptitle}", "~/residential-plots.aspx")
        
        routes.MapPageRoute("register-now-thankyou", "{lang}/register-now-thankyou", "~/register-now-thankyou.aspx")
        routes.MapPageRoute("news-subscribtion", "{lang}/news-subscribtion", "~/news-subscribtion.aspx")
        routes.MapPageRoute("news-subscribtion-thankyou", "{lang}/news-subscribtion-thankyou", "~/news-subscribtion-thankyou.aspx")
        
    End Sub
       

    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)
      
        Dim domainName As String
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Threading.Thread.CurrentThread.CurrentCulture = newCulture
           If Request.Url.ToString().ToLower = "http://www.saadiyat.ae/jawahersaadiyat" Then
            Response.Redirect("http://saadiyat.ae/en/your-home-details/9/Jawaher-Saadiyat") ' put what url you need to redirect  
        End If
        
    End Sub
    
    
    
</script>