﻿Imports System.Data.SqlClient

Partial Class album_category
    Inherits System.Web.UI.Page
    Public PageList As String
    Public Title As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = Page.RouteData.Values("catid"), SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        CategoryDetails(Page.RouteData.Values("catid"), HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        hdnID.Value = HTMLID

        With DynamicSEO
            .PageType = "Gallery Category"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 BannerID, Title, BigImage from Banner where TableName=@TableName  and Status=1 order by NewID()"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        ' cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = Page.RouteData.Values("module").ToString().Replace("Your-", "List_")
        '  and TableID=@TableID and Lang=@Lang
        'If Page.RouteData.Values("type") <> "video" Then
        '    cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 16
        'Else
        '    cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 17
        'End If
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftNav(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang,MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li>"
            If link.Contains(Page.RouteData.Values("type")) Then
                retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><div class=""thumbnail-box activeBW""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            Else
                retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><div class=""thumbnail-box""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            End If

            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            If titlearray(i).ToLower = "abu" Then

            Else
                firstpart += titlearray(i) & " "
            End If

        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            If titlearray(i).ToLower = "dhabi" Then
                If secondpart.ToLower.Contains("abu") = False Then
                    secondpart += "Abu " & titlearray(i) & " "
                Else
                    secondpart += titlearray(i) & " "
                End If
            Else
                secondpart += titlearray(i) & " "
            End If

        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub CategoryDetails(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT *  FROM GCategory where CatID=@CatID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CatID", Data.SqlDbType.Int)
        cmd.Parameters("CatID").Value = MasterID


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            Title = reader("DisplayName") & ""
            SmallDetails = reader("ArDisplayName") & ""


        End If
        conn.Close()
    End Sub
    Private Function getPageNumber() As Integer
        Dim totalRows As Integer = 0
        Dim secondvalue As Integer = 0
        Dim sConn As String
        Dim selectString1 As String = "Select COUNT(0) from Gallery where CategoryID=@CategoryID and ParentGalleryID=@PGalleryID  and Status=1"
        '  Dim selectString2 As String = "SELECT COUNT(0)  FROM [dbo].[CommonGallery] where Status=1 GROUP BY TableName,TableMasterID"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

        If Not Page.RouteData.Values("catid") Is Nothing Then
            cmd.Parameters.Add("CategoryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("catid")
        Else
            cmd.Parameters.Add("CategoryID", Data.SqlDbType.Int, 32).Value = 1
        End If
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = 0
        ' cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang").ToString()
        totalRows = Math.Ceiling((cmd.ExecuteScalar) / 9)
        cn.Close()
        Return totalRows
    End Function
    Public Function Gallery() As String
        PageList = ""
        Dim currentPage As Integer = 0
        Dim pageNumber As Integer = 0
        Dim searchCriteria As String = ""
        If Page.RouteData.Values("page") Is Nothing Then
            currentPage = 1
        Else
            currentPage = Page.RouteData.Values("page")
        End If

        searchCriteria = Session("domainName").ToString() & Session("lang").ToString() & "/gallery/" & Page.RouteData.Values("type") & "/" & Page.RouteData.Values("catid") & "/" & Page.RouteData.Values("module") & "/"
        pageNumber = getPageNumber()


        If pageNumber > 1 Then
            PageList = GetPager(currentPage, pageNumber, 5, searchCriteria)
        End If
        Dim retstr As String = "<ul class=""diningListings mediaLanding"">"
        Dim link1 As String = ""
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString1 As String = ""




        '  selectString1 += "  (Select Gallery.GalleryId as MasterID, Gallery.Title, Gallery.ArTitle  , Gallery.SmallImage , Gallery.LastUpdated , 'Gallery' as TName, ParentGalleryID from Gallery where Status=1 and ParentGalleryID=@PGalleryID)"
        Dim selectString As String = "DECLARE @PageNum AS INT; DECLARE @PageSize AS INT; SET @PageNum =" & currentPage & "; SET @PageSize = 9; Select * from ( select  ROW_NUMBER()  over(ORDER BY GalleryID, Gallery.Title ASC) AS RowNum  , GalleryID, Title , SmallImage, LastUpdated from Gallery where  ParentGalleryID=@PGalleryID and CategoryID=@CategoryID and Status=1 "
        selectString += ") as MyTable WHERE RowNum BETWEEN (@PageNum - 1) * @PageSize + 1 AND @PageNum * @PageSize"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        If Not Page.RouteData.Values("catid") Is Nothing Then
            cmd.Parameters.Add("CategoryID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("catid")
        Else
            cmd.Parameters.Add("CategoryID", Data.SqlDbType.Int, 32).Value = 1
        End If
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = 0
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang").ToString()
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read

                link1 = "<a href=""" & Session("domainName").ToString() & Session("lang") & "/gallery/" & Page.RouteData.Values("type") & "/" & Page.RouteData.Values("catid") & "/" & Page.RouteData.Values("module") & "/" & reader("GalleryID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>"


                retstr += "<li class=""col-md-4"">"
                retstr += "<div class=""swooshHolder"">"
                retstr += "<div class=""imgHold"">"
                retstr += link1 & "<img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a>"


                retstr += "<h2>" & link1 & FormateTitle(reader("Title").ToString()) & "</a></h2>"

                retstr += "</div><span class=""swoosh"">"
                retstr += "<img src=""/ui/media/dist/saadiyat/swoosh-small.png"" alt=""""></span></div>" & Utility.showEditButton(Request, Session("domainName").ToString() & "Admin/A-Gallery/GalleryEdit.aspx?galleryid=" & reader("GalleryID")) & "</li>"


            End While
            retstr = retstr + "</ul>"
        Else

            retstr = "Sorry there is no photo(s) to show."

            retstr += Utility.showAddButton(Request, Session("domainName").ToString() & "Admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & Page.RouteData.Values("id"))

        End If
        
        cn.Close()
        If PageList = "" Then
            pnlPageination.Visible = False
        End If


        Return retstr
    End Function
    Public Shared Function GetPager(ByVal presentPageNum As Integer, ByVal totalNumOfPage As Integer, ByVal totalPageNumToShow As Integer, ByVal urlToNavigateWithQStr As String) As String
        Dim i As Integer
        Dim loopStartNum, loopEndNum, presentNum, maxShownNum As Integer
        Dim pagerString As String = ""
        presentNum = presentPageNum
        maxShownNum = totalPageNumToShow
        Dim middleFactor As Integer = maxShownNum / 2
        pagerString = "<ul class=""paginationList"">"
        If totalNumOfPage <= totalPageNumToShow Then
            loopStartNum = 1
            loopEndNum = totalNumOfPage
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum <= 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li>"
            ' pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        Else
            loopStartNum = If(presentNum <= (middleFactor + 1), 1, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage - (maxShownNum - 1), presentNum - middleFactor))
            loopEndNum = If(presentNum <= (middleFactor + 1), maxShownNum, If(presentNum + middleFactor >= totalNumOfPage, totalNumOfPage, presentNum + middleFactor))
            loopEndNum = If(loopEndNum > totalNumOfPage, totalNumOfPage, loopEndNum)
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & "1"">First</a></div>"
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = 1, totalNumOfPage, (presentNum - 1)) & """>Previous</a></li>"
            For i = loopStartNum To loopEndNum
                If (i = presentNum) Then
                    pagerString = pagerString & "<li class=""active""><a href=""javascript:;"">" & i & "</a></li>"
                Else
                    pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & i & """>" & i & "</a></li>"
                End If
            Next
            pagerString = pagerString & "<li><a href=""" & urlToNavigateWithQStr & If(presentNum = totalNumOfPage, 1, (presentNum + 1)) & """>Next</a></li> "
            'pagerString = pagerString & "<div><a href=""" & urlToNavigateWithQStr & totalNumOfPage & """>Last</a></div>"
        End If

        pagerString = pagerString & "</ul>"
        Return pagerString
    End Function
End Class
