﻿
Partial Class book_your_stay
    Inherits System.Web.UI.Page
    Public title, styledtitle As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT HTMLID, Title, BigDetails, BigImage  from HTML where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 4
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                lblTitle.Text = reader("Title").ToString()
                hdnID.Value = reader("HTMLID")

                lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hmlid=" & hdnID.Value)
                title = reader("Title").ToString()
                DynamicSEO.PageType = "HTML"
                DynamicSEO.PageID = hdnID.Value

            End While
            conn.Close()
            styledtitle = FormateTitle(title)
        End If
        hdnBookingLinkPk.Value = getBookingLink(2)
        hdnBookingLinkSt.Value = getBookingLink(1)
        If txtCheckIn.Text <> "" Then
            Dim diff As TimeSpan = CDate(txtCheckOut.Text) - CDate(txtCheckIn.Text)
            txtNight.Text = diff.Days
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 4
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getResortThumbList() As String
        Dim retstr As String = ""
        Dim selectString As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        selectString = "SELECT ListID, Title, SmallImage, SmallDetails,MasterID from List_Stay where  Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If hdnID.Value = reader("ListID") Then
                retstr += "<li>"
            Else
                retstr += "<li>"
            End If
            retstr += "<a href=""" & Session("domainName") & Session("lang") & "/stay-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>"
            retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        reader.Close()
        selectString = "SELECT Title, SmallImage  from HTML where MasterID=@MasterID and Lang=@Lang"
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd1.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 4
        cmd1.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()

        While reader1.Read()
            retstr += "<li><a href=""" & Session("domainName") & Session("lang") & "/book-your-stay"">"
            retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader1("SmallImage").ToString() & """ alt=""" & reader1("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader1("Title").ToString()) & "</h2></a></li>"

        End While
        reader1.Close()
        conn.Close()
        Return retstr
    End Function
    Public Function TripAdvisorWidgetList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT TripAdvisorCode from List_Stay where Status=1 and Lang=@Lang order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>" & reader("TripAdvisorCode").ToString() & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getBookingLink(ByVal hotelid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  BookingLink from List_Stay where  MasterID=@MasterID and Lang='en'"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = hotelid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += reader("BookingLink").ToString()



        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub btnCheckAvaibility_Click(sender As Object, e As EventArgs) Handles btnCheckAvaibility.Click
        Dim monthyear1, monthyear2, day1, day2 As String

        monthyear1 = CDate(txtCheckIn.Text).Month.ToString() + "%" + CDate(txtCheckIn.Text).Year.ToString()
        day1 = CDate(txtCheckIn.Text).Day.ToString()
        monthyear2 = CDate(txtCheckOut.Text).Month.ToString() + "%" + CDate(txtCheckOut.Text).Year.ToString()
        day2 = CDate(txtCheckOut.Text).Day.ToString()
        Dim night As Integer = CInt(day2) - CInt(day1)
        txtNight.Text = night
        Dim returl As String = ""
        If ddlHotels.SelectedValue = 1 Then
            returl = hdnBookingLinkSt.Value & "&departureDate=" & Date.Parse(txtCheckOut.Text).ToString("yyyy-MM-dd") & "&arrivalDate=" & Date.Parse(txtCheckIn.Text).ToString("yyyy-MM-dd") & "&lengthOfStay=" & txtNight.Text & "&numberOfRooms=1&numberOfAdults=1&numberOfChildren=0&language=en_US&ES=LPS_3400_EN_ST_BOOKWIDGET_ME_EAME"
        Else

            returl = hdnBookingLinkPk.Value & "&adults=2&kids=0&rooms=1&monthyear1=" + monthyear1 + "&day1=" + day1 + "&monthyear2=" + monthyear2 + "&day2=" + day2 + "&gpnum=&srcd=dayprop&Lang=en&src=saadiyat_roombooking"

        End If
        ' If ddlHotels.SelectedValue = 1 Then
        'hdnBookingLink.Value = getBookingLink(ddlHotels.SelectedValue) & "&departureDate=" & Date.Parse(txtCheckOut.Text).ToString("yyyy-mm-dd") & "&arrivalDate=" & Date.Parse(txtCheckOut.Text).ToString("yyyy-mm-dd") & "&lengthOfStay=" & txtNight.Text & "&numberOfRooms=1&numberOfAdults=1&numberOfChildren=0&language=en_US&ES=LPS_3400_EN_ST_BOOKWIDGET_ME_EAME"
        '' Else


        'hdnBookingLink.Value = getBookingLink(ddlHotels.SelectedValue) & "&adults=2&kids=0&rooms=&monthyear1=" + monthyear1 + "&day1=" + day1 + "&monthyear2=" + monthyear2 + "&day2=" + day2 + "&gpnum=&srcd=dayprop&Lang=en&src=prop_nplk_facebook_tab_roombooking"
        '  End If
        'Calling script from Code Behind
        'Dim scriptKey As String = "UniqueKeyForThisScript"
        'Dim javaScript As String = "<script type='text/javascript'>window.open('" & returl & "', '_blank');</script>"
        'ClientScript.RegisterStartupScript(Me.GetType(), scriptKey, javaScript)
        Response.Redirect(returl)
    End Sub

    
End Class
