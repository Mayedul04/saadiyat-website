﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<%@ Register src="F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">

<!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <%= Language.Read("Contact Us", Page.RouteData.Values("lang"))%> 
                        <%= Language.Read("", Page.RouteData.Values("lang"))%>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") & Session("lang") & "/home" %>'> <%= Language.Read("Home", Page.RouteData.Values("lang"))%></a></li>
                        <li class="active"> <%= Language.Read("Contact Us", Page.RouteData.Values("lang"))%> </li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                <div class="row">
                    
                    <div class="col-md-6 col-sm-6">
                        
                        <!-- Register Section -->
                        <div class="checkAvailabityWidget mediaRequestForm">

                           <p> <%= Language.Read("Feel free to contact us with your queries. We will respond as soon as we can.", Page.RouteData.Values("lang"))%> </p>
                            
                            <div class="row formRowMargin">
                                
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><%= Language.Read("First Name", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>

                                        </label>
                                        
                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control" ></asp:TextBox>

                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><%= Language.Read("Last Name", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        </label>
                                        <asp:TextBox ID="txtLastName" runat="server" class="form-control" ></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div class="row formRowMargin">
                                
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><%= Language.Read("Email", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="contact-full" 
                                            ErrorMessage="RegularExpressionValidator" SetFocusOnError="True" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email</asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" ></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label"><%= Language.Read("Country", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCountryCode" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        </label>
                                        <asp:TextBox ID="txtCountryCode" runat="server" class="form-control" placeholder="+971" ></asp:TextBox>
                                    </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                        <label class="control-label"><%= Language.Read("Area", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtArea" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        </label>
                                        <asp:TextBox ID="txtArea" runat="server" class="form-control" placeholder="05"></asp:TextBox>
                                    </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                        <label class="control-label"><%= Language.Read("Mobile", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        </label>
                                        <asp:TextBox ID="txtMobile" runat="server" class="form-control" placeholder="00000000"></asp:TextBox>
                                    </div>
                                    </div>
                                    <asp:HiddenField ID="hdnMobile" runat="server" />
                                </div>
                                </div>
                            </div>

                            <div class="formRowMargin">
                                
                                <div class="form-group">
                                    <label class="control-label"><%= Language.Read("Message", Page.RouteData.Values("lang"))%><asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMessage" 
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="contact-full" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                    </label>
                                    <asp:TextBox ID="txtMessage" runat="server" class="form-control" TextMode="MultiLine" Rows="3" ></asp:TextBox>
                                </div>

                            </div>
                            <asp:HiddenField ID="hdnContactDatetime" runat="server" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Contact" CssClass="submitButton" ValidationGroup="contact-full" />
                            <asp:HiddenField ID="hdnName" runat="server" />
                            <asp:SqlDataSource ID="sdsContact" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                                DeleteCommand="DELETE FROM [Contact] WHERE [ContactID] = @ContactID" 
                                InsertCommand="INSERT INTO [Contact] ([FullName], [Email], [Mobile], [Message], [ContactDate]) VALUES (@FullName, @Email, @Mobile, @Message, @ContactDate)" 
                                SelectCommand="SELECT [ContactID], [FullName], [Email], [Mobile], [Message], [ContactDate] FROM [Contact]" 
                                UpdateCommand="UPDATE [Contact] SET [FullName] = @FullName, [Email] = @Email, [Mobile] = @Mobile, [Message] = @Message, [ContactDate] = @ContactDate WHERE [ContactID] = @ContactID">
                                <DeleteParameters>
                                    <asp:Parameter Name="ContactID" Type="Int32" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:ControlParameter ControlID="hdnName" Name="FullName" PropertyName="Value" 
                                        Type="String" />
                                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                                        Type="String" />
                                    <asp:ControlParameter ControlID="hdnMobile" Name="Mobile" PropertyName="Value" 
                                        Type="String" />
                                    <asp:ControlParameter ControlID="txtMessage" Name="Message" PropertyName="Text" 
                                        Type="String" />
                                    <asp:ControlParameter ControlID="hdnContactDatetime" Name="ContactDate" 
                                        PropertyName="Value" Type="DateTime" />
                                </InsertParameters>
                                <UpdateParameters>
                                    <asp:Parameter Name="FullName" Type="String" />
                                    <asp:Parameter Name="Email" Type="String" />
                                    <asp:Parameter Name="Mobile" Type="String" />
                                    <asp:Parameter Name="Message" Type="String" />
                                    <asp:Parameter Name="ContactDate" Type="DateTime" />
                                    <asp:Parameter Name="ContactID" Type="Int32" />
                                </UpdateParameters>
                            </asp:SqlDataSource>
                        </div>
                        <!-- Register Section -->

                    </div>

                    <div class="col-md-6 col-sm-6">
                        
                        <!-- Login Section -->
                        <div class="checkAvailabityWidget mediaRequestForm blue">

                            <div class="imap">
                            <a href='<%= Session("domainName") & Session("lang") & "/interactive-map" %>' class="mappop" data-fancybox-type="iframe">
                                <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                            </a>
                        </div>


                        </div>
                        <!-- Login Section -->

                    </div>

                </div>

                <h2 class="subtitle">
                    <asp:Literal ID="ltrContactInfoTitle" runat="server"></asp:Literal></h2>
                

                <!-- Contact Listings -->
                <div class="row">
                    <asp:Literal ID="ltrContactInfo" runat="server"></asp:Literal>
                    <%--<ul class="contactInfoListings">
                        
                        <li class="col-md-4 col-sm-4">

                            <div class="contentSection">
                            
                                <h3>Media Contacts</h3>
                                <p>Email : <a href="javascript:;">pressroom@tdic.ae</a> <br>
                                Tel : +971 800-TDIC (8342) </p>
                            </div>
                        </li>

                    </ul>--%>
                
                </div>
                <!-- Contact Listings -->
<uc1:DynamicSEO ID="DynamicSEO1" runat="server" />


            </div>
            <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

