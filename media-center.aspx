﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="media-center.aspx.vb" Inherits="media_center" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
       <!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        Media <span> Centre </span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") %>'>Home</a></li>
                        <li class="active"> Media Centre </li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                    
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                </div>

                <!-- Dining Lists -->
                <div class="row">
                    <ul class="diningListings mediaLanding">
                            
                       <%= getHTMLContent("25", "news/press-release")%>
                       <%= getHTMLContent("26", "news/features")%>
                       <%= getHTMLContent("27", "news/new")%>
                       <%= getHTMLContent("28", "media-image-request")%>
                       <%= getHTMLContent("29", "social-details")%>
                                              

                    </ul>
                </div>
                <!-- Dining Lists -->


            </div>
            <!-- Main Content Section -->
     <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

