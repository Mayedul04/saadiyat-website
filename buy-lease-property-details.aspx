﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="buy-lease-property-details.aspx.vb" Inherits="buy_lease_property_details" %>

<%@ Register src="~/F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<%@ Register src="~/CustomControl/UserGalleryControl.ascx" tagname="UserGalleryControl" tagprefix="uc2" %>

<%@ Register src="~/CustomControl/RegisterNow.ascx" tagname="RegisterNow" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">

        <div class=" fadeInLeft animated home innerdetail">
            <div class="heading">
                <span>
                    <asp:Literal ID="ltrLeftPanelTitle" runat="server"></asp:Literal></span>
            </div>
            <!-- -- heading ends here -- -->
            <a href="javascript:;" class="scrollers"><img src="/ui/media/dist/elements/down.png" alt="">
            </a>
            <div class="panel-content nano">
                <div class="scroller nano-content">
                    <ul class="list-unstyled">
                        <asp:Literal ID="ltrList" runat="server"></asp:Literal>  
                    </ul>
                </div>
            </div>
            <!-- -- panel-content ends here -->

        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">



            <!-- Main Banner Section -->
            <div class="section-slider flexslider">
                <ul class="list-unstyled slides">
                    <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/buy-lease-properties" %>'>Buy or lease properties</a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/buy-lease-property-list/" & Page.RouteData.Values("pid") & "/" & Page.RouteData.Values("ptitle")   %>'><asp:Literal ID="ltrBreadcumParentTitle" runat="server"></asp:Literal></a>
                        </li>
                        <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <div class="extralinks">
                <asp:Literal ID="ltrExtraLinks" runat="server"></asp:Literal>                
            </div>


            <!-- Main Content Section -->
            <div class="main-content-area relativeDiv">

                <!-- Call To Action Buttons -->
                <asp:Literal ID="ltrBadge" runat="server"></asp:Literal>
                <%--<ul data-wow-iteration="100" class="badgeInner"><li>Ready to move In</li></ul>--%>
                <!-- Call To Action Buttons -->

               
                
                <!-- About The Designs Section -->
                <div class="aboutDesignBox">
                    
                    <h2 class="maintitle">
                        <asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></h2>
                    <div class="imgHold">
                        <asp:Image ID="imgBigImage" runat="server" />
                    </div>

                
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                    <!-- Misinary TCab Boxes -->
                    <ul class="misinaryBoxesListings csrMisinaryListing tabBoxes">
                        <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>
                        

                    </ul>
                    <!-- Misinary TCab Boxes -->

                </div>
                <!-- About The Designs Section -->

                <!-- About the life style & Testimonials -->
                <div class="row">
                    
                    <div class="col-md-8">
                        
                        <h2 class="subtitle yellow">
                           <span>About the</span> lifestyle 
                        </h2>

                        <asp:Literal ID="ltrBigDetails2" runat="server"></asp:Literal>
                    </div>

                    <!-- Testimonial Section -->
                    <div class="col-sm-4">
                        <h2 class="subtitle">
                            Testimonials
                        </h2>
                        <asp:Literal ID="ltrTestimonials" runat="server"></asp:Literal>
                        <%--<div class="well testimonials"><span>"</span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus voluptatibus fugit rerum iure. Sint quidem natus illum, sed eius numquam, dolor iste velit voluptatum nihil dolorem deleniti illo tempore iusto?</div>
                        <div class="by">By Roger Jack</div>--%>
                    </div>
                    <!-- Testimonial Section -->

                </div>
                <!-- About the life style & Testimonials -->

                <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>                            
                
                <!-- Download List Container -->

                                
                <!-- Featured Bullet Listings -->
                <asp:Literal ID="ltrFeature" runat="server"></asp:Literal>
                <!-- Featured Bullet Listings -->

                
                <!-- Enquire Now and Testimonials Section -->
                <uc3:RegisterNow ID="RegisterNow1" runat="server"   />
                <!-- Enquire Now and Testimonials Section -->
                <!-- Map & Gallery -->
                <div class="row">
                    
                    <div class="col-sm-6">
                        <div class="gallerybox">
                            <h2 class="subtitle yellow">
                                Images and <span>Video</span><asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                            </h2>
                            <div class="flexsliders">
                                <ul class="list-unstyled gallerysection slides">
                                    <uc2:UserGalleryControl ID="UserGalleryControl1" runat="server" Gallery_Relevency="Gallery" />
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <h2 class="subtitle yellow">
                            Interactive  <span>Map</span>
                        </h2>
                        <div class="imap">
                            <a href='<%= Session("domainName") & Session("lang") & "/interactive-map/residentials" %>' class="mappop" data-fancybox-type="iframe">
                                <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                            </a>
                        </div>

                    </div>


                </div>
                <!-- Map & Gallery -->


                <!-- contact Info & Video Section -->
                <div class="row marginBtm20">
                    
                    <!-- EnQuire Now Box -->
                    <div class="col-sm-6">
                        <h2 class="subtitle">
                            Quick <span>Contact</span>
                        </h2>
                        
                        <!-- Contact Detail Section -->
                        <div class="contactDetailsSection">

                            <ul class="contactListings">
                                
                                
                            <asp:Literal ID="ltrContact" runat="server"></asp:Literal>

                            </ul>
                            
                            

                        </div>
                        <!-- Contact Detail Section -->

                    </div>
                    <!-- EnQuire Now Box -->
                    
                    <!-- Testimonial Section -->                     
                        <!-- Video Slider Section -->
                        <asp:Literal ID="ltrVideo" runat="server"></asp:Literal>
                        <!-- Video Slider Section -->
                    
                    <!-- Testimonial Section -->

                </div>
                <!-- contact Info & Video Section -->

                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
            </div>
            <!-- Main Content Section -->


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

