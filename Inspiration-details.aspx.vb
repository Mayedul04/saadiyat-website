﻿
Partial Class Inspiration_details
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, BigImage, Link, MapImage, GalleryID, SecondText from List_Inspiration where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                If Page.RouteData.Values("id") = "1" Or Page.RouteData.Values("id") = "5" Then
                    pnlnormal.Visible = True
                    lblTitle.Text = "<h2 class=""maintitle tagline"">" & FormateTitle(reader("SubTitle").ToString()) & "</h2>"
                    lblSecondDetails.Text = reader("SecondText").ToString()
                    lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, "/Admin/A-Inspiration/CulturalCentreEdit.aspx?lid=" & hdnID.Value)
                Else
                    lblTitle.Text = "<h2 class=""maintitle tagline"">" & FormateTitle(reader("SubTitle").ToString()) & "</h2>"
                    pnlContents.Visible = True
                    lblContent.Text = Contents("List_Inspiration", hdnID.Value)
                End If
                
               

                title = reader("Title").ToString()
                galid = reader("GalleryID")
                If Page.RouteData.Values("id") = 5 Then

                    '   pnlDownload.Visible = True
                    '  lblAddFile.Text = Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Inspiration&TID=" & hdnID.Value & "&t=" & Utility.EncodeTitle(title, "-"))
                    If IsDBNull(reader("Link").ToString()) = False Then
                        lblDownloadable.Text = "<a class=""clickHereButton"" href=""" & reader("Link").ToString() & """ target=""_blank"">View Website <span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png""  alt=""""></span></a>&nbsp;&nbsp;"
                    End If
                    lblDownloadable.Text &= "<span class=""clickHereButton downLoadListTrigger"" data-wow-iteration=""100"">Downloadable Documents <span>"
                    lblDownloadable.Text &= "<img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></span>"

                   

                    lblDownloadable.Text &= "<div class=""downloadListContainer""><div class=""contentSection"">" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Inspiration&TID=" & hdnID.Value & "&t=" & Utility.EncodeTitle(title, "-"))
                    lblDownloadable.Text &= "<ul class=""listings"">" & getDownloadableFiles(hdnID.Value) & "</ul></div></div>"
                Else
                    If IsDBNull(reader("Link").ToString()) = False Then
                        lblDownloadable.Text = "<a class=""clickHereButton"" href=""" & reader("Link").ToString() & """ target=""_blank"">View Website <span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png""  alt=""""></span></a>"
                    End If
                End If
                '  lblMap.Text = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            End While
            conn.Close()
            If galid <> 0 Then

                UserGalleryControl.Gallery_ID = galid
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If
            styledtitle = FormateTitle(title)
            ' lblVgal.Text = GetFeaturedVideos()
            
            If Page.RouteData.Values("id") = 3 Then
                pnlHtml.Visible = True

            End If
            
            
        End If
    End Sub
    Public Function LoadHtmlContent(ByVal htmlmasterid As Integer, ByVal imageclass As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT HTMLID, Title ,BigDetails,BigImage,ImageAltText,GalleryID from HTML where MasterID=@MasterID and Lang=@Lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.NVarChar, 50).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()


        While reader.Read()
            
            Dim photos As String = ""
            If IsDBNull(reader("GalleryID")) = False Then
                retstr += "<div class=""imgHold " & imageclass & """>"
                ' If IsDBNull(reader("GalleryID")) = False Then
                retstr += "<div class=""detailVideoSlider flexsliders""><ul class=""slides"">"
                ' Dim videos As String = GetFeaturedVideos(reader("GalleryID").ToString())
                photos = getGalleryItems(reader("GalleryID").ToString())
                Dim videos As String = GetFeaturedVideos(reader("GalleryID"))
                If htmlmasterid = "32" Then
                    If videos <> "" Then
                        retstr += videos & "</ul>" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</div>"
                    Else
                        retstr += Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</ul></div>"
                    End If
                Else
                    If photos <> "" Then
                        retstr += photos & "</ul>" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</div>"
                    Else
                        retstr += Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</ul></div>"
                    End If
                End If
                
                retstr += "</div>"
            End If


            

           

            retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&SmallImage=0&ImageAltText=0&SmallDetails=0&SecondImage=0&File=0&BigImageWidth=485&BigImageHeight=287")
        End While

        conn.Close()

        Return retstr
    End Function
    Public Function getDownloadableFiles(ByVal id As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID  and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Inspiration"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName").ToString() & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID").ToString() & "&TName=List_Inspiration&TID=" & id) & "</li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getGalleryItems(ByVal galleryid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,BigImage,Title from GalleryItem where  GalleryID=@GalleryID and ItemType='Image'"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galleryid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li>" & Utility.showEditButton(Request, Session("domainName") & "/admin/A-Gallery/GalleryItemEdit.aspx?galleryItemId=" & reader("GalleryItemID").ToString())
                retstr += "<a href=""" & Session("domainName").ToString() & "PhotoPop.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("GalleryItemID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ class=""photopopup fancybox.iframe"" rel=""" & galleryid & """>"
                retstr += "<img alt="""" src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """></a></li>"


            End While
        End If

        conn.Close()



        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = "<span>" & firstpart & "</span>" & secondpart
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Inspiration"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getCentreThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ListID, Title, SmallImage, SmallDetails,MasterID from List_Inspiration where  Lang=@Lang order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"
            
            retstr += "<a href=""" & Session("domainName") & Session("lang") & "/inspiration-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>"
            If Page.RouteData.Values("id") = reader("MasterID") Then
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">" & title & " at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Inspiration"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Inspiration"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = Math.Ceiling(total / 2) Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        If count < 1 Then
            retstr = ""
        End If
        Return retstr


      
    End Function
    Public Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            If Page.RouteData.Values("id") = 5 Then
                retstr += "<div class=""uaePavalionContainer""><div class=""imgHold""><img src=""" & Session("domainName") & "Admin/" & reader("Image").ToString() & """ alt=""""></div>" & reader("DetailText").ToString() & "</div>"
            Else
                If counter = 0 Then
                    retstr += "<div class=""row aboutDarchitect""><div class=""col-md-4""><div class=""imgHold""><img src=""" & Session("domainName") & "Admin/" & reader("Image").ToString() & """ alt=""""></div>"
                    retstr += "</div><div class=""col-md-8""><h2 class=""maintitle"">" & reader("Title").ToString() & "</h2><span class=""clearAll""></span>"
                    retstr += reader("DetailText").ToString() & "</div>" & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=313&ImageHeight=313&Title=1&Image=1&Text=1&link=0") & "</div>"
                ElseIf counter = 1 Then
                    retstr += "<div class=""aboutDesignBox""><h2 class=""maintitle"">" & reader("Title").ToString() & "</h2><div class=""imgHold"">"
                    retstr += "<img src=""" & Session("domainName") & "Admin/" & reader("Image").ToString() & """ alt = """" ></div>"
                    retstr += reader("DetailText").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=999&ImageHeight=297&Title=1&Image=1&Text=1&link=0") & "<ul class=""misinaryBoxesListings csrMisinaryListing tabBoxes"">" & LoadCollage("List_Inspiration", hdnID.Value) & "</ul>" & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=" & tableName & "&TID=" & tableID) & "</div>"
                Else
                    retstr += "<h2 class=""maintitle"">" & reader("Title").ToString() & "</h2>" & reader("DetailText").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&Title=1&Image=0&Text=1&link=0")
                End If

                counter += 1
            End If

        End While
        conn.Close()

        Return retstr
    End Function
    Public Function LoadCollage(ByVal tname As String, ByVal tid As Integer) As String
        Dim retstr As String = ""
        Dim substr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT dbo.CollageIteams.IteamID, dbo.CollageIteams.Title, dbo.CollageIteams.SortIndex, dbo.CollageIteams.IteamID, dbo.CollageIteams.CollageImage, dbo.CollageIteams.SortIndex, dbo.CollageIteams.Status, dbo.Collages.Templete FROM  dbo.CollageIteams INNER JOIN dbo.Collages ON dbo.CollageIteams.CollageID = dbo.Collages.ID WHERE  (dbo.Collages.TableID = @TID) AND (dbo.Collages.TableName = @TName) and dbo.CollageIteams.Status=1 order by dbo.CollageIteams.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TName", Data.SqlDbType.NVarChar, 50).Value = tname
        cmd.Parameters.Add("TID", Data.SqlDbType.Int, 32).Value = tid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If reader("Templete").ToString() = "1" Then
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            ElseIf reader("Templete").ToString() = "2" Then
                retstr += "<li><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
            ElseIf reader("Templete").ToString() = "3" Then
                If reader("SortIndex").ToString() = "3" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            Else
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a class=""photopopup"" href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            End If


        End While
        conn.Close()
        'If retstr <> "" Then
        '    retstr = "<div class=""culturalDisrictSlider flexslider""><ul class=""slides"">" & retstr & "</ul>" & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=" & tname & "&TID=" & tid) & "</div>"
        'End If
        '  retstr += 
        Return retstr
    End Function
    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")
        Return bigText
    End Function
    Public Function GetFeaturedVideos(ByVal gallryid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = gallryid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""230""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getAddresses() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email , Details from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Inspiration"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"
            'retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            'If IsDBNull(reader("IntPhone")) = False Then
            '    retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>"
            'End If
            retstr += reader("Details").ToString() & Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
            ' retstr += Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=List_Inspiration&TID=" & hdnID.Value & "&t=" & Server.UrlEncode(title)) & retstr
        Return retstr
    End Function
    Public Function getfirstFooter() As String
        Dim retstr As String = ""
        Dim videos As String = GetFeaturedVideos(galid)
        'If Page.RouteData.Values("id") = 6 Then
        '    retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        'Else
        retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">" & Language.Read("Video <span>Section</span>", Page.RouteData.Values("lang")) & "</h2><div class=""detailVideoSlider video-flexsliders"">"
        retstr += "<ul class=""slides"">" & videos & "</ul></div></div>"

        '  End If

        Return retstr
    End Function
    Public Function getSecondFooter() As String
        Dim retstr As String = ""
        Dim addr As String = getAddresses()
        'If addr <> "" Then
        '    retstr += "<div class=""col-sm-6"">Quick <span>Contact</span><div class=""contactDetailsSection"">"
        '    retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        'Else
        '    retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        'End If
       
        retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">" & Language.Read("Quick <span>Contact</span>", Page.RouteData.Values("lang")) & "</h2><div class=""contactDetailsSection"">"
            retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        If Page.RouteData.Values("id") = "1" Then
            retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">" & Language.Read("Interactive  <span>Map</span>", Page.RouteData.Values("lang")) & "</h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/9,10,11,12,13"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        Else

            retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">" & Language.Read("Interactive  <span>Map</span>", Page.RouteData.Values("lang")) & "</h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/cultural/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        End If



        Return retstr
    End Function
End Class
