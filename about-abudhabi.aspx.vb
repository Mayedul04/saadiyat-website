﻿
Partial Class about_abudhabi
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails,  MapImage, GalleryID, SecondText, Link from List_Saadiyat where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 9
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                lblTitle.Text = "<h2 class=""maintitle tagline"">" & FormateTitle(reader("SubTitle").ToString()) & "</h2>"
                lblSecondDetails.Text = reader("SecondText").ToString()
                lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, "/Admin/A-Saadiyat/ItemEdit.aspx?lid=" & hdnID.Value)
                link1 = reader("Link").ToString()

                title = reader("Title").ToString()
                galid = reader("GalleryID")
                DynamicSEO.PageType = "List_Saadiyat"
                DynamicSEO.PageID = hdnID.Value
                '  lblMap.Text = "<div class=""col-sm-4""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            End While
            conn.Close()
            If galid <> 0 Then

                'UserGalleryControl.Gallery_ID = galid
                'ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If
            styledtitle = FormateTitle(title)
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where MasterID not in (5,6,7,8) and Lang=@Lang and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"

            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If


            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    
    Public Function LoadHtmlContent(ByVal htmlmasterid As Integer) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT HTMLID, Title ,BigDetails,BigImage,ImageAltText,GalleryID from HTML where MasterID=@MasterID and Lang=@Lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.NVarChar, 50).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()


        While reader.Read()
            If htmlmasterid = 30 Then
                retstr += "<div class=""imgHold"">"
                retstr += "<img alt=""" & reader("Title").ToString() & """ src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ >"
                retstr += "</div><h3 class=""subtitle yellow"">" & reader("Title").ToString() & "</h3>"
                retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&SmallImage=0&ImageAltText=0&SmallDetails=0&SecondImage=0&File=0") & "</div>"
            Else
                'sretstr += 
                retstr += "<div class=""row aboutAuhVideoSection""><div class=""col-md-6""><h3 class=""subtitle yellow"">" & reader("Title").ToString() & "</h3>" & reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&SmallImage=0&ImageAltText=0&SmallDetails=0&SecondImage=0&File=0") & "</div>"
                retstr += "<div class=""col-md-6""><div class=""detailVideoSlider flexsliders""><ul class=""slides"">"
                Dim videos As String = GetFeaturedVideos(reader("GalleryID").ToString())

                If videos <> "" Then
                    retstr += videos & "</ul>" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</div></div></div>"
                Else
                    retstr += Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</ul></div></div></div>"
                End If
            End If
           
        End While

        conn.Close()

        Return retstr
    End Function
    Public Function LoadCollage(ByVal tname As String, ByVal tid As Integer) As String
        Dim retstr As String = ""
        Dim substr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT dbo.CollageIteams.IteamID, dbo.CollageIteams.Title, dbo.CollageIteams.SortIndex, dbo.CollageIteams.IteamID, dbo.CollageIteams.CollageImage, dbo.CollageIteams.SortIndex, dbo.CollageIteams.Status, dbo.Collages.Templete FROM  dbo.CollageIteams INNER JOIN dbo.Collages ON dbo.CollageIteams.CollageID = dbo.Collages.ID WHERE  (dbo.Collages.TableID = @TID) AND (dbo.Collages.TableName = @TName) and dbo.CollageIteams.Status=1 order by dbo.CollageIteams.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TName", Data.SqlDbType.NVarChar, 50).Value = tname
        cmd.Parameters.Add("TID", Data.SqlDbType.Int, 32).Value = tid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If reader("Templete").ToString() = "1" Then
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            ElseIf reader("Templete").ToString() = "2" Then
                retstr += "<li><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
            ElseIf reader("Templete").ToString() = "3" Then
                If reader("SortIndex").ToString() = "3" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            Else
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            End If


        End While
        conn.Close()
        '' retstr += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=" & tname & "&TID=" & tid)
        Return retstr
    End Function
    Public Function GetFeaturedVideos(ByVal galleryid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galleryid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""230""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function loadQuote(ByVal tmasterid As Integer) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * from List_Testimonial where ID=@ID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ID", Data.SqlDbType.Int, 32).Value = tmasterid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <span class=""pull-left quote open"">"
            retstr += " <img src=""" & Session("domainName").ToString() & "ui/media/dist/elements/quote.png"" alt=""""></span>"
            retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & tmasterid & "&image=0") & "<span class=""pull-right quote closeq"">"
            retstr += "<img src=""" & Session("domainName").ToString() & "ui/media/dist/elements/quote-close.png"" alt=""""> </span>"
            retstr += "<h6>" & reader("TestimonialBy").ToString() & "</h6>"


        End While

        conn.Close()
        Return retstr
    End Function
End Class
