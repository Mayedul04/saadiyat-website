﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="gallery-items.aspx.vb" Inherits="gallery_items" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span><b><%= Language.Read("Gallery", Page.RouteData.Values("lang"))%></b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src="/ui/media/dist/elements/down.png" alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">

                    <%= getLeftNav("16", "gallery/photo")%>
                    <%= getLeftNav("17", "gallery/video")%>
                    <%= getLeftNav("18", "experiences")%>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class=""><%= FormateTitle(Title) %>
            </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/gallery" %>'><%= Language.Read("Gallery", Page.RouteData.Values("lang"))%></a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/gallery/" & Page.RouteData.Values("type") & "/" & Page.RouteData.Values("catid") & "/" & Page.RouteData.Values("module") %>'><%= Utility.DecodeTitle(Page.RouteData.Values("module"),"-") %></a>
                </li>
                 
                <li class="active"><%= Title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">
        <asp:HiddenField ID="hdnID" runat="server" />
        <!-- Gallery Listing -->
        <div class="row">
            <ul class="diningListings mediaLanding">

                <%= GalleryItems() %>
            </ul>
        </div>
        <asp:Panel ID="pnlPageination" runat="server">

            <%= PageList %>
        </asp:Panel>

    </div>
    <!-- Main Content Section -->
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

