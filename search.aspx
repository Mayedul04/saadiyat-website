﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="search.aspx.vb" Inherits="search" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider landingSlider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%=Title%>
            </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") & Session("Lang") & "/home" %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a></li>
                <li class="active"><%= Title%> </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>

    <!-- Main Content Section -->
    <div class="main-content-area">
        <asp:Panel  ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="searchEngine"  >

                <div class="row formSection">

                    <div class="col-md-7 col-md-offset-1 col-sm-8">

                        <div class="form-group">
                            <asp:TextBox ID="txtSearch" type="search" runat="server" placeholder="Search..." class="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtSearch" Display="Dynamic" ValidationGroup="Search1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </div>

                    </div>

                    <div class="col-md-3 col-sm-4">
                        <asp:Button ID="btnSearch" runat="server" CssClass="searchButton" ValidationGroup="Search1" Text="Search"></asp:Button>
                    </div>

                </div>

            </div>
        </asp:Panel>
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />


        <asp:Literal ID="ltrsaadiyat" runat="server"></asp:Literal>
        <ul class="paginationList">
            <asp:Literal ID="lbPagingBottom" runat="server"></asp:Literal>
        </ul>



    </div>
    <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

