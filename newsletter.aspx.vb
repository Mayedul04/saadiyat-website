﻿
Partial Class newsletter
    Inherits System.Web.UI.Page
    Public Title As String = "", htmlmasterid As String = "29"
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
      
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""


        HTML(htmlmasterid, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        hdnID.Value = HTMLID
        lblDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftNav(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang, MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li>"
            retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """>"
            If Request.Url.AbsoluteUri.Contains(link) Then
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            Else
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            End If

            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        hdnDate.Value = DateTime.Now.ToShortDateString
        Dim retVal As Integer = 0
        retVal = sdsNewsLetter.Insert()
        If retVal > 0 Then
            Dim body As String = mailsndtoClient("Thank you for subscribing to our newsletter. We will be in touch with you soon.")
            Utility.SendMail("Saadiyat Newsletter", "noreply@saadiyat.com", txtEmail.Text, "", "mayedul.islam@wvss.net", "Newslettter Subscription for Saadiyat", body)

            Dim body1 As String = mailsndtoLPD()
            Utility.SendMail("Client", txtEmail.Text, "mayedul.islam@wvss.net", "", "mayedul@digitalnexa.com", "Request for Newsletter subscription", body1)

            Response.Redirect(Session("domainName") & "Thank-you/newsletter")

        End If
       

    End Sub
    Public Function mailsndtoClient(ByVal body As String) As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">This is an AUTOMATED MESSAGE. Please do not reply or respond back to this mail.</p>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">" + body + "</p>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 0 0; padding:0; color:#595959;""><strong>Best Regards,</strong><br />"
        M += "Saadiyat</p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function

    Public Function mailsndtoLPD() As String
        Dim M As String = String.Empty

        M += "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        M += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        M += "<head>"
        M += "<meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        M += "<title>Al Noon</title>"
        M += "</head>"

        M += "<body style=""margin:0; padding:0; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#595959; text-align:justify;"">"
        M += "<table width=""700"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""padding:50px 0; background:#f7f7f7;""><table width=""600"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td style=""background:#fff; padding:15px 0;""><table width=""570"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        M += "<tr>"
        M += "<td>"

        M += "<p style=""font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; padding:0; color:#595959;"">Request for Newsletter subscription from email ID: " & txtEmail.Text & " </p>"
        M += "</td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table></td>"
        M += "</tr>"
        M += "</table>"
        M += "</body>"
        M += "</html>"

        Return M


    End Function
End Class
