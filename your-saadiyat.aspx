﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="your-saadiyat.aspx.vb" Inherits="your_saadiyat" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
    <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                   <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        Your<span>Saadiyat</span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") %>'>Home</a></li>
                        <li class="active">Your Saadiyat</li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                   <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </div>

                <!-- Inspiration Landing List -->
                <ul class="parallaxlisting list-unstyled">

                  <%= getItemList() %>

                </ul>

                <!-- Inspiration Landing List -->
                 
<uc1:DynamicSEO runat="server" ID="DynamicSEO" />
            </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
     <script src='<%= Session("domainName") & "ui/js/dist/jquery.gray.min.js"%>'></script>
</asp:Content>

