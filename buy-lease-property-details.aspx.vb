﻿
Partial Class buy_lease_property_details
    Inherits System.Web.UI.Page

    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About],ParentHTMLMasterID  FROM  [dbo].[List_Property] where  Lang=@Lang and Status=1 and ParentHTMLMasterID=@ParentHTMLMasterID order by SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            cmd.Parameters.Add("ParentHTMLMasterID", Data.SqlDbType.Int).Value = Page.RouteData.Values("pid")

            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()
                'If reader("MasterID") = "4" Or reader("MasterID") = "5" Then
                '    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                'Else
                'reader("ParentHTMLMasterID") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(ltrBreadcumTitle.Text, "-") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
                HTML(Page.RouteData.Values("pid"), HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)

                ltrBreadcumParentTitle.Text = Title
                link = Session("domainName") & Session("lang") & "/property/" & reader("ParentHTMLMasterID") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(Title, "-") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")

                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "javascript:;", link) & """>" & _
                       "         <div class=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "thumbnail-box activeBW", "thumbnail-box") & """ >" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = Page.RouteData.Values("id") Then
                    currentListID = reader("ListID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()
                    ltrBigDetails.Text = reader("BigDetails").ToString & Utility.showEditButton(Request, "/admin/A-Properties/PropertyEdit.aspx?lId=" & reader("ListID") & "&ParentHTMLMasterID=" & reader("ParentHTMLMasterID"))
                    'hlMap.NavigateUrl = reader("MapCode").ToString
                    'imgMap.ImageUrl = Session("domainName") & "Admin/" & reader("MapImage").ToString()
                    currentGalleryID = reader("GalleryID").ToString()
                    ltrBigDetails2.Text = reader("About").ToString()
                    If reader("Badge").ToString() <> "" Then
                        ltrBadge.Text = "<ul data-wow-iteration=""100"" class=""badgeInner""><li>" & reader("Badge") & "</li></ul>"
                    End If

                    '[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]
                    If reader("ViewFloorPlans").ToString() <> "" Then
                        ltrExtraLinks.Text &= "<a href=""/Admin/" & reader("ViewFloorPlans").ToString() & """ class=""linkbtn"" target=""_blank"">View Floor Plans</a>"
                    End If
                    If reader("ProximityMap").ToString() <> "" Then
                        ltrExtraLinks.Text &= "<a href=""/Admin/" & reader("ProximityMap").ToString() & """ class=""linkbtn"" target=""_blank"">Proximity Map</a>"
                    End If
                    If reader("Brochure").ToString() <> "" Then
                        ltrExtraLinks.Text &= "<a href=""/Admin/" & reader("Brochure").ToString() & """ class=""linkbtn"" target=""_blank"">Brochure</a>"
                    End If
                    If reader("LocationMap").ToString() <> "" Then
                        ltrExtraLinks.Text &= "<a href=""/Admin/" & reader("LocationMap").ToString() & """ class=""linkbtn"" target=""_blank"">Location Map</a>"
                    End If

                    ltrSubTitle.Text = (reader("SubTitle").ToString())

                    imgBigImage.ImageUrl = Session("domainName").ToString() & "Admin/" & reader("MediumImage")
                    '
                    imgBigImage.AlternateText = reader("Title").ToString()

                    With DynamicSEO1
                        .PageType = "List_Property"
                        .PageID = currentListID
                    End With

                End If

                i = i + 1
            End While
            conn.Close()

            If currentGalleryID <> "" Then
                ltrVideo.Text = GetFeaturedVideos(currentGalleryID)
                UserGalleryControl1.Gallery_ID = currentGalleryID
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & currentGalleryID)
            End If

            ltrFeature.Text = getSpecialFeatres("List_Property", currentListID)
            ltrAccordianContent.Text = LoadCollage("List_Property", currentListID)
            ltrTestimonials.Text = Testimonials("List_Property", currentListID)
            ltrContact.Text = getAddresses("List_Property", currentListID)
            ltrDownloadFiles.Text = getDownloadableFiles("List_Property", currentListID)
            ltrBanner.Text = getBanners("List_Property", currentListID)

            With RegisterNow1
                .TableName = "List_Property"
                .TableID = currentListID
                .MasterID = Page.RouteData.Values("id")
                .Lang = Session("lang")
            End With

        End If
    End Sub

    Public Function LoadCollage(ByVal tname As String, ByVal tid As Integer) As String
        Dim retstr As String = ""
        Dim substr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT dbo.CollageIteams.Title, dbo.CollageIteams.SortIndex, dbo.CollageIteams.IteamID, dbo.CollageIteams.CollageImage, dbo.CollageIteams.SortIndex, dbo.CollageIteams.Status, dbo.Collages.Templete FROM  dbo.CollageIteams INNER JOIN dbo.Collages ON dbo.CollageIteams.CollageID = dbo.Collages.ID WHERE  (dbo.Collages.TableID = @TID) AND (dbo.Collages.TableName = @TName) and dbo.CollageIteams.Status=1 order by dbo.CollageIteams.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TName", Data.SqlDbType.NVarChar, 50).Value = tname
        cmd.Parameters.Add("TID", Data.SqlDbType.Int, 32).Value = tid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If reader("Templete").ToString() = "1" Then
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & Session("lang") & "/photo-popup/" & reader("IteamID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & Session("lang") & "/photo-popup/" & reader("IteamID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            ElseIf reader("Templete").ToString() = "2" Then
                retstr += "<li><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
            ElseIf reader("Templete").ToString() = "3" Then
                If reader("SortIndex").ToString() = "3" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "photo-popup"" rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "photo-popup"" rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            Else
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "photo-popup"" rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "photo-popup"" rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            End If


        End While
        conn.Close()
        retstr += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=" & tname & "&TID=" & tid)
        Return retstr
    End Function

    Public Function getDownloadableFiles(TableName As String, TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName") & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & _
                    Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID") & "&TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                    "</li>"

        End While
        conn.Close()
        If retstr <> "" Then
            retstr = "<a href='javascript:;' class='clickHereButton downLoadListTrigger' data-wow-iteration='100'>Downloadable Information <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>" & _
                           " <div class='downloadListContainer'>" & _
                           "     <div class='contentSection'>" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                           "         <ul class='listings'>" & _
                           retstr & _
                           "         </ul>" & _
                           "     </div>" & _
                           " </div>"
        End If


        Return retstr
    End Function



    Public Function getSpecialFeatres(TableName As String, ByVal TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr &= "<li class=""col-sm-6"">" & reader("Details").ToString() & "</li>"
        End While
        conn.Close()

        If retstr <> "" Then
            retstr = "<h2 class=""subtitle yellow"">" & ltrBreadcumTitle.Text & " at glance</h2><ul class=""featurelist"">" & retstr & "</ul>"
        End If

        Return retstr
    End Function

    Public Function GetFeaturedVideos(ByVal GalleryID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT galleryItemId,SmallImage ,BigImage,Title, VideoEmbedCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Uploaded Video'"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = GalleryID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """>"
            retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""><source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/webm""></video></div>"
            retstr += "<h2>" & reader("Title").ToString() & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & GalleryID & "&galleryItemId=" & reader("galleryItemId")) & "</h2><a class=""playButton"" href=""javascript:;""><img src=""/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

        End While
        conn.Close()

        If retstr <> "" Then
            retstr = "<div class=""col-sm-6""> <h2 class=""subtitle"">Video <span>Section</span></h2><div class=""detailVideoSlider video-flexsliders""><ul class=""slides"">" & retstr & "</ul></div></div>"
        End If

        Return retstr
    End Function

    Private Function Testimonials(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT [ID]      ,[Title]      ,[SmallImage]      ,[ImageAltText]      ,[BigDetails]      ,[TestimonialBy]      ,[TestimonialDate]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[TableName]      ,[TableMasterID]  FROM [dbo].[List_Testimonial]  where TableName=@TableName and TableMasterID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()

            retstr &= "<div class=""well testimonials""><span>""</span>" & reader("BigDetails") & "</div><div class=""by"">By " & reader("TestimonialBy") & "</div>" & _
            Utility.showEditButton(Request, "/admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & reader("ID") & "&smallImageWidth=478&smallImageHeight=233&image=0&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text))

            counter += 1
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Testimonial/TestimonialEdit.aspx?smallImageWidth=478&smallImageHeight=233&image=0&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & retstr

        Return retstr
    End Function

    Private Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()

            retstr &= "<li class=""item " & If(counter Mod 2 <> 0, "twoBox", "") & """>" & _
                     "       <div class=""imgHold"">" & _
                     "           <a href=""/Admin/" & reader("Image") & """ rel=""1"" class=""photopopup""><img src=""/Admin/" & reader("Image") & """ alt=""""></a>" & _
                     "       </div>" & _
                     Utility.showEditButton(Request, "/admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=478&ImageHeight=160&Title=0&Image=1&Text=0&link=0") & _
                     "   </li>"
            counter += 1
        End While
        conn.Close()
        '
        retstr = Utility.showAddButton(Request, "/admin/A-Content/ContentEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=478&ImageHeight=160&Title=0&Image=1&Text=0&link=0") & retstr
        Return retstr
    End Function

    Public Function getAddresses(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID,Heading ,Phone, IntPhone, Email from ContactDetails where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><p>" & reader("Heading").ToString() & "</p>"
            retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>" & _
                Utility.showEditButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & retstr
        Return retstr
    End Function


    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function

    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
