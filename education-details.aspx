﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="education-details.aspx.vb" Inherits="education_details" %>

<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated education innerdetail">
        <div class="heading">
            <span><%= Language.Read("Your", Page.RouteData.Values("lang"))%>
                <br />
                <b><%= Language.Read("Education", Page.RouteData.Values("lang"))%></b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= domainName & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getInstituteThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle%>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= domainName  %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                <li><a href='<%= domainName & Session("lang") & "/your-education" %>'><%= Language.Read("Your", Page.RouteData.Values("lang"))%> <%= Language.Read("Education", Page.RouteData.Values("lang"))%></a>
                </li>
                <li class="active"><%= title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->



    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">

        <h2 class="maintitle">
            <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
        </h2>
        <span class="clearAll"></span>

        <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
        <a class="clickHereButton" href='<%= link1 %>' target="_blank"><%= Language.Read("View Website", Page.RouteData.Values("lang"))%> <span>
            <img src='<%= domainName & "ui/media/dist/icons/readmore-arrow.png" %>' alt=""></span>
        </a>
        <span class="clickHereButton downLoadListTrigger" data-wow-iteration="100"><%= Language.Read("Downloadable Documents", Page.RouteData.Values("lang"))%> <span>
            <img src='<%= domainName & "ui/media/dist/icons/readmore-arrow.png"  %>' alt=""></span></span>
        

        <div class="downloadListContainer">

            <div class="contentSection">

                <asp:Literal ID="lblAddFile" runat="server"></asp:Literal>

                <ul class="listings">

                    <%= getDownloadableFiles() %>
                </ul>

            </div>
        </div>
        <!-- Download List Container -->

       


        <!-- Featured Bullet Listings -->
       
            <%= getSpecialFeatres() %>
       
        <!-- Featured Bullet Listings -->





        <!-- Map & Gallery -->
        <div class="row">

            <div class="col-sm-6 bottomBoxesMinHeight">
                <div class="gallerybox">
                    <h2 class="subtitle yellow"><%= Language.Read("Images", Page.RouteData.Values("lang"))%> <%= Language.Read("and", Page.RouteData.Values("lang"))%> <span><%= Language.Read("Video", Page.RouteData.Values("lang"))%></span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc1:UserGalleryControl runat="server" ID="UserGalleryControl1" Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />

                        </ul>
                    </div>
                </div>
            </div>
             <%= getfirstFooter() %>
             <!-- Testimonial Section -->
           <%-- <asp:Panel ID="pnlVideo"  runat="server">
            <div class="col-sm-6 bottomBoxesMinHeight">
                <h2 class="subtitle">Video <span>Section</span>
                </h2>
                <!-- Video Slider Section -->
                <div class="detailVideoSlider video-flexsliders">
                    <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid) %>
                    <ul class="slides">
                        <asp:Literal ID="lblVgal" runat="server"></asp:Literal>
                       
                    </ul>

                </div>
                <!-- Video Slider Section -->
            </div>
                </asp:Panel>--%>
            <!-- Testimonial Section -->
            



        </div>
        <div class="row">
            <%= getSecondFooter() %>
            </div>
        <!-- EnQuire Now Box -->
           <%-- <div class="col-sm-6 bottomBoxesMinHeight">
                <h2 class="subtitle">Quick <span>Contact</span>
                </h2>

                <!-- Contact Detail Section -->
                <div class="contactDetailsSection">
                    
                    <ul class="contactListings">

                        <%=getAddresses() %>
                    </ul>



                </div>
                <!-- Contact Detail Section -->

            </div>
            <!-- EnQuire Now Box -->
            <asp:Literal ID="lblMap" runat="server"></asp:Literal>--%>
        <!-- Map & Gallery -->
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />

    </div>
</asp:Content>

