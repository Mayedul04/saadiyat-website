﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>
<%@ Register src="~/CustomControl/MenuLeftSide.ascx" tagname="MenuLeftSide" tagprefix="uc1" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/CustomControl/WhereToGoControl.ascx" TagPrefix="uc1" TagName="WhereToGoControl" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<!DOCTYPE html>
<html lang="en">

<%--<html xmlns="http://www.w3.org/1999/xhtml">--%>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function MM_openBrWindow(theURL, winName, features) { //v2.0
            window.open(theURL, winName, features);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="panel-header animated fadeInLeft" style="display:none" id="left-panel">
            <nav class="menu slide-menu-left">
                <a href="javascript:;" class="close-menu" id="close-panel-bt">
                    <img src="<%= Session("domainName") %>ui/media/dist/elements/menu-btn-close.jpg" alt="">
                </a>
                <a href='<%= Session("domainName") & "default"%>' class="nav-logo">
                    <img src="<%= Session("domainName") %>ui/media/dist/elements/color-logo.png" alt="">
                </a>
                <uc1:MenuLeftSide ID="MenuLeftSide1" runat="server" />
                <a class="tdic-logo" href='<%= Session("domainName") & Session("lang") & "/about-tdic"%>' target="_blank"> 
                    <img src="<%= Session("domainName") %>ui/media/dist/elements/tdic-logo.png" alt="">
                </a>
            </nav>
        </div>

        <a href="#left-panel" id="left-panel-link" class="main-menu-trigger home-menu">
            <img src="<%= Session("domainName") %>ui/media/dist/elements/menu-btn.jpg" alt="">
        </a>

        <div id="main-wrapper" class="content">

            <uc1:WhereToGoControl runat="server" ID="WhereToGoControl" />
            <div class="inner">
                
                <%= LoadModules("Saadiyat", "List_Saadiyat", "saadiyat-details", "8", "Your <br /><b>Saadiyat</b>")%>
                <%= LoadModules("Inspiration", "List_Inspiration", "inspiration-details", "5","Your <br /><b>Inspiration</b>")%>
                <%= LoadModules("Leisure", "List_Leisure", "your-leisure-details", "7", "Your <br /><b>Leisure</b>")%>
                <%= LoadModules("Home", "List_Home", "your-home-details", "6","Your <br /><b>Home</b>")%>
                <%= LoadModules("Education", "List_Education", "education-details", "1", "Your <br /><b>Education</b>")%>
                <%= LoadModules("Stay", "List_Stay", "stay-details", "3", "Your <br /><b>Stay</b>")%>
                <%= LoadModules("Environment", "List_Environment", "environment-details", "2", "Your <br /><b>Environment</b>")%>
                <!-- column ends here
                ---------------------------------------------------------------------- -->

            </div>
        </div>
        <span class="scroll-right wow pulse" data-wow-iteration="100">
            <a href="javascript:;" class="rightArrow">
                <img src="<%= Session("domainName") %>ui/media/dist/images/mainArrow-right.png" alt="">
            </a>
        </span>
        <!-- scroll-right ends here 
        -------------------------------------------- -->

        <span class="scroll-left wow pulse" data-wow-iteration="100">
            <a href="javascript:;" class="leftArrow">
                <img src="<%= Session("domainName") %>ui/media/dist/images/mainArrow-left.png" alt="">
            </a>
        </span>
        <!-- scroll-left ends here 
        -------------------------------------------- -->
        <footer class="footer-inner">
            <ul class="list-unstyled links">
                <%--<li>
                    <a href="javascript;;">Sitemap</a>
                </li>--%>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/terms-conditions"%>'><%= Language.Read("Terms & Conditions", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/privacy-policy"%>'><%= Language.Read("Privacy Policy", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <uc1:StaticSEO runat="server" id="StaticSEO" SEOID="20470" />
                </li>
            </ul>

            <p>
                <%= Language.Read("Copyright©AL SAADIYAT. All Rights Reserved", Page.RouteData.Values("lang"))%>
                 <%=Date.Now.Year %>. 
                <%= Language.Read("Web design by", Page.RouteData.Values("lang"))%> <a href="http://www.digitalnexa.com" target="_blank" rel="nofollow">Nexa</a>
            </p>
        </footer>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.js"%>'></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<%= Session("domainName") %>ui/js/std/bootstrap.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.jscrollpane.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.panelslider.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/wow.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.fancybox.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.flexslider.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
         <script src="<%= Session("domainName") %>ui/js/dist/jquery.BlackAndWhite.js"></script>
        <script src="<%= Session("domainName") %>ui/js/std/uniform.min.js"></script>
        <script src='<%= Session("domainName") & "ui/js/dist/masonry.pkgd.min.js"%>'></script>
         <script src='<%= Session("domainName") & "ui/js/dist/jquery.jscrollpane.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.mousewheel.js"%>'></script>
        <script src="<%= Session("domainName") %>ui/js/dist/uicreep-custom.js"></script>
          <script>
              (function (i, s, o, g, r, a, m) {
                  i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                      (i[r].q = i[r].q || []).push(arguments)
                  }, i[r].l = 1 * new Date(); a = s.createElement(o),
                  m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
              })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

              ga('create', 'UA-22376844-1', 'auto');
              ga('send', 'pageview');

</script>
<script type="text/javascript">
    setTimeout(function () {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0026/0777.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true; a.type = "text/javascript"; b.parentNode.insertBefore(a, b)
    }, 1);
</script>
    </form>
</body>
</html>
