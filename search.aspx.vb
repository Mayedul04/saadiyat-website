﻿
Partial Class search
    Inherits System.Web.UI.Page
    Public Title As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("43", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        'ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        txtSearch.Text = RouteData.Values("q")
        If txtSearch.Text = "" Then
            ltrsaadiyat.Text = "<h2>No Result Found</h2>"
            Exit Sub
        End If
        ltrsaadiyat.Text = ShowData()
        If ltrsaadiyat.Text.Trim.Length > 6 Then
            ltrsaadiyat.Text = "<ul  class=""searchResultsListing"">" & ltrsaadiyat.Text & "</ul>"
        Else
            ltrsaadiyat.Text = "<h2>No Result Found</h2>"
        End If
        If Page.RouteData.Values("Lang") = "ar" Then
            txtSearch.Attributes.Add("placeholder", "بحث")
            btnSearch.Text = "بحث"
        End If

        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "43"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
    Public Function ShowData() As String
        Dim str As String = ""
        Dim rowCount As Integer = 0
        Dim inStream As IO.StreamReader
        Dim webRequest As Net.WebRequest
        Dim webresponse As Net.WebResponse
        Dim currentPage As String = 1
        Dim searchCriteria As String = ""
        If RouteData.Values("page") <> "" Then
            currentPage = RouteData.Values("page")
        End If
        If currentPage > 1 Then
            currentPage = ((CInt(currentPage) * 20) - 20).ToString()
        End If
        If Page.RouteData.Values("Lang") = "ar" Then
            searchCriteria = "http://www.google.com/search?q=" + txtSearch.Text + "&hl=ar&ie=utf8&oe=utf8&start=" + currentPage + "&num=20&output=xml&client=google-csbe&cx=001915627491718564308:lqfmpb4wkqo"
        Else
            searchCriteria = "http://www.google.com/search?q=" + txtSearch.Text + "&hl=en&start=" + currentPage + "&num=20&output=xml&client=google-csbe&cx=001915627491718564308:4lxqkyvlj1a"
        End If


        webRequest = Net.WebRequest.Create(searchCriteria)
        webresponse = webRequest.GetResponse()
        inStream = New IO.StreamReader(webresponse.GetResponseStream())
        Dim sb As String = inStream.ReadToEnd().ToString()


        Dim m1 As MatchCollection = Regex.Matches(sb, "<M>(.*?)</M>", RegexOptions.Singleline)
        Dim noOfRecord As Integer = 0
        For Each m As Match In m1
            Dim value As String = m.Groups(1).Value
            noOfRecord = value
            'noOfRec.Text = noOfRecord.ToString()
        Next

        If noOfRecord > 0 Then
            'pnlResultFound.Visible = True
            'pnlNotResult.Visible = False
            'pnlAllResult.Visible = True
            Dim time As MatchCollection = Regex.Matches(sb, "<TM>(.*?)</TM>", RegexOptions.Singleline)
            Dim totalTime As Double = 0
            For Each m As Match In time
                Dim value As String = m.Groups(1).Value
                totalTime = CDbl(value)
                'noOfRec.Text = noOfRec.Text + "Total Time: " + totalTime.ToString().Substring(0, 4)
            Next



            Dim m2 As MatchCollection = Regex.Matches(sb, "<U>(.*?)</U>", RegexOptions.Singleline)
            Dim allURL As New List(Of String)()
            For Each m As Match In m2
                Dim value As String = m.Groups(1).Value
                allURL.Add(value)

            Next


            Dim m3 As MatchCollection = Regex.Matches(sb, "<T>(.*?)</T>", RegexOptions.Singleline)
            Dim allTitle As New List(Of String)()
            For Each m As Match In m3
                Dim value As String = m.Groups(1).Value
                allTitle.Add(value)

            Next


            Dim m4 As MatchCollection = Regex.Matches(sb, "<S>(.*?)</S>", RegexOptions.Singleline)
            Dim allSub As New List(Of String)()
            For Each m As Match In m4
                Dim value As String = m.Groups(1).Value
                allSub.Add(value)

            Next

            Dim multiDimensionalArray1 As String(,) = New String(allURL.Count, 3) {}

            For i As Integer = 0 To allURL.Count - 1
                multiDimensionalArray1(i, 0) = allURL(i).ToString()
            Next

            For i As Integer = 0 To allTitle.Count - 1
                multiDimensionalArray1(i, 1) = allTitle(i).ToString()
            Next

            For i As Integer = 0 To allSub.Count - 1
                multiDimensionalArray1(i, 2) = allSub(i).ToString()
            Next

            For k As Integer = 0 To allURL.Count - 1
                Dim link = Server.HtmlDecode(multiDimensionalArray1(k, 0))
                'str += "<p><a target=""_blank"" href=""" & multiDimensionalArray1(k, 0).Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & """>" & multiDimensionalArray1(k, 1).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & "</a> - " & multiDimensionalArray1(k, 2).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&").Replace("<br>", " ") & "</p>"
                str &= "<li><div class=""csrContentSection""><div class=""contentPart"">" & _
                      "  <h2 ><a href=""" & link & """>" & multiDimensionalArray1(k, 1).ToString().Replace("&lt;", "<").Replace("&gt;", ">").Replace("&amp;#39;", "'").Replace("%20", " ").Replace("&amp;", "&") & "</a></h2>" & _
                      "  <p>" & Server.HtmlDecode(multiDimensionalArray1(k, 2).ToString()).Replace("<br>", " ") & "</p>" & _
                      "  <a href=""" & link & """ class=""button"">" & Language.Read("More Information", RouteData.Values("Lang")) & "</a>" & _
                      "</div></div></li>"
            Next

            Dim page As String = If(String.IsNullOrEmpty(RouteData.Values("page")), "1", RouteData.Values("page"))
            Dim iPage As Int32 = 0
            If Not Integer.TryParse(page, iPage) Then
                iPage = 1
            End If
            Dim paging As String = New Pager(iPage, CInt(Math.Floor(noOfRecord / 20) + 1), 9, Session("domainName") & RouteData.Values("Lang") & "/search/" + txtSearch.Text + "/").GetPager
            lbPagingBottom.Text = "&nbsp;" + paging
            'lbPagingTop.Text = "&nbsp;" + paging
        Else
            'pnlResultFound.Visible = False
            'pnlNotResult.Visible = True
            'pnlAllResult.Visible = False
        End If
        Return str
    End Function
    
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Response.Redirect(Session("domainName") & Page.RouteData.Values("Lang") & "/Search/" & txtSearch.Text.Trim, True)
    End Sub
End Class
