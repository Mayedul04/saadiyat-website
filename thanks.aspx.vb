﻿
Partial Class thanks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Page.RouteData.Values("subject") = "newsletter" Then
                lblDetails.Text = "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum architecto autem nisi iste facere harum in libero odit perferendis laboriosam at veniam ratione, nesciunt itaque officiis asperiores, illum! Maiores, numquam.</p>"
            End If
            If Page.RouteData.Values("subject") = "media" Then
                lblDetails.Text = "<p>Thank you for Signup to get Media Images. You will get an email shorly with password to access the media images.</p>"
                ltrLogin.Text = " <a href=""" & Session("domainName").ToString() & Session("lang") & "/media-image-request""  class=""clickHereButton"">Go Back Login <span><img alt="""" src=""/ui/media/dist/icons/readmore-arrow.png""></span></a>"
            End If
        End If
    End Sub
End Class
