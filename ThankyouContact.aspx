﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ThankyouContact.aspx.vb" Inherits="Thankyou" %>

<%@ Register Src="~/CustomControl/UserHTMLTitle1.ascx" TagPrefix="uc1" TagName="UserHTMLTitle1" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/CustomControl/Gallery_05.ascx" TagPrefix="uc1" TagName="Gallery_05" %>
<%@ Register Src="~/CustomControl/Gallery_06.ascx" TagPrefix="uc1" TagName="Gallery_06" %>
<%@ Register Src="~/CustomControl/UserHTMLImage.ascx" TagPrefix="uc1" TagName="UserHTMLImage" %>
<%@ Register Src="~/F-SEO/StaticSEO.ascx" TagPrefix="uc1" TagName="StaticSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
      <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="884329134912584" />
    <meta property="og:type" content="website" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="container mainWrapper aboutTheme">

        <!-- Header Section -->
        <header>


            <div class="row">

                <!-- Logo Section -->
                <div class="col-md-3 pull-left">
                    <a class="" href='<%= domainName%>'>
                        <uc1:UserHTMLImage runat="server" ID="UserHTMLImage1" HTMLID="4" />
                    </a>
                </div>
                <!-- Logo Section -->

                <!-- Call Us -->
                <div class="phonenumber">
                    <img class="icon" src='<%=domainName & "ui/media/dist/header/call.png"%>' alt="">
                    <p>Call For Reservations</p>
                    <span>800 CROWNE (276963)</span>
                    <div class="crowneLogo">
                        <a href="http://www.ihg.com/crowneplaza/hotels/us/en/dubai/dubtc/hoteldetail" target="_blank">
                            <uc1:UserHTMLImage runat="server" ID="UserHTMLImage" HTMLID="3" />
                        </a>
                    </div>
                </div>
                <!-- Call Us -->

            </div>


        </header>
        <!-- Header Section -->

        <!-- Main Content Container -->


        <section class="mainContentContainer">

            <div class="title">

                <!-- Page Title -->
                <div class="pageTitle">

                    <div class="row">
                        <div class="col-md-8">
                            <h1>
                                Thank you
                            </h1>
                        </div>
                        <div class="col-md-4">
                            <!-- Content Social Plugins -->
                            <asp:Literal ID="ltSocialBar" runat="server"></asp:Literal>
                            <!-- Content Social Plugins -->
                        </div>
                    </div>

                </div>
                <!-- Page Title -->


                <!-- Bread Crumbs -->
                <ul class="BreadCrumbs">

                    <li><a href='<%=domainName%>'>Home</a></li>
                    <li>
                        <img src='<%=domainName & "ui/media/dist/icons/breadcrumb-arrow.png"%>'  alt=""></li>
                    <li class="active"><a href="javascript:;">Thank you</a></li>

                </ul>
                <!-- Bread Crumbs -->

            </div>

            <!-- Scroll Section -->
            <div class="scrollSection">
                <div class="nano">

                    <div class="overthrow content description">

                        <!-- Put Content Over Here -->

                        <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt" HTMLID="25" />

                      

                    </div>

                </div>
            </div>
            <!-- Scroll Section -->
            <uc1:StaticSEO runat="server" ID="StaticSEO" SEOID="172" />
        </section>

        <!-- Main Content Container -->

         
    </section>
    <!-- Main Content Wrapper -->
    
</asp:Content>

