﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="Inspiration-details.aspx.vb" Inherits="Inspiration_details" %>

<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated inspiration innerdetail">
        <div class="heading">
            <span><%= Language.Read("Your", Page.RouteData.Values("lang"))%>
                <br />
                <b><%= Language.Read("Inspiration", Page.RouteData.Values("lang"))%></b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getCentreThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName")  %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/your-inspiration" %>'><%= Language.Read("Your Inspiration", Page.RouteData.Values("lang"))%></a>
                </li>
                <li class="active"><%= title %></li>

            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">
        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
        <asp:Panel ID="pnlnormal" runat="server" Visible="false">


            <span class="clearAll"></span>
            <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
            <div class="culturalDisrictSlider flexslider">

                <ul class="slides">

                    <%= LoadCollage("List_Inspiration",hdnID.Value) %>
                </ul>
                <%= Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=List_Inspiration&TID=" & hdnID.Value)%>
            </div>


            <asp:Literal ID="lblSecondDetails" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="pnlContents" Visible="False" runat="server">
            <asp:Literal ID="lblContent" runat="server"></asp:Literal>
        </asp:Panel>
        <asp:Panel ID="pnlHtml" Visible="false" runat="server">
            <h2 class="maintitle">
                <%= Language.Read("<span>About </span>the Progress", Page.RouteData.Values("lang"))%>
                 
                </h2>
            <div class="unitySection csr louvre">
                <%= LoadHtmlContent(32,"") %>
                <div class="mediaLogo">

                    <a href="http://www.thenational.ae/uae/tourism/louvre-abu-dhabi" target="_blank">
                        <img src="/ui/media/dist/inspiration/LAD/natinal-logo.jpg" alt="">
                    </a>

                </div>
                
                <%--<a class="clickHereButton" href="javascript:;" target="_blank">View Website <span>
                    <img src="/ui/media/dist/icons/readmore-arrow.png" alt=""></span>
                </a>--%>
            </div>
            <%= LoadHtmlContent(33, "")%>
        </asp:Panel>
        


        <span class="clearAll"></span>

        <!-- Featured Bullet Listings -->
        <asp:Literal ID="lblDownloadable" runat="server"></asp:Literal>
        <%= getSpecialFeatres() %>
        
         
        
       
        <!-- Featured Bullet Listings -->
        <% If Page.RouteData.Values("id") = 5 Then%>
        <h2 class="subtitle yellow">
            <%= Language.Read("UAE <span>Pavilion </span>", Page.RouteData.Values("lang"))%>
        </h2>


        <%= Contents("List_Inspiration",hdnID.Value) %>


        <% End If%>
        
        <!-- Map & Gallery -->
        <div class="row">

            <div class="col-sm-6">
                <div class="gallerybox">
                    <h2 class="subtitle yellow"><%= Language.Read("Images", Page.RouteData.Values("lang"))%> <%= Language.Read("and", Page.RouteData.Values("lang"))%> <span><%= Language.Read("Video", Page.RouteData.Values("lang"))%></span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">

                            <uc1:UserGalleryControl runat="server" ID="UserGalleryControl" Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />
                        </ul>
                    </div>
                </div>
            </div>
            <%= getfirstFooter() %>
           
        </div>
        <div class="row">
            <%= getSecondFooter() %>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
    <script src='<%= Session("domainName") & "ui/js/dist/masonry.pkgd.min.js" %>'></script>
    <script src='<%= Session("domainName") &"ui/js/dist/jquery.gray.min.js"%>'></script>
</asp:Content>

