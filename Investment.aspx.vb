﻿
Partial Class Investment
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("38", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        ' lblAddFile.Text = Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=HTML&TID=77&t=" & Utility.EncodeTitle(Title, "-"))
        lblMap.Text = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        lblVgal.Text = GetFeaturedVideos()
        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "38"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText , link from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            retstr += "<li><div class=""csrContentSection""> <div class=""contentPart""><div class=""imgHold""><img src=""" & Session("domainName") & "Admin/" & reader("Image").ToString() & """ alt=""""></div><h2>" & reader("Title").ToString() & "</h2>" & reader("DetailText").ToString()
            retstr += " <a class=""clickHereButton"" href=""" & reader("Link").ToString() & """> More Information <span><img src=""/ui/media/dist/icons/readmore-arrow.png"" alt=""""></span> </a>"
            retstr += "</div></div>" & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&Title=1&Image=1&Text=1&link=1") & "</li>"


        End While
        conn.Close()

        Return retstr
    End Function
    Public Function getDownloadableFiles() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID  and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 77

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName").ToString() & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID").ToString() & "&TName=List_Inspiration&TID=" & id) & "</li>"

        End While
        conn.Close()
        Return retstr
    End Function
    
    Public Function GetFeaturedVideos() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = 100

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""357""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
