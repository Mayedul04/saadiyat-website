﻿
Partial Class saadiyat_experiences
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("37", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")

        GetValues()
        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
   
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "37"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
    Public Sub GetValues()
        If Page.RouteData.Values("id") = "1" Then 'Family-Time
            ltrstay.Text = StayList("1,2")
            ltrenvironment.Text = EnvironmentList("1")
            ltrinspiration.Text = InspirationList("5")
            ltrleisure.Text = LeisureList("1,2,3,4")
            ltrEvents.Text = HTMLEventList("42")
        ElseIf Page.RouteData.Values("id") = "2" Then 'Art lovers
            ltrsaadiyat.Text = SaadiyatList("5")
            ltrinspiration.Text = InspirationList("3,5,4,6")
            ltrleisure.Text = LeisureList("4")
        ElseIf Page.RouteData.Values("id") = "3" Then 'Hang around
            ltrleisure.Text = LeisureList("1,2,3,4,5,6")
            ltrEvents.Text = HTMLEventList("42")
        ElseIf Page.RouteData.Values("id") = "4" Then 'Food
            ltrstay.Text = StayList("1,2")
            ltrleisure.Text = LeisureList("1,4")
        ElseIf Page.RouteData.Values("id") = "5" Then 'Activity
            ltrleisure.Text = LeisureList("1,2,3")
            ltrEvents.Text = HTMLEventList("42")
        ElseIf Page.RouteData.Values("id") = "6" Then 'nature
            ltrenvironment.Text = EnvironmentList("1,2")
            ltrleisure.Text = LeisureList("1,3")
        ElseIf Page.RouteData.Values("id") = "7" Then 'education
            ltreducation.Text = EducationList("1,2,3")
        ElseIf Page.RouteData.Values("id") = "8" Then 'Dream Homes
            ltrhome.Text = HomeList("2,3,4,5,9")
        ElseIf Page.RouteData.Values("id") = "9" Then 'Business Zone
            ltrInvest.Text = HTMLList("38", "Investment Opportunities", "blue", Session("lang") & "/investment-opportunities")
            ltrBuy.Text = HTMLList("19", "Buy or lease properties", "blue", Session("lang") & "/buy-lease-properties")
        End If


    End Sub
    Public Function HTMLList(ByVal id As String, ByVal title As String, ByVal colorclass As String, ByVal link1 As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select HTMLID,Title, SmallImage,BigDetails, SmallDetails, ImageAltText, MasterID from [dbo].[HTML] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")
        Dim link As String = ""
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<h2 class=""whereToGoHead " & colorclass & """>" & title & "</h2>"
            M += "<ul class=""searchResultsListing " & colorclass & """>"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If

                link = "/calender-popup.aspx?lang=" & Session("lang")

                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString())
                M += "<a class=""clickHereButton"" href=""" & Session("domainName") & link1 & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function HTMLEventList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select HTMLID,Title, SmallImage,BigDetails, SmallDetails, ImageAltText, MasterID from [dbo].[HTML] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")
        Dim link As String = ""
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<h2 class=""whereToGoHead yellow"">Events</h2>"
            M += "<ul class=""searchResultsListing yellow"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If

                link = "/calender-popup.aspx?lang=" & Session("lang")

                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString())
                M += "<a class=""clickHereButton calenderPopup"" data-fancybox-type=""iframe"" href=""" & Session("domainName") & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function SaadiyatList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Saadiyat] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")
        Dim link As String = ""
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<h2 class=""whereToGoHead darkblue"">Your Saadiyat</h2>"
            M += "<ul class=""searchResultsListing darkblue"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                If reader("MasterID").ToString() = "1" Then
                    link = Session("domainName") & Session("lang") & "/about-saadiyat"
                ElseIf reader("MasterID").ToString() = "2" Then
                    link = Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"
                ElseIf reader("MasterID").ToString() = "3" Then
                    link = Session("domainName") & Session("lang") & "/our-partners"
                ElseIf reader("MasterID").ToString() = "5" Then
                    link = Session("domainName") & Session("lang") & "/about-districts/5/Saadiyat-Cultural-District"
                ElseIf reader("MasterID").ToString() = "9" Then
                    link = Session("domainName") & Session("lang") & "/about-abudhabi"
                Else
                    link = Session("domainName") & Session("lang") & "/about-tdic"
                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Saadiyat/ItemEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function InspirationList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText , MasterID from [dbo].[List_Inspiration] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then

            M += "<h2 class=""whereToGoHead yellow"">Your Inspiration</h2>"
            M += "<ul class=""searchResultsListing yellow"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Inspiration/CulturalCentreEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & Session("domainName") & Session("lang") & "/inspiration-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function LeisureList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Leisure] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead blue"">Your Leisure</h2>"
            M += "<ul class=""searchResultsListing blue"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                If reader("MasterID") = "4" Or reader("MasterID") = "5" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-content/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                ElseIf reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-retail"
                Else
                    link = Session("domainName") & Session("lang") & "/your-leisure-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Leisure/LeisureEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function HomeList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Home] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead darkblue"">Your Home</h2>"
            M += "<ul class=""searchResultsListing darkblue"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                If reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-home-advantage-card"
                ElseIf reader("MasterID") = "7" Then
                    link = Session("domainName") & Session("lang") & "/your-home-finance-mortgage"
                Else
                    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-YourHome/YourHomeEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function EducationList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Education] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead green"">Your Education</h2>"
            M += "<ul class=""searchResultsListing green"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
              
                link = Session("domainName") & Session("lang") & "/education-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")

                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Education/InstituteEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function StayList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Stay] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead yellow"">Your Stay</h2>"
            M += "<ul class=""searchResultsListing yellow"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"

                link = Session("domainName") & Session("lang") & "/stay-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")

                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Stay/ResortEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function EnvironmentList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Environment] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead green"">Your Environment</h2>"
            M += "<ul class=""searchResultsListing green"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"

                link = Session("domainName") & Session("lang") & "/environment-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")

                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Environment/EnvironmentEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
    Public Function BuyPropertyList(ByVal id As String) As String
        Dim M As String = ""
        Dim x As String() = id.Split(",")

        Dim i As String

        For Each i In x

        Next



        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "Select ListID,Title, SmallImage,SmallDetails,ImageAltText, MasterID from [dbo].[List_Leisure] where Lang=@Lang  and MasterID in (" + id + ")"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar)
        cmd.Parameters("Lang").Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Dim link As String = ""
            M += "<h2 class=""whereToGoHead darkblue"">Buy or lease properties</h2>"
            M += "<ul class=""searchResultsListing darkblue"">"
            While reader.Read


                M += "<li>"
                M += "<div class=""csrContentSection"">"
                M += "<div class=""contentPart"">"
                M += "<div class=""imgHold"">"
                If IsDBNull(reader("SmallImage")) = True Then
                    M += "<img src=""" & Session("domainName") & "Admin/Content/noimagefound.jpg"" alt=""""/>"

                Else
                    M += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """/>"

                End If
                M += "</div>"
                M += "<h2>" & reader("Title").ToString() & "</h2>"
                M += "<p>" & reader("SmallDetails").ToString() & "</p>"
                If reader("MasterID") = "4" Or reader("MasterID") = "5" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-content/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                ElseIf reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-retail"
                Else
                    link = Session("domainName") & Session("lang") & "/your-leisure-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                M += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Leisure/LeisureEdit.aspx?lid=" + reader("ListID").ToString())
                M += "<a class=""clickHereButton"" href=""" & link & """>More Information"
                M += "<span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
                M += "</div></div>"
                M += "</li>"




            End While
            M += "</ul>"
            M += "</div>"
        End If
        conn.Close()
        Return M
    End Function
End Class
