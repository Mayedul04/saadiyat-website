﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Intro.aspx.vb" Inherits="Intro" %>

<%@ Register src="CustomControl/WhereToGoControl.ascx" tagname="WhereToGoControl" tagprefix="uc3" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat - Premier Island Destination in Abu Dhabi - Project by TDIC</title>

    <!-- Bootstrap -->
    <link href="/ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <meta name="keywords" content="Saadiyat, Saadiyat abu dhabi, Saadiyat island abu dhabi" />
    <meta name="description" content="Saadiyat is the flagship development project of the Tourism Development and Investment Company (TDIC). It is a multi-faceted destination, which features a wide range of luxury-based experiences, including hospitality, leisure and retail." /><meta name="generator" content="WVSS v 1.1" />
    <meta name="robots" content="index,follow" />
</head>

<body class="intro">
    <form id="form1" runat="server">
    <nav class="menu slide-menu-left">
        <a href="javascript:;" class="close-menu">
            <img src="/ui/media/dist/elements/menu-btn-close.jpg" alt="">
        </a>
        <a href='<%= Session("domainName") & "default"%>' class="nav-logo">
            <img src="/ui/media/dist/elements/color-logo.png" alt="">
        </a>
        <ul>
                <li class="active"><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/buy-lease-properties" %>'>Buy or lease properties</a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/investment-opportunities"%>'>Investment opportunites</a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/interactive-map" %>' class="mappop" data-fancybox-type="iframe">Saadiyat masterplan</a>
                </li>
                <li>
                    <a class="werToGoTrigger" href="javascript:;">Plan your Experience</a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"%>'>CSR</a>
                </li>
               <%-- <li>
                    <a href="/calender-popup.aspx?lang=<%=  Session("lang") %>" class="calenderPopup" data-fancybox-type="iframe">Events</a>
                </li>--%>
               <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/news-subscribtion" %>' class="newsPopup" data-fancybox-type="iframe">Register for News</a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/media-center" %>'>Media Centre</a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/gallery" %>'>Gallery</a>
                </li>
                <li>
                    <a class="socialPopup" href='<%= Session("domainName") & Session("lang") & "/social" %>' data-fancybox-type="iframe">Social</a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/contact-us" %>'>Contact us</a>
                </li>
            </ul>
        <a class="tdic-logo" href="javascript:;">
            <img src="/ui/media/dist/elements/tdic-logo.png" alt="">
        </a>
    </nav>
    <!-- slide-menu-left ends here
    --------------------------------------------- -->
    <uc3:WhereToGoControl ID="WhereToGoControl1" runat="server" />
    <div id="wrapper" class="wrapper-video">

        <!-- For Mobile BG -->
        <div class="mobileBg">
            <img src="ui/media/dist/images/mobile-bg.jpg" alt="" class="bigimage">
            <img src="ui/media/dist/images/tab-portrait-img.jpg" alt="" class="tabPortimage">
            <img src="ui/media/dist/images/tab-landscape-img.jpg" alt="" class="tabLandbigimage">

            <a href="javascript:;" class="playbtnmobile"><img src="ui/media/dist/inner-imgs/play-button-white.png" alt=""></a>
        </div>
        <!-- For Mobile BG -->


       <%-- <!-- For Mobile BG -->
        <div class="mobileBg">
            <img src="/ui/media/dist/images/mobile-bg.jpg" height="954" width="600" alt="" class="bigimage">
            <a href="javascript:;" class="playbtnmobile"><img src="/ui/media/dist/inner-imgs/play-button-white.png" alt=""></a>
        </div>
        <!-- For Mobile BG -->--%>
        <!-- Video Mobile Holder -->
        <div class="videoMobileHolder">
            <span class="closeButton"></span>
            <video autoplay  muted="muted" poster="ui/media/dist/new1.jpg">
                <source src="/ui/media/dist/Jawaher Saadiyat 15s - website_360p.mp4" type="video/mp4">
               <%-- <source src="ui/media/dist/video1.mp4" type="video/webm">--%>
            </video>
        </div>
        <!-- Video Mobile Holder -->
        <div class="video-panel">
            
            <video id="myVideo" autoplay="autoplay"  poster="ui/media/dist/bg/video-opening.jpg">
                <source src="/ui/media/dist/Jawaher Saadiyat 15s - website_360p.mp4" type="video/mp4">
                <%--<source src="/ui/media/dist/Saadiyat_TVC_30s_ENG.mp4" type="video/webm">--%>
            </video>
        </div>
         <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js">
    </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#myVideo").bind('ended', function () {
                    location.href = "http://www.saadiyat.ae/default";
                });
            });
    </script>
        <a href="javascript:;" class="main-menu-trigger nav-toggler toggle-slide-left">
            <img src="/ui/media/dist/elements/menu-btn.jpg" alt="">
        </a>
        <!-- main-menu-trigger ends here
        -------------------------------------------- -->

        <a href='<%= Session("domainName") & "default"%>' class="intro-logo">
            <img src="/ui/media/dist/video/white-logo.png" alt="">
        </a>
        <!-- intro-logo ends here
        -------------------------------------------- -->

        <div class="video-ovarlay">

            <a href='<%= Session("domainName") & Session("lang") &  "/your-home-details/9/Jawaher-Saadiyat" %>' class="pull-left introButton" >Click to know more</a>
            <a href="<%= Session("domainName") & "default" %>" class="pull-right skip-btn introButton">Skip</a>
        </div>
        <!-- video-ovarlay ends here
        -------------------------------------------- -->

        <footer class="video-bar"></footer>
        <!-- video-bar ends here
        -------------------------------------------- -->
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src='<%= Session("domainName") & "ui/js/dist/jquery.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/classie.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/nav.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/std/bootstrap.min.js"%>'></script>
        
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.jscrollpane.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.panelslider.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/wow.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.flexslider.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.fancybox.js" %>'></script>
        
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.gray.min.js" %>'></script>
        <script src='<%= Session("domainName") & "ui/js/std/uniform.min.js" %>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/masonry.pkgd.min.js"%>'></script>
        <script src='<%= Session("domainName") &"ui/js/dist/jquery.horizontal.scroll.js"%>'></script>  
         <script src='<%= Session("domainName") & "ui/js/dist/jquery.jscrollpane.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.mousewheel.js"%>'></script>
    <script src='<%= Session("domainName") & "ui/js/dist/uicreep-custom.js"%>'></script> 

         <script>
             (function (i, s, o, g, r, a, m) {
                 i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                     (i[r].q = i[r].q || []).push(arguments)
                 }, i[r].l = 1 * new Date(); a = s.createElement(o),
                 m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
             })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

             ga('create', 'UA-22376844-1', 'auto');
             ga('send', 'pageview');

</script>

<script type="text/javascript">
    setTimeout(function () {
        var a = document.createElement("script");
        var b = document.getElementsByTagName("script")[0];
        a.src = document.location.protocol + "//script.crazyegg.com/pages/scripts/0026/0777.js?" + Math.floor(new Date().getTime() / 3600000);
        a.async = true; a.type = "text/javascript"; b.parentNode.insertBefore(a, b)
    }, 1);
</script>
    </form>
</body>

</html>

