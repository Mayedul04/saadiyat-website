﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="about-districts.aspx.vb" Inherits="about_districts" %>

<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated saadiyat innerdetail">
        <div class="heading">
            <span>Your
                <br />
                <b>Saadiyat</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName")  %>'>Home</a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/your-saadiyat" %>'>Your Saadiyat</a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/about-saadiyat" %>'>About Saadiyat</a>
                </li>
                <li class="active"><%= title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>


    <div class="main-content-area">

        <div class="unitySection csr">
        <h2 class="maintitle">
            <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
        </h2>


        <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
        </div>
        <asp:Literal ID="lblink" runat="server"></asp:Literal>

       
            <%= getSpecialFeatres() %>
       
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
        <div class="row">

            <div class="col-sm-6 bottomBoxesMinHeight">
                <div class="gallerybox">
                    <h2 class="subtitle yellow">Images and <span>Video</span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc1:UserGalleryControl runat="server" ID="UserGalleryControl" Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Testimonial Section -->
            <asp:Panel ID="pnlVideo" runat="server">
                <div class="col-sm-6 bottomBoxesMinHeight">
                    <h2 class="subtitle">Video <span>Section</span>
                    </h2>
                    <!-- Video Slider Section -->
                    <div class="detailVideoSlider video-flexsliders">
                        <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid) %>
                        <ul class="slides">
                            <asp:Literal ID="lblVideos" runat="server"></asp:Literal>


                        </ul>

                    </div>
                    <!-- Video Slider Section -->
                </div>
                <!-- Testimonial Section -->
            </asp:Panel>

            <!-- EnQuire Now Box -->
            <asp:Panel ID="pnlQC" runat="server">
                <div class="col-sm-6 bottomBoxesMinHeight">
                    <h2 class="subtitle">Quick <span>Contact</span>
                    </h2>

                    <!-- Contact Detail Section -->
                    <div class="contactDetailsSection">

                        <ul class="contactListings">

                            <%= getAddresses() %>
                        </ul>



                    </div>
                    <!-- Contact Detail Section -->

                </div>
            </asp:Panel>
            <!-- EnQuire Now Box -->

            <!-- Testimonial Section -->
            <asp:Literal ID="lblmap" runat="server"></asp:Literal>
            <!-- Testimonial Section -->
        </div>
        <!-- contact Info & Video Section -->
        <%--<div class="row marginBtm20">

            

        </div>--%>
        <!-- contact Info & Video Section -->
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

