﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="your-home-advantage-card.aspx.vb" Inherits="your_home_advantage_card" %>

<%@ Register src="~/F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<%@ Register src="~/CustomControl/UserGalleryControl.ascx" tagname="UserGalleryControl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">


<div class=" fadeInLeft animated home innerdetail">
            <div class="heading">
                <span>Your <br /><b>Home</b></span>
            </div>
            <!-- -- heading ends here -- -->
            <a href="javascript:;" class="scrollers"><img src="/ui/media/dist/elements/down.png" alt="">
            </a>
            <div class="panel-content nano">
                <div class="scroller nano-content">
                    <ul class="list-unstyled">
                        <asp:Literal ID="ltrList" runat="server"></asp:Literal>  
                </div>
            </div>
            <!-- -- panel-content ends here -->

        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">


            <!-- Main Banner Section -->
            <div class="section-slider flexslider">
                <ul class="list-unstyled slides">
                    <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/your-home" %>'>Your Home</a>
                        </li>
                        <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->
                
            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <h2 class="maintitle">
                    <asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal>
                </h2>

                <div class="unitySection">

                    <div class="imgHold">
                        <a  id="lnkImage" runat="server" href="" rel="1" class="photopopup">
                            <asp:Image ID="imgBigImage" runat="server" />
                        </a>
                    </div>
                    
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                </div>

                
                <!-- Download List Container -->
                <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>
                <!-- Download List Container -->


                <h2 class="subtitle yellow">
                    Where You Can Take <span>Advantage</span>
                </h2>


                <!-- Logo Slider Section -->
                <div class="logoListSliderSection flexslider">

                    
                        <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>
                    
                </div>
                <!-- Logo Slider Section -->

                <!-- Map & Video -->
                <div class="row">
                     <!-- EnQuire Now Box -->
            <div class="col-sm-6 bottomBoxesMinHeight">
                <h2 class="subtitle">Quick <span>Contact</span>
                </h2>

                <!-- Contact Detail Section -->
                <div class="contactDetailsSection">
                    
                    <ul class="contactListings">

                        <asp:Literal ID="lblQC" runat="server"></asp:Literal>
                    </ul>



                </div>
                <!-- Contact Detail Section -->

            </div>
            <!-- EnQuire Now Box -->
                    <!-- Video Slider Section -->
                        <asp:Literal ID="ltrVideo" runat="server"></asp:Literal>
                        <!-- Video Slider Section -->

                    <%--<div class="col-sm-6">
                        <h2 class="subtitle yellow">
                            Interactive  <span>Map</span>
                        </h2>
                        <div class="imap">
                            <a href='<%= Session("domainName") & Session("lang") & "/interactive-map" %>' class="mappop" data-fancybox-type="iframe">
                                <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                            </a>
                        </div>

                    </div>--%>


                </div>
                <!-- Map & Video -->

                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />

            </div>
            <!-- Main Content Section -->


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

