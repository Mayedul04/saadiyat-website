﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="social.aspx.vb" Inherits="social" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>



<!DOCTYPE html>
<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="/ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function MM_openBrWindow(theURL, winName, features) { //v2.0
            window.open(theURL, winName, features);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="fb-root"></div>
        <script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

        <!-- Main Content Section -->
        <div class="socialContanerSection">

            <ul class="socialWidgetsListings">

                <li class="darkblue">

                    <div class="contentContainer">

                        <h2 class="subtitle">Facebook
                            </h2>

                        <div class="facebookWidget">
                            <div class="fb-like-box" data-href="https://www.facebook.com/saadiyatae" width="100%" height="270" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                        </div>

                    </div>

                </li>

                <li class="yellow">

                    <div class="contentContainer">

                        <h2 class="subtitle">Twitter
                            </h2>

                        <div class="twitterWidget">

                            <a class="twitter-timeline" width="100%" height="270" href="https://twitter.com/saadiyatae" data-widget-id="570867704165171200">Tweets by @saadiyatae</a>
                            <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>

                        </div>

                    </div>

                </li>

                <li class="blue">

                    <div class="contentContainer">

                        <h2 class="subtitle">Instagram 
                       
                      <a href="https://instagram.com/saadiyatae?ref=badge" target="_blank" class="ig-b- ig-b-16">
                          <img src="//badges.instagram.com/static/images/ig-badge-16.png" alt="Instagram" /></a>
                        </h2>


                        <div class="instagramSliderSection">
                            <style>
                                .ig-b- {
                                    display: inline-block;
                                }

                                    .ig-b- img {
                                        visibility: hidden;
                                    }

                                    .ig-b-:hover {
                                        background-position: 0 -60px;
                                    }

                                    .ig-b-:active {
                                        background-position: 0 -120px;
                                    }

                                .ig-b-16 {
                                    width: 16px;
                                    height: 16px;
                                    background: url(//badges.instagram.com/static/images/ig-badge-sprite-16.png) no-repeat 0 0;
                                }

                                @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                                    .ig-b-16 {
                                        background-image: url(//badges.instagram.com/static/images/ig-badge-sprite-16@2x.png);
                                        background-size: 60px 178px;
                                    }
                                }
                            </style>

                            <div class="flexslider">

                                <ul class="slides">

                                    <%= GetInstagram() %>
                                </ul>

                            </div>

                        </div>
                        <!-- Instagram Slider -->

                    </div>

                </li>

                <li class="green">

                    <div class="contentContainer">

                        <h2 class="subtitle">Youtube
                            </h2>

                        <!-- Instagram Slider -->
                        <div class="instagramSliderSection">

                            <div class="flexslider">

                                <ul class="slides">

                                    <%= GetYoutubeVideos() %>
                                </ul>

                            </div>

                        </div>
                        <!-- Instagram Slider -->

                    </div>

                </li>

            </ul>


        </div>
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
        <!-- Main Content Section -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<%= Session("domainName") %>ui/js/std/bootstrap.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.jscrollpane.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.panelslider.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/wow.min.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.flexslider.js"></script>
        <script src="<%= Session("domainName") %>ui/js/dist/jquery.fancybox.js"></script>

        <script src='<%= Session("domainName") & "ui/js/dist/jquery-ui-1.10.4.custom.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/masonry.pkgd.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.gray.min.js"%>'></script>
        <script src='<%= Session("domainName") & "ui/js/std/uniform.min.js" %>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/jquery.horizontal.scroll.js" %>'></script>
        <script src='<%= Session("domainName") & "ui/js/dist/uicreep-custom.js"%>'></script>
    </form>
</body>

</html>
