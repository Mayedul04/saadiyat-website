﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="your-stay.aspx.vb" Inherits="your_stay" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
    <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                   <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        Your<span>Stay</span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") & Session("lang") %>'>Home</a></li>
                        <li class="active">Your Stay</li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                   <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </div>

                <!-- Inspiration Landing List -->
                <ul class="parallaxlisting list-unstyled">

                  <%= getResortList() %>

                </ul>
                <!-- Inspiration Landing List -->
                <uc1:DynamicSEO runat="server" ID="DynamicSEO" />

            </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
    <script src='<%= Session("domainName") &  "ui/js/dist/jquery.BlackAndWhite.js"%>'></script>
    <script src='<%= Session("domainName") &  "ui/js/std/uniform.min.js"%>'></script>
</asp:Content>

