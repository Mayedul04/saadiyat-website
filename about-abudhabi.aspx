﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="about-abudhabi.aspx.vb" Inherits="about_abudhabi" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated saadiyat innerdetail">
        <div class="heading">
            <span>Your
                <br />
                <b>Saadiyat</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">

            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName")  %>'>Home</a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/your-saadiyat" %>'>Your Saadiyat</a>
                </li>
                <li class="active"><%= title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <div class="main-content-area ">

         <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
        <span class="clearAll"></span>

        <div class="">


             <asp:Label ID="lblDetails" runat="server" Text=""></asp:Label>
       



            <!-- Misinary TCab Boxes -->
            <ul class="misinaryBoxesListings tabBoxes">

                <%= LoadCollage("List_Saadiyat",hdnID.Value)%>
            </ul>
            <!-- Misinary TCab Boxes -->
              <%= Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=List_Saadiyat&TID=" & hdnID.Value) %>

            <!-- Quotes Section Container -->
            <div class="quotesSection">

                <%= loadQuote(7)%>
            </div>
            <!-- Quotes Section Container -->
            <asp:Literal ID="lblSecondDetails" runat="server"></asp:Literal>

           


            <div class="unitySection">

                <%= LoadHtmlContent(30)%>

            </div>




            <!-- <h2 class="subtitle yellow">Quotes about History:</h2> -->



          <%= LoadHtmlContent(31) %>

            <span class="clearAll"></span>


            <!-- Text animating Banner -->
            <ul class="textAnimatingBanner wow animated fadeInRight">

                <li class="first">Infrastructure devolopment</li>
                <li class="second">Revitalisation</li>
                <li class="third">Economic devolpment</li>
                <li class="forth">human achivement</li>
                <li class="fifth">Cultural Heritage</li>
                <li class="sixth">Connectivity</li>
                <li class="seventh">Community enhancement</li>
                <li class="eighth">Environmental sustainability</li>

            </ul>
            <!-- Text animating Banner -->


            <!-- Quotes Section Container -->
            <div class="quotesSection">
                  <%= loadQuote(8)%>

            </div>
            <!-- Quotes Section Container -->


            <a target="_blank" href="<%= link1 %>" " class="clickHereButton">View Website <span>
                <img alt="" src="/ui/media/dist/icons/readmore-arrow.png"/></span>
            </a>

            <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
            <div class="abudhabiMapSection">
                <img src="/ui/media/dist/saadiyat/abudhabi/abudhabimap.jpg" alt=""/>
            </div>


        </div>



    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

