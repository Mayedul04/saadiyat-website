﻿Imports System.Data.SqlClient

Partial Class calender_popup
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            For i = 0 To 11
                ddlEventMonth.Items.Add(New ListItem(Date.Today.AddMonths(i).ToString("MMMM, yyy"), Date.Today.AddMonths(i).ToString("MM-1-yyy")))
                ddlPromotionMonth.Items.Add(New ListItem(Date.Today.AddMonths(i).ToString("MMMM, yyy"), Date.Today.AddMonths(i).ToString("MM-1-yyy")))
            Next
            ltrCurrentMonth.Text = Date.Today.ToString("MMMM")
            ltrCurrentMonthPromotion.Text = Date.Today.ToString("MMMM")
            For i = 0 To 0
                ddlEventYear.Items.Add(New ListItem(Date.Today.AddYears(i).ToString("yyy"), Date.Today.AddYears(i).ToString("1-1-yyy")))
                ddlPromotionYear.Items.Add(New ListItem(Date.Today.AddYears(i).ToString("yyy"), Date.Today.AddYears(i).ToString("1-1-yyy")))
            Next
            ddlEventYear.SelectedIndex = 0
            ddlPromotionYear.SelectedIndex = 0
            ltrCurrentyear.Text = Date.Now.Year
            ltrCurrentyearPromotion.Text = Date.Now.Year
        End If
    End Sub


    Protected Sub ddlEventMonth_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEventMonth.SelectedIndexChanged
        ltrCurrentMonth.Text = Date.Parse(ddlEventMonth.SelectedValue).ToString("MMMM")
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        ltrCurrentyear.Text = Date.Parse(ddlEventYear.SelectedValue).ToString("yyyy")
    End Sub

    ''' <summary>
    ''' Monthly Events
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getAllEvents() As String
        Dim retVal As String = ""
       
        Dim today As Date = Date.Parse(ddlEventMonth.SelectedValue)
        Dim totalDays As Integer = Date.DaysInMonth(today.Year, today.Month)
        Dim monthStartDate As Date = today.AddDays(1 - today.Day)
        Dim calendarStartDate As Date = monthStartDate.AddDays(-monthStartDate.DayOfWeek)
        Dim monthEndDate As Date = monthStartDate.AddDays(totalDays - 1)
        Dim calendarEndDate As Date = monthEndDate.AddDays(6 - monthEndDate.DayOfWeek)

        Dim totalDayInCalenderPage As Integer = totalDays + (monthStartDate.DayOfWeek) + (6 - monthEndDate.DayOfWeek)




        Dim tempDate As Date
        For i = 0 To totalDayInCalenderPage - 1
            tempDate = calendarStartDate.AddDays(i)
            Dim diffStart = DateDiff(DateInterval.Day, monthStartDate, tempDate)
            Dim diffEnd = DateDiff(DateInterval.Day, tempDate, monthEndDate)

            If diffStart >= 0 And diffEnd >= 0 Then
                eventCount = 0
                Dim dateEvents = getDistinctEvent(tempDate)
                retVal = retVal & "<li><div class=""dateHolder""><h2>" & tempDate.Day & "</h2>" & If(eventCount > 0, "<h3 class='eventCount'>" & eventCount & " Events</h3>", "") & "</div>" & _
                    dateEvents & _
                "</li>"

            Else
                retVal = retVal & "<li class='disactive'><div class=""dateholder"">" & tempDate.Day & "</div></li>"
            End If
        Next



        Return retVal

    End Function

    Dim eventCount As Int16 = 0
    Dim tempEventTitle As String = ""
    Private Function getDistinctEvent(aDate As Date) As String
        Dim retVal As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = " SELECT        List_Event.EventID, List_Event.CategoryID, List_Event.Title,List_Event.HostName,List_Event.Vanue, List_Event.SmallDetails, List_Event.SmallImage, List_Event.ImageAltText, List_Event.MasterID,  List_Event.Link, " & _
                            " List_Event.Lang, List_Event_Details.StartDate, List_Event_Details.EndDate, List_Event_Details.Time " & _
                            " FROM            List_Event INNER JOIN " & _
                            "                 List_Event_Details ON List_Event.MasterID = List_Event_Details.EventMasterID " & _
                            " WHERE        (List_Event.Lang = @Lang) and List_Event.Status=1 and  StartDate<=@aDate and EndDate>= @aDate " & _
                            " Order by List_Event.SortIndex"
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.Parameters.Add("aDate", Data.SqlDbType.DateTime).Value = aDate
        sqlcomm.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Request.QueryString("lang")

        eventCount = 0
        Dim listString As String = ""
        Dim dataRowString As String = ""
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()



        While reader.Read()
            eventCount += 1
            
            tempEventTitle = reader("Title").ToString()
            dataRowString &= "<li>" & _
                            "    <div class='row'>" & _
                            "        <div class='col-md-4'>" & _
                            "            <div class='imgHold'>" & _
                            "                <img src='/ui/media/dist/saadiyat/csr/img-1.jpg' alt=''>" & _
                            "            </div>" & _
                            "        </div>" & _
                            "        <div class='col-md-8'>" & _
                            "            <h2>" & reader("Title") & Utility.showEditButton(Request, "/admin/A-Event/EventsEdit.aspx?eventId=" & reader("eventId")) & "</h2>" & _
                            "            <ul class='timings'>"
            If IsDBNull(reader("HostName")) = False Then
                dataRowString += "<li><span>Host</span> " & reader("HostName") & "</li>"
            End If

            If IsDBNull(reader("Vanue")) = False Then
                dataRowString += "<li><span>Venue:</span> " & reader("Vanue") & "</li>"
            End If

            dataRowString += "            </ul>" & _
                            "            <p>" & Mid(reader("SmallDetails").ToString(), 1, 140) & "...</p>"
            If IsDBNull(reader("Link")) = False Then
                dataRowString += "<a target='_parent'  href='" & reader("Link") & "' class='clickHereButton'>" & _
                            "                More Information <span><img alt='' src='/ui/media/dist/icons/readmore-arrow.png'></span>" & _
                            "            </a>"
            End If
          
            dataRowString &= "           <a href='javascript:;' class='clickHereButton downLoadListTrigger'>Downloadable Content <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>                                                                        " & _
                            "            <!-- Download List Container -->" & _
                            getDownloadableFiles("List_Event", reader("EventID").ToString()) & _
                            "            <!-- Download List Container -->" & _
                            "        </div>" & _
                            "    </div>" & _
                            "</li>"

        End While
        sqlConn.Close()
        If eventCount > 0 Then
            retVal = "<!-- Calender Events Pop up -->" & _
                     "   <div class=""calenderEventPopup"">" & _
                     "       <div class=""eventContainer"">" & _
                     "           <a class=""eventPopupCloseButton"" href=""javascript:;""> X </a>" & _
                     "           <div class=""contentSection"">" & _
                     "               <!-- Event Listings -->" & _
                     "               <ul class=""eventListings"">" & dataRowString & _
                     "               </ul>" & _
                     "               <!-- Event Listings -->" & _
                     "           </div>" & _
                     "       </div>" & _
                     "   </div>" & _
                     "<!-- Calender Event Pop up -->"

        End If


        Return retVal
    End Function

    Public Function getDownloadableFiles(TableName As String, TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            
            retstr &= "                        <li><span class='icon'><img src='/ui/media/dist/inner-imgs/pdf-icon.png' alt=''></span> " & reader("FileName") & Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID") & "&TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(tempEventTitle)) & "</li>"

        End While
        conn.Close()
        If retstr <> "" Then
            retstr = "            <div class='downloadListContainer'>" & _
                    "                <div class='contentSection'>" & _
                    "                    <ul class='listings'>" & _
                    retstr & _
                    "                    </ul>" & _
                    "                </div>" & _
                    "            </div>"
        End If

        'Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(tempEventTitle))

        Return retstr
    End Function

    ''' <summary>
    ''' Events in one year
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getAllYearlyEvents() As String
        
        Dim retVal As String = ""

        Dim startDate As Date = Date.Parse(ddlEventMonth.SelectedValue) 'this date is 1st of January
        Dim endDate As Date

        For i = 0 To 11

            endDate = startDate.AddDays(Date.DaysInMonth(startDate.Year, startDate.Month) - 1)
            eventCount = 0
            Dim monthlyEvents = getDistinctYearlyEvent(startDate, endDate)
            
            retVal = retVal & "<li><div class=""dateHolder"">" & If(eventCount > 0, "<h3 class='eventCount'>" & eventCount & " Events</h3>", "") & "<h2>" & startDate.ToString("MMMM") & "</h2></div>" & _
                    monthlyEvents & _
                "</li>"
            startDate = startDate.AddMonths(1)
        Next

        Return retVal

    End Function


    Private Function getDistinctYearlyEvent(sDate As Date, eDate As Date) As String
        Dim retVal As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = " SELECT        List_Event.EventID, List_Event.CategoryID, List_Event.Title,List_Event.HostName,List_Event.Vanue, List_Event.SmallDetails, List_Event.SmallImage, List_Event.ImageAltText, List_Event.MasterID,  List_Event.Link,  " & _
                            " List_Event.Lang, List_Event_Details.StartDate, List_Event_Details.EndDate, List_Event_Details.Time " & _
                            " FROM            List_Event INNER JOIN " & _
                            "                 List_Event_Details ON List_Event.MasterID = List_Event_Details.EventMasterID " & _
                            " WHERE        (List_Event.Lang = @Lang) and List_Event.Status=1 and  (StartDate>=@sDate and StartDate<= @eDate) or (EndDate>=@sDate and EndDate<=@eDate) " & _
                            " Order by List_Event.SortIndex"
        'sqlcomm.CommandText = " select *  FROM Events where Events.Status=1 and  (EventStartDate>=@sDate and EventStartDate<= @eDate) or (EventEndDate>=@sDate and EventEndDate<=@eDate) "
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.Parameters.Add("sDate", Data.SqlDbType.DateTime).Value = sDate
        sqlcomm.Parameters.Add("eDate", Data.SqlDbType.DateTime).Value = eDate
        sqlcomm.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Request.QueryString("lang")

        eventCount = 0
        Dim listString As String = ""
        Dim dataRowString As String = ""
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        While reader.Read()
            eventCount += 1

            tempEventTitle = reader("Title")
            dataRowString &= "<li>" & _
                            "    <div class='row'>" & _
                            "        <div class='col-md-4'>" & _
                            "            <div class='imgHold'>" & _
                            "                <img src='/ui/media/dist/saadiyat/csr/img-1.jpg' alt=''>" & _
                            "            </div>" & _
                            "        </div>" & _
                            "        <div class='col-md-8'>" & _
                            "            <h2>" & reader("Title") & Utility.showEditButton(Request, "/admin/A-Event/EventsEdit.aspx?eventId=" & reader("eventId")) & "</h2>" & _
                            "            <ul class='timings'>"
            If IsDBNull(reader("HostName")) = False Then
            dataRowString += "<li><span>Host</span> " & reader("HostName") & "</li>"
            End If

            If IsDBNull(reader("Vanue")) = False Then
                dataRowString += "<li><span>Venue:</span> " & reader("Vanue") & "</li>"
            End If

            dataRowString += "            </ul>" & _
                            "            <p>" & Mid(reader("SmallDetails").ToString(), 1, 140) & "...</p>"
            If IsDBNull(reader("Link")) = False Then
                dataRowString += "<a target='_parent'  href='" & reader("Link") & "' class='clickHereButton'>" & _
                            "                More Information <span><img alt='' src='/ui/media/dist/icons/readmore-arrow.png'></span>" & _
                            "            </a>"
            End If
            
            dataRowString &= "            <a href='javascript:;' class='clickHereButton downLoadListTrigger'>Downloadable Content <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>                                                                        " & _
                            "            <!-- Download List Container -->" & _
                            getDownloadableFiles("List_Event", reader("EventID").ToString()) & _
                            "            <!-- Download List Container -->" & _
                            "        </div>" & _
                            "    </div>" & _
                            "</li>"

        End While
        sqlConn.Close()
        If eventCount > 0 Then
            retVal = "<!-- Calender Events Pop up -->" & _
                     "   <div class=""calenderEventPopup"">" & _
                     "       <div class=""eventContainer"">" & _
                     "           <a class=""eventPopupCloseButton"" href=""javascript:;""> X </a>" & _
                     "           <div class=""contentSection"">" & _
                     "               <!-- Event Listings -->" & _
                     "               <ul class=""eventListings"">" & dataRowString & _
                     "               </ul>" & _
                     "               <!-- Event Listings -->" & _
                     "           </div>" & _
                     "       </div>" & _
                     "   </div>" & _
                     "<!-- Calender Event Pop up -->"

        End If


        Return retVal
    End Function


    ''' <summary>
    ''' Monthly Promotions
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getAllPromotions() As String
        Dim retVal As String = ""

        Dim today As Date = Date.Parse(ddlEventMonth.SelectedValue)
        Dim totalDays As Integer = Date.DaysInMonth(today.Year, today.Month)
        Dim monthStartDate As Date = today.AddDays(1 - today.Day)
        Dim calendarStartDate As Date = monthStartDate.AddDays(-monthStartDate.DayOfWeek)
        Dim monthEndDate As Date = monthStartDate.AddDays(totalDays - 1)
        Dim calendarEndDate As Date = monthEndDate.AddDays(6 - monthEndDate.DayOfWeek)

        Dim totalDayInCalenderPage As Integer = totalDays + (monthStartDate.DayOfWeek) + (6 - monthEndDate.DayOfWeek)




        Dim tempDate As Date
        For i = 0 To totalDayInCalenderPage - 1
            tempDate = calendarStartDate.AddDays(i)
            Dim diffStart = DateDiff(DateInterval.Day, monthStartDate, tempDate)
            Dim diffEnd = DateDiff(DateInterval.Day, tempDate, monthEndDate)

            If diffStart >= 0 And diffEnd >= 0 Then

                Dim dateEvents = getDistinctPromotion(tempDate)
                retVal = retVal & "<li><div class=""dateHolder""><h2>" & tempDate.Day & "</h2>" & If(promotionCount > 0, "<h3 class='eventCount'>" & promotionCount & " Promotions</h3>", "") & "</div>" & _
                    dateEvents & _
                "</li>"

            Else
                retVal = retVal & "<li class='disactive'><div class=""dateholder"">" & tempDate.Day & "</div></li>"
            End If
        Next



        Return retVal

    End Function

    Dim promotionCount As Int16 = 0
    Dim tempPromotionTitle As String = ""
    Private Function getDistinctPromotion(aDate As Date) As String
        Dim retVal As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = " SELECT        List_Promotion.PromotionID, List_Promotion.CategoryID, List_Promotion.Title,List_Promotion.HostName,List_Promotion.Vanue, List_Promotion.SmallDetails, List_Promotion.SmallImage, List_Promotion.ImageAltText, List_Promotion.MasterID, List_Promotion.Link,  " & _
                            " List_Promotion.Lang, List_Promotion_Details.StartDate, List_Promotion_Details.EndDate, List_Promotion_Details.Time " & _
                            " FROM            List_Promotion INNER JOIN " & _
                            "                 List_Promotion_Details ON List_Promotion.MasterID = List_Promotion_Details.PromotionMasterID " & _
                            " WHERE        (List_Promotion.Lang = @Lang) and List_Promotion.Status=1 and  StartDate<=@aDate and EndDate>= @aDate " & _
                            " Order by List_Promotion.SortIndex"
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.Parameters.Add("aDate", Data.SqlDbType.DateTime).Value = aDate
        sqlcomm.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Request.QueryString("lang")

        promotionCount = 0
        Dim listString As String = ""
        Dim dataRowString As String = ""
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()



        While reader.Read()
            promotionCount += 1

            tempPromotionTitle = reader("Title").ToString()
            tempEventTitle = reader("Title").ToString()
            dataRowString &= "<li>" & _
                            "    <div class='row'>" & _
                            "        <div class='col-md-4'>" & _
                            "            <div class='imgHold'>" & _
                            "                <img src='/ui/media/dist/saadiyat/csr/img-1.jpg' alt=''>" & _
                            "            </div>" & _
                            "        </div>" & _
                            "        <div class='col-md-8'>" & _
                            "            <h2>" & reader("Title") & Utility.showEditButton(Request, "/admin/A-Promotion/PromotionEdit.aspx?PromotionID=" & reader("PromotionID")) & "</h2>" & _
                            "            <ul class='timings'>"
            If IsDBNull(reader("HostName")) = False Then
            dataRowString += "<li><span>Host</span> " & reader("HostName") & "</li>"
            End If

            If IsDBNull(reader("Vanue")) = False Then
                dataRowString += "<li><span>Venue:</span> " & reader("Vanue") & "</li>"
            End If

            dataRowString += "            </ul>" & _
                            "            <p>" & Mid(reader("SmallDetails").ToString(), 1, 140) & "...</p>"
            If IsDBNull(reader("Link")) = False Then
            dataRowString += "<a target='_parent'  href='" & reader("Link") & "' class='clickHereButton'>" & _
                        "                More Information <span><img alt='' src='/ui/media/dist/icons/readmore-arrow.png'></span>" & _
                        "            </a>"
            End If
            dataRowString += "            <a href='javascript:;' class='clickHereButton downLoadListTrigger'>Downloadable Content <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>                                                                        " & _
                            "            <!-- Download List Container -->" & _
                            getDownloadableFiles("List_Promotion", reader("PromotionID").ToString()) & _
                            "            <!-- Download List Container -->" & _
                            "        </div>" & _
                            "    </div>" & _
                            "</li>"

        End While
        sqlConn.Close()
        If promotionCount > 0 Then
            retVal = "<!-- Calender Promotion Pop up -->" & _
                     "   <div class=""calenderEventPopup"">" & _
                     "       <div class=""eventContainer"">" & _
                     "           <a class=""eventPopupCloseButton"" href=""javascript:;""> X </a>" & _
                     "           <div class=""contentSection"">" & _
                     "               <!-- Event Listings -->" & _
                     "               <ul class=""eventListings"">" & dataRowString & _
                     "               </ul>" & _
                     "               <!-- Event Listings -->" & _
                     "           </div>" & _
                     "       </div>" & _
                     "   </div>" & _
                     "<!-- Calender Promotion Pop up -->"

        End If


        Return retVal
    End Function

    ''' <summary>
    ''' Promotions in one year
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getAllYearlyPromotions() As String

        Dim retVal As String = ""

        Dim startDate As Date = Date.Parse(ddlPromotionMonth.SelectedValue) 'this date is 1st of January
        Dim endDate As Date

        For i = 0 To 11

            endDate = startDate.AddDays(Date.DaysInMonth(startDate.Year, startDate.Month) - 1)
            promotionCount = 0
            Dim monthlyPromotions = getDistinctYearlyPromotion(startDate, endDate)

            retVal = retVal & "<li><div class=""dateHolder"">" & If(promotionCount > 0, "<h3 class='eventCount'>" & promotionCount & " Promotions</h3>", "") & "<h2>" & startDate.ToString("MMMM") & "</h2></div>" & _
                    monthlyPromotions & _
                "</li>"
            startDate = startDate.AddMonths(1)
        Next

        Return retVal

    End Function


    Private Function getDistinctYearlyPromotion(sDate As Date, eDate As Date) As String
        Dim retVal As String = ""
        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()
        sqlcomm.CommandText = " SELECT        List_Promotion.PromotionID, List_Promotion.CategoryID, List_Promotion.Title,List_Promotion.HostName,List_Promotion.Vanue, List_Promotion.SmallDetails, List_Promotion.SmallImage, List_Promotion.ImageAltText, List_Promotion.MasterID, List_Promotion.Link, " & _
                            " List_Promotion.Lang, List_Promotion_Details.StartDate, List_Promotion_Details.EndDate, List_Promotion_Details.Time " & _
                            " FROM            List_Promotion INNER JOIN " & _
                            "                 List_Promotion_Details ON List_Promotion.MasterID = List_Promotion_Details.PromotionMasterID " & _
                            " WHERE        (List_Promotion.Lang = @Lang) and List_Promotion.Status=1 and  (StartDate>=@sDate and StartDate<= @eDate) or (EndDate>=@sDate and EndDate<=@eDate) " & _
                            " Order by List_Promotion.SortIndex"
        'sqlcomm.CommandText = " select *  FROM Promotions where Promotions.Status=1 and  (PromotionStartDate>=@sDate and PromotionStartDate<= @eDate) or (PromotionEndDate>=@sDate and PromotionEndDate<=@eDate) "
        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text
        sqlcomm.Parameters.Add("sDate", Data.SqlDbType.DateTime).Value = sDate
        sqlcomm.Parameters.Add("eDate", Data.SqlDbType.DateTime).Value = eDate
        sqlcomm.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Request.QueryString("lang")

        promotionCount = 0
        Dim listString As String = ""
        Dim dataRowString As String = ""
        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        While reader.Read()
            promotionCount += 1

            tempPromotionTitle = reader("Title")
            tempEventTitle = reader("Title")
            dataRowString &= "<li>" & _
                            "    <div class='row'>" & _
                            "        <div class='col-md-4'>" & _
                            "            <div class='imgHold'>" & _
                            "                <img src='/ui/media/dist/saadiyat/csr/img-1.jpg' alt=''>" & _
                            "            </div>" & _
                            "        </div>" & _
                            "        <div class='col-md-8'>" & _
                            "            <h2>" & reader("Title") & Utility.showEditButton(Request, "/admin/A-Promotion/PromotionEdit.aspx?PromotionID=" & reader("PromotionID")) & "</h2>" & _
                            "            <ul class='timings'>"
            If IsDBNull(reader("HostName")) = False Then
            dataRowString += "<li><span>Host</span> " & reader("HostName") & "</li>"
            End If

            If IsDBNull(reader("Vanue")) = False Then
                dataRowString += "<li><span>Venue:</span> " & reader("Vanue") & "</li>"
            End If

            dataRowString += "            </ul>" & _
                            "            <p>" & Mid(reader("SmallDetails").ToString(), 1, 140) & "...</p>"
            If IsDBNull(reader("Link")) = False Then
                dataRowString += "<a target='_parent'  href='" & reader("Link") & "' class='clickHereButton'>" & _
                            "                More Information <span><img alt='' src='/ui/media/dist/icons/readmore-arrow.png'></span>" & _
                            "            </a>"
            End If

            dataRowString += "            <a href='javascript:;' class='clickHereButton downLoadListTrigger'>Downloadable Content <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>                                                                        " & _
                            "            <!-- Download List Container -->" & _
                            getDownloadableFiles("List_Promotion", reader("PromotionID").ToString()) & _
                            "            <!-- Download List Container -->" & _
                            "        </div>" & _
                            "    </div>" & _
                            "</li>"

        End While
        sqlConn.Close()
        If promotionCount > 0 Then
            retVal = "<!-- Calender Promotions Pop up -->" & _
                     "   <div class=""calenderEventPopup"">" & _
                     "       <div class=""eventContainer"">" & _
                     "           <a class=""eventPopupCloseButton"" href=""javascript:;""> X </a>" & _
                     "           <div class=""contentSection"">" & _
                     "               <!-- Promotion Listings -->" & _
                     "               <ul class=""eventListings"">" & dataRowString & _
                     "               </ul>" & _
                     "               <!-- Promotion Listings -->" & _
                     "           </div>" & _
                     "       </div>" & _
                     "   </div>" & _
                     "<!-- Calender Promotion Pop up -->"

        End If


        Return retVal
    End Function

End Class
