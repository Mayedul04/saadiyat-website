﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="Gallery.aspx.vb" Inherits="Gallery" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
     <!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <span>&nbsp; &nbsp;Gallery </span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") %>'>Home</a></li>
                        <li class="active"> Gallery </li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                    
                   <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                </div>

                <!-- Gallery Listing -->
                <div class="row">
                    <ul class="diningListings mediaLanding">
                        
                       <%= getHTMLContent("16", "gallery/photo")%>
                       <%= getHTMLContent("17", "gallery/video")%>
                       <%= getHTMLContent("18", "experiences")%>
                    </ul>
                </div>
                <!-- Gallery Listing -->


            </div>
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
            <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

