﻿
Partial Class education_details
    Inherits System.Web.UI.Page
    Public domainName, title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Language.Lang = Nothing
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, BigImage, Link, MapImage, GalleryID from List_Education where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = Page.RouteData.Values("id")
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                lblTitle.Text = FormateTitle(reader("SubTitle").ToString())
                hdnID.Value = reader("ListID").ToString()
                lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, "/Admin/A-Education/InstituteEdit.aspx?lid=" & hdnID.Value)
                link1 = reader("Link").ToString()
                title = reader("Title").ToString()
                galid = reader("GalleryID")
                DynamicSEO.PageID = reader("ListID").ToString()
                DynamicSEO.PageType = "List_Education"
                '    lblMap.Text = "<div class=""col-sm-6 bottomBoxesMinHeight""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/education/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            End While
            conn.Close()
            If galid <> 0 Then

                UserGalleryControl1.Gallery_ID = galid
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If

            lblAddFile.Text = Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Education&TID=" & hdnID.Value & "&t=" & Utility.EncodeTitle(title, "-"))
            styledtitle = FormateTitle(title)

            'lblVgal.Text = GetFeaturedVideos()
            'If Page.RouteData.Values("id") = 1 Then
            '    pnlVideo.Visible = False
            'End If
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = "<span>" & firstpart & "</span>" & secondpart
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getInstituteThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ListID, Title, SmallImage, SmallDetails,MasterID from List_Education where  Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
           
            retstr += "<li><a href=""" & domainName & Session("lang") & "/education-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>"
            If hdnID.Value = reader("ListID") Then
                retstr += "<div class=""thumbnail-box activeBW"">"
            Else
                retstr += "<div class=""thumbnail-box"">"
            End If
            retstr += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"
           
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">" & title & " at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = Math.Ceiling(total / 2) Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr
    End Function
    Public Function getAddresses() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email, webSite, Details from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"
            'If IsDBNull(reader("Phone")) = False Then
            '    retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("IntPhone")) = False Then
            '    retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("Email")) = False Then
            '    retstr += "<h5>Email : <a href=""mailto:" & reader("Email").ToString() & """ target=""_blank"">" & reader("Email").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("WebSite")) = False Then
            '    retstr += "<h5>Web : <a href=""" & reader("WebSite").ToString() & """ target=""_blank"">" & reader("WebSite").ToString() & "</h5>"
            'End If
            retstr += reader("Details").ToString() & Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=List_Education&TID=" & hdnID.Value & "&t=" & Server.UrlEncode(title)) & retstr
        Return retstr
    End Function
    Public Function getDownloadableFiles() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID  and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & domainName & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & domainName & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID").ToString() & "&TName=List_Education&TID=" & hdnID.Value) & "</li>"
            
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getfirstFooter() As String
        Dim retstr As String = ""
        If Page.RouteData.Values("id") = 1 Then
            retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">" & Language.Read("Interactive  <span>Map</span>", Page.RouteData.Values("lang")) & "</h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/education/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        Else

            Dim videos As String = GetFeaturedVideos()
            If videos <> "" Then
                retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">" & Language.Read("Video <span>Section</span>", Page.RouteData.Values("lang")) & "</h2><div class=""detailVideoSlider video-flexsliders"">"
                retstr += "<ul class=""slides"">" & videos & "</ul></div></div>"
            Else
                retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">" & Language.Read("Interactive  <span>Map</span>", Page.RouteData.Values("lang")) & "</h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/education/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
            End If
        End If
        

        Return retstr
    End Function
    Public Function getSecondFooter() As String
        Dim retstr As String = ""
        Dim addr As String = getAddresses()
        retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">" & Language.Read("Quick <span>Contact</span>", Page.RouteData.Values("lang")) & "</h2><div class=""contactDetailsSection"">"
        retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        If getfirstFooter.Contains("Interactive") = False Then
            retstr += "<div class=""col-sm-6 bottomBoxesMinHeight""><div class=""sketchbox""><h3 class=""subtitle yellow"">" & Language.Read("Interactive  <span>Map</span>", Page.RouteData.Values("lang")) & "</h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map/education/" & Page.RouteData.Values("id") & "/" & Page.RouteData.Values("title") & """ class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        End If

        Return retstr
    End Function
    
    Public Function GetFeaturedVideos() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""230""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
End Class
