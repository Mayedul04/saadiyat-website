﻿
Partial Class Admin_A_Education_AllInstitutions
    Inherits System.Web.UI.Page


    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Institute.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlLang.DataBind()
            HTMLs1.MasterIDs = "1"
            HTMLs1.Lang = ddlLang.SelectedValue
        End If

    End Sub
End Class
