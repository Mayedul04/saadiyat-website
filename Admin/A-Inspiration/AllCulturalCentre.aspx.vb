﻿
Partial Class Admin_A_Inspiration_AllCulturalCentre
    Inherits System.Web.UI.Page
    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("CulturalCentre.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString
            Response.Cookies("backurlAdmin").Expires = Date.Today.AddDays(+1)
            ddlLang.DataBind()
            HTMLs.MasterIDs = "5,32,33"
            HTMLs.Lang = ddlLang.SelectedValue
        End If

    End Sub
End Class
