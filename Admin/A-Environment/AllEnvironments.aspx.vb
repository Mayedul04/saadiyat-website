﻿
Partial Class Admin_A_Environment_AllEnvironments
    Inherits System.Web.UI.Page


    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Environment.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ddlLang.DataBind()
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString
            Response.Cookies("backurlAdmin").Expires = Date.Today.AddDays(+1)
            HTMLs1.MasterIDs = "2"
            HTMLs1.Lang = ddlLang.SelectedValue
        End If

    End Sub
End Class
