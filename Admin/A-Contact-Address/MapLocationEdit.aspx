﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="MapLocationEdit.aspx.vb" Inherits="Admin_A_Contact_Address_MapLocationEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Map Location Details</h1>
    <div class="btn-toolbar">
        <a href="../A-Contact-Address/MapLocations.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <label class="success-left-top"></label>
            <label class="success-right-top"></label>
            <label
                class="success-left-bot">
            </label>
            <label class="success-right-bot"></label>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <label class="error-left-top"></label>
            <label class="error-right-top"></label>
            <label class="error-left-bot">
            </label>
            <label class="error-right-bot"></label>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">

            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Category:</label>
                <asp:DropDownList ID="ddCategory" CssClass="input-xlarge" runat="server" AppendDataBoundItems="True">
                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                    <asp:ListItem>residentials</asp:ListItem>
                    <asp:ListItem>resorts</asp:ListItem>
                    <asp:ListItem>leisure</asp:ListItem>
                    <asp:ListItem>cultural</asp:ListItem>
                    <asp:ListItem>education</asp:ListItem>

                </asp:DropDownList>

            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload  Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>


            <p>
                <label>
                    Details:
                </label>
                <asp:TextBox ID="txtDetails" runat="server" class="txt" TextMode="MultiLine" />
                <script>

                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.

                    CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                                            );

                </script>
                <label>
                </label>
            </p>
            <p>
                <label>Co-Ordinates (Longitude ,Latitude)</label>
                <asp:TextBox ID="txtposition" runat="server" class="txt" MaxLength="200" />
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtposition"
                        ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="form">*</asp:RequiredFieldValidator>

                </label>
            </p>
            <p>
                <label>
                    Link :</label>
                <asp:TextBox ID="txtWebsite" runat="server" class="txt" MaxLength="200" />
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="txtWebsite"
                        ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="form">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtWebsite"
                        ErrorMessage="RegularExpressionValidator" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
                        ValidationGroup="form">*</asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>


        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>
        <p>
            <label>
                Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" Enabled="false" CssClass="input-xlarge"
                DataTextField="LangFullName" DataValueField="Lang">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
        </p>
        <asp:HiddenField ID="hdnMasterID" runat="server" />
        <asp:SqlDataSource ID="sdsMap" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            InsertCommand="INSERT INTO [MapLocations] ([Title], [Details], [Link], [Status], [Category], [SmallImage],[Lang],[MasterID],[Coordinates]) VALUES (@Title, @Details, @Link, @Status, @Category, @SmallImage,@Lang,@MasterID,@Coordinates)" 
             UpdateCommand="UPDATE [MapLocations] SET [Title] = @Title, [Details] = @Details, [Link] = @Link, [Status] = @Status, [Category] = @Category, [SmallImage] = @SmallImage, [Lang]=@Lang, [Coordinates]=@Coordinates WHERE [LocationID] = @LocationID" >
            <DeleteParameters>
                <asp:Parameter Name="LocationID" Type="Int32" />
            </DeleteParameters>

            <InsertParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtWebsite" Name="Link" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter Name="Category" Type="String" ControlID="ddCategory" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtposition" Name="Coordinates" PropertyName="Text" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtWebsite" Name="Link" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter Name="Category" Type="String" ControlID="ddCategory" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value" Type="String" />
                <asp:QueryStringParameter Name="LocationID" QueryStringField="mid" Type="Int32" />
                <asp:ControlParameter ControlID="txtposition" Name="Coordinates" PropertyName="Text" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            </UpdateParameters>

        </asp:SqlDataSource>
</asp:Content>

