﻿
Partial Class Admin_HTMLs
    Inherits System.Web.UI.UserControl

    Private masterIDs_ As String = ""
    Private lang_ As String = "en"

    Public Property MasterIDs As String
        Get
            Return masterIDs_
        End Get

        Set(ByVal value As String)
            masterIDs_ = value
        End Set
    End Property

    Public Property Lang() As String
        Get
            Return lang_
        End Get

        Set(ByVal value As String)
            lang_ = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            sdsHTML.SelectCommand = "SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang  where  HTML.MasterID in (" & masterIDs_ & ")  order by MasterID, Languages.SortIndex"
            ' sdsHTML.SelectParameters("MasterIDs").DefaultValue = masterIDs_
            '  sdsHTML.SelectParameters("Lang").DefaultValue = lang_

        End If

    End Sub
End Class
