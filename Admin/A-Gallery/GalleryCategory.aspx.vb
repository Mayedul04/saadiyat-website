﻿Imports System.Data.SqlClient

Partial Class Admin_A_Gallery_GalleryCategory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Request.QueryString("cid") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM GCategory where CatID=" & Request.QueryString("cid")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()

                txtDisplay.Text = reader("DisplayName").ToString()
                txtArDisplay.Text = reader("ArDisplayName").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("ThumbImage").ToString()
                hdnSmallImage.Value = reader("ThumbImage").ToString()
               
                sqlConn.Close()
            End If
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Try

            If fuSmallImage.FileName <> "" Then
                hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtDisplay.Text <> "", Utility.EncodeTitle(txtDisplay.Text, "-") & "-Small", Utility.EncodeTitle(txtDisplay.Text, "-") & "-Small"), Server)
                imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
                imgSmallImage.Visible = True
            End If

            Dim a As Integer

            If String.IsNullOrEmpty(Request.QueryString("cid")) Then

            Else
                a = SqlDataSourceGallery.Update()
            End If

            If a > 0 Then
                divSuccess.Visible = True
                divError.Visible = False
            Else
                divSuccess.Visible = False
                divError.Visible = True
            End If
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
        End Try
    End Sub
End Class
