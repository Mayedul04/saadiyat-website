﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Content.aspx.vb" Inherits="Admin_A_Content_Content" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<script>
    $(document).ready(function () {

        $('.HowDidYouHearUs').change(function () {
            //alert($("select.HowDidYouHearUs option:selected").val());
            if ($("select.HowDidYouHearUs option:selected").val() == "Other") {
                $(".other-info").removeAttr("style");
                $(".other-textbox").val("");
            }
            else {
                $(".other-info").attr("style", "display:none;");
                $(".other-textbox").val($("select.HowDidYouHearUs option:selected").val());
            }
        });

    });    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">Content for <%= Request.QueryString("t") %></h1>
    <div class="btn-toolbar">    
        <a href="../A-Content/AllContents.aspx?TName=<%= Request.QueryString("TName")%>&TID=<%= Request.QueryString("TID")%>&t=<%= Request.QueryString("t") %>&ImageWidth=<%= Request.QueryString("ImageWidth")%>&ImageHeight=<%= Request.QueryString("ImageHeight") %>&Title=<%= Request.QueryString("Title") %>&Image=<%= Request.QueryString("Image") %>&Text=<%= Request.QueryString("Text") %>&link=<%=  Request.QueryString("link") %>&category=<%= Request.QueryString("category") %>"  data-toggle="modal" class="btn">Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Content"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:Panel ID="pnlTitle" runat="server">
                <p>
                    <label>
                        Title:</label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </p>
            </asp:Panel>
           
            <asp:Panel ID="pnlBigImage" runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                    <label>
                        Upload Big Image :(Width=<%= Request.QueryString("ImageWidth")%>; Height=<%= Request.QueryString("ImageHeight")%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                </p>
            </asp:Panel>
            

            <asp:Panel ID="pnlImageAltText" runat="server">
                <p>
                    <label>
                        Image Alt Text:</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
           
            <asp:Panel ID="pnlBigDetails" runat="server">
                <p>
                    <label>
                        Details:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                        {
                                            filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                            "extraPlugins": "imagebrowser",
                                            "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                        }
                                            );

                    </script>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlCategory" runat="server" Visible="false">
                <p>
                    <label>Category:</label>
                    <asp:DropDownList ID="ddlHowDidYouHearUs" runat="server" 
                        CssClass="HowDidYouHearUs input-xlarge" DataSourceID="SqlDataSource1" 
                        DataTextField="Category" DataValueField="Category" > 
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                        SelectCommand="SELECT distinct [Category] FROM [Contents] WHERE ([TableName] = @TableName) and TableID=@TableID">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="TableName" QueryStringField="TName" Type="String" />
                            <asp:QueryStringParameter Name="TableID" QueryStringField="TID" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:TextBox ID="txtCategory" runat="server"  style="display:none"  cssclass="input-xlarge other-textbox other-info" ></asp:TextBox>


                      
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlLink" runat="server">
                <p>
                    <label>
                        Link:</label>
                    <asp:TextBox ID="txtLink" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" Display="Dynamic"
                        SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                        ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
             <p>
                <label>
                    Status :</label>
                <asp:CheckBox ID="chkStatus" CssClass="chkbox" runat="server"  />
            </p>
            
          
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>

            <div class="btn-group">
            </div>
        </div>

    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Contents] WHERE [ContentID] = @ContentID"
        InsertCommand="INSERT INTO [Contents] ([Title], [DetailText], [Image], [ImageAltText], Link,[SortIndex], [Status], [TableName], [TableID],Category) VALUES (@Title, @DetailText, @Image, @ImageAltText, @Link, @SortIndex, @Status, @TableName, @TableID,@Category)"
        SelectCommand="SELECT * FROM [Contents]"
        UpdateCommand="UPDATE [Contents] SET [Title] = @Title, [DetailText] = @DetailText, [Image] = @Image, [ImageAltText] = @ImageAltText, Link=@Link , [SortIndex] = @SortIndex, [Status] = @Status, Category=@Category  WHERE [ContentID] = @ContentID">
        <DeleteParameters>
            <asp:Parameter Name="ContentID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="DetailText" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="Image" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter Name="Status" Type="Boolean" ControlID="chkStatus" PropertyName="Checked" />
            <asp:QueryStringParameter Name="TableName" QueryStringField="TName" Type="String" />
            <asp:QueryStringParameter Name="TableID" QueryStringField="TID" Type="Int32" />
           <asp:ControlParameter ControlID="txtCategory" Name="Category" PropertyName="Text" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="DetailText" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="Image" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtLink" Name="Link" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter Name="Status" Type="Boolean" ControlID="chkStatus" PropertyName="Checked" />
            <asp:QueryStringParameter Name="ContentID" QueryStringField="CID" Type="Int32" />
            <asp:ControlParameter ControlID="txtCategory" Name="Category" PropertyName="Text" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

