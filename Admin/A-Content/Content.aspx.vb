﻿
Partial Class Admin_A_Content_Content
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("Title") = "0" Then
                pnlTitle.Visible = False
            Else
                pnlTitle.Visible = True
            End If

            If Request.QueryString("Image") = "0" Then
                pnlBigImage.Visible = False
            Else
                pnlBigImage.Visible = True
            End If


            If Request.QueryString("Text") = "0" Then
                pnlBigDetails.Visible = False
            Else
                pnlBigDetails.Visible = True
            End If

            If Request.QueryString("link") = "0" Then
                pnlLink.Visible = False
            Else
                pnlLink.Visible = True
            End If

            If Request.QueryString("Category") = "1" Then
                pnlCategory.Visible = True
            Else
                pnlCategory.Visible = False
            End If


            If Not String.IsNullOrEmpty(Request.QueryString("cid")) Then


                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Content"
                ddlHowDidYouHearUs.DataBind()
                LoadContent(Request.QueryString("cid"))
            End If

        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick


        

        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If

        If String.IsNullOrEmpty(Request.QueryString("cid")) Then

            If sdsObject.Insert() > 0 Then
                Response.Redirect("AllContents.aspx?TName=" & Request.QueryString("TName") & "&TID=" & Request.QueryString("TID") & "&t=" & Request.QueryString("t") & "&ImageWidth=" & Request.QueryString("ImageWidth") & "&ImageHeight=" & Request.QueryString("ImageHeight") & "&Title=" & Request.QueryString("Title") & "&Image=" & Request.QueryString("Image") & "&Text=" & Request.QueryString("Text") & "&link=" & Request.QueryString("link") & "&category=" & Request.QueryString("category"))
            Else
                divError.Visible = True
            End If
        Else
            If sdsObject.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = True
            End If
        End If

    End Sub

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  *  FROM Contents where ContentID=@CID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CID", Data.SqlDbType.Int)
        cmd.Parameters("CID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            txtTitle.Text = reader("Title") & ""
            txtDetails.Text = reader("DetailText") & "" 'hdnDetails.Value


            hdnBigImage.Value = reader("Image") & ""

            imgBigImage.Visible = True
         
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
     
            txtImgAlt.Text = reader("ImageAltText") & ""
            txtSortIndex.Text = reader("SortIndex").ToString()
            Boolean.TryParse(reader("Status").ToString(), chkStatus.Checked)
            txtLink.Text = reader("link").ToString()
            Try
                ddlHowDidYouHearUs.SelectedValue = reader("Category").ToString()
                txtCategory.Text = reader("Category").ToString()
            Catch ex As Exception

            End Try

        Else
            conn.Close()
            Response.Redirect("AllContents.aspx?TName=" & Request.QueryString("TName") & "&TID=" & Request.QueryString("TID") & "&t=" & Request.QueryString("t") & "&ImageWidth=" & Request.QueryString("ImageWidth") & "&ImageHeight=" & Request.QueryString("ImageHeight") & "&Title=" & Request.QueryString("Title") & "&Image=" & Request.QueryString("Image") & "&Text=" & Request.QueryString("Text") & "&link=" & Request.QueryString("link") & "&category=" & Request.QueryString("category"))
        End If
        conn.Close()
    End Sub

    Protected Sub ddlHowDidYouHearUs_DataBound(sender As Object, e As System.EventArgs) Handles ddlHowDidYouHearUs.DataBound
        ddlHowDidYouHearUs.Items.Insert(0, New ListItem("<- Select ->", ""))
        ddlHowDidYouHearUs.Items.Add("Other")
    End Sub
End Class
