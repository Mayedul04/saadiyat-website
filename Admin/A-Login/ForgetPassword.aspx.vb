﻿Imports System.Data.SqlClient

Partial Class Admin_A_Login_ForgetPassword
    Inherits System.Web.UI.Page

    Protected Sub btnForgetPassword_Click(sender As Object, e As System.EventArgs) Handles btnForgetPassword.Click
        CaptchaControl1.ValidateCaptcha(txtCaptcha.Text.ToLower())
        If CaptchaControl1.UserValidated Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "select * from AdminPanelLogin where UN_1=@userID "
            Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
            cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50)
            cmd.Parameters("userID").Value = txtUserID.Text

            Dim pass As String = "", email As String = "", fullName As String = ""
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                pass = reader("PS_1").ToString()
                email = reader("Email").ToString()
                fullName = reader("Title").ToString()
            End If
            conn.Close()

            If email <> "" Then
                Dim m_ToAddress As String = email
                Dim m_CCAddress As String = ""
                Dim m_BCCAddress As String = "debashis.chowdhury@wvss.net"
                Dim m_EmailFromName As [String] = "NEXA CMS"
                Dim m_EmailSubject As [String] = "NEXA CMS : Forget Password"
                Dim m_EmailBody As [String] = GetRegistrationInfoMessage(fullName, pass)

                Utility.SendMail(m_EmailFromName, "service@digitalnexa.com", m_ToAddress, m_CCAddress, m_BCCAddress, m_EmailSubject, m_EmailBody)

                lblMessage.Text = "An email is sent to your registered email."
                lblMessage.ForeColor = Drawing.Color.Green
            Else
                lblMessage.Text = "Your email is not found. Please contact with NEXA or email to service@digitalnexa.com"

            End If

            txtCaptcha.Text = ""
            txtUserID.Text = ""

        End If
        


    End Sub

    Private Function GetRegistrationInfoMessage(Name As String, password As String) As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
"<html lang='en'>" & _
"<head>" & _
"    <meta charset='utf-8'>" & _
"    <title>Royal Rose</title>" & _
"    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" & _
"    <body style='margin:0px; padding:0px;'>" & _
"        <table bgcolor='#ac7e14' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>" & _
"            <tr>" & _
"                <td style='padding:2px 0;'>" & _
"                    <table bgcolor='#fff' width='610' cellspacing='0' cellpadding='0' border='0' align='center' style='background:#ffffff;'>" & _
"                        <tr>" & _
"                            <td align='center'>" & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>&nbsp;</p>" & _
"                                <table width='90%' cellspacing='0' cellpadding='0' align='center' border='1' style='border:1px solid #eee; border-collapse: collapse; margin:0 0 20px;'>" & _
"                                    <tr bgcolor='#eee'>" & _
"                                        <td colspan='2'>" & _
"                                            <h3 style='font-family:arial; color:#490109; font-size:24px; text-align:center; margin:10px 0;'>NEXA CMS: Forget password</h3>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & Name & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Password</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & password & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                </table>                                " & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>© Copyright " & Date.Now.Year & ". All rights reserved by ~<a href='http://www.digitalnexa.com/'>NEXA</a>~.</p>" & _
"                            </td>" & _
"                        </tr>  " & _
"                    </table>" & _
"                </td>" & _
"            </tr>" & _
"        </table>" & _
"    </body>" & _
"</html>"


        '"                        <tr>" & _
        '"                            <td align='center' style='padding:15px 0 7px;'>" & _
        '"                                <a href=' http://Site-URL.com '><img src='http://" & Request.Url.Host & "/ui/media/dist/email/hilton_logo2.png' width='200' alt=''></a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        '"                        <tr>" & _
        '"                            <td align='center' style='padding:0 0 10px;'>" & _
        '"                                <a style='font-family:arial; color:#490109; text-decoration:; font-size:14px;' href='http://Site-URL.com' target='_blank'>~Site Name~</a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        Return retVal
    End Function
End Class
