﻿
Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page

    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("NewUser.aspx")
    End Sub
    Protected Sub btnLoginStatistics_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoginStatistics.ServerClick
        Response.Redirect("UserLoginStatistics.aspx")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Cookies("userRole") Is Nothing = False Then
            If Request.Cookies("userRole").Value.ToString() = "sa" Then
                pnlLoginStatistics.Visible = True
            End If
        End If
    End Sub
End Class
