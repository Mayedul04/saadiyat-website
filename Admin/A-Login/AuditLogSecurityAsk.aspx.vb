﻿Imports System.Data.SqlClient
Partial Class AuditLogSecurityAsk
    Inherits System.Web.UI.Page
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Dim email As String = txtEmail.Text.Trim()
        Dim password As String = txtPassword.Text.Trim()
        If email.Contains("'") Or String.IsNullOrEmpty(email) Then
            lblMessage.Text = "email or password invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If
        If password.Contains("'") Or String.IsNullOrEmpty(password) Then
            lblMessage.Text = "email or password invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select * from AdminPanelLogin where Email=@Email and PS_1=@password"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("Email", Data.SqlDbType.VarChar, 100).Value = txtEmail.Text
        cmd.Parameters.Add("password", Data.SqlDbType.VarChar, 100).Value = txtPassword.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            Response.Redirect("UserLoginStatistics.aspx")
        Else
            lblMessage.Text = "Your information is not found with given email or password. thanks!"
            lblMessage.ForeColor = Drawing.Color.Red
        End If
    End Sub
End Class
