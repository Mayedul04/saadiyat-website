﻿Imports System.Data.SqlClient
Partial Class Admin_A_Login_NewUser
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnLastUpdated.Value = DateTime.Now
        lblMessage.Text = ""
        txtEmail.Text = txtEmail.Text.Trim()
        txtUserID.Text = txtUserID.Text.Trim()
        txtUserName.Text = txtUserName.Text.Trim()
        hdnRole.Value = "admin"
        If IsIDExists(txtUserID.Text) Then
            lblMessage.Text = "The ID is already exists. Please try another ID."
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        If IsEmailExists(txtEmail.Text) Then
            lblMessage.Text = "The email is already exists. Please try another email."
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        If txtPassword.Text <> "" Then
            If System.Text.RegularExpressions.Regex.IsMatch(txtPassword.Text, "^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$") = True Then
                If IsNotSequentialChars(txtUserID.Text.Trim(), txtPassword.Text, 3) = True And IsNotSequentialChars(txtUserName.Text.Replace(" ", "").Trim(), txtPassword.Text, 3) = True Then
                    If txtPassword.Text <> txtRetypePassword.Text Then
                        lblMessage.Text = "Password not matched"
                        lblMessage.ForeColor = Drawing.Color.Red
                        Exit Sub
                    End If
                Else
                    lblMessage.Text = "Password should not contain parts of the user name or login id that exceed 2 consecutive characters (ex: part of admin like adm, dmi, min etc)"
                    lblMessage.ForeColor = Drawing.Color.Red
                    Exit Sub
                End If
            Else
                lblMessage.Text = "Password must contain: Minimum 8 characters atleast 1 Alphabet, 1 Number and 1 Special Character"
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        Else
            lblMessage.Text = "Please enter password"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        Try
            sdsAdminPanelLogin.Insert()
            Response.Redirect("../A-Login/AllUser.aspx")
        Catch ex As Data.SqlClient.SqlException
            lblMessage.Text = "Error: " & ex.Message
            lblMessage.ForeColor = Drawing.Color.Red
        End Try
    End Sub

    Public Function IsNotSequentialChars(ByVal Src As String, ByVal Dest As String, ByVal check_len As Integer) As Boolean
        If check_len < 1 OrElse Src.Length < check_len Then
            Return True
        End If
        Dim m As Match = Regex.Match(Src, "(?=(.{" + check_len.ToString() + "})).")
        Dim bOK As Boolean = m.Success
        While bOK AndAlso m.Success
            bOK = Not Regex.Match(Dest, "(?i)" + Regex.Escape(m.Groups(1).Value)).Success
            'If Not bOK Then
            '    Console.WriteLine("Destination contains " + check_len + " sequential source letter(s) '" + m.Groups(1).Value + "'")
            'End If
            m = m.NextMatch()
        End While
        Return bOK
    End Function

    Private Function IsEmailExists(ByVal email As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where email=@email "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("email", Data.SqlDbType.VarChar, 100).Value = email
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Function IsIDExists(ByVal UN_1 As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where UN_1=@UN_1 "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UN_1", Data.SqlDbType.VarChar, 100).Value = UN_1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function



End Class
