﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="experiences.aspx.vb" Inherits="experiences" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span><b>Gallery</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src="/ui/media/dist/elements/down.png" alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">

                    <%= getLeftNav("16", "gallery/photo")%>
                    <%= getLeftNav("17", "gallery/video")%>
                    <%= getLeftNav("18", "experiences")%>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
         <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
     <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">Your <span>Experiences</span>
            </h1>
            <ol class="breadcrumb">
               <li><a href='<%= Session("domainName") %>'>Home</a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/gallery" %>'>Gallery</a>
                </li>
               
                <li class="active">Your Experiences </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->
            
        <div class="main-content-area">

                <asp:Literal ID="lblDetails" runat="server"></asp:Literal>

                <div class="row">
                    
                    <div class="col-md-6 col-sm-6">
                        
                        <!-- Register Section -->
                        <div class="checkAvailabityWidget mediaRequestForm">

                            <h2 class="subtitle">
                                Instagram
                            </h2>
                            
                            <!-- Instagram Slider -->
                            <div class="instagramSlider flexslider">
                                <ul class="slides">
                                    
                                   <%= GetInstagram() %>
                                </ul>
                            </div>
                            <!-- Instagram Slider -->


                        </div>
                        <!-- Register Section -->

                    </div>

                    <div class="col-md-6 col-sm-6">
                        
                        <!-- Login Section -->
                        <div class="checkAvailabityWidget mediaRequestForm blue">

                            <h2 class="subtitle">
                                twitter
                            </h2>
                            
                            <div class="twitterWidget">

                                <a class="twitter-timeline"  width="456" height="320"  href="https://twitter.com/saadiyatae" data-widget-id="579929013600174080">Tweets by @saadiyatae</a>
<script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>
                                   
          
                            </div>  


                        </div>
                        <!-- Login Section -->

                    </div>

                </div>

            </div>

  
    <!-- Main Content Section -->
    <uc1:dynamicseo runat="server" id="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

