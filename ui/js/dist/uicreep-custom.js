// Playground for saadiyat to add a bit of extra 
// ============================================
var windowWidth = $(window).width();
var windowHH = $(window).height();
$('.mobileBg').width(windowWidth);
$('.mobileBg').height(windowHH);

$('.playbtnmobile').click(function(){
    //$('.mobileBg').hide();
    $('.videoMobileHolder').show();

    $('.videoMobileHolder').find("video")[0].play();
    
})


$(".closeButton").click(function(){

    $('.videoMobileHolder').hide();
    //$('.mobileBg').show();

})


$(window).load(function() {


    var $container = $('.tabBoxes');
    //autoWrapperHeight();
    // initialize
    $container.masonry({

        itemSelector: '.item',
        isAnimated: true
    });

    // For Fancybox Photopopup

    $("a.photopopup").addClass("fancybox.iframe");

    // For Splash Screen
    // ===========================================================

    $(".spashScreen").fadeOut('10000');

    // For Splash Screen
    // ===========================================================

    $(".mainTimelineContainer").parents("div.content-area-inner").addClass("timeOverflow");
    $(".mainTimelineContainer").parents("div.content").addClass("overFlowBlock");
    $(".thumbSlider").parents("div.content-area-inner").addClass("timeOverflow");



});



// if($(document).find(".socialContanerSection").size() > 0) {

//     //alert("working")

//     $(".fb_iframe_widget, .fb_iframe_widget span, .fb_iframe_widget span iframe[style]").css( 'width' , '100%');
// };


var windowHeight = $(window).height();
var dwindowHeight = $(document).height();
$('#main-wrapper .inner .column').height(windowHeight - 44);
$('.column .panel-content').height(windowHeight - 138);
$('.innerdetail').height(windowHeight - 44);





// Nano scroll triggering 
// --------------------------------------------------------------------


// $(".column .nano").nanoScroller({
//     alwaysVisible: false
// });

// $(".column .nano").bind("scrollend", function (e) {

//     $(this).parent().find('.scrollers').removeClass('scrolldown');
//     $(this).parent().find('.scrollers').addClass('scrollup');
//     $(this).parent().find('.scrollers img').attr("src", "/ui/media/dist/elements/scrollup.png");
//     scroll_up();
// });

// $(".column .nano").bind("scrolltop", function (e) {

//     $(this).parent().find('.scrollers').removeClass('scrollup');
//     $(this).parent().find('.scrollers').addClass('scrolldown');
//     $(this).parent().find('.scrollers img').attr("src", "/ui/media/dist/elements/down.png");
//     scroll_down();
// });


// $(".innerdetail .nano").bind("scrollend", function (e) {

//     $(this).parent().find('.scrollers').removeClass('scrolldown');
//     $(this).parent().find('.scrollers').addClass('scrollup');
//     $(this).parent().find('.scrollers img').attr("src", "/ui/media/dist/elements/scrollup.png");
//     scroll_up();
// });

// $(".innerdetail .nano").bind("scrolltop", function (e) {

//     $(this).parent().find('.scrollers').removeClass('scrollup');
//     $(this).parent().find('.scrollers').addClass('scrolldown');
//     $(this).parent().find('.scrollers img').attr("src", "/ui/media/dist/elements/down.png");
//     scroll_down();
// });


// $(".innerdetail .panel-content").nanoScroller({
//     alwaysVisible: false
// });

// // scroll down functunality in a panel 
// // -------------------------------------------------------------------

// $('.scrollers').click(function () {

//     $(this).siblings(".nano").nanoScroller({
//         scroll: 'bottom'
//     });

// });


// function scroll_down() {
//     $('.scrollers').click(function () {
//         $(this).siblings(".nano").nanoScroller({
//             scroll: 'bottom'
//         });
//     });
// }

// function scroll_up() {
//     $('.scrollers').click(function () {
//         $(this).siblings(".nano").nanoScroller({
//             scroll: 'top'
//         });
//     });
// }


// Nano scroll triggering 
// --------------------------------------------------------------------




// For Adding Class and Scroll Up and down Buttons FOr Panels
// ===============================================================


$(".column .scrollers, .innerdetail .scrollers").addClass("scrollDown")

var scrolDownButton = $("<a class='scrollers scrollUp'><img src='/ui/media/dist/elements/scrollup.png' alt=''></a>");

$(".scrollDown").after(scrolDownButton);

// End Of For Adding Class and Scroll Up and down Buttons FOr Panels
// ======================================================================


// For Multiple Scroll panels FOr Home Page
// ========================================================


$(".column").each(function() {

    var apis = $(this).find('.nano').jScrollPane().data('jsp');

    $(this).find('.scrollUp').bind(
        'mousedown',
        function() {
            var interval = setInterval(
                function() {
                    apis.scrollByY(-30);

                },
                100
            );
            $(window).bind(
                'mouseup.jspExample',
                function() {
                    clearInterval(interval);
                    $(document).unbind('.jspExample');
                }
            );
        }
    );

    // Scroll Down Function
    // ====================================================
    $(this).find('.scrollDown').bind(
        'mousedown',
        function() {
            var interval = setInterval(
                function() {
                    apis.scrollByY(30);
                },
                100
            );
            $(window).bind(
                'mouseup.jspExample',
                function() {
                    clearInterval(interval);
                    $(document).unbind('.jspExample');
                }
            );
        }
    );

});


// End of For Multiple Scroll panels FOr Home Page
// =========================================================


if($(window).width() > 580 ) { 

// For Scroll Thing FOr Inner Page
// ========================================================

var api = $('.innerdetail .nano').jScrollPane().data('jsp');


// Scroll Up Function
// ========================================================

$('.innerdetail .scrollUp').bind(
    'mousedown',
    function() {
        var interval = setInterval(
            function() {
                api.scrollByY(-30);

            },
            100
        );
        $(window).bind(
            'mouseup.jspExample',
            function() {
                clearInterval(interval);
                $(document).unbind('.jspExample');
            }
        );
    }
);


// Scroll Down Function
// ========================================================

$('.innerdetail .scrollDown').bind(
    'mousedown',
    function() {
        var interval = setInterval(
            function() {
                api.scrollByY(30);
            },
            100
        );
        $(window).bind(
            'mouseup.jspExample',
            function() {
                clearInterval(interval);
                $(document).unbind('.jspExample');
            }
        );
    }
);


// End of Scroll Thing FOr Inner Page
// ========================================================

}



// fancybox starts here
// --------------------------------------------------------------------

$(".fancybox-thumb").fancybox({
    prevEffect: 'none',
    nextEffect: 'none',
    helpers: {
        title: {
            type: 'outside'
        },
        thumbs: {
            width: 50,
            height: 50
        }
    }
});

$(".mappop").fancybox({
    maxWidth: 1000,
    maxHeight: 500,
    fitToView: false,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});

$('.photopopup').fancybox({

    maxWidth: 850,
    maxHeight: 750,
    fitToView: false,
    width: '90%',
    height: '90%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'


});

$('.instaPhotopopup').fancybox({

    maxWidth: 650,
    maxHeight: 650,
    fitToView: false,
    width: '90%',
    height: '90%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'


});

$(".calenderPopup").fancybox({
    maxWidth: 1000,
    maxHeight: 650,
    fitToView: false,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});


$(".newsPopup").fancybox({
    maxWidth: 1000,
    maxHeight: 213,
    fitToView: false,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});


$(".videoPopUp").fancybox({
    maxWidth: 800,
    maxHeight: 600,
    fitToView: false,
    width: '70%',
    height: '70%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});


$(".socialPopup").fancybox({
    maxWidth: 1200,
    maxHeight: 470,
    fitToView: false,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});

// For Fancybox
// ------------------------------------------------------------------





// Initialising wow 
// -------------------------------------------------------------------
new WOW().init();

// Initialising panel menu 
// -------------------------------------------------------------------
$('#left-panel-link').panelslider({
    clickClose: false
});
$('#left-panel-link').click(function() {
    $('.leftArrow').animate({
        left: '200'
    })
});

$('#close-panel-bt').click(function() {
    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });
});
// For Download List Scroll
// ========================================================

jQuery.fn.liScroll = function(settings) {
    settings = jQuery.extend({
        travelocity: 0.03
    }, settings);
    return this.each(function() {
        var $strip = jQuery(this);
        $strip.addClass("newsticker")
        var stripWidth = 1;
        $strip.find("li").each(function(i) {
            stripWidth += jQuery(this, i).outerWidth(true); // 
        });
        var $mask = $strip.wrap("<div class='scrollmask'></div>");
        var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");
        var containerWidth = $strip.parent().parent().width(); //a.k.a. 'mask' width   
        stripWidth = stripWidth + 100;
        $strip.width(stripWidth);
        var totalTravel = stripWidth + containerWidth;
        var defTiming = totalTravel / settings.travelocity; // thanks to Scott Waye     
        function scrollnews(spazio, tempo) {
            $strip.animate({
                left: '-=' + spazio
            }, tempo, "linear", function() {
                $strip.css("left", containerWidth);
                scrollnews(totalTravel, defTiming);
            });
        }
        scrollnews(totalTravel, defTiming);
        $strip.hover(function() {
                jQuery(this).stop();
            },
            function() {
                var offset = jQuery(this).offset();
                var residualSpace = offset.left + stripWidth;
                var residualTime = residualSpace / settings.travelocity;
                scrollnews(residualSpace, residualTime);
            });
    });
};


/ news SCrolling /
$(function() {
    $("ul#ticker01").liScroll({
        travelocity: 0.05
    });
});

// $(".bigvideo .playbutton").click(function () {

//     $('.bigvideo video')[0].play();
//     $(this).hide();
//     $('.bigvideo video').css('opacity', '1');
// });




// For Download List Scroll
// ========================================================
// Initialising Flexslider
// -------------------------------------------------------------------

$('.flexslider').flexslider({
    slideshow: 'true',
    animation: "slide",
    useCSS: false
});

$('.smaller-slider').flexslider({
    slideshow: 'true',
    animation: "slide",
    useCSS: false
});

$('.flexsliders').flexslider({
    slideshow: 'false',
    animation: "slide",
    useCSS: true
});

$('.video-flexsliders').flexslider({
    slideshow: false,
    animation: "slide",
    useCSS: false
});

$(".playButton").click(function() {

    $(this).parent("li").find("video")[0].play();
    $(this).hide();

})


$(".downLoadListTrigger").click(function() {
    $("div.downloadListContainer").slideToggle();
})

$(".closeButton").click(function() {

    $("div.downloadListContainer").slideToggle();

})


$('.circles a').click(function() {
    var lol = $(this).attr("class");
    $('.circle').find('.active').removeClass('active');
    $(this).addClass('active');
    $('.content-timeline.active').removeClass('active');
    $('.content-timeline' + '#' + lol).addClass('active');
})

// $(".datepicker").datepicker({
//     showOn: "both",
//     buttonImage: "/ui/media/dist/icons/calender-icon.jpg",
//     buttonImageOnly: true
// });


// For Date Picker
// ==================================================================

$datepicker = $(".datepicker").size();
//alert($datepicker);
if ($datepicker > 0) {
    $(function() {
        $(".checkIn").datepicker({
            firstDay: 0,
            dateFormat: "mm/dd/yy",
            showOn: "both",
            buttonImage: "/ui/media/dist/icons/calender-icon.jpg",
            buttonImageOnly: true,


            minDate: 0,
            defaultDate: "0",

            numberOfMonths: 1,
            onClose: function(selectedDate) {
                    if (selectedDate != "") {
                        var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
                        nextDayDate.setDate(nextDayDate.getDate() + 1);

                        $(".checkOut").datepicker("option", "minDate", nextDayDate);
                    }
                }
                //onClose: function (selectedDate) {
                //    if (selectedDate != "") {
                //        var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
                //        nextDayDate.setDate(nextDayDate.getDate() + 1);

            //        $(".checkOut").datepicker("option", "minDate", nextDayDate);
            //    }
            //}
        });


        //$('.checkIn').change(function () {
        //    var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
        //    nextDayDate.setDate(nextDayDate.getDate() + 1);
        //    $('.checkOut').datepicker('setDate', nextDayDate);
        //});
        $('.checkIn').change(function() {
            var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
            nextDayDate.setDate(nextDayDate.getDate() + 1);
            $('.checkOut').datepicker('setDate', nextDayDate);
        });

        $(".checkOut").datepicker({

            minDate: 0,
            dateFormat: "mm/dd/yy",
            defaultDate: +1,
            showOn: "both",
            buttonImage: "/ui/media/dist/icons/calender-icon.jpg",
            buttonImageOnly: true,

            numberOfMonths: 1,
            /*onClose: function( selectedDate ) {
            $( ".checkIn" ).datepicker( "option", "maxDate", selectedDate );
            }*/

        });
    });
}


// For Date Picker
// ==================================================================


$("ul.csrListing li:odd").addClass("right");

$("ul.csrListingStyle li:odd").addClass("right");

// if($(document).find(".nano").size() > 0) {

// $(function(){
//   $(".nano").nanoScroller();
// });

// }


// For Misinary
//====================================================

// $(".activeBW").BlackAndWhite({

//     hoverEffect: false,
//     webworkerPath: false

// });



// For Misinary
//====================================================


// For Accordian
// ===================================================

$("div.accordianContent.first").show();

$(".accordianListingcontainer h2.first").find("span").addClass("arrowUp");

$(".accordianListingcontainer h2").click(function() {

    $(this).next("div.accordianContent").slideToggle().siblings("div.accordianContent").slideUp();
    $(this).siblings().find("span").removeClass("arrowUp");
    $(this).find("span").toggleClass("arrowUp");

})

// For Accordian
// ===================================================




// FOr Black & White Hover Image
// =====================================================

$(".activeBW img").addClass("grayscale");


$(".titlebox img").hover(function() {

    $(this).removeClass("grayscale");

}, function() {

    $(this).addClass("grayscale");

})


// FOr Black & White Hover Image
// =====================================================



// Custom Styling For Select Box
// ===================================================
$("select").uniform();

// Custom Styling For Select Box




// For Where to go Popup
// ====================================================

$(".werToGoTrigger").click(function() {

    $(".werToGoSerchSection").fadeToggle();

    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });

});


$(".closeButton").click(function() {

    $(this).parents("div.werToGoSerchSection").fadeOut();

});

// For Where to go Popup
// ====================================================


// For Where to go Popup
// ====================================================

$(".calenderTrigger").click(function() {

    $(".calenderContainerSection").fadeToggle();

});


$(".closeButton").click(function() {

    $(this).parents("div.calenderContainerSection").fadeOut();

});

// For Where to go Popup
// ====================================================

$(".mappop").click(function(){

    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });

})

$(".calenderPopup").click(function(){

    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });

})

$(".socialPopup").click(function(){

    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });

})


$(".newsPopup").click(function(){

    $.panelslider.close();
    $('.leftArrow').animate({
        left: '0'
    });
    
})



// For Event Popup
// ==========================================================

$("h3.eventCount").click(function() {

    $(this).parents("li").find("div.calenderEventPopup").fadeIn();

})


$(".eventPopupCloseButton").click(function() {

    $(this).parents("li").find("div.calenderEventPopup").fadeOut();

});


// For Event Popup
// ==========================================================



// For Calender Tabbing
// ============================================================

$(".eventPromoContentContainer > div").hide();
$(".eventPromoContentContainer > div:first-child").show();


$("ul.eventPromotionNavigation li").click(function() {


    var $this = $(this);

    if ($(this).attr("class") != "active") {


        var $index = $this.index();
        $index++;
        $this.addClass("active").siblings().removeClass("active");

        $(".eventPromoContentContainer > div").hide();
        $(".eventPromoContentContainer > div.content-" + $index).fadeIn();



    }


})




$(".calenderTabContentContainer > div").hide();
$(".calenderTabContentContainer > div:first-child").show();


$("ul.calenderTabNavigation li").click(function() {


    var $this = $(this);

    if ($(this).attr("class") != "active") {


        var $index = $this.index();
        $index++;
        $this.addClass("active").siblings().removeClass("active");

        $(".calenderTabContentContainer > div").hide();
        $(".calenderTabContentContainer > div.content-" + $index).fadeIn();


    }


})

// For Calender Tabbing
// ==========================================================
// ==========================================================





// For Mobile Left Panel Navigation Toggle
// ===========================================================

var $mobileTrigger = $("<div class='mobileTriggerLeftPanel'></div>");

$(".innerdetail div.heading").append($mobileTrigger);

$(".mobileTriggerLeftPanel").click(function() {

    $(this).parents("div.innerdetail").find("div.panel-content").slideToggle();

})


// For Mobile Main Section Navigation Toggle
// ===========================================================

var $mobileSectionTrigger = $("<div class='mobileSectionNavTrigger'><span><img src='/ui/media/dist/icons/mobNavarrow.png' alt=''></span></div>");

$(".content-area-inner").prepend($mobileSectionTrigger);

$(".mobileSectionNavTrigger span").click(function() {

    //alert('click');

    $(this).parents("div.content-area-inner ").find("ul.sub-menu").slideToggle();

})


// For Mobile Navigation Toggle
// ===========================================================


// For Mobile Main Section Navigation Toggle
// ===========================================================

var $mobileHomeLogo = $("<div class='mobileLogo'><span><img src='/ui/media/dist/elements/color-logo.png' alt=''></span></div>");

$("#main-wrapper").prepend($mobileHomeLogo);



// For Mobile Fixes
// ============================================================

if ($(window).width() < 480) {

    $('.innerdetail').height('auto');

    $(".innerdetail .panel-content").removeClass("nano");

    var mobileWidth = $(window).width() + 17;

    var homeBoxContainer = mobileWidth * 7;

    //alert(homeBoxContainer);

    //$(".inner .column").css( "width", mobileWidth );

    //$("#main-wrapper .inner").css( "width", homeBoxContainer );

    // scroll right functunality on the index page 
    // -------------------------------------------------------------------
    $(".rightArrow").click(function() {
        var leftPos = $('.content').scrollLeft();
        $(".content").animate({
            scrollLeft: leftPos + mobileWidth
        }, 800);
    });


    $(".leftArrow").click(function() {
        var rightPos = $('.content').scrollLeft();
        $(".content").animate({
            scrollLeft: rightPos - mobileWidth
        }, 800);
    });


} else {

    $('.home-menu').trigger('click');

    // scroll right functunality on the index page 
    // -------------------------------------------------------------------
    $(".rightArrow").click(function() {
        var leftPos = $('.content').scrollLeft();
        $(".content").animate({
            scrollLeft: leftPos + 320
        }, 800);
    });


    $(".leftArrow").click(function() {
        var rightPos = $('.content').scrollLeft();
        $(".content").animate({
            scrollLeft: rightPos - 320
        }, 800);
    });


    //  Animation to the saadiyat site
    //  -------------------------------------------------------------



    $('.column').hover(function() {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).siblings().find('.panel-overlay').css('opacity', '1');
            $(this).siblings().find('.panel-overlay').css('top', '94');
        } else {
            $(this).siblings().find('.panel-overlay').css('opacity', '0');
            $(this).siblings().find('.panel-overlay').css('top', '94');
        }
    })



}

// For Mobile Fixes
// ============================================================


// For Tablet Portrait
// ============================================================

if ($(window).width() < 1024 && $(window).width() >= 750) {

    //alert("tab");
    $('.innerdetail').height('auto');

    $(".innerdetail .panel-content").removeClass("nano");


}

// For Tablet Portrait
// ============================================================


// For Horizontal Scroll for Saadiyat Timeline
// ============================================================

if($(window).width() > 580 ) { 

var viewWidth = $('#horiz_container_outer').width(),
    itemWidth = viewWidth / 8,
    itemLength = $(".timeline > .circles").children('.circle').length,
    totalWidthSet = itemWidth * itemLength + 5;

$('#horiz_container').css('width', totalWidthSet);
$(".timeline > .circles").children('.circle').css('width', itemWidth);


if (itemLength > 8) {
    $('#horiz_container_outer').horizontalScroll();

} else {

    $("#track").hide();


};

}


// For Horizontal Scroll for Saadiyat Timeline
// ============================================================


// $("#linkButton").click( function(){

// $(this).attr('target','_blank');

// $link = $(this).attr("title");
// window.location.href = $link;


// });


// Acordian Content Toggle
// ===========================================================

$(".viewMoreAccordian").parent("p").addClass("viewMoreTrigger");


$(".viewMoreTrigger span").click(function() {


    if ($(this).text() == 'View More') {

        $(this).text('View Less');

    } else {

        $(this).text('View More');

    }

    $(this).parents("p.viewMoreTrigger").prev(".extendContents").slideToggle();

    //$(this).parents("p.viewMoreTrigger").addClass("su");


})

// Acordian Content Toggle
// ===========================================================

if($(window).width() < 580) {

//alert('test')

$(".calenderPopup").fancybox({
    maxWidth: 1000,
    maxHeight: 1000,
    fitToView: false,
    width: '100%',
    height: '100%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});

$(".newsPopup").fancybox({
    maxWidth: 800,
    maxHeight: 342,
    fitToView: false,
    width: '100%',
    height: '80%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
});

$('.photopopup').fancybox({

    maxWidth: 850,
    maxHeight: 550,
    fitToView: false,
    width: '90%',
    height: '70%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'


});


}

var prm = Sys.WebForms.PageRequestManager.getInstance();

prm.add_endRequest(function() { 
  $(".checkAvailabityWidget select").uniform();

    // For Date Picker
    // ==================================================================

    $datepicker = $(".datepicker").size();
    //alert($datepicker);
    if ($datepicker > 0) {
        $(function() {
            $(".checkIn").datepicker({
                firstDay: 0,
                dateFormat: "mm/dd/yy",
                showOn: "both",
                buttonImage: "/ui/media/dist/icons/calender-icon.jpg",
                buttonImageOnly: true,


                minDate: 0,
                defaultDate: "0",

                numberOfMonths: 1,
                onClose: function(selectedDate) {
                        if (selectedDate != "") {
                            var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
                            nextDayDate.setDate(nextDayDate.getDate() + 1);

                            $(".checkOut").datepicker("option", "minDate", nextDayDate);
                        }
                    }
                    //onClose: function (selectedDate) {
                    //    if (selectedDate != "") {
                    //        var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
                    //        nextDayDate.setDate(nextDayDate.getDate() + 1);

                //        $(".checkOut").datepicker("option", "minDate", nextDayDate);
                //    }
                //}
            });


            //$('.checkIn').change(function () {
            //    var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
            //    nextDayDate.setDate(nextDayDate.getDate() + 1);
            //    $('.checkOut').datepicker('setDate', nextDayDate);
            //});
            $('.checkIn').change(function() {
                var nextDayDate = $('.checkIn').datepicker('getDate', '+1d');
                nextDayDate.setDate(nextDayDate.getDate() + 1);
                $('.checkOut').datepicker('setDate', nextDayDate);
            });

            $(".checkOut").datepicker({

                minDate: 0,
                dateFormat: "mm/dd/yy",
                defaultDate: +1,
                showOn: "both",
                buttonImage: "/ui/media/dist/icons/calender-icon.jpg",
                buttonImageOnly: true,

                numberOfMonths: 1,
                /*onClose: function( selectedDate ) {
                $( ".checkIn" ).datepicker( "option", "maxDate", selectedDate );
                }*/

            });
        });
    }


    // For Date Picker
    // ==================================================================

});
