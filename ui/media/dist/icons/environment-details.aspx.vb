﻿
Partial Class environment_details
    Inherits System.Web.UI.Page
    Public title As String = ""
    Public currentGalleryID As String = ""
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Environment"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = Page.RouteData.Values("id")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SmallImage,BigImage,SmallDetails,BigDetails, MapImage,MapCode,GalleryID, MasterID from List_Environment where  Lang=@Lang and Status=1 order by SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = ""

            While reader.Read()
                title = reader("Title").ToString()
                link = Session("domainName") & Session("lang") & "/environment-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                Dim formatedTitle As String = FormateTitle(title)
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "javascript:;", link) & """>" & _
                       "       <div class=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "thumbnail-box activeBW", "thumbnail-box") & """>" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = Page.RouteData.Values("id") Then
                    currentListID = reader("ListID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()
                    ltrBigDetails.Text = reader("BigDetails").ToString & Utility.showEditButton(Request, "/Admin/A-Environment/EnvironmentEdit.aspx?lid=" & reader("ListID"))

                    currentGalleryID = reader("GalleryID").ToString()
                End If

                i = i + 1
            End While
            conn.Close()
            ' ltrFeature.Text = getSpecialFeatres(currentListID)
            If currentGalleryID <> "" Then

                UserGalleryControl1.Gallery_ID = currentGalleryID
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & currentGalleryID)
            End If
            'If Page.RouteData.Values("id") = "2" Then
            '    pnlDownload.Visible = False
            'Else
            '    lblAddFile.Text = Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Environment&TID=" & currentListID & "&t=" & Utility.EncodeTitle(Title, "-"))
            '    lblMediafiles.Text = getDownloadableFiles(currentListID)
            'End If
            
            ltrAccordianContent.Text = Contents("List_Environment", currentListID)
            'If Page.RouteData.Values("id") = "2" Then
            '    pnlVideosecond.Visible = True
            'Else
            '    pnlVideofirst.Visible = False
            'End If
        End If
    End Sub

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getDownloadableFiles(ByVal id As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID  and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Environment"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName").ToString() & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID").ToString() & "&TName=List_Environment&TID=" & id) & "</li>"

        End While
        conn.Close()
        Return retstr
    End Function


    Public Function getSpecialFeatres(ByVal id As String) As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">" & title & " at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Environment"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = id
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Environment"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = id
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = Math.Ceiling(total / 2) Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr

       
    End Function

    Public Function GetFeaturedVideos(ByVal galid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
                'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
                'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

                retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""230""></iframe></div>"
                retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
            End While

        End If
        
        conn.Close()
        Return retstr
    End Function

    Private Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            If counter = 0 Then
                retstr &= "<h2 class=""first"">" & reader("Title") & "<span class=""arrowDown""></span></h2>"
                retstr &= "<div class=""accordianContent first"">"
            Else
                retstr &= "<h2>" & reader("Title") & "<span class=""arrowDown""></span></h2>"
                retstr &= "<div class=""accordianContent"">"
            End If
            retstr &= If(reader("Image").ToString() <> "", "     <div class=""imgHold""><a class=""fancybox-thumb"" href=""" & Session("domainName") & "Admin/" & reader("Image") & """>" & _
                        "       <img src=""" & Session("domainName") & "Admin/" & reader("Image") & """ alt=""" & reader("Title") & """></a></div>", "") & _
                        ManipulateListing(reader("DetailText").ToString()) & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=485&ImageHeight=287&Title=1&Image=1&Text=1&link=0") & _
                        "</div>"
            If counter = 0 And tableID = 1 Then
                retstr &= "<span class=""clickHereButton downLoadListTrigger"" data-wow-iteration=""100"">Downloadable Documents <span>"
                retstr &= "<img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></span>"
                retstr &= "<div class=""downloadListContainer""><div class=""contentSection"">" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Environment&TID=" & tableID & "&t=" & Utility.EncodeTitle(title, "-"))
                retstr &= "<ul class=""listings"">" & getDownloadableFiles(tableID) & "</ul></div></div>"
            End If
            counter += 1
        End While
        conn.Close()


        Return retstr
    End Function

    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function
    Public Function getfirstFooter() As String
        Dim retstr As String = ""
        Dim videos As String = GetFeaturedVideos(currentGalleryID)
        If videos <> "" Then
            retstr += "<div class=""col-sm-6""><h2 class=""subtitle"">Video <span>Section</span></h2><div class=""detailVideoSlider video-flexsliders"">"
            retstr += "<ul class=""slides"">" & videos & "</ul></div></div>"
            'Else
            '    retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        End If

        Return retstr
    End Function
    Public Function getSecondFooter() As String
        Dim retstr As String = ""

        'If addr <> "" Then
        '    retstr += "<div class=""col-sm-6"">Quick <span>Contact</span><div class=""contactDetailsSection"">"
        '    retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        'Else
        '    retstr = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        'End If
        '  If Page.RouteData.Values("id") = 2 Then
        'If Page.RouteData.Values("id") = "2" Then
        '    If getfirstFooter.Contains("Interactive") = False Then
        '        retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        '    End If

        'End If

        'Else
        'retstr += "<div class=""col-sm-6"">Quick <span>Contact</span><div class=""contactDetailsSection"">"
        'retstr += " <ul class=""contactListings"">" & addr & "</ul></div></div>"
        'retstr += "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><div class=""imap""> <a href=""" & Session("domainName") & Session("lang") & "/interactive-map"" class=""mappop"" data-fancybox-type=""iframe""><img src=""" & Session("domainName") & "ui/media/dist/home/sbr/map.jpg"" alt=""""></a></div></div></div>"
        'End If

        Return retstr
    End Function
End Class
