// Playground for saadiyat to add a bit of extra 
// ============================================
$(window).load(function() {
    $('.home-menu').trigger('click');
})
var windowHeight = $(window).height();
var dwindowHeight = $(document).height();
$('#main-wrapper .inner .column').height(windowHeight - 44);
$('.column .panel-content').height(windowHeight - 138);
$('.innerdetail').height(windowHeight - 44);





//  Animation to the saadiyat site
//  -------------------------------------------------------------



$('.column').hover(function(){
	$(this).toggleClass('active');
	if ($(this).hasClass('active')) {
		$(this).siblings().find('.panel-overlay').css('opacity','1');
		$(this).siblings().find('.panel-overlay').css('top','94');
	} else {
		$(this).siblings().find('.panel-overlay').css('opacity','0');
		$(this).siblings().find('.panel-overlay').css('top','94');
	}
})





// Nano scroll triggering 
// --------------------------------------------------------------------


    $(".column .nano").nanoScroller({
        alwaysVisible: false
    });
    $(".column .nano").bind("scrollend", function(e){
       
        $(this).parent().find('.scrollers').removeClass('scrolldown');
        $(this).parent().find('.scrollers').addClass('scrollup');
        $(this).parent().find('.scrollers img').attr("src","ui/media/dist/elements/scrollup.png");
        scroll_up();
    });

    $(".column .nano").bind("scrolltop", function(e){
        
        $(this).parent().find('.scrollers').removeClass('scrollup');
        $(this).parent().find('.scrollers').addClass('scrolldown');
        $(this).parent().find('.scrollers img').attr("src","ui/media/dist/elements/down.png");
        scroll_down();
    });

     $(".innerdetail .panel-content").nanoScroller({
        alwaysVisible: false
    });

// scroll down functunality in a panel 
// -------------------------------------------------------------------
$('.scrollers').click(function() {
    
    $(this).siblings(".nano").nanoScroller({
        scroll: 'bottom'
    });
});



    function scroll_down() {
        $('.scrollers').click(function() {
            $(this).siblings(".nano").nanoScroller({
                scroll: 'bottom'
            });
        });
    }

    function scroll_up() {
        $('.scrollers').click(function() {
            $(this).siblings(".nano").nanoScroller({
                scroll: 'top'
            });
        });
    }




// fancybox starts here
// --------------------------------------------------------------------
$(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});

$(".mappop").fancybox({
		maxWidth	: 1000,
		maxHeight	: 500,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

$('.photopopup').fancybox({

      'autoScale'            : false,
      'transitionIn'          : 'none',
      'transitionOut'        : 'none',
      'type'                 : 'iframe',
      'width'                : 850,
      'height'               : 560,
      'scrolling'            : 'no'


});
// For Fancybox


// scroll right functunality on the index page 
// -------------------------------------------------------------------
$(".rightArrow").click(function() {
    var leftPos = $('.content').scrollLeft();
    $(".content").animate({
        scrollLeft: leftPos + 320
    }, 800);
});


$(".leftArrow").click(function() {
    var rightPos = $('.content').scrollLeft();
    $(".content").animate({
        scrollLeft: rightPos - 320
    }, 800);
});


// For Working with Keyborad Arrows
// ====================================================

// $(".rightArrow").keydown(function(ev) {
//     //alert('working');
//     if(ev.which == $.ui.keyCode.RIGHT) {

//         var leftPos = $('.content').scrollLeft();
//         $(".content").animate({
//             scrollLeft: leftPos + 320
//         }, 800);

//         ev.preventDefault();
//     }
// });


// $(".leftArrow").keydown(function(ev) {
//     //alert('working');
//     if(ev.which == $.ui.keyCode.LEFT) {

//         var rightPos = $('.content').scrollLeft();
//         $(".content").animate({
//             scrollLeft: rightPos - 320
//         }, 800);

//         ev.preventDefault();
//     }
// });


// For Working with Keyborad Arrows
// ====================================================


// Initialising wow 
// -------------------------------------------------------------------
new WOW().init();

// Initialising panel menu 
// -------------------------------------------------------------------
$('#left-panel-link').panelslider({clickClose: false});
$('#left-panel-link').click(function() {
        $('.scroll-left').animate({
           left: '200' 
        })
});

$('#close-panel-bt').click(function() {
    $.panelslider.close();
    $('.scroll-left').animate({
       left: '0' 
    });
});
// For Download List Scroll
// ========================================================

jQuery.fn.liScroll = function(settings) {
        settings = jQuery.extend({
        travelocity: 0.03
        }, settings);       
        return this.each(function(){
                var $strip = jQuery(this);
                $strip.addClass("newsticker")
                var stripWidth = 1;
                $strip.find("li").each(function(i){
                stripWidth += jQuery(this, i).outerWidth(true); // 
                });
                var $mask = $strip.wrap("<div class='scrollmask'></div>");
                var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");                             
                var containerWidth = $strip.parent().parent().width();  //a.k.a. 'mask' width   
                stripWidth = stripWidth +100;
                $strip.width(stripWidth);           
                var totalTravel = stripWidth+containerWidth;
                var defTiming = totalTravel/settings.travelocity;   // thanks to Scott Waye     
                function scrollnews(spazio, tempo){
                $strip.animate({left: '-='+ spazio}, tempo, "linear", function(){$strip.css("left", containerWidth); scrollnews(totalTravel, defTiming);});
                }
                scrollnews(totalTravel, defTiming);             
                $strip.hover(function(){
                jQuery(this).stop();
                },
                function(){
                var offset = jQuery(this).offset();
                var residualSpace = offset.left + stripWidth;
                var residualTime = residualSpace/settings.travelocity;
                scrollnews(residualSpace, residualTime);
                });         
        }); 
};


/ news SCrolling /
$(function(){
    $("ul#ticker01").liScroll({travelocity: 0.05});
});

$(".bigvideo .playbutton").click(function(){
 
    $('.bigvideo video')[0].play();
    $(this).hide();
    $('.bigvideo video').css('opacity','1');
});


$(".playButton").click(function(){

    $(this).parent("li").find("video")[0].play();

})

// For Download List Scroll
// ========================================================
// Initialising Flexslider
// -------------------------------------------------------------------

$('.flexslider').flexslider({
    slideshow: 'true',
    animation: "slide",
    useCSS : false
});

$('.smaller-slider').flexslider({
    slideshow: 'true',
    animation: "slide",
    useCSS : false
});

$('.flexsliders').flexslider({
    slideshow: 'false',
    animation: "slide",
    useCSS : false
});

$('.video-flexsliders').flexslider({
    slideshow: false,
    animation: "slide",
    useCSS : false
});


$(".downLoadListTrigger").click(function(){
    $("div.downloadListContainer").slideToggle();
})

$(".closeButton").click(function(){

    $("div.downloadListContainer").slideToggle();

})


$('.circles a').click(function(){
    var lol = $(this).attr("class");
    $('.circle').find('.active').removeClass('active');
    $(this).addClass('active');
    $('.content-timeline.active').removeClass('active');
    $('.content-timeline' + '#' + lol ).addClass('active');
})

$( ".datepicker" ).datepicker({
showOn: "button",
buttonImage: "ui/media/dist/icons/calender-icon.jpg",
buttonImageOnly: true
});


$("ul.csrListing li:odd").addClass("right");

$("ul.csrListingStyle li:odd").addClass("right");

// if($(document).find(".nano").size() > 0) {

// $(function(){
//   $(".nano").nanoScroller();
// });

// }


// For Misinary
//====================================================

$(window).load(function(){

var $container = $('.tabBoxes');
//autoWrapperHeight();
// initialize
$container.masonry({
 
  itemSelector: '.item',
  isAnimated:true
});


})



// For Misinary
//====================================================


// For Accordian
// ===================================================

$("div.accordianContent.first").show();

$(".accordianListingcontainer h2.first").find("span").addClass("arrowUp");

$(".accordianListingcontainer h2").click(function(){

    $(this).next("div.accordianContent").slideToggle().siblings("div.accordianContent").slideUp();
    $(this).siblings().find("span").removeClass("arrowUp");
    $(this).find("span").toggleClass("arrowUp");

})

// For Accordian
// ===================================================


// $(".activeBW").BlackAndWhite({

//     hoverEffect : false, // default true
//     // set the path to BnWWorker.js for a superfast implementation
//     webworkerPath : false,
//     // for the images with a fluid width and height 
//     responsive:true

// });


// FOr Hover Image
// =====================================================

$(".activeBW img").addClass("grayscale");


$(".titlebox img").hover(function(){

    $(this).removeClass("grayscale");

},function(){

    $(this).addClass("grayscale");

})


// FOr Hover Image
// =====================================================



// Custom Styling For Select Box
// ===================================================
$("select").uniform();

// Custom Styling For Select Box
// ===================================================


// for testing toggle slide RIGHT
// ====================================================

// $(".toggleLeftTest").click(function(){

//     var effect = "slide";

//     var options = { direction: 'right'}

//     var duration = 700;

//     $(".testDiv").toggle( effect, options, duration);

// })

// for testing toggle slide RIGHT
// ====================================================

