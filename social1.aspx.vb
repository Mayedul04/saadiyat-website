﻿
Partial Class social
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("34", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")


        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "34"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GetInstagram() As String
        Dim M As String = String.Empty
        Dim retinnner As String = ""
        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT InstagramImage.* FROM InstagramImage where InstagramImage.Status=1 order by ImageID desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try

            M += "<li>"
            M += "<ul class=""instagramListings"">"
            While reader.Read()
                i = i + 1
                retinnner += "<li><div class=""imgHold"">"
                retinnner += "<a class=""fancybox-thumb"" href=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """  rel=""instagallery"">"
                retinnner += "<img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                retinnner += "</a></div></li>"

                If i Mod 9 = 0 Then
                     M += retinnner + "</ul></li>"
                    retinnner = ""
                End If

            End While
            If retinnner <> "" Then
                M += "<li><ul class=""instagramListings"">" & retinnner & "</ul></li>"
            End If
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function
    Public Function GetYoutubeVideos() As String
        Dim M As String = String.Empty
        Dim retinnner1 As String = ""
        Dim i As Integer = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM GalleryItem where ItemType='Online Video' and Status=1 order by GalleryItemID desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        'cmd.Parameters.Add("RestaurantID", Data.SqlDbType.Int)
        'cmd.Parameters("RestaurantID").Value = 0
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Try


            While reader.Read()
                i = i + 1
                retinnner1 += "<li><div class=""imgHold relativeDiv"">"
                retinnner1 += "<a href=""http://www.youtube.com/embed/" & reader("VideoVCode") & """?autoplay=1"" class=""videoPopUp fancybox.iframe"" rel=""youtube"">"
                retinnner1 += "<img src=""" & reader("VideoImageURL").ToString() & """ alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                retinnner1 += "<span class=""playIcon""></span></a>"
                retinnner1 += "</div></li>"

                'retinnner1 += "<li><div class=""imgHold relativeDiv"">"
                'retinnner1 += "<a href=""http://www.youtube.com/embed/" & reader("VideoVCode") & """?autoplay=1"" class=""videoPopUp fancybox.iframe relativeDiv"" rel=""youtube"">"
                'retinnner1 += "<img src=""" & reader("VideoImageURL").ToString() & """ alt=""" & reader("Title").ToString.Replace("#", "-").Replace("@", "-").Replace("?", "-") & """ >"
                'retinnner1 += "<span class=""playIcon""></span></a>"
                'retinnner1 += "</div></li>"

                If i Mod 12 = 0 Then
                    M += "<li><ul class=""instagramListings youtube"">" & retinnner1 + "</ul></li>"
                    retinnner1 = ""

                End If

            End While
            If retinnner1 <> "" Then
                M += "<li><ul class=""instagramListings youtube"">" & retinnner1 & "</ul></li>"
            End If
           
            conn.Close()

            Return M
        Catch ex As Exception
            Return M
        End Try



    End Function

    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
