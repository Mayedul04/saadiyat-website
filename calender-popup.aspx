﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="calender-popup.aspx.vb" Inherits="calender_popup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <!-- Calender Section -->
    <div class="calenderContainerSection">
        
        <div class="calenderContent">

            <!-- Event & Promotion Navigation -->
            <ul class="eventPromotionNavigation">
                <li><a class="events" href="javascript:;">Events</a></li>
               <%-- <li><a class="promotions" href="javascript:;">Promotions</a></li>--%>
            </ul>
            <!-- Event & Promotion Navigation -->
            
            <!-- Event & Promotion Content Section -->
            <div class="eventPromoContentContainer">
                
                <!-- Event Content Section -->
                <div class="content-1 events">
                    
                    <div class="contentSection">
                
                        <span class="arrow"></span>
                        <a class="closeButton" href="javascript:;"> X </a>
                        
                        <!-- Calender Tabber -->
                        <div class="calenderTabber">
                            
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlEventMonth" runat="server" AutoPostBack="true" CssClass="form-group current-month-controls"  >
                                                </asp:DropDownList>
                                        <asp:DropDownList ID="ddlEventYear" runat="server" AutoPostBack="true" CssClass="form-group current-year-controls" style="display:none;"  >
                                                </asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="col-md-5">
                                    <h2><label class="current-month-controls"><asp:Literal ID="ltrCurrentMonth" runat="server" ></asp:Literal></label>
                                    <label class="current-year-controls" ><asp:Literal ID="ltrCurrentyear" runat="server" ></asp:Literal></label>
                                    </h2>
                                </div>

                                <div class="col-md-4">
                                    <ul class="calenderTabNavigation">
                                        <li class="active"><a href="javascript:;">Days</a></li>
                                        <li><a href="javascript:;">Month</a></li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                        <!-- Calender Tabber -->
                        
                        <!-- Calender tab Content Section -->
                        <div class="calenderTabContentContainer">
                            
                            <!-- Days Section -->
                            <div class="content-1">
                                
                                <!-- Calender Main Wrapper -->
                                <div class="mainCalenderWrapper">
                                    
                                    <!-- days Section -->
                                    <ul class="days">
                                        
                                        <li>sun</li>
                                        <li>Mon</li>
                                        <li>Tue</li>
                                        <li>Wed</li>
                                        <li>Thu</li>
                                        <li>Fri</li>
                                        <li>Sat</li>

                                    </ul>
                                    <!-- days Section -->
                                    
                                    <!-- Date Section -->
                                    <ul class="dates">
                                        
                                        <%= getAllEvents() %>
                                        
                                    </ul>
                                    <!-- Date Section -->

                                </div>
                                <!-- Calender Main Wrapper -->

                            </div>
                            <!-- Days Section -->
                            
                            <!-- Month Section -->
                            <div class="content-2">
                                
                                <div class="mainCalenderWrapper">
                                    
                                    <!-- Month Section -->
                                    <ul class="dates month">
                                        <%= getAllYearlyEvents() %>
                                    </ul>
                                    <!-- Month Section -->

                                </div>

                            </div>
                            <!-- Month Section -->

                        </div>
                        <!-- Calender tab Content Section -->
                

                    </div>

                </div>
                <!-- Event Content Section -->

                <!-- Promotion Content Section -->
                <div class="content-2 promotions">
                    
                    <div class="contentSection">
                
                        <span class="arrow"></span>
                        <a class="closeButton" href="javascript:;"> X </a>
                        
                        <!-- Calender Tabber -->
                        <div class="calenderTabber">
                            
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlPromotionMonth" runat="server" AutoPostBack="true" CssClass="form-group current-month-controls"  >
                                                </asp:DropDownList>
                                        <asp:DropDownList ID="ddlPromotionYear" runat="server" AutoPostBack="true" CssClass="form-group current-year-controls" style="display:none;"  >
                                                </asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="col-md-5">
                                    <h2>
                                    <label class="current-month-controls"><asp:Literal ID="ltrCurrentMonthPromotion" runat="server" ></asp:Literal></label>
                                    <label class="current-year-controls" ><asp:Literal ID="ltrCurrentyearPromotion" runat="server" ></asp:Literal></label>
                                    </h2>
                                </div>

                                <div class="col-md-4">
                                    <ul class="calenderTabNavigation">
                                        <li class="active"><a href="javascript:;">Days</a></li>
                                        <li><a href="javascript:;">Month</a></li>
                                    </ul>
                                </div>

                            </div>

                        </div>
                        <!-- Calender Tabber -->
                        
                        <!-- Calender tab Content Section -->
                        <div class="calenderTabContentContainer">
                            
                            <!-- Days Section -->
                            <div class="content-1">
                                
                                <!-- Calender Main Wrapper -->
                                <div class="mainCalenderWrapper">
                                    
                                    <!-- days Section -->
                                    <ul class="days">
                                        
                                        <li>sun</li>
                                        <li>Mon</li>
                                        <li>Tue</li>
                                        <li>Wed</li>
                                        <li>Thu</li>
                                        <li>Fri</li>
                                        <li>Sat</li>

                                    </ul>
                                    <!-- days Section -->
                                    
                                    <!-- Date Section -->
                                    <ul class="dates">
                                        
                                        <%= getAllPromotions() %>

                                    </ul>
                                    <!-- Date Section -->

                                </div>
                                <!-- Calender Main Wrapper -->

                            </div>
                            <!-- Days Section -->
                            
                            <!-- Month Section -->
                            <div class="content-2">
                                
                                <div class="mainCalenderWrapper">
                                    
                                    <!-- Month Section -->
                                    <ul class="dates month">
                                        <%= getAllYearlyPromotions() %>
                                    </ul>
                                    <!-- Month Section -->

                                </div>

                            </div>
                            <!-- Month Section -->

                        </div>
                        <!-- Calender tab Content Section -->
                

                    </div>

                </div>
                <!-- Promotion Content Section -->

            </div>
            <!-- Event & Promotion Content Section -->

        </div>

    </div>
    <!-- Calender Section -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="ui/js/dist/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="ui/js/std/bootstrap.min.js"></script>
    <script src="ui/js/dist/jquery.jscrollpane.min.js"></script>
    <script src="ui/js/dist/jquery.panelslider.min.js"></script>
    <script src="ui/js/dist/wow.min.js"></script>
    <script src="ui/js/dist/jquery.flexslider.js"></script>
    <script src="ui/js/dist/jquery.fancybox.js"></script>
    <script src="ui/js/dist/masonry.pkgd.min.js"></script>
    <script src="ui/js/dist/jquery.gray.min.js"></script>
    <script src="ui/js/std/uniform.min.js"></script>
    <script src="ui/js/dist/jquery.horizontal.scroll.js"></script>

      
         <script src= "ui/js/dist/jquery.jscrollpane.js"></script>
        <script src="ui/js/dist/jquery.mousewheel.js"></script>
    <script src="ui/js/dist/uicreep-custom.js"></script>

    <script>
        $(window).load(function () {
        $("ul.calenderTabNavigation li").click(function () {


            var $this = $(this);

            if ($this.attr("class") != "active") {


                var $index = $this.index(); $index++;


                $this.addClass("active").siblings().removeClass("active");

                //                    $(".calenderTabContentContainer > div").hide();
                //                    $(".calenderTabContentContainer > div.content-" + $index).fadeIn();

                if ($index == 1) {
                    $(".current-month-controls").attr("style", "");
                    $("#uniform-ddlEventMonth").attr("style", "");
                    $(".current-year-controls").attr("style", "display:none;");
                    $("#uniform-ddlEventYear").attr("style", "display:none;");
                }
                else {
                    $(".current-month-controls").attr("style", "display:none;");
                    $("#uniform-ddlEventMonth").attr("style", "display:none;");
                    $(".current-year-controls").attr("style", "");
                    $("#uniform-ddlEventYear").attr("style", "");
                }
            }


        });
        })
    </script>

    </form>
</body>
</html>
