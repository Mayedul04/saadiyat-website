﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="news.aspx.vb" Inherits="news" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%=fbUrl%>

    <%=fbTitle%>

    <%=fbDesc%>

    <%=fbImglnk%>

    <meta property="fb:app_id" content="786986411388776" />
    <meta property="og:type" content="website" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span><%= Language.Read("Media <br /><b>Centre</b>", Page.RouteData.Values("lang"))%></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftNav("25", "news/press-release")%>
                    <%= getLeftNav("26", "news/features")%>
                    <%= getLeftNav("27", "news/new")%>
                    <%= getLeftNav("28", "media-image-request")%>
                    <%= getLeftNav("29", "social-details")%>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= FormateTitle(Title) %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                
                <li class="active"><%= Title %> </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">
        <!-- Catagory Social Container -->
        <asp:Panel ID="pnlSocial" runat="server">
            <div class="catagorySocialContainer">

                <div class="row formSection">

                    <div class="col-md-7">
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddsType" class="form-control" runat="server" AutoPostBack="true" DataSourceID="sdsType" DataTextField="NewsType" DataValueField="NewsType" AppendDataBoundItems="true">
                                        <asp:ListItem Value="">Type</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sdsType" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [NewsType] FROM [List_News] WHERE ([Category] = @Category)">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="News" Name="Category" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlYear" class="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="">Year</asp:ListItem>
                                        <asp:ListItem>2015</asp:ListItem>
                                        <asp:ListItem>2014</asp:ListItem>
                                        <asp:ListItem>2013</asp:ListItem>
                                        <asp:ListItem>2012</asp:ListItem>
                                        <asp:ListItem>2011</asp:ListItem>
                                        <asp:ListItem>2010</asp:ListItem>
                                        
                                    </asp:DropDownList>
                                   <%-- <asp:TextBox ID="txtDate" class="form-control datepicker normal" AutoPostBack="true" runat="server"></asp:TextBox>--%>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlMonth" class="form-control" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="">Month</asp:ListItem>
                                        <asp:ListItem Value="1">Jan</asp:ListItem>
                                        <asp:ListItem Value="2">Feb</asp:ListItem>
                                        <asp:ListItem Value="3">Mar</asp:ListItem>
                                        <asp:ListItem Value="4">Apr</asp:ListItem>
                                        <asp:ListItem Value="5">May</asp:ListItem>
                                        <asp:ListItem Value="6">Jun</asp:ListItem>
                                         <asp:ListItem Value="7">Jul</asp:ListItem>
                                         <asp:ListItem Value="8">Aug</asp:ListItem>
                                         <asp:ListItem Value="9">Sep</asp:ListItem>
                                         <asp:ListItem Value="10">Oct</asp:ListItem>
                                         <asp:ListItem Value="11">Nov</asp:ListItem>
                                         <asp:ListItem Value="12">Dec</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">

                        <asp:Literal ID="ltSocialBar" runat="server"></asp:Literal>

                    </div>

                </div>

            </div>
        </asp:Panel>
        <!-- Catagory Social Container -->
        <%= NewsGallery() %>

        <!-- Pagination Listings -->
        <asp:Panel ID="pnlPageination" runat="server">

            <%= PageList %>
        </asp:Panel>
        <!-- Pagination Listings -->
        <!-- Register Section -->
        <asp:Panel ID="pnlNewsSubs" runat="server">
        <div class="checkAvailabityWidget mediaRequestForm newsLetter">

            <h2 class="subtitle"><%= Language.Read("Register For News", Page.RouteData.Values("lang"))%>
        </h2>

            <div class="row formRowMargin">

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">
                            <%= Language.Read("First Name", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                        ErrorMessage="RequiredFieldValidator"
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>

                        </label>

                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">
                            <%= Language.Read("Last Name", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                        ErrorMessage="RequiredFieldValidator"
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                        </label>
                        <asp:TextBox ID="txtLastName" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">
                            <%= Language.Read("Email", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="RequiredFieldValidator"
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="Subscribe"
                                ErrorMessage="RegularExpressionValidator" SetFocusOnError="True"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email</asp:RegularExpressionValidator>
                        </label>
                        <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                    </div>
                </div>

            </div>


            <p>
                <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
            </p>
            <asp:HiddenField ID="hdnContactDatetime" runat="server" />
            <asp:Button ID="btnSubmit" runat="server" Text="Subscribe" CssClass="submitButton" ValidationGroup="Subscribe" />

            <asp:SqlDataSource ID="sdsSubscribtion" runat="server"
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [NewsletterSubscription] WHERE [NewSubID] = @NewSubID"
                InsertCommand="INSERT INTO [NewsletterSubscription] ([FirstName], [LastName], [Email], [SubscriptionDate]) VALUES (@FirstName, @LastName, @Email, @SubscriptionDate)"
                SelectCommand="SELECT [NewSubID], [FirstName], [LastName], [Email], [SubscriptionDate] FROM [NewsletterSubscription]"
                UpdateCommand="UPDATE [NewsletterSubscription] SET [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [SubscriptionDate] = @SubscriptionDate WHERE [NewSubID] = @NewSubID">
                <DeleteParameters>
                    <asp:Parameter Name="NewSubID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtFirstName" Name="FirstName"
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtLastName" Name="LastName"
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnContactDatetime" Name="SubscriptionDate"
                        PropertyName="Value" Type="DateTime" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FirstName" Type="String" />
                    <asp:Parameter Name="LastName" Type="String" />
                    <asp:Parameter Name="Email" Type="String" />
                    <asp:Parameter Name="SubscriptionDate" Type="DateTime" />
                    <asp:Parameter Name="NewSubID" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>


        </div>
          </asp:Panel>
            <!-- Register Section -->
          
    </div>
    <!-- Main Content Section -->
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

