﻿
Partial Class your_stay
    Inherits System.Web.UI.Page
    Protected lang As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("3", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")


        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "7"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getResortList() As String
        Dim retstr As String = ""
        Dim selectString As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        selectString = "SELECT ListID, Title, BigImage, SmallDetails,MasterID from List_Stay where  Lang=@Lang and status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li style=""background-image:url('" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & "')""" & ">"
            If i Mod 2 = 0 Then
                retstr += "<div class=""whiteContentSection wow fadeInLeft"" data-wow-duration=""2s"">"
            Else
                retstr += "<div class=""whiteContentSection right wow fadeInRight"" data-wow-duration=""2s"">"
            End If

            retstr += "<div class=""contentSection""><h2>" & reader("Title").ToString() & "</h2>"
            retstr += "<p>" & reader("SmallDetails").ToString() & "</p><a class=""readMore"" href=""" & Session("domainName") & Session("lang") & "/stay-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>More Information</a>"
            retstr += "</div><span class=""swoosh""><img src=""" & Session("domainName") & "ui/media/dist/inspiration/swoosh.png"" alt=""" & reader("Title").ToString() & """></span></div>"
            retstr += Utility.showEditButton(Request, "/Admin/A-Stay/ResortEdit.aspx?lid=" & reader("ListID")) & "</li>"
            i = i + 1
        End While
        reader.Close()
        selectString = "SELECT Title, SmallDetails, BigImage, MasterID  from HTML where MasterID=@MasterID and Lang=@Lang"
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd1.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 4
        cmd1.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader1 As Data.SqlClient.SqlDataReader = cmd1.ExecuteReader()

        While reader1.Read()
            retstr += "<li style=""background-image:url('" & Session("domainName") & "Admin/" & reader1("BigImage").ToString() & "')""" & ">"
            If i Mod 2 = 0 Then
                retstr += "<div class=""whiteContentSection wow fadeInLeft"" data-wow-duration=""2s"">"
            Else
                retstr += "<div class=""whiteContentSection right wow fadeInRight"" data-wow-duration=""2s"">"
            End If

            retstr += "<div class=""contentSection""><h2>" & reader1("Title").ToString() & "</h2>"
            retstr += "<p>" & reader1("SmallDetails").ToString() & "</p><a class=""readMore"" href=""" & Session("domainName") & Session("lang") & "/book-your-stay"">More Information</a>"
            retstr += "</div><span class=""swoosh""><img src=""" & Session("domainName") & "ui/media/dist/inspiration/swoosh.png"" alt=""" & reader1("Title").ToString() & """></span></div>"
            retstr += Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hmlid=4") & "</li>"
            i = i + 1
        End While
        reader1.Close()
        conn.Close()

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
