﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="saadiyat-details.aspx.vb" Inherits="saadiyat_details" %>

<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>
<%@ Register Src="~/CustomControl/UserHTMLTxt.ascx" TagPrefix="uc1" TagName="UserHTMLTxt" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated saadiyat innerdetail">
        <div class="heading">
            <span>Your
                <br />
                <b>Saadiyat</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server"  />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName")  %>'>Home</a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/your-saadiyat" %>'>Your Saadiyat</a>
                </li>
                <li class="active"><%= title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>


    <div class="main-content-area ">
       <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
        <div class="row">
            <div class="col-sm-8">
                 <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
            </div>

            <asp:Literal ID="lblMap" runat="server"></asp:Literal>
               
        </div>
        <span class="clickHereButton downLoadListTrigger" data-wow-iteration="100">Downloadable Documents <span>
            <img src='<%= Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"  %>' alt=""></span></span>
        <div class="downloadListContainer">

            <div class="contentSection">

                <asp:Literal ID="lblAddFile" runat="server"></asp:Literal>

                <ul class="listings">

                    <asp:Literal ID="lblMediafiles" runat="server"></asp:Literal>
                </ul>

            </div>
        </div>
        <div class="row">
            <%= getInsideDistricts() %>
        </div>

        <div class="islandbox">
            <h3 class="subtitle">What's on the Island
                    </h3>
            <%= Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=List_Saadiyat&TID=" & hdnID.Value) %>
            <div class="islandimg smaller-slider">
                <ul class="list-unstyled slides">
                   <%= LoadCollage("List_Saadiyat",hdnID.Value)%>
                </ul>
               
            </div>
             
            <asp:Literal ID="lblSecondDetails" runat="server"></asp:Literal>
        </div>
        
        <%= getSpecialFeatres() %>
        <!-- Featured Bullet Listings -->
       
        <div class="awards">
            <h3 class="subtitle">Saadiyat in Numbers: 
                   </h3>
            <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt1"  MasterID="11" ShowEdit="True"/>
            <h3 class="subtitle">Awards and Accreditations
                    </h3>
            
            <uc1:UserHTMLTxt runat="server" ID="UserHTMLTxt2" MasterID="10" ShowEdit="true" />
            
            <div class="mainTimelineContainer">
                
                            <div id="horiz_container_outer" class="timeLineContainer">

                                <div id="horiz_container_inner">
                                
                                    <div id="horiz_container" class="timeline">
                                        
                                        <div class="circles">
                                              <%= TimeLineTags("List_Saadiyat",hdnID.Value) %>
                                          
                                        </div>
                                        
                                        <span class="bar"></span>

                                    </div>

                                </div>

                            </div>
               <asp:Literal ID="ltrAdd" runat="server"></asp:Literal>
                            <div id="scrollbar">
                                <a id="left_scroll" class="mouseover_left" href="#"></a>
                                <div id="track">
                                     <div id="dragBar"></div>
                                </div>
                                <a id="right_scroll" class="mouseover_right" href="#"></a>
                            </div>

                        <asp:Literal ID="lblTimelines" runat="server"></asp:Literal>
    
                    

                    </div>
        </div>


        <div class="row">

            <div class="col-sm-6">
                <div class="gallerybox">
                    <h2 class="subtitle yellow">Images and <span>Video</span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc1:UserGalleryControl runat="server" ID="UserGalleryControl" Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />
                        </ul>
                    </div>
                </div>
            </div>

            <!-- Testimonial Section -->
            <asp:Panel ID="pnlVgallery" runat="server">
            <div class="col-sm-6">
                <h2 class="subtitle">Video <span>Section</span>
                </h2>
                <!-- Video Slider Section -->
                <div class="detailVideoSlider video-flexsliders">
                    <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid) %>
                    <ul class="slides">
                        
                         
                        <asp:Literal ID="lblVideoGal" runat="server"></asp:Literal>
                    </ul>

                </div>
                <!-- Video Slider Section -->
            </div>
</asp:Panel>
            <!-- Testimonial Section -->

        </div>
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

