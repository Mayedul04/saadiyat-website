﻿
Partial Class Intro
    Inherits System.Web.UI.Page
    Protected Function GetCurrentClass(ParamArray pageNames As String()) As String
        Dim virtualPath As String = Request.Url.ToString.ToLower()
        
        For Each elem As String In pageNames
            If virtualPath.Contains(elem.ToLower()) Then
                Return "active"
            End If
        Next
        Return ""
    End Function

    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Session("lang") = "" Then
            Session("lang") = "en"
        End If
    End Sub
End Class
