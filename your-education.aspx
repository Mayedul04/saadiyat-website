﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="your-education.aspx.vb" Inherits="your_education" %>

<%@ Register Src="~/CustomControl/UserHTMLSmallText.ascx" TagPrefix="uc1" TagName="UserHTMLSmallText" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
     <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                   <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <%= Language.Read("Your", Page.RouteData.Values("lang"))%><span><%= Language.Read("Education", Page.RouteData.Values("lang"))%></span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= domainName %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a></li>
                        <li class="active"><%= Title %></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                   <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </div>

                <!-- Inspiration Landing List -->
                <ul class="parallaxlisting list-unstyled">

                  <%= getInstituteList() %>

                </ul>

                <!-- Inspiration Landing List -->
                  <uc1:DynamicSEO runat="server" ID="DynamicSEO" />

            </div>
</asp:Content>

