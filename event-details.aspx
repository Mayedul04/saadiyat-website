﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="event-details.aspx.vb" Inherits="event_details" %>

<%@ Register src="~/F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">

            <!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>

                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a></li>
                        <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area ">
                <h2 class="maintitle">
                    <asp:Literal ID="ltrH2" runat="server"></asp:Literal>
                </h2>

                <ul class="timings">
                    <li><span>Host</span> <asp:Literal ID="ltrHost" runat="server"></asp:Literal></li>
                    <li><span>Venue:</span> <asp:Literal ID="ltrVenue" runat="server"></asp:Literal></li>
                </ul>
                
                <span class="clearAll"></span>

                <p>
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </p>

                <a href='javascript:;' class='clickHereButton downLoadListTrigger' data-wow-iteration='100'>Downloadable Information <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>
                <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>

                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
            </div>
            <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

