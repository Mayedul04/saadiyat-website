﻿
Partial Class your_education
    Inherits System.Web.UI.Page
    Public domainName As String
    Protected lang As String = ""
    Public Title As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Language.Lang = Nothing
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("1", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")


        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "1"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & domainName & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getInstituteList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ListID, Title, BigImage, SmallDetails,MasterID from List_Education where  Lang=@Lang and status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li style=""background-image:url('" & domainName & "Admin/" & reader("BigImage").ToString() & "')""" & ">"
            If i Mod 2 = 0 Then
                retstr += "<div class=""whiteContentSection wow fadeInLeft"" data-wow-duration=""2s"">"
            Else
                retstr += "<div class=""whiteContentSection right wow fadeInRight"" data-wow-duration=""2s"">"
            End If
            If reader("MasterID").ToString() = "3" Then
                retstr += "<div class=""contentSection""><h2>" & FormateTitle(reader("Title").ToString()) & "</h2>"
            Else

                retstr += "<div class=""contentSection""><h2>" & reader("Title").ToString() & "</h2>"
            End If

            retstr += "<p>" & reader("SmallDetails").ToString() & "</p><a class=""readMore"" href=""" & domainName & Session("lang") & "/education-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """>" & Language.Read("More Information", Page.RouteData.Values("lang")) & "</a>"
            retstr += "</div><span class=""swoosh""><img src=""" & domainName & "ui/media/dist/environment/swoosh.png"" alt=""" & reader("Title").ToString() & """></span></div>"
            retstr += Utility.showEditButton(Request, "/Admin/A-Education/InstituteEdit.aspx?lid=" & reader("ListID")) & "</li>"
            i = i + 1
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "</br>" & secondpart
        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
