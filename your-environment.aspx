﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="your-environment.aspx.vb" Inherits="your_environment" %>

<%@ Register src="F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
<!-- Main Banner Section -->
            <div class="section-slider flexslider landingSlider">
                <ul class="list-unstyled slides">
                    <%= getBanners() %>
                </ul>
                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                       <%= Language.Read("Your", Page.RouteData.Values("lang"))%> <span><%= Language.Read("Environment", Page.RouteData.Values("lang"))%></span>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName") & Session("lang") & "/home" %>'><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a></li>
                        <li class="active"><%= Title %></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <div class="intro-block">
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                    
                </div>

                <!-- Inspiration Landing List -->
                <ul class="parallaxlisting list-unstyled">

                    <%= getEnvironmentList() %>

                </ul>
                <!-- Inspiration Landing List -->
                
                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />

            </div>
            <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

