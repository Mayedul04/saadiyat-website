﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MenuControl.ascx.vb" Inherits="CustomControl_MenuControl" %>

    <li class="saadiyat <%= GetCurrentClass("/your-saadiyat") %>">
        <a href='<%= Session("domainName") & Session("lang") &  "/your-saadiyat" %>'><%= Language.Read("Your Saadiyat", Page.RouteData.Values("lang"))%></a>
    </li>
    <li class="inspiration <%= GetCurrentClass("/your-inspiration") %>">
        <a href='<%= Session("domainName") & Session("lang") &  "/your-inspiration" %>'><%= Language.Read("Your Inspiration", Page.RouteData.Values("lang"))%></a>
    </li>
    <li class="leisure  <%= GetCurrentClass("/your-leisure") %>">
        <a href='<%= Session("domainName") & Session("lang") &  "/your-leisure" %>'><%= Language.Read("Your Leisure", Page.RouteData.Values("lang"))%></a>
    </li>
    <li class="home <%= GetCurrentClass("/your-home") %>">
        <a href='<%= Session("domainName") & Session("lang") &  "/your-home" %>'><%= Language.Read("Your Home", Page.RouteData.Values("lang"))%></a>
    </li>
    <li class="education <%= GetCurrentClass("/your-education","/education-details") %>">
        <a href='<%= Session("domainName") & Session("lang") & "/your-education" %>'><%= Language.Read("Your Education", Page.RouteData.Values("lang"))%></a>
    </li>

    <li class="stay <%= GetCurrentClass("/your-stay","/stay-details") %>">
        <a href='<%= Session("domainName") & Session("lang") & "/your-stay" %>'><%= Language.Read("Your Stay", Page.RouteData.Values("lang"))%></a>
    </li>

    <li class="environment <%= GetCurrentClass("/your-environment","/environment-details") %>">
        <a href='<%= Session("domainName") & Session("lang") & "/your-environment" %>'><%= Language.Read("Your Environment", Page.RouteData.Values("lang"))%></a>
    </li>

