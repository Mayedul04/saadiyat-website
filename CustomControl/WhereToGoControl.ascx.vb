﻿
Partial Class CustomControl_WhereToGoControl
    Inherits System.Web.UI.UserControl

    Protected Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Response.Redirect(Session("domainName") & Session("lang") & "/saadiyat-experience/" & ddlOptions.SelectedValue & "/" & Utility.EncodeTitle(ddlOptions.SelectedItem.Text, "-"))
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Page.RouteData.Values("Lang") = "ar" Then
            ddlOptions.Items.Clear()
            ddlOptions.Items.Add(Language.Read("Family Time", Page.RouteData.Values("Lang")))
            ddlOptions.Items(0).Value = "1"
            ddlOptions.Items.Add(Language.Read("Art Lovers", Page.RouteData.Values("Lang")))
            ddlOptions.Items(1).Value = "2"
            ddlOptions.Items.Add(Language.Read("Hanging out with Friends", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "3"
            ddlOptions.Items.Add(Language.Read("Foodies", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "4"
            ddlOptions.Items.Add(Language.Read("Activity seekers", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "5"
            ddlOptions.Items.Add(Language.Read("Nature Seekers", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "6"
            ddlOptions.Items.Add(If(Page.RouteData.Values("Lang") = "ar", "التعليم", "Education"))
            ddlOptions.Items(2).Value = "7"
            ddlOptions.Items.Add(Language.Read("Dream Homes", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "8"
            ddlOptions.Items.Add(Language.Read("Business Zone", Page.RouteData.Values("Lang")))
            ddlOptions.Items(2).Value = "9"
            btnGo.Text = Language.Read("Search", Page.RouteData.Values("Lang"))
        End If
    End Sub
End Class
