﻿Imports System.Data.SqlClient

Partial Class CustomControl_UserControlEditButton
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public domainName As String
    Public img_Edit As Boolean = False
    Public IMG_Type As String
    Public IMG_Width As String
    Public IMG_Height As String
    Public Txt_Type As String
    Public title_Edit As Boolean = False
    Public text_Edit As Boolean = False
    Public link_Edit As Boolean = False
    Public file_Edit As Boolean = False
    Public pgallery_Edit As Boolean = False
    Public vgallery_Edit As Boolean = False
    Public Property TextType() As String
        Get
            Return Txt_Type
        End Get

        Set(ByVal value As String)
            Txt_Type = value
        End Set
    End Property


    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property HTMLID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property
    Public Property ImageType() As String
        Get
            Return IMG_Type
        End Get

        Set(ByVal value As String)
            IMG_Type = value
        End Set
    End Property
    Public Property ImageEdit() As Boolean
        Get
            Return img_Edit
        End Get

        Set(ByVal value As Boolean)
            img_Edit = value
        End Set
    End Property
    Public Property TitleEdit() As Boolean
        Get
            Return title_Edit
        End Get

        Set(ByVal value As Boolean)
            title_Edit = value
        End Set
    End Property
    Public Property LinkEdit() As Boolean
        Get
            Return link_Edit
        End Get

        Set(ByVal value As Boolean)
            link_Edit = value
        End Set
    End Property
    Public Property FileEdit() As Boolean
        Get
            Return file_Edit
        End Get

        Set(ByVal value As Boolean)
            file_Edit = value
        End Set
    End Property
    Public Property PGalleryEdit() As Boolean
        Get
            Return pgallery_Edit
        End Get

        Set(ByVal value As Boolean)
            pgallery_Edit = value
        End Set
    End Property
    Public Property VGalleryEdit() As Boolean
        Get
            Return vgallery_Edit
        End Get

        Set(ByVal value As Boolean)
            vgallery_Edit = value
        End Set
    End Property
    Public Property TextEdit() As Boolean
        Get
            Return text_Edit
        End Get

        Set(ByVal value As Boolean)
            text_Edit = value
        End Set
    End Property
    Public Property ImageWidth() As String
        Get
            Return IMG_Width
        End Get

        Set(ByVal value As String)
            IMG_Width = value
        End Set
    End Property
    Public Property ImageHeight() As String
        Get
            Return IMG_Height
        End Get

        Set(ByVal value As String)
            IMG_Height = value
        End Set
    End Property
    Function HTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE HtmlID=@HtmlID"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("HtmlID", HTMLID)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()
                Dim returrl As String = ""
                returrl = domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&VideoLink=0&VideoCode=0&Map=0&SecondImage=0"


                If TitleEdit = True Then
                    returrl += "&Title=1"
                Else
                    returrl += "&title=0"
                End If

                If ImageEdit = True Then
                    If IMG_Type = "Small" Then
                        returrl += "&BigImage=0&ImageAltText=0&SmallImage=1&SmallImageWidth=" & IMG_Width & "&SmallImageHeight=" & IMG_Height
                    Else
                        returrl += "&SmallImage=0&ImageAltText=1&BigImage=1&BigImageWidth=" & IMG_Width & "&BigImageHeight=" & IMG_Height
                    End If

                Else
                    returrl += "&smallimage=0&BigImage=0&ImageAltText=0"
                End If

                If TextEdit = True Then
                    If TextType = "Small" Then
                        returrl += "&SmallDetails=1&BigDetails=0"
                    Else
                        returrl += "&SmallDetails=0&BigDetails=1"
                    End If
                Else
                    returrl += "&SmallDetails=0&BigDetails=0"
                End If

                If LinkEdit = True Then
                    returrl += "&link=1"
                Else
                    returrl += "&link=0"
                End If

                If FileEdit = False Then
                    returrl += "&file=0"
                Else
                    returrl += "&file=1"
                End If

                If PGalleryEdit = False Then
                    returrl += "&pg=0"
                Else
                    returrl += "&pg=1"
                End If
                If VGalleryEdit = False Then
                    returrl += "&vg=0"
                Else
                    returrl += "&vg=1"
                End If
                M = Utility.showEditButton(Request, returrl)

            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

End Class
