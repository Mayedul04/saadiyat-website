﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WhereToGoControl.ascx.vb" Inherits="CustomControl_WhereToGoControl" %>

<!-- Wer to go Search Section -->
<div class="werToGoSerchSection">

    <div class="searchSection">

        <div class="contentSection">
            <span class="arrow"></span>

            <a class="closeButton" href="javascript:;">X </a>

            <div class="row">

                <div class="col-md-2">

                    <h2><%= Language.Read("Plan Your <span>Experience</span>",Page.RouteData.Values("Lang")) %></h2>

                </div>

                <div class="col-md-7">

                    <div class="form-group">
                        <asp:DropDownList ID="ddlOptions" class="form-control" runat="server">
                            <asp:ListItem Value="1">Family Time</asp:ListItem>
                            <asp:ListItem Value="2">Art Lovers</asp:ListItem>
                            <asp:ListItem Value="3">Hanging out with Friends</asp:ListItem>
                            <asp:ListItem Value="4">Foodies</asp:ListItem>
                            <asp:ListItem Value="5">Activity seekers</asp:ListItem>
                            <asp:ListItem Value="6">Nature Seekers</asp:ListItem>
                            <asp:ListItem Value="7">Education</asp:ListItem>
                            <asp:ListItem Value="8">Dream Homes</asp:ListItem>
                            <asp:ListItem Value="9">Business Zone</asp:ListItem>
                        </asp:DropDownList>


                    </div>

                </div>

                <div class="col-md-3">
                    <asp:Button ID="btnGo" class="submitButton" runat="server" Text="Search" />


                </div>

            </div>

        </div>

    </div>

</div>
<!-- Wer to go Search Section -->
