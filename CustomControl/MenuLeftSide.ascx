﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MenuLeftSide.ascx.vb" Inherits="CustomControl_MenuLeftSide" %>

<ul>
                <li class="active"><a href="<%= Session("domainName") &  Session("lang") & "/home" %>"><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/buy-lease-properties" %>'><%= Language.Read("Buy or lease properties", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/investment-opportunities"%>'><%= Language.Read("Investment opportunites", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/interactive-map" %>' class="mappop" data-fancybox-type="iframe"><%= Language.Read("Saadiyat masterplan", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a class="werToGoTrigger" href="javascript:;"><%= Language.Read("Plan your Experience", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%=  Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"%>'><%= Language.Read("CSR", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href="/calender-popup.aspx?lang=<%=  Session("lang") %>" class="calenderPopup" data-fancybox-type="iframe"><%= Language.Read("Events", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/news-subscribtion" %>' class="newsPopup" data-fancybox-type="iframe"><%= Language.Read("Register for News", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/media-center" %>'><%= Language.Read("Media Centre", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/gallery" %>'><%= Language.Read("Gallery", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a class="socialPopup" href='<%= Session("domainName") & Session("lang") & "/social" %>' data-fancybox-type="iframe"><%= Language.Read("Social", Page.RouteData.Values("lang"))%></a>
                </li>
                <li>
                    <a href='<%= Session("domainName") & Session("lang") & "/contact-us" %>'><%= Language.Read("Contact us", Page.RouteData.Values("lang"))%></a>
                </li>
            </ul>