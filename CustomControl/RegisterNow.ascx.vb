﻿Imports System.Net
Imports System.IO

Partial Class CustomControl_RegisterNow
    Inherits System.Web.UI.UserControl

    Dim tableName_ As String = ""
    Dim tableID_ As String = ""
    Dim masterID_ As String = ""
    Dim lang_ As String = ""
    Dim url_ As String = ""
    
    Public Property TableID() As String
        Get
            Return tableID_
        End Get

        Set(ByVal value As String)
            tableID_ = value
            hdnTableID.Value = value
        End Set
    End Property

    Public Property TableName() As String
        Get
            Return tableName_
        End Get

        Set(ByVal value As String)
            tableName_ = value
            hdnTableName.Value = value
        End Set
    End Property

    Public Property MasterID() As String
        Get
            Return masterID_
        End Get

        Set(ByVal value As String)
            masterID_ = value
            hdnMasterID.Value = value
        End Set
    End Property

    Public Property Lang() As String
        Get
            Return lang_
        End Get

        Set(ByVal value As String)
            lang_ = value
            hdnLang.Value = value
        End Set
    End Property

    Public Property URL() As String
        Get
            Return url_
        End Get

        Set(ByVal value As String)
            url_ = value
            hdnURL.Value = value
        End Set
    End Property

    '^[1?(-?\d{3})-?)?(\d{3})(-?\d{4}]{0,7}$
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        URL = Request.Url.ToString()
        
        If Session("Lang") = "ar" Then
            txtFName.Attributes.Add("placeholder", "الإسم الأول")
            txtLName.Attributes.Add("placeholder", "اسم العائلة")
            txtEmail.Attributes.Add("placeholder", "البريد الالكتروني")
            txtMessage.Attributes.Add("placeholder", "رسالة")
            btnSubmit.Text = "إرسال"
            If TableName <> "HTML" Then
                ddlPType.Items.Clear()
                ddlPType.Items.Add(Language.Read("Product Type", Page.RouteData.Values("Lang")))
                ddlPType.Items(0).Value = ""
                ddlPType.Items.Add(Language.Read("Jawaher Saadiyat", Page.RouteData.Values("Lang")))
                ddlPType.Items(0).Value = "194661"
                ddlPType.Items.Add(Language.Read("Mamsha Al Saadiyat", Page.RouteData.Values("Lang")))
                ddlPType.Items(1).Value = "194991"
                ddlPType.Items.Add(Language.Read("Saadiyat Beach Residences", Page.RouteData.Values("Lang")))
                ddlPType.Items(2).Value = "194883"
                ddlPType.Items.Add(Language.Read("Saadiyat Beach Villas", Page.RouteData.Values("Lang")))
                ddlPType.Items(3).Value = "194661"
                ddlPType.Items.Add(Language.Read("The Residences at St. Regis", Page.RouteData.Values("Lang")))
                ddlPType.Items(3).Value = "194823"
            Else
                ddlPType.Items.Clear()

                ddlPType.Items.Add(Language.Read("Product Type", Page.RouteData.Values("Lang")))
                ddlPType.Items(0).Value = ""
                ddlPType.Items.Add(Language.Read("Saadiyat Cultural District", Page.RouteData.Values("Lang")))
                ddlPType.Items(1).Value = "194991"
                ddlPType.Items.Add(Language.Read("Saadiyat Beach District", Page.RouteData.Values("Lang")))
                ddlPType.Items(2).Value = "194661"
                ddlPType.Items.Add(Language.Read("Saadiyat Marina", Page.RouteData.Values("Lang")))
                ddlPType.Items(3).Value = "194885"
            End If
        Else
            If TableName = "HTML" Then
                ddlPType.Items.Clear()

                ddlPType.Items.Add(Language.Read("Product Type", Page.RouteData.Values("Lang")))
                ddlPType.Items(0).Value = ""
                ddlPType.Items.Add(Language.Read("Saadiyat Cultural District", Page.RouteData.Values("Lang")))
                ddlPType.Items(1).Value = "194991"
                ddlPType.Items.Add(Language.Read("Saadiyat Beach District", Page.RouteData.Values("Lang")))
                ddlPType.Items(2).Value = "194661"
                ddlPType.Items.Add(Language.Read("Saadiyat Marina", Page.RouteData.Values("Lang")))
                ddlPType.Items(3).Value = "194885"

            End If
        End If
        
    End Sub
    Function WRequest(URL As String, method As String, POSTdata As String) As String
        Dim responseData As String = ""
        Try
            Dim cookieJar As New Net.CookieContainer()
            Dim hwrequest As HttpWebRequest = CType(WebRequest.Create(URL), HttpWebRequest)
            'Dim hwrequest As Net.HttpWebRequest = Net.WebRequest.Create(URL)
            hwrequest.CookieContainer = cookieJar
            hwrequest.Accept = "*/*"
            hwrequest.AllowAutoRedirect = True
            hwrequest.UserAgent = "http_requester/1.1"
            hwrequest.Timeout = 60000
            hwrequest.Method = method
            If hwrequest.Method = "POST" Then
                hwrequest.ContentType = "application/x-www-form-urlencoded"
                Dim encoding As New Text.ASCIIEncoding() 'Use UTF8Encoding for XML requests
                Dim postByteArray() As Byte = encoding.GetBytes(POSTdata)
                hwrequest.ContentLength = postByteArray.Length
                Dim postStream As Stream = hwrequest.GetRequestStream()
                postStream.Write(postByteArray, 0, postByteArray.Length)
                postStream.Close()
            End If

            Dim hwresponse As WebResponse = hwrequest.GetResponse()
            responseData = New StreamReader(hwresponse.GetResponseStream()).ReadToEnd()
            'If hwresponse.StatusCode = Net.HttpStatusCode.OK Then
            '    Dim responseStream As IO.StreamReader = _
            '      New IO.StreamReader(hwresponse.GetResponseStream())
            '    responseData = responseStream.ReadToEnd()
            'End If
            hwresponse.Close()
        Catch e As Exception
            responseData = "An error occurred: " & e.Message
        End Try
        Return responseData
    End Function
    Public Function getLatestRefferedID() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 1 ReferredTo from RegisterNow order by RegID Desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows = True Then
            reader.Read()
            Dim id As String = reader("ReferredTo").ToString()
            If id = "1070" Then
                retstr = "1218"
            ElseIf id = "1218" Then
                retstr = "1066"
            ElseIf id = "1066" Then
                retstr = "1070"
            End If
        Else
            retstr = "1070"
        End If

        conn.Close()
        Return retstr
    End Function
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        hdnDate.Value = DateTime.Now
        hdnName.Value = txtFName.Text + " " + txtLName.Text
        hdnMobile.Value = txtCountryCode.Text + txtArea.Text + txtMobile.Text

        hdnRefID.Value = getLatestRefferedID()

        If SqlDataSource1.Insert() > 0 Then

            Dim subJ = "Saadiyat: Register Now message"
            Dim FromName = "Saadiyat"
            Dim FromEmail = txtEmail.Text
            Dim ReceiverEmail = "info@tdic.ae"

            Dim CC = ""
            Dim BCC = "mayedul@digitalnexa.com"
            Dim msg = GetRegistrationInfoMessage()
            Dim productid As String = ""
            If ddlPType.SelectedValue = "194661" Then
                productid = "3345"
            ElseIf ddlPType.SelectedValue = "194991" Then
                productid = "3339"
            End If
            Dim bedroom As String = ""
            Dim budget As String = ""
            Dim budget2 As String = ""
            Dim unittype As String = ""
            If ddlPType.SelectedItem.Text = "Saadiyat Cultural District" Or ddlPType.SelectedItem.Text = "Saadiyat Beach District" Or ddlPType.SelectedItem.Text = "Saadiyat Marina" Then
                unittype = "194733"
            Else
                If ddlPType.SelectedItem.Text = "Jawaher Saadiyat" Then
                    bedroom = "79731"
                    budget = "436"
                    budget2 = "442"
                    unittype = "20"
                ElseIf ddlPType.SelectedItem.Text = "Mamsha Al Saadiyat" Then
                    bedroom = "79731"
                    budget = "429"
                    budget2 = "436"
                    unittype = "19"
                ElseIf ddlPType.SelectedItem.Text = "Saadiyat Beach Residences" Then
                    bedroom = "79731"
                    budget = "429"
                    budget2 = "436"
                    unittype = "19"
                ElseIf ddlPType.SelectedItem.Text = "Saadiyat Beach Villas" Then
                    bedroom = "79731"
                    budget = "436"
                    budget2 = "442"
                    unittype = "20"
                ElseIf ddlPType.SelectedItem.Text = "The Residences at St. Regis" Then
                    bedroom = "79731"
                    budget = "429"
                    budget2 = "436"
                    unittype = "19"
                End If
            End If


            hdnName.Value = txtFName.Text + " " + txtLName.Text
            Dim postData As String = ""

            postData = "AccessCode=1261a689f6&GroupCode=2048&TitleID=194787&FirstName=" & HttpUtility.UrlEncode(txtFName.Text) & "&FamilyName=" & HttpUtility.UrlEncode(txtLName.Text) & "&MobileCountryCode=" & HttpUtility.UrlEncode(txtCountryCode.Text) & "&MobileAreaCode=" & HttpUtility.UrlEncode(txtArea.Text) & "&MobilePhone=" & HttpUtility.UrlEncode(txtMobile.Text) & "&TelephoneCountryCode=+971&TelephoneAreaCode=4&Telephone=4257733&Email=" & HttpUtility.UrlEncode(txtEmail.Text) & "&NationalityID=&CompanyID=&Remarks=" & HttpUtility.UrlEncode(txtMessage.Text) & "&RequirementType=91212&ContactType=1&CountryID=65946&StateID=55377&CityID=54798&DistrictID=65296&CommunityID=59927&SubCommunityID=" & HttpUtility.UrlEncode(ddlPType.SelectedValue) & "&PropertyID=" & productid & "&UnitType=" & unittype & "&MethodOfContact=65873&MediaType=79265&MediaName=194672&ReferredByID=1078&ReferredToID=" & hdnRefID.Value & "&DeactivateNotification=&Bedroom=" & bedroom & "&Budget=" & budget & "&Budget2=" & budget2 & "&RequirementCountryID=&ExistingClient=&CompaignSource=&CompaignMedium=&Company=&NumberOfEmployee=&"
            Label1.Text = postData

            Dim wData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/ContactInsert?", "POST", postData)

            If wData.Contains("successfully") Then
                Utility.SendMail(FromName, FromEmail, ReceiverEmail, CC, BCC, subJ, msg)
                Response.Redirect(Session("domainName") & Session("lang") & "/register-now-thankyou")
            Else
                Utility.SendMail(FromName, FromEmail, ReceiverEmail, CC, BCC, subJ, msg)
                Response.Redirect(Session("domainName") & Session("lang") & "/register-now-thankyou")
            End If




        End If






    End Sub

    Private Function GetRegistrationInfoMessage() As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
                "<html xmlns='http://www.w3.org/1999/xhtml' lang='en'>" & _
                "<head>" & _
                "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>" & _
                "<title>Untitled Document</title>" & _
                "</head>" & _
                "<body style='margin:0; padding:0; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>" & _
                "<table width='800' border='0' align='center' cellpadding='0' cellspacing='0'>" & _
                "<tr>" & _
                "<td style='height:30px;'>&nbsp;</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td><table width='740' border='0' align='center' cellpadding='0' cellspacing='0'>" & _
                "<tr>" & _
                "<td>" & _
                "<h2 style='font-family:Arial, Helvetica, sans-serif; font-size:14px; margin:0 0 15px 0; color:#000;'>Contact message</h2>" & _
                "</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='border:1px solid #ccc; '><table width='740' border='0' cellspacing='0' cellpadding='0'>" & _
                "<tr>" & _
                "<td style='padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>Name </td>" & _
                "<td style='padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>" & hdnName.value & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>Email   </td>" & _
                "<td style='padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>" & txtEmail.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>Contact  number    </td>" & _
                "<td style='padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>" & txtMobile.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>Contact for    </td>" & _
                "<td style='padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>" & ddlPType.SelectedItem.Text & "</td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>Message</td>" & _
                "<td style='padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;'>" & txtMessage.Text & "</td>" & _
                "</tr>" & _
                "</table></td>" & _
                "</tr>" & _
                "<tr>" & _
                "<td style='height:30px;'>" & Request.Url.Host & "</td>" & _
                "</tr>" & _
                "</table>" & _
                "</body>" & _
                "</html>"
        Return retVal

    End Function

End Class
