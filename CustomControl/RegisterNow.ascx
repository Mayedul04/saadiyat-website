﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RegisterNow.ascx.vb" Inherits="CustomControl_RegisterNow" %>

<div class="row registerNataglanceContainer">

    <!-- Register Now Box -->
    <div class="col-sm-12">
        <h2 class="subtitle"><%= Language.Read("Register <span>your Interest</span>", Page.RouteData.Values("lang"))%>
        </h2>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <div class="enquirenowbox withType">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group marginBtm">
                                <asp:TextBox ID="txtFName" runat="server" class="form-control" placeholder="First Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* is required " Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtFName" ValidationGroup="registerNowWizad"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group marginBtm">
                                <asp:TextBox ID="txtLName" runat="server" class="form-control" placeholder="Last Name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*  is required " Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtLName" ValidationGroup="registerNowWizad"></asp:RequiredFieldValidator>
                            
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnName" runat="server" />
                    <div class="row marginBtm">
                        <div class="col-md-5">
                            <div class="form-group">
                                <asp:TextBox ID="txtEmail" type="email" runat="server" class="form-control" placeholder="Email Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Email address is required " Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="registerNowWizad"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ErrorMessage="* Email is invalid" Display="Dynamic" SetFocusOnError="true"
                                    ControlToValidate="txtEmail" ValidationGroup="registerNowWizad"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row phones">
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <asp:TextBox ID="txtCountryCode" runat="server" class="form-control" placeholder="+971"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCountryCode"
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="registerNowWizad" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                       <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="Dynamic"
                                            runat="server" ControlToValidate="txtCountryCode" ValidationExpression="^\d{3}$"
                                            ErrorMessage="*" ValidationGroup="registerNowWizad" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                         </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">

                                        <asp:TextBox ID="txtArea" runat="server" class="form-control" placeholder="00"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtArea"
                                            ErrorMessage="RequiredFieldValidator" ValidationGroup="registerNowWizad" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" Display="Dynamic"
                                            runat="server" ControlToValidate="txtArea" ValidationExpression="^\d{2}$"
                                            ErrorMessage="*" ValidationGroup="registerNowWizad" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                         </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtMobile" runat="server" class="form-control" placeholder="0000000"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Mobile number is required " Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMobile" ValidationGroup="registerNowWizad"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                            runat="server" ControlToValidate="txtMobile" ValidationExpression="^\d{7}$"
                                            ErrorMessage="* 7 digit" ValidationGroup="registerNowWizad" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdnMobile" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="ddlPType" class="form-control" runat="server">
                            <asp:ListItem Value="">Product Type</asp:ListItem>
                            <asp:ListItem value="194661">Jawaher Saadiyat</asp:ListItem>
                            <asp:ListItem Value="194991">Mamsha Al Saadiyat</asp:ListItem>
                            <asp:ListItem Value="194883">Saadiyat Beach Residences</asp:ListItem>
                            <asp:ListItem Value="194661">Saadiyat Beach Villas</asp:ListItem>
                            <asp:ListItem Value="194823">The Residences at St. Regis</asp:ListItem>

                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="ddlPType" runat="server" ValidationGroup="registerNowWizad" ErrorMessage="* Product Type is required " Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>

                    </div>

                </div>
                <asp:HiddenField ID="hdnRefID" runat="server" />


                <div class="col-sm-6">
                    <div class="form-group">
                        <asp:TextBox ID="txtMessage" runat="server" class="form-control" TextMode="MultiLine" Rows="4" placeholder="Message"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Message is required " Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMessage" ValidationGroup="registerNowWizad"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Button ID="btnSubmit" runat="server" Text="Send" CssClass="sectionbtn new" ValidationGroup="registerNowWizad" />
                </div>

            </div>
        </div>
    </div>
    <!-- Register Now Box -->

</div>
<asp:SqlDataSource ID="SqlDataSource1" runat="server"
    ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
    DeleteCommand="DELETE FROM [RegisterNow] WHERE [RegID] = @RegID"
    InsertCommand="INSERT INTO [RegisterNow] ([Name], [EmailAddress], [Mobile], [Message], [TableName], [tableID], [MasterID], [Lang], [URL], [RegisterDate],[ReferredTo]) VALUES (@Name, @EmailAddress, @Mobile, @Message, @TableName, @tableID, @MasterID, @Lang, @URL, @RegisterDate,@ReferredTo)"
    SelectCommand="SELECT * FROM [RegisterNow]"
    UpdateCommand="UPDATE [RegisterNow] SET [Name] = @Name, [EmailAddress] = @EmailAddress, [Mobile] = @Mobile, [Message] = @Message, [TableName] = @TableName, [tableID] = @tableID, [MasterID] = @MasterID, [Lang] = @Lang, [URL] = @URL, [RegisterDate] = @RegisterDate WHERE [RegID] = @RegID">
    <DeleteParameters>
        <asp:Parameter Name="RegID" Type="Int32" />
    </DeleteParameters>
    <InsertParameters>
        <asp:ControlParameter ControlID="hdnName" Name="Name" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="txtEmail" Name="EmailAddress"
            PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="txtMessage" Name="Message" PropertyName="Text"
            Type="String" />
        <asp:ControlParameter ControlID="hdnTableName" Name="TableName"
            PropertyName="Value" Type="String" />
        <asp:ControlParameter ControlID="hdnTableID" Name="tableID"
            PropertyName="Value" Type="Int32" />
        <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID"
            PropertyName="Value" Type="Int32" />
        <asp:ControlParameter ControlID="hdnLang" Name="Lang" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="hdnURL" Name="URL" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="hdnDate" Name="RegisterDate"
            PropertyName="Value" Type="DateTime" />
        <asp:ControlParameter ControlID="hdnRefID" Name="ReferredTo" PropertyName="Value" />
    </InsertParameters>
    <UpdateParameters>
        <asp:Parameter Name="Name" Type="String" />
        <asp:Parameter Name="EmailAddress" Type="String" />
        <asp:Parameter Name="Mobile" Type="String" />
        <asp:Parameter Name="Message" Type="String" />
        <asp:Parameter Name="TableName" Type="String" />
        <asp:Parameter Name="tableID" Type="Int32" />
        <asp:Parameter Name="MasterID" Type="Int32" />
        <asp:Parameter Name="Lang" Type="String" />
        <asp:Parameter Name="URL" Type="String" />
        <asp:Parameter Name="RegisterDate" Type="DateTime" />
        <asp:Parameter Name="RegID" Type="Int32" />
    </UpdateParameters>
</asp:SqlDataSource>
<asp:HiddenField ID="hdnDate" runat="server" />
<asp:HiddenField ID="hdnTableID" runat="server" />
<asp:HiddenField ID="hdnTableName" runat="server" />
<asp:HiddenField ID="hdnMasterID" runat="server" />
<asp:HiddenField ID="hdnLang" runat="server" />
<asp:HiddenField ID="hdnURL" runat="server" />
