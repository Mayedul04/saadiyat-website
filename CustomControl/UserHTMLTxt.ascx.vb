﻿Imports System.Data.SqlClient
Partial Class CustomControl_UserHTMLTxt
    Inherits System.Web.UI.UserControl
    Public html_ID As Integer
    Public domainName As String
    Public show_Edit As Boolean = True
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Public Property MasterID() As Integer
        Get
            Return html_ID
        End Get

        Set(ByVal value As Integer)
            html_ID = value
        End Set
    End Property

    Public Property ShowEdit() As Boolean
        Get
            Return show_Edit
        End Get

        Set(ByVal value As Boolean)
            show_Edit = value
        End Set
    End Property
    Function HTMLText() As String
        Dim M As String = ""
        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        Dim sql = "SELECT * FROM HTML WHERE MasterID=@MasterID and Lang=@lang"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("MasterID", MasterID)
        cmd.Parameters.AddWithValue("Lang", Session("lang"))
        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                reader.Read()

                M += ManipulateListing(reader("BigDetails").ToString())

                If show_Edit = True Then
                    M += Utility.showEditButton(Request, domainName & "Admin/A-HTML/HTMLEdit.aspx?hid=" + reader("HTMLID").ToString() + "&SmallImage=0&SmallDetails=0&VideoLink=0&VideoCode=0&Map=0&BigImage=0&Link=0")

                End If
               
            End If
            reader.Close()
            con.Close()
            Return M
        Catch ex As Exception
            Return M
        End Try

    End Function

    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""factslist"">")
        Dim _htmlliRegex As New Regex("<li[^>]*>", RegexOptions.Compiled)
        bigText = _htmlliRegex.Replace(bigText, "<li class=""col-sm-6"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")
        Return bigText
    End Function
End Class
