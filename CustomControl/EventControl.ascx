﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EventControl.ascx.vb" Inherits="CustomControl_EventControl" %>



            
            <!-- Calender Section -->
            <div class="calenderContainerSection">
                
                <div class="calenderContent">

                    <!-- Event & Promotion Navigation -->
                    <ul class="eventPromotionNavigation">
                        <li><a class="events" href="javascript:;">Events</a></li>
                        <li><a class="promotions" href="javascript:;">Promotions</a></li>
                    </ul>
                    <!-- Event & Promotion Navigation -->
                    
                    <!-- Event & Promotion Content Section -->
                    <div class="eventPromoContentContainer">
                        
                        <!-- Event Content Section -->
                        <div class="content-1 events">
                            
                            <div class="contentSection">
                        
                                <span class="arrow"></span>
                                <a class="closeButton" href="javascript:;"> X </a>
                                
                                <!-- Calender Tabber -->
                                <div class="calenderTabber">
                                    
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="form-control">
                                                  <option>January</option>
                                                  <option>February</option>
                                                  <option>March</option>
                                                  <option>April</option>
                                                  <option>May</option>
                                                  <option>June</option>
                                                  <option>July</option>
                                                  <option>August</option>
                                                  <option>September</option>
                                                  <option>October</option>
                                                  <option>November</option>
                                                  <option>December</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-5">
                                            <h2>January</h2>
                                        </div>

                                        <div class="col-md-4">
                                            <ul class="calenderTabNavigation">
                                                <li class="active"><a href="javascript:;">Days</a></li>
                                                <li><a href="javascript:;">Month</a></li>
                                            </ul>
                                        </div>

                                    </div>

                                </div>
                                <!-- Calender Tabber -->
                                
                                <!-- Calender tab Content Section -->
                                <div class="calenderTabContentContainer">
                                    
                                    <!-- Days Section -->
                                    <div class="content-1">
                                        
                                        <!-- Calender Main Wrapper -->
                                        <div class="mainCalenderWrapper">
                                            
                                            <!-- days Section -->
                                            <ul class="days">
                                                
                                                <li>sun</li>
                                                <li>Mon</li>
                                                <li>Tue</li>
                                                <li>Wed</li>
                                                <li>Thu</li>
                                                <li>Fri</li>
                                                <li>Sat</li>

                                            </ul>
                                            <!-- days Section -->
                                            
                                            <!-- Date Section -->
                                            <ul class="dates">
                                                
                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>28</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>29</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>30</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>31</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>1</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>2</h2>
                                                    </div>
                                                    
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>3</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>4</h2>
                                                    </div>
                                                </li>


                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>5</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>6</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>7</h2>
                                                        <h3 class="eventCount">
                                                            7 Events
                                                        </h3>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>8</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>9</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>10</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>11</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>12</h2>
                                                        <h3 class="eventCount">
                                                            4 Events
                                                        </h3>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>13</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>14</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>15</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>16</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>17</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>18</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>19</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>20</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>21</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            8 Events
                                                        </h3>
                                                        <h2>22</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>23</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>24</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>25</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>26</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>27</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>28</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>29</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>30</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>31</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>1</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>2</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>3</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>4</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>5</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>6</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>7</h2>
                                                    </div>
                                                </li>

                                            </ul>
                                            <!-- Date Section -->

                                        </div>
                                        <!-- Calender Main Wrapper -->

                                    </div>
                                    <!-- Days Section -->
                                    
                                    <!-- Month Section -->
                                    <div class="content-2">
                                        
                                        <div class="mainCalenderWrapper">
                                            
                                            <!-- Month Section -->
                                            <ul class="dates month">
                                                
                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>January</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            9 Events
                                                        </h3>
                                                        <h2>February</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            12 Events
                                                        </h3>
                                                        <h2>March</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            15 Events
                                                        </h3>
                                                        <h2>April</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>May</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>June</h2>
                                                    </div>
                                                    
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>July</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            6 Events
                                                        </h3>
                                                        <h2>August</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            14 Events
                                                        </h3>
                                                        <h2>September</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            9 Events
                                                        </h3>
                                                        <h2>October</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>November</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>December</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                            </ul>
                                            <!-- Month Section -->

                                        </div>

                                    </div>
                                    <!-- Month Section -->

                                </div>
                                <!-- Calender tab Content Section -->
                        

                            </div>

                        </div>
                        <!-- Event Content Section -->

                        <!-- Promotion Content Section -->
                        <div class="content-2 promotions">
                            
                            <div class="contentSection">
                        
                                <span class="arrow"></span>
                                <a class="closeButton" href="javascript:;"> X </a>
                                
                                <!-- Calender Tabber -->
                                <div class="calenderTabber">
                                    
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select class="form-control">
                                                  <option>January</option>
                                                  <option>February</option>
                                                  <option>March</option>
                                                  <option>April</option>
                                                  <option>May</option>
                                                  <option>June</option>
                                                  <option>July</option>
                                                  <option>August</option>
                                                  <option>September</option>
                                                  <option>October</option>
                                                  <option>November</option>
                                                  <option>December</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-5">
                                            <h2>January</h2>
                                        </div>

                                        <div class="col-md-4">
                                            <ul class="calenderTabNavigation">
                                                <li class="active"><a href="javascript:;">Days</a></li>
                                                <li><a href="javascript:;">Month</a></li>
                                            </ul>
                                        </div>

                                    </div>

                                </div>
                                <!-- Calender Tabber -->
                                
                                <!-- Calender tab Content Section -->
                                <div class="calenderTabContentContainer">
                                    
                                    <!-- Days Section -->
                                    <div class="content-1">
                                        
                                        <!-- Calender Main Wrapper -->
                                        <div class="mainCalenderWrapper">
                                            
                                            <!-- days Section -->
                                            <ul class="days">
                                                
                                                <li>sun</li>
                                                <li>Mon</li>
                                                <li>Tue</li>
                                                <li>Wed</li>
                                                <li>Thu</li>
                                                <li>Fri</li>
                                                <li>Sat</li>

                                            </ul>
                                            <!-- days Section -->
                                            
                                            <!-- Date Section -->
                                            <ul class="dates">
                                                
                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>28</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>29</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>30</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>31</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>1</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>2</h2>
                                                    </div>
                                                    
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>3</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>4</h2>
                                                    </div>
                                                </li>


                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>5</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>6</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>7</h2>
                                                        <h3 class="eventCount">
                                                            7 Events
                                                        </h3>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>8</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>9</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>10</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>11</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>12</h2>
                                                        <h3 class="eventCount">
                                                            4 Events
                                                        </h3>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>13</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>14</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>15</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>16</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>17</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>18</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>19</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>20</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>21</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            8 Events
                                                        </h3>
                                                        <h2>22</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->

                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>23</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>24</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>25</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>26</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>27</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>28</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>29</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>30</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>31</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>1</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>2</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>3</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>4</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>5</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>6</h2>
                                                    </div>
                                                </li>

                                                <li class="disactive">
                                                    <div class="dateHolder">
                                                        <h2>7</h2>
                                                    </div>
                                                </li>

                                            </ul>
                                            <!-- Date Section -->

                                        </div>
                                        <!-- Calender Main Wrapper -->

                                    </div>
                                    <!-- Days Section -->
                                    
                                    <!-- Month Section -->
                                    <div class="content-2">
                                        
                                        <div class="mainCalenderWrapper">
                                            
                                            <!-- Month Section -->
                                            <ul class="dates month">
                                                
                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>January</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            9 Events
                                                        </h3>
                                                        <h2>February</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            12 Events
                                                        </h3>
                                                        <h2>March</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            15 Events
                                                        </h3>
                                                        <h2>April</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>May</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>June</h2>
                                                    </div>
                                                    
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>July</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            6 Events
                                                        </h3>
                                                        <h2>August</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            14 Events
                                                        </h3>
                                                        <h2>September</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Event 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            9 Events
                                                        </h3>
                                                        <h2>October</h2>
                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h2>November</h2>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="dateHolder">
                                                        <h3 class="eventCount">
                                                            5 Events
                                                        </h3>
                                                        <h2>December</h2>
                                                    </div>

                                                    <!-- Calender Event Pop up -->
                                                    <div class="calenderEventPopup">
                                                        
                                                        <div class="eventContainer">

                                                            <a class="eventPopupCloseButton" href="javascript:;"> X </a>
                                                            
                                                            <div class="contentSection">
                                                                
                                                                <!-- Event Listings -->
                                                                <ul class="eventListings">
                                                                    
                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 1</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 2</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 3</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 4</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="imgHold">
                                                                                    <img src="ui/media/dist/saadiyat/csr/img-1.jpg" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                <h2>Promotion 5</h2>
                                                                                <ul class="timings">
                                                                                    <li><span>Host</span> Name</li>
                                                                                    <li><span>Venue:</span> Saadiyat Bech Club</li>
                                                                                </ul>
                                                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
                                                                                <a href="javascript:;" class="clickHereButton">
                                                                                    More Information <span><img alt="" src="ui/media/dist/icons/readmore-arrow.png"></span>
                                                                                </a>
                                                                                <a href="javascript:;" class="clickHereButton downLoadListTrigger">Downloadable Content <span><img src="ui/media/dist/icons/readmore-arrow.png" alt=""></span></a>
                                                                                
                                                                                <!-- Download List Container -->
                                                                                <div class="downloadListContainer">

                                                                                    <div class="contentSection">

                                                                                        <ul class="listings">

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 1
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 2
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 3
                                                                                            </li>

                                                                                            <li>
                                                                                                <span class="icon"><img src="ui/media/dist/inner-imgs/pdf-icon.png" alt=""></span> Downloadable Documents 4
                                                                                            </li>

                                                                                        </ul>

                                                                                    </div>

                                                                                </div>
                                                                                <!-- Download List Container -->
                                                                            </div>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                                <!-- Event Listings -->

                                                            </div>

                                                        </div>

                                                    </div>
                                                    <!-- Calender Event Pop up -->
                                                </li>

                                            </ul>
                                            <!-- Month Section -->

                                        </div>

                                    </div>
                                    <!-- Month Section -->

                                </div>
                                <!-- Calender tab Content Section -->
                        

                            </div>

                        </div>
                        <!-- Promotion Content Section -->

                    </div>
                    <!-- Event & Promotion Content Section -->

                </div>

            </div>
            <!-- Calender Section -->
            