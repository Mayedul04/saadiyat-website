﻿Imports System.Data.SqlClient

Partial Class CustomControl_UserGalleryControl
    Inherits System.Web.UI.UserControl
    Public gal_ID As Integer
    Public domainName As String
    Public gal_rel As String
    Public fancy_class As String
    Public show_Edit As Boolean = False
    Public M As String = ""
    Public counter As Integer = 0

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Public Property Gallery_ID() As Integer
        Get
            Return gal_ID
        End Get

        Set(ByVal value As Integer)
            gal_ID = value
        End Set
    End Property
    Public Property Gallery_Relevency() As String
        Get
            Return gal_rel
        End Get

        Set(ByVal value As String)
            gal_rel = value
        End Set
    End Property

    Public Property Fancybox_Class() As String
        Get
            Return fancy_class
        End Get

        Set(ByVal value As String)
            fancy_class = value
        End Set
    End Property

    Public Property ShowEdit() As Boolean
        Get
            Return show_Edit
        End Get

        Set(ByVal value As Boolean)
            show_Edit = value
        End Set
    End Property
    
    Public Function LoadGallery() As String
        Dim retstr As String = ""
        Dim count As Integer = 1
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Select GalleryID from Gallery where  ParentGalleryID=@PGalleryID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.NVarChar, 50).Value = Gallery_ID
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += IntermediateGallery(reader("GalleryID").ToString())
                
            End While
            If M <> "" Then
                retstr += "<li>" & M & "</li>"
                M = ""
            End If
        Else
            retstr = LoadCurrentGallery(Gallery_ID)
            'If M <> "" Then
            '    retstr += "<li>" & M & "</li>"
            '    M = ""
            'End If
        End If
        cn.Close()
        Return retstr
    End Function
    Public Function IntermediateGallery(ByVal galid As String) As String
        Dim retstr As String = ""
        Dim tempstr As String = ""
        Dim sConn As String
        Dim count As Integer = 1
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Select GalleryID from Gallery where  ParentGalleryID=@PGalleryID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.NVarChar, 50).Value = galid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                tempstr = LoadCurrentGallery(reader("GalleryID").ToString())
                If tempstr <> "" Then
                    retstr += tempstr
                    tempstr = ""
                Else
                    If counter > 15 Then
                        If counter Mod 16 = 0 Then
                            retstr += "<li>" & M & "</li>"
                            M = ""
                        End If
                    End If
                End If


            End While
           
        Else

            retstr = LoadCurrentGallery(galid)
            
        End If
        cn.Close()
        
        Return retstr
    End Function
    Public Function LoadCurrentGallery(ByVal galid As String) As String

        Dim retstr As String = ""

        Dim con = New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString())
        con.Open()
        'and  isnull(Featured, 0) <> 1
        Dim sql = "SELECT * FROM GalleryItem WHERE GalleryID=@GalID and Status=1 and isnull( ItemType,'') <>'Uploaded Video'  order by SortIndex"
        Dim cmd = New SqlCommand(sql, con)
        cmd.Parameters.Clear()
        cmd.Parameters.AddWithValue("GalID", galid)

        Try
            Dim reader = cmd.ExecuteReader()
            If reader.HasRows = True Then
                While reader.Read()

                    If reader("ItemType").ToString() = "Online Video" Then
                        M += "<a href=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?autoplay=1"" class=""videoPopUp fancybox.iframe relativeDiv"" rel=""" & Gallery_Relevency & """>"
                        M += "<img src=""" & reader("VideoImageURL").ToString() & """ alt=""" & reader("Title") & """><span class=""playIcon""></span></a>"
                    Else
                        M += "<a href=""" & domainName & "PhotoPop.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("GalleryItemID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ class=""photopopup fancybox.iframe"" rel=""" & Gallery_Relevency & """>"
                        M += "<img src=""" & domainName & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title") & """></a>"

                    End If

                    counter += 1

                    If counter Mod 16 = 0 Then
                        retstr += "<li>" & M & "</li>"
                        M = ""
                    End If



                End While


            End If
            If M <> "" Then
                retstr += "<li>" & M & "</li>"
                M = ""
            End If
            reader.Close()
            con.Close()
            Return retstr
        Catch ex As Exception
            con.Close()
            Return retstr
        End Try
    End Function
End Class
