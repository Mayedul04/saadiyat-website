﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="your-leisure-details.aspx.vb" Inherits="your_leisure_content" %>

<%@ Register src="~/F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>
<%@ Register src="~/CustomControl/UserGalleryControl.ascx" tagname="UserGalleryControl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">

    <div class=" fadeInLeft animated leisure innerdetail">
        <div class="heading">
            <span><%= Language.Read("Your", Page.RouteData.Values("lang"))%> <br /><b><%= Language.Read("Leisure", Page.RouteData.Values("lang"))%></b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers"><img src="/ui/media/dist/elements/down.png" alt=""></a>
        <div class="panel-content nano">
            <div class="scroller nano-content">                    
                <ul class="list-unstyled">
                    <asp:Literal ID="ltrList" runat="server"></asp:Literal>  
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">

<!-- Main Banner Section -->
            <div class="section-slider flexslider">
                <ul class="list-unstyled slides">                    
                    <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>"><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/your-leisure" %>'><%= Language.Read("Your", Page.RouteData.Values("lang"))%> <%= Language.Read("Leisure", Page.RouteData.Values("lang"))%></a>
                        </li>
                        <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

                <!-- <span class="badge wow pulse" data-wow-iteration="100">Sales | Lease</span> -->
            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <!-- Main Content Section -->
            <div class="main-content-area">

                <h2 class="maintitle"><asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></h2>

                <span class="clearAll"></span>

                <!-- Top Logo Container -->
                <div class="topLogoContainer">
                    
                    <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                    

                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
                </div>
                <!-- Top Logo Container -->


                <!-- Featured Bullet Listings -->
                <asp:Literal ID="ltrFeature" runat="server"></asp:Literal>
                <!-- Featured Bullet Listings -->
                  <asp:Panel ID="pnlDownloadbale" runat="server">
                       <asp:Literal ID="ltrLink" runat="server"></asp:Literal>
                <a href='javascript:;' class='clickHereButton downLoadListTrigger' data-wow-iteration='100'><%= Language.Read("Downloadable Documents", Page.RouteData.Values("lang"))%> <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>
                
                 <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>    
                <!-- Download List Container -->
                
                </asp:Panel>

                <!-- Map & Gallery -->
                <div class="row">

                    <div class="col-sm-6">
                        <div class="gallerybox">
                            <h2 class="subtitle yellow">
                                <%= Language.Read("Images", Page.RouteData.Values("lang"))%> <%= Language.Read("and", Page.RouteData.Values("lang"))%> <span>Video</span> 
                                <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                            </h2>
                            <div class="flexsliders">
                                <ul class="list-unstyled gallerysection slides">
                                    <uc2:UserGalleryControl ID="UserGalleryControl1" runat="server" Gallery_Relevency="Gallery" />
                                </ul>
                            </div>
                        </div>
                    </div>
                    <%= getfirstFooter() %>
                    


                </div>
                <!-- Map & Gallery -->


                <!-- contact Info & Video Section -->
                <div class="row marginBtm20">
                    <%= getSecondFooter() %>
                   <%-- <!-- EnQuire Now Box -->
                    <div class="col-sm-6">
                        <h2 class="subtitle">
                            Quick <span>Contact</span>
                        </h2>

                        <!-- Contact Detail Section -->
                        <div class="contactDetailsSection">

                            <ul class="contactListings">

                                <asp:Literal ID="ltrContact" runat="server"></asp:Literal>

                            </ul>



                        </div>
                        <!-- Contact Detail Section -->

                    </div>
                    <!-- EnQuire Now Box -->

                    <!-- Testimonial Section -->
                    <div class="col-sm-6">
                        <h2 class="subtitle">
                            Video <span>Section</span>
                        </h2>
                         <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & currentGalleryID) %>
                        <!-- Video Slider Section -->
                        <div class="detailVideoSlider video-flexsliders">
                         <ul class="slides">
                        <asp:Literal ID="ltrVideo" runat="server"></asp:Literal>
                             </ul>
                            </div>
                        <!-- Video Slider Section -->
                    </div>
                    <!-- Testimonial Section -->--%>

                </div>
                <!-- contact Info & Video Section -->

                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
            </div>
            <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

