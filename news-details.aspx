﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="news-details.aspx.vb" Inherits="news_details" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span>Media
                <br />
                <b>Centre</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftNav("25", "news/press-release")%>
                    <%= getLeftNav("26", "news/features")%>
                    <%= getLeftNav("27", "news/new")%>
                    <%= getLeftNav("28", "media-image-request")%>
                    <%= getLeftNav("29", "social-details")%>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
     <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                News
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'>Home</a>
                </li>
                 <li><a href='<%= Session("domainName")  & Session("lang") & "/news/new"%>'>News</a>
                </li>
                <li class="active"><%= Title %> </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">
        <h2 class="maintitle">
                    <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                </h2>
                
                <span class="clearAll"></span>

               <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
               <asp:Literal ID="lblExtLink" runat="server"></asp:Literal>
                
    </div>
    <!-- Main Content Section -->
     <uc1:dynamicseo runat="server" id="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

