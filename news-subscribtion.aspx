﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="news-subscribtion.aspx.vb" Inherits="news_subscribtion" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="/ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server" target="_parent">
    <!-- Register Section -->
    <div class="checkAvailabityWidget mediaRequestForm newsLetterPopup">

        <h2 class="subtitle">
           <%= Language.Read("Register For News", Page.RouteData.Values("lang"))%> 
        </h2>
        
        <div class="row formRowMargin">
            
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><%= Language.Read("First Name", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName" 
                                            ErrorMessage="RequiredFieldValidator" 
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>

                                        </label>
                                        
                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control" ></asp:TextBox>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><%= Language.Read("Last Name", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName" 
                                            ErrorMessage="RequiredFieldValidator" 
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        </label>
                                        <asp:TextBox ID="txtLastName" runat="server" class="form-control" ></asp:TextBox>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label"><%= Language.Read("Email", Page.RouteData.Values("lang"))%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtEmail" 
                                            ErrorMessage="RequiredFieldValidator" 
                        ValidationGroup="Subscribe" Display="Dynamic" SetFocusOnError="true">* Required</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                            ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="Subscribe" 
                                            ErrorMessage="RegularExpressionValidator" SetFocusOnError="True" 
                                            
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email</asp:RegularExpressionValidator>
                                        </label>
                                        <asp:TextBox ID="txtEmail" runat="server" class="form-control" ></asp:TextBox>
                </div>
            </div>

        </div>


        <p>
        <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal></p>
        <asp:HiddenField ID="hdnContactDatetime" runat="server" />
                            <asp:Button ID="btnSubmit" runat="server" Text="Subscribe" CssClass="submitButton" ValidationGroup="Subscribe" />
        
        <asp:SqlDataSource ID="sdsSubscribtion" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            DeleteCommand="DELETE FROM [NewsletterSubscription] WHERE [NewSubID] = @NewSubID" 
            InsertCommand="INSERT INTO [NewsletterSubscription] ([FirstName], [LastName], [Email], [SubscriptionDate]) VALUES (@FirstName, @LastName, @Email, @SubscriptionDate)" 
            SelectCommand="SELECT [NewSubID], [FirstName], [LastName], [Email], [SubscriptionDate] FROM [NewsletterSubscription]" 
            UpdateCommand="UPDATE [NewsletterSubscription] SET [FirstName] = @FirstName, [LastName] = @LastName, [Email] = @Email, [SubscriptionDate] = @SubscriptionDate WHERE [NewSubID] = @NewSubID">
            <DeleteParameters>
                <asp:Parameter Name="NewSubID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtFirstName" Name="FirstName" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtLastName" Name="LastName" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="hdnContactDatetime" Name="SubscriptionDate" 
                    PropertyName="Value" Type="DateTime" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="FirstName" Type="String" />
                <asp:Parameter Name="LastName" Type="String" />
                <asp:Parameter Name="Email" Type="String" />
                <asp:Parameter Name="SubscriptionDate" Type="DateTime" />
                <asp:Parameter Name="NewSubID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

<asp:Label ID="Label1" runat="server" Text=""></asp:Label>
    </div>
    <!-- Register Section -->

    

    </form>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/ui/js/dist/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/ui/js/std/bootstrap.min.js"></script>
    <script src="/ui/js/dist/jquery.jscrollpane.min.js"></script>
    <script src="/ui/js/dist/jquery.panelslider.min.js"></script>
    <script src="/ui/js/dist/wow.min.js"></script>
    <script src="/ui/js/dist/jquery.flexslider.js"></script>
    <script src="/ui/js/dist/jquery.fancybox.js"></script>
    <script src="/ui/js/dist/masonry.pkgd.min.js"></script>
    <script src="/ui/js/dist/jquery.gray.min.js"></script>
    <script src="/ui/js/std/uniform.min.js"></script>
    <script src="/ui/js/dist/jquery.horizontal.scroll.js"></script>
    <script src="/ui/js/dist/uicreep-custom.js"></script>
</body>
</html>
