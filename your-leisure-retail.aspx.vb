﻿
Partial Class your_leisure_content
    Inherits System.Web.UI.Page



    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About]  FROM  [dbo].[List_Leisure] where  Lang=@Lang and Status=1 order by SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()
                If reader("MasterID") = "4" Or reader("MasterID") = "5" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-content/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                ElseIf reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-leisure-retail"
                Else
                    link = Session("domainName") & Session("lang") & "/your-leisure-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = "6", "javascript:;", link) & """>" & _
                       "         <div class=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "thumbnail-box activeBW", "thumbnail-box") & """ >" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = "6" Then
                    currentListID = reader("ListID").ToString()

                    ltrBreadcumTitle.Text = reader("Title").ToString()
                    ltrBigDetails.Text = reader("BigDetails").ToString & Utility.showEditButton(Request, "/Admin/A-Leisure/LeisureEdit.aspx?lid=" & reader("ListID"))

                    currentGalleryID = reader("GalleryID").ToString()

                    With DynamicSEO1
                        .PageType = "List_Leisure"
                        .PageID = currentListID
                    End With



                End If

                i = i + 1
            End While
            conn.Close()


            ltrAccordianContentShopping.Text = Contents("List_Leisure", currentListID, "Shopping")
            ltrAccordianContentConvenience.Text = Contents("List_Leisure", currentListID, "Convenience")
            ltrBanner.Text = getBanners("List_Leisure", currentListID)
        End If
    End Sub


    Private Function Contents(tableName As String, tableID As String, category As String) As String
        ltrAdd.Text = Utility.showAddButton(Request, "/admin/A-Content/ContentEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=215&ImageHeight=171&Title=1&Image=1&Text=1&link=1&category=1")

        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText,Link from Contents where TableName=@TableName and TableID=@TableID and category=@category and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        cmd.Parameters.Add("category", Data.SqlDbType.NVarChar, 200).Value = category
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            Dim link = reader("Link").ToString()
            retstr &= "<li class=""col-md-4 item"">" & _
                      "     <div class='swooshHolder'>" & _
                      "          <div class='imgHold'>" & _
                      "              <a href='" & If(link <> "", link, "javascript:;") & "' target='_blank'><img src='/admin/" & reader("Image") & "' alt='" & reader("Title") & "'></a>" & _
                      "              <h2><a href='" & If(link <> "", link, "javascript:;") & "' target='_blank'>" & FormateTitle(reader("Title").ToString()) & "</a></h2>" & _
                      "          </div>" & _
                      "          <span class='swoosh'>" & _
                      "              <img src='/ui/media/dist/saadiyat/swoosh-small.png' alt=''>" & _
                      "          </span>" & _
                      "      </div>" & _
                      "      <div class=""contentTextContainer"">" & reader("DetailText") & "</div>" & _
                      Utility.showEditButton(Request, "/admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=215&ImageHeight=171&Title=1&Image=1&Text=1&link=1&category=1") & _
                      "      " & If(reader("Link").ToString() <> "", "<a href='" & reader("Link") & "' class='clickHereButton' target='_blank'>View Website <span><img alt='' src='/ui/media/dist/icons/readmore-arrow.png'></span></a>", "") & _
                      "</li>"


            counter += 1
        End While

        conn.Close()
        '
        retstr = "<ul class=""diningListings"">" & retstr & "</ul>"
        Return retstr
    End Function


    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function


End Class
