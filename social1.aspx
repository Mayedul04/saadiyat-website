﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageMain.master" AutoEventWireup="false" CodeFile="social.aspx.vb" Inherits="social" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider landingSlider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <span>Social </span>
            </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'>Home</a></li>
                <li class="active">Social </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">

        <ul class="socialWidgetsListings">

            <li class="darkblue">

                <div class="contentContainer">

                    <h2 class="subtitle">Facebook
                            </h2>

                    <div class="facebookWidget">
                        <div class="fb-like-box" data-href="https://www.facebook.com/saadiyatae" width="100%" height="270" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                    </div>

                </div>

            </li>

            <li class="yellow">

                <div class="contentContainer">

                    <h2 class="subtitle">Twitter
                            </h2>

                    <div class="twitterWidget">

                        <a class="twitter-timeline" width="100%" height="270" href="https://twitter.com/saadiyatae" data-widget-id="570867704165171200">Tweets by @saadiyatae</a>
                        <script>!function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https'; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");</script>

                    </div>

                </div>

            </li>

            <li class="blue">

                <div class="contentContainer">

                    <h2 class="subtitle">Instagram 
                       
                      <a href="https://instagram.com/saadiyatae?ref=badge" target="_blank" class="ig-b- ig-b-v-24">
                        <img src="//badges.instagram.com/static/images/ig-badge-view-24.png" alt="Instagram" /></a>  
                    </h2>

                    
                    <div class="instagramSliderSection">
                        <style>
                            .ig-b- {
                                display: inline-block;
                            }

                                .ig-b- img {
                                    visibility: hidden;
                                }

                                .ig-b-:hover {
                                    background-position: 0 -60px;
                                }

                                .ig-b-:active {
                                    background-position: 0 -120px;
                                }

                            .ig-b-v-24 {
                                width: 137px;
                                height: 24px;
                                background: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24.png) no-repeat 0 0;
                            }

                            @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
                                .ig-b-v-24 {
                                    background-image: url(//badges.instagram.com/static/images/ig-badge-view-sprite-24@2x.png);
                                    background-size: 160px 178px;
                                }
                            }
                        </style>
                        <div class="flexslider">

                            <ul class="slides">

                                <%= GetInstagram() %>
                            </ul>

                        </div>

                    </div>
                    <!-- Instagram Slider -->

                </div>

            </li>

            <li class="green">

                <div class="contentContainer">

                    <h2 class="subtitle">Youtube
                            </h2>

                    <!-- Instagram Slider -->
                    <div class="instagramSliderSection">

                        <div class="flexslider">

                            <ul class="slides">

                                <%= GetYoutubeVideos() %>
                            </ul>

                        </div>

                    </div>
                    <!-- Instagram Slider -->

                </div>

            </li>

        </ul>


    </div>
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
    <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

