﻿
Partial Class photo_gallery
    Inherits System.Web.UI.Page
    Public Title As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        If Page.RouteData.Values("type") <> "video" Then
            HTML("16", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        Else
            HTML("17", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        End If

        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        hdnID.Value = HTMLID

        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        If Page.RouteData.Values("type") <> "video" Then
            cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 16
        Else
            cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 17
        End If

        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function GalleryCategory() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  *  FROM  GCategory where Status=1 and CatID<8"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        Dim link1 As String
        While reader.Read()
            If Session("lang").ToString() = "en" Then
                link1 = "<a href=""" & Session("domainName").ToString() & Session("lang") & "/gallery/" & Page.RouteData.Values("type") & "/" & reader("CatID") & "/" & Utility.EncodeTitle(reader("DisplayName").ToString(), "-") & """>"
            Else
                link1 = "<a href=""" & Session("domainName").ToString() & Session("lang") & "/gallery/" & Page.RouteData.Values("type") & "/" & reader("CatID") & "/" & Utility.EncodeTitle(reader("ArDisplayName").ToString(), "-") & """>"
            End If

            retstr += "<li class=""col-md-4"">"
            retstr += "<div class=""swooshHolder"">"
            retstr += "<div class=""imgHold"">"
            retstr += link1 & "<img src=""" & Session("domainName").ToString() & "Admin/" & reader("ThumbImage").ToString() & """ alt=""" & reader("DisplayName").ToString() & """></a>"

            If Session("lang").ToString() = "en" Then
                retstr += "<h2>" & link1 & FormateTitle(reader("DisplayName").ToString()) & "</a></h2>"
            Else
                retstr += "<h2>" & link1 & FormateTitle(reader("ArDisplayName").ToString()) & "</a></h2>"
            End If
            retstr += "</div><span class=""swoosh"">"
            retstr += "<img src=""/ui/media/dist/saadiyat/swoosh-small.png"" alt=""""></span></div>" & Utility.showEditButton(Request, "/Admin/A-Gallery/GalleryCategory.aspx?cid=" & reader("CatID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftNav(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang,MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li>"
            If link.Contains(Page.RouteData.Values("type")) Then
                retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><div class=""thumbnail-box activeBW""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            Else
                retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><div class=""thumbnail-box""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            End If

            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
