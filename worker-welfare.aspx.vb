﻿
Partial Class worker_welfare
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, Link, MapImage, GalleryID, SecondText from List_Saadiyat where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 8
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                lblDetails.Text = ManipulateListing(reader("BigDetails").ToString()) & Utility.showEditButton(Request, "/Admin/A-Saadiyat/ItemEdit.aspx?lid=" & hdnID.Value)
                link1 = reader("Link").ToString()

                title = reader("Title").ToString()
                galid = reader("GalleryID")

                ' lblMap.Text = "<div class=""col-sm-6""><div class=""sketchbox""><h3 class=""subtitle yellow"">Interactive  <span>Map</span></h3><img src=""" & Session("domainName") & "Admin/" & reader("MapImage").ToString() & """ alt="""" class=""img-responsive""/></div></div>"
            End While
            conn.Close()
            If galid <> 0 Then
                lblVideoGal.Text = GetFeaturedVideos(galid)
                UserGalleryControl1.Gallery_ID = galid
                ltrGalleryAdd.Text = Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid)
            End If
            With DynamicSEO
                .PageType = "List_Saadiyat"
                .PageID = hdnID.Value
            End With
            styledtitle = FormateTitle(title)
            lblAddFile.Text = Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=List_Saadiyat&TID=" & hdnID.Value & "&t=" & Utility.EncodeTitle(title, "-"))
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where MasterID not in (5,6,7,8) and Lang=@Lang and Status=1 order by  SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If hdnID.Value = reader("ListID") Then
                retstr += "<li class=""active"">"
            Else
                retstr += "<li>"
            End If
            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
            End If

            retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function LoadCollage(ByVal tname As String, ByVal tid As Integer) As String
        Dim retstr As String = ""
        Dim substr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT dbo.CollageIteams.IteamID, dbo.CollageIteams.Title, dbo.CollageIteams.SortIndex, dbo.CollageIteams.IteamID, dbo.CollageIteams.CollageImage, dbo.CollageIteams.SortIndex, dbo.CollageIteams.Status, dbo.Collages.Templete FROM  dbo.CollageIteams INNER JOIN dbo.Collages ON dbo.CollageIteams.CollageID = dbo.Collages.ID WHERE  (dbo.Collages.TableID = @TID) AND (dbo.Collages.TableName = @TName) and dbo.CollageIteams.Status=1 order by dbo.CollageIteams.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TName", Data.SqlDbType.NVarChar, 50).Value = tname
        cmd.Parameters.Add("TID", Data.SqlDbType.Int, 32).Value = tid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            If reader("Templete").ToString() = "1" Then
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            ElseIf reader("Templete").ToString() = "2" Then
                retstr += "<li><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
            ElseIf reader("Templete").ToString() = "3" Then
                If reader("SortIndex").ToString() = "3" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            Else
                If reader("SortIndex").ToString() = "2" Then
                    retstr += "<li class=""item twoBox""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                Else
                    retstr += "<li class=""item""><div class=""imgHold""><a href=""" & Session("domainName") & "Collage-Popup.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("IteamID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ rel=""1"" class=""photopopup""><img src=""" & Session("domainName") & "Admin/" & reader("CollageImage").ToString() & """ alt=""" & reader("Title").ToString() & """></a></div></li>"
                End If
            End If


        End While
        conn.Close()
        retstr += Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=" & tname & "&TID=" & tid)
        Return retstr
    End Function
    Public Function LoadHtmlContent(ByVal htmlmasterid As Integer, ByVal imageclass As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT HTMLID, Title ,BigDetails,BigImage,ImageAltText,GalleryID from HTML where MasterID=@MasterID and Lang=@Lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.NVarChar, 50).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()


        While reader.Read()
            retstr += "<div class=""imgHold " & imageclass & """>"
            ' If IsDBNull(reader("GalleryID")) = False Then
            retstr += "<div class=""detailVideoSlider flexsliders""><ul class=""slides"">"
            ' Dim videos As String = GetFeaturedVideos(reader("GalleryID").ToString())
            Dim photos As String = getGalleryItems(reader("GalleryID").ToString())
            Dim videos As String = GetFeaturedVideos(reader("GalleryID").ToString())
            If photos <> "" Then
                retstr += photos & "</ul>" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</div>"
            ElseIf videos <> "" Then
                retstr += videos & "</ul></div>"
            Else
                retstr += Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & reader("GalleryID").ToString()) & "</ul></div>"
            End If

            'Else
            'retstr += "<a class=""photopopup"" rel=""1"" href=""" & Session("domainName") & Session("lang") & "/photo-pop" & reader("HTMLID").ToString() & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-") & """ > "
            'retstr += "<img alt=""" & reader("Title").ToString() & """ src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ ></a>"
            'End If
            retstr += "</div><h3 class=""subtitle yellow"">" & reader("Title").ToString() & "</h3>"
            retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&SmallImage=0&ImageAltText=0&SmallDetails=0&SecondImage=0&File=0&BigImageWidth=485&BigImageHeight=287")
        End While

        conn.Close()

        Return retstr
    End Function
    Public Function Contents(tableName As String, tableID As String) As String

        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText,Link from Contents where TableName=@TableName and TableID=@TableID  and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim count As Integer = 0
        While reader.Read()
            If count = 0 Then
                retstr += "<h2 class=""first"">" & reader("Title").ToString() & "<span class=""arrowDown""></span></h2>"
                retstr += "<div class=""accordianContent first"">"
            Else
                retstr += "<h2>" & reader("Title").ToString() & "<span class=""arrowDown""></span></h2>"
                retstr += "<div class=""accordianContent"">"
            End If
            retstr += "<div class=""row"">"
            If (reader("Image").ToString() <> "") Then
                retstr += "<div class=""col-sm-4""><div class=""imgHold""><a class=""fancybox-thumb"" href=""" & Session("domainName") & "Admin/" & reader("Image") & """>"
                retstr += "<img src=""" & Session("domainName") & "Admin/" & reader("Image") & """ alt=""" & reader("Title") & """></a></div></div>"
                '   retstr += "<div class=""col-sm-8"">" & ManipulateListing(reader("DetailText").ToString()) & "</div>"
                Dim first As String = ""
                Dim second As String = ""
                Dim formateddetails As String = ManipulateListing(reader("DetailText").ToString())
                If formateddetails.Length > 600 Then
                    Dim _htmlRegex As New Regex("</p>", RegexOptions.Compiled)
                    first = _htmlRegex.Split(formateddetails).First
                    second = formateddetails.Substring(first.Length + 4)
                    retstr += "<div class=""col-sm-8"">" & first & "</p>" & "<div class=""extendContents"">" & second & "</div><p><a class=""viewMoreAccordian"" href=""javascript:;""><span>View More</span></a></p>" & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=485&ImageHeight=287&Title=1&Image=1&Text=1&link=0") & "</div>"
                Else
                    retstr += "<div class=""col-sm-8"">" & formateddetails & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=485&ImageHeight=287&Title=1&Image=1&Text=1&link=0") & "</div>"
                End If

            Else
                retstr += "<div class=""col-sm-12"">" & ManipulateListing(reader("DetailText").ToString()) & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?CID=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(reader("Title").ToString()) & "&ImageWidth=485&ImageHeight=287&Title=1&Image=1&Text=1&link=0") & "</div>"
            End If
            retstr += "</div></div>"


            

            count += 1
        End While

        conn.Close()

        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">Saadiyat at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = total / 2 Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr

       
    End Function
    Public Function getAddresses() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Education"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><p>For " & reader("Heading").ToString() & " please email " & reader("Email").ToString() & " or Call:</p>"
            retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            retstr += "<h5>Int’l: " & reader("Phone").ToString() & "</h5>" & Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getDownloadableFiles() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and   Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        '' cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
       
        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName") & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getGalleryItems(ByVal galleryid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,BigImage,Title from GalleryItem where  GalleryID=@GalleryID and ItemType='Image'"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galleryid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                retstr += "<li>" & Utility.showEditButton(Request, Session("domainName") & "/admin/A-Gallery/GalleryItemEdit.aspx?galleryItemId=" & reader("GalleryItemID").ToString())
                retstr += "<a href=""" & Session("domainName").ToString() & "PhotoPop.aspx?lang=" & Page.RouteData.Values("lang") & "&id=" & reader("GalleryItemID").ToString() & "&t=" & Utility.EncodeTitle(reader("Title"), "-") & "&url=" & Server.UrlEncode(Request.Url.ToString()) & """ class=""photopopup fancybox.iframe"" rel=""" & galleryid & """>"
                retstr += "<img alt="""" src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """></a></li>"


            End While
        End If
        
        conn.Close()



        Return retstr
    End Function
    Public Function loadQuote(ByVal tmasterid As Integer) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * from List_Testimonial where ID=@ID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("ID", Data.SqlDbType.Int, 32).Value = tmasterid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <span class=""pull-left quote open"">"
            retstr += " <img src=""" & Session("domainName").ToString() & "ui/media/dist/elements/quote.png"" alt=""""></span>"
            retstr += reader("BigDetails").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Testimonial/TestimonialEdit.aspx?cgid=" & tmasterid & "&image=0") & "<span class=""pull-right quote closeq"">"
            retstr += "<img src=""" & Session("domainName").ToString() & "ui/media/dist/elements/quote-close.png"" alt=""""> </span>"
            retstr += "<h6>" & reader("TestimonialBy").ToString() & "</h6>"


        End While

        conn.Close()
        Return retstr
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    
    Public Function GetFeaturedVideos(ByVal galleryid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryItemID, SmallImage ,Title, VideoEmbedCode, VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = galleryid

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            'retstr += "<li><div class=""videoHolder""><video class=""video"" muted=""muted"" poster=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """>"
            'retstr += "<source src=""" & Session("domainName") & "Admin/" & reader("VideoEmbedCode") & """ type=""video/mp4""></video></div>"
            'retstr += "<h2>" & reader("Title").ToString() & "</h2><a class=""playButton"" href=""javascript:;""><img src=""" & "/ui/media/dist/inner-imgs/play-button.png"" alt=""""></a></li>"

            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""100%"" frameborder=""0"" height=""250""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    
    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle dbluebg"">")
        Return bigText
    End Function
End Class
