﻿
Partial Class event_details
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = " SELECT        List_Event.EventID, List_Event.CategoryID, List_Event.Title,List_Event.HostName,List_Event.Vanue, List_Event.SmallDetails, List_Event.BigDetails, List_Event.SmallImage, List_Event.BigImage, List_Event.ImageAltText, List_Event.MasterID, " & _
                            " List_Event.Lang, List_Event_Details.StartDate, List_Event_Details.EndDate, List_Event_Details.Time " & _
                            " FROM            List_Event INNER JOIN " & _
                            "                 List_Event_Details ON List_Event.MasterID = List_Event_Details.EventMasterID " & _
                            " WHERE        (List_Event.Lang = @Lang) and List_Event.MasterID=@MasterID " & _
                            " Order by List_Event.SortIndex"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = Page.RouteData.Values("id")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            If reader.Read() Then

                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrBanner.Text = "<li><img src='/Admin/" & reader("BigImage") & "' title='" & reader("Title") & "' alt='" & reader("Title") & "' /></li>"
                currentListID = reader("EventID").ToString()
                ltrH1.Text = formatedTitle
                ltrH2.Text = formatedTitle
                ltrBreadcumTitle.Text = reader("Title").ToString()
                ltrBigDetails.Text = ManipulateListing(reader("BigDetails").ToString) & Utility.showEditButton(Request, "/admin/A-Event/EventsEdit.aspx?eventId=" & reader("eventId"))

            End If
            conn.Close()

            With DynamicSEO1
                .PageType = "List_Event"
                .PageID = currentListID
            End With
            getDownloadableFiles("List_Event", currentListID)

        End If
    End Sub

    Public Function getDownloadableFiles(TableName As String, TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName") & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & _
                    Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID") & "&TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                    "</li>"

        End While
        conn.Close()
        retstr = " <div class='downloadListContainer'>" & _
                        "     <div class='contentSection'>" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                        "         <ul class='listings'>" & _
                        retstr & _
                        "         </ul>" & _
                        "     </div>" & _
                        " </div>"


        Return retstr
    End Function

    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function

End Class
