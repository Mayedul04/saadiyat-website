﻿Imports System.Data.SqlClient

Partial Class Collage_Popup
    Inherits System.Web.UI.Page
    Public PageList As String
    Public section As String
    Public shareurl As String = ""

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then

            LoadContent(Request.QueryString("id"))

        End If
    End Sub
    Public Sub LoadContent(ByVal id As Integer)
        Dim sConn As String
        Dim selectString1 As String = ""

        selectString1 = "SELECT * FROM CollageIteams WHERE IteamID=@IteamID "

        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString

        Dim cn As SqlConnection = New SqlConnection(sConn)

        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("IteamID", Data.SqlDbType.Int).Value = id
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                Title = reader("Title").ToString()
                ImgBig.ImageUrl = Session("domainName") & "Admin/" & reader("CollageImage").ToString()
                shareurl = Request.QueryString("url") 'Session("domainName") & Session("lang") & "Photos/Gallery/" & reader("GalleryItemID") & "/" & Title
                'lblDetails.Text = reader("BigDetails").ToString() & Utility.showEditButton(Request, domainName & "Admin/A-Event/EventsEdit.aspx?EventID=" & reader("EventID") & "&Small=0&Big=1&Footer=0")

            End While
        End If
        cn.Close()
    End Sub
End Class
