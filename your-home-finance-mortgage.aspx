﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="your-home-finance-mortgage.aspx.vb" Inherits="your_home_finance_mortgage" %>

<%@ Register src="F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span>Your <br /><b>Home</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers"><img src="/ui/media/dist/elements/down.png" alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <asp:Literal ID="ltrList" runat="server"></asp:Literal>  
            </div>
        </div>
        <!-- -- panel-content ends here -->

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">            
            <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/your-home" %>'>Your Home</a>
                </li>
                <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->
                
    </div>
    <!-- Main Banner Section -->
    <!-- Main Content Section -->
    <div class="main-content-area">

        <h2 class="maintitle">
            <asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal>
        </h2>

        <p><asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal></p>

        <div class="bankpartners">
            <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>                    
        </div>

        <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
    </div>
    <!-- Main Content Section -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

