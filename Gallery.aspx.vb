﻿
Partial Class Gallery
    Inherits System.Web.UI.Page
    Protected lang As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("15", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")


        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
   
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "31"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName").ToString() & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getHTMLContent(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang,MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li class=""col-md-4"">"
            retstr += "<div class=""swooshHolder"">"
            retstr += "<div class=""imgHold"">"
            retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><img src=""" & Session("domainName").ToString() & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """></a>"
            retstr += "<h2><a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """>" & FormateTitle(reader("Title").ToString()) & "</a></h2>"
            retstr += "</div><span class=""swoosh"">"
            retstr += "<img src=""/ui/media/dist/saadiyat/swoosh-small.png"" alt=""""></span></div>" & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=0&BigDetails=0&SmallImageWidth=275&SmallImageHeight=183&BigImageWidth=504&BigImageHeight=336") & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub
End Class
