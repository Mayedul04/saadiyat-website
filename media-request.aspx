﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="media-request.aspx.vb" Inherits="media_request" %>

<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>
<%@ Register Src="~/MyCaptcha.ascx" TagPrefix="uc1" TagName="MyCaptcha" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated home innerdetail">
        <div class="heading">
            <span>Media
                <br />
                <b>Centre</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftNav("25", "news/press-release")%>
                    <%= getLeftNav("26", "news/features")%>
                    <%= getLeftNav("27", "news/new")%>
                    <%= getLeftNav("28", "media-image-request")%>
                    <%= getLeftNav("29", "social-details")%>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= FormateTitle(Title) %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName") %>'>Home</a>
                </li>
                <li class="active"><%= Title %> </li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">

        <asp:Literal ID="lblDetails" runat="server"></asp:Literal>

        <div class="row">

            <div class="col-md-6 col-sm-6">

                <!-- Register Section -->
                <div class="checkAvailabityWidget mediaRequestForm">

                    <h2 class="subtitle">Register
                            </h2>

                    <div class="row formRowMargin">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">
                                    First Name
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="form3" ControlToValidate="txtFName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </label>
                                <asp:TextBox ID="txtFName" class="form-control" runat="server"></asp:TextBox>


                            </div>
                        </div>
                        <div class="col-md-6">
                            
                                <div class="form-group">
                                    <label class="control-label">
                                        Last Name
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form3" ControlToValidate="txtLName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </label>
                                    <asp:TextBox ID="txtLName" runat="server" class="form-control"></asp:TextBox>
                                </div>
                           

                        </div>
                    </div>
                    <div class="formRowMargin">

                        <div class="form-group">
                            <label class="control-label">
                                Email
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form3" ControlToValidate="txtEmail" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="form3" ControlToValidate="txtEmail" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </label>
                            <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                        </div>

                    </div>
                    <div class="formRowMargin">
                        <div class="form-group">
                            <uc1:MyCaptcha runat="server" ID="MyCaptcha" vgroup="form3" />

                            <asp:Label ID="lbCaptchaError" ForeColor="Red" runat="server"></asp:Label>
                        </div>
                    </div>
                    <p>By clicking Sign Up, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use.</p>
                    <asp:Button ID="btnSend" runat="server" ValidationGroup="form3" class="submitButton" Text="Sign Up" />
                    <asp:HiddenField ID="hdnDate" runat="server" />
                    <asp:HiddenField ID="hdnPass" runat="server" />

                </div>
                <asp:SqlDataSource ID="sdsRegister" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    InsertCommand="INSERT INTO [RegisteredUser] ([FName], [LName], [Email], [Pass1], [User1], [RegisteredDate],[Approve]) VALUES (@FName, @LName, @Email, @Pass1, @User1, @RegisteredDate,@Approve)">

                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtFName" Name="FName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtLName" Name="LName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnPass" Name="Pass1" PropertyName="Value" Type="String" />
                        <asp:ControlParameter ControlID="txtFName" Name="User1" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnDate" Name="RegisteredDate" PropertyName="Value" Type="DateTime" />
                        <asp:Parameter DefaultValue="True" Name="Approve" />
                    </InsertParameters>

                </asp:SqlDataSource>
                <!-- Register Section -->

            </div>

            <div class="col-md-6 col-sm-6">

                <!-- Login Section -->
                <div class="checkAvailabityWidget mediaRequestForm blue">

                    <h2 class="subtitle">Login
                            </h2>

                    <div class="formRowMargin">

                        <div class="form-group">
                            <label class="control-label">
                                Email
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="form5" ControlToValidate="txtEmail1" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="form4" ControlToValidate="txtEmail1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="form4" ControlToValidate="txtEmail1" ErrorMessage="Invalid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </label>
                            <asp:TextBox ID="txtEmail1" runat="server" class="form-control"></asp:TextBox>
                        </div>

                    </div>

                    <div class="formRowMargin">

                        <div class="form-group">
                            <label class="control-label">
                                Password
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="form4" ControlToValidate="txtPass" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </label>
                            <asp:TextBox ID="txtPass" runat="server" TextMode="Password" class="form-control"></asp:TextBox>
                        </div>

                    </div>
                    <asp:Button ID="btnSubmit" class="submitButton" ValidationGroup="form4" runat="server" Text="Sign In" />
                    <asp:LinkButton ID="btnForgotPass" ValidationGroup="form5" runat="server">Forgot Password?</asp:LinkButton>
                </div>
                <!-- Login Section -->

            </div>

        </div>
    </div>

        <!-- Main Content Section -->
    <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

