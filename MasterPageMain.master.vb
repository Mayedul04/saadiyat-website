﻿
Partial Class MasterPageMain
    Inherits System.Web.UI.MasterPage

    Public domainName As String
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Session.Add("domainName", ConfigurationManager.AppSettings("RedirectUrl").ToString)
        Session.Add("lang", If(Page.RouteData().Values("lang") IsNot Nothing, Page.RouteData().Values("lang"), "en"))
        Dim css1 As New HtmlLink()
        If Session("lang") <> "" Then
            If Session("lang") = "ar" Then
                css1.Href = "ui-ar/stylesheets/css/bootstrap.css"
                css1.Attributes.Add("rel", "stylesheet")
                css1.Attributes.Add("type", "text/css")
                Page.Header.Controls.Add(css1)
            Else
                css1.Href = "ui/stylesheets/css/bootstrap.css"
                css1.Attributes.Add("rel", "stylesheet")
                css1.Attributes.Add("type", "text/css")
                Page.Header.Controls.Add(css1)
            End If
        Else
            css1.Href = "ui/stylesheets/css/bootstrap.css"
            css1.Attributes.Add("rel", "stylesheet")
            css1.Attributes.Add("type", "text/css")
            Page.Header.Controls.Add(css1)
        End If
    End Sub

    Protected Function GetCurrentClass() As String
        Dim virtualPath As String = Request.Url.ToString.ToLower()

        If virtualPath.Contains("/your-education") Or virtualPath.Contains("/education-details") Then
            Return "education"
        ElseIf virtualPath.Contains("/your-environment") Or virtualPath.Contains("/environment-details") Then
            Return "environment"
        ElseIf virtualPath.Contains("/your-stay") Or virtualPath.Contains("/stay-details") Or virtualPath.Contains("/book-your-stay") Then
            Return "stay"
        ElseIf virtualPath.Contains("/your-inspiration") Or virtualPath.Contains("/inspiration-details") Then
            Return "inspiration"
        ElseIf virtualPath.Contains("/your-home") Or virtualPath.Contains("/news") Or virtualPath.Contains("buy-lease-property-list") Or virtualPath.Contains("/buy-lease-properties") Or virtualPath.Contains("/gallery/photo") Or virtualPath.Contains("property") Then
            Return "home"
        ElseIf virtualPath.Contains("/your-leisure") Or virtualPath.Contains("/investment-opportunities") Or virtualPath.Contains("media-center") Then
            Return "leisure"
        ElseIf virtualPath.Contains("/your-saadiyat") Or virtualPath.Contains("/about-saadiyat") Or virtualPath.Contains("/about-tdic") Or virtualPath.Contains("/our-partners") Or virtualPath.Contains("/corporate-social-responsibility") Or virtualPath.Contains("/worker-welfare") Or virtualPath.Contains("/about-districts") Or virtualPath.Contains("/about-abudhabi") Then
            Return "saadiyat"
        End If
        Return ""
    End Function
End Class

