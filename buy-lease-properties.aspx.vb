﻿
Partial Class buy_lease_properties
    Inherits System.Web.UI.Page

    Protected lang As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("19", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        ltrTitle.Text = Title
        ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&File=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")


        With DynamicSEO1
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub

    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = "19"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getYourHomeList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang  where HTML.Lang = @Lang and MasterID in(20,21,22,23) order by MasterID, Languages.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        Dim link As String = ""
        While reader.Read()
            If reader("MasterID") = "23" Then
                link = Session("domainName") & Session("lang") & "/property/23/Residential-Plots"
            Else
                link = Session("domainName") & Session("lang") & "/buy-lease-property-list/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
            End If

            retstr += "<li style=""background-image:url('" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & "')""" & ">"
            If i Mod 2 = 0 Then
                retstr += "<div class=""whiteContentSection wow fadeInLeft animated"" data-wow-duration=""2s"">"
            Else
                retstr += "<div class=""whiteContentSection right wow fadeInRight animated"" data-wow-duration=""2s"">"
            End If

            retstr += "<div class=""contentSection""><h2>" & reader("Title").ToString() & "</h2>"
            retstr += "<p>" & reader("SmallDetails").ToString() & "</p><a class=""readMore"" href=""" & link & """>More Information</a>"
            retstr += "</div><span class=""swoosh""><img src=""" & Session("domainName") & "ui/media/dist/leisure/swoosh.png"" alt=""""></span></div>" & _
                Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=1&BigDetails=0&File=0&SmallImageWidth=275&SmallImageHeight=183&BigImageWidth=1269&BigImageHeight=500") & "</li>"
            i = i + 1
        End While
        conn.Close()
        Return retstr
    End Function

    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub

End Class
