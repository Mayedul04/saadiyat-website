﻿Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class news_subscribtion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Language.Lang = Nothing

        '   btnSubmit.Text = Language.Read("Subscribe", Page.RouteData.Values("lang"))
        btnSubmit.Text = Language.Read("Subscribe", "en")
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("39", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        '' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=0&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        'With DynamicSEO1
        '    .PageType = "HTML"
        '    .PageID = HTMLID
        'End With

    End Sub


    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = "en" 'Page.RouteData.Values("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub



    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.Click
        Dim _htmlRegex As New Regex("http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?", RegexOptions.Singleline Or RegexOptions.IgnoreCase)
        Dim pattern As String = "((https?|ftp)\:\/\/)?" ' SCHEME
        pattern += "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?" ' // User and Pass
        pattern += "([a-z0-9-.]*)\.([a-z]{2,4})" '; // Host or IP
        pattern += "(\:[0-9]{2,5})?" '; // Port 
        pattern += "(\/([a-z0-9+\$_-]\.?)+)*\/?" '; // Path
        pattern += "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?" '; // GET Query
        pattern += "(#[a-z_.-][a-z0-9+\$_.-]*)?" '; // Anchor
        Dim _htmlRegex1 As New Regex(pattern, RegexOptions.Singleline Or RegexOptions.IgnoreCase)

        If Not _htmlRegex1.IsMatch(txtFirstName.Text) Then
            hdnContactDatetime.Value = DateTime.Now

            txtFirstName.Text = _htmlRegex1.Replace(txtFirstName.Text, "")
            txtLastName.Text = _htmlRegex1.Replace(txtLastName.Text, "")

            If sdsSubscribtion.Insert > 0 Then

                Dim m_ToAddress As String = "info@tdic.ae"
                Dim m_CCAddress As String = ""
                Dim m_BCCAddress As String = "mayedul@digitalnexa.com"
                Dim m_EmailFromName As [String] = txtFirstName.Text & " " & txtLastName.Text
                Dim m_EmailSubject As [String] = "Saadiyat: Subscribe Message"
                Dim m_EmailBody As [String] = GetRegistrationInfoMessage()



                'Posting to MasterKey

                ' sendDataCRM()
                Dim postData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactClass=1&Salutation=194787&FirstName=" & HttpUtility.UrlEncode(txtFirstName.Text) & "&FamilyName=" & HttpUtility.UrlEncode(txtLastName.Text) & "&Remarks=&MobileCountryCode=&MobileAreaCode=&MobileNumber=&TelephoneCountryCode=&TelephoneAreaCode=&Telephone=&EmailAddress1=" & HttpUtility.UrlEncode(txtEmail.Text) & "&EmailAddress2=&DirectMarketing=194577~1&NationalityID=&DateofBirth=&Gender=&MaritalStatus=&ResidenceCountryID=&PassportNumber=&PassportIssueDate=&PassportExpiryDate=&IDTypeID=&IDNumber=&"
                Dim wData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/InsertContactWithoutLead?", "POST", postData)
                '  MsgBox(wData)
                If wData.Contains("Success") Then
                    Utility.SendMail(m_EmailFromName, txtEmail.Text, m_ToAddress, m_CCAddress, m_BCCAddress, m_EmailSubject, m_EmailBody)
                    Response.Redirect("http://" & Request.Url.Host & "/" & Page.RouteData.Values("lang") & "/news-subscribtion-thankyou")
                Else

                    Dim doc = new XmlDocument()
                    doc.LoadXml(wData)
                    '  doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText
                    Dim contactId As String = Right(doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText, 6).Replace(".", "")
                    Dim secondpostData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactID=" & contactId & "&ContactEmail=" & HttpUtility.UrlEncode(txtEmail.Text) & "&UpdateValues=194577~1&"

                    Dim secondwData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/UpdateDirectMarketing?", "POST", secondpostData)

                    If secondwData.Contains("successfully") Then
                        Utility.SendMail(m_EmailFromName, txtEmail.Text, m_ToAddress, m_CCAddress, m_BCCAddress, m_EmailSubject, m_EmailBody)
                        Response.Redirect("http://" & Request.Url.Host & "/" & Page.RouteData.Values("lang") & "/news-subscribtion-thankyou")
                    End If
                End If
                '   response.Redirect("http://" & request.Url.Host & "/" & Page.RouteData.Values("lang") & "/news-subscribtion-thankyou")

            End If
        Else
            txtFirstName.Text = _htmlRegex1.Replace(txtFirstName.Text, "")
            txtLastName.Text = _htmlRegex1.Replace(txtLastName.Text, "")
        End If


    End Sub
    Function WRequest(URL As String, method As String, POSTdata As String) As String
        Dim responseData As String = ""
        'Try
        Dim cookieJar As New Net.CookieContainer()
        Dim hwrequest As HttpWebRequest = CType(WebRequest.Create(URL), HttpWebRequest)
        'Dim hwrequest As Net.HttpWebRequest = Net.WebRequest.Create(URL)
        hwrequest.CookieContainer = cookieJar
        hwrequest.Accept = "*/*"
        hwrequest.AllowAutoRedirect = True
        hwrequest.UserAgent = "http_requester/1.1"
        hwrequest.Timeout = 60000
        hwrequest.Method = method
        If hwrequest.Method = "POST" Then
            hwrequest.ContentType = "application/x-www-form-urlencoded"
            Dim encoding As New Text.ASCIIEncoding() 'Use UTF8Encoding for XML requests
            Dim postByteArray() As Byte = encoding.GetBytes(POSTdata)
            hwrequest.ContentLength = postByteArray.Length
            Dim postStream As Stream = hwrequest.GetRequestStream()
            postStream.Write(postByteArray, 0, postByteArray.Length)
            postStream.Close()
        End If

        Dim hwresponse As WebResponse = hwrequest.GetResponse()
        responseData = New StreamReader(hwresponse.GetResponseStream()).ReadToEnd()
        'If hwresponse.StatusCode = Net.HttpStatusCode.OK Then
        '    Dim responseStream As IO.StreamReader = _
        '      New IO.StreamReader(hwresponse.GetResponseStream())
        '    responseData = responseStream.ReadToEnd()
        'End If
        hwresponse.Close()
        'Catch e As Exception
        '    responseData = "An error occurred: " & e.Message
        'End Try
        Return responseData
    End Function


    Private Function GetRegistrationInfoMessage() As String
        Dim retVal As String = ""
        retVal = "<!DOCTYPE html>" & _
"<html lang='en'>" & _
"<head>" & _
"    <meta charset='utf-8'>" & _
"    <title>Royal Rose</title>" & _
"    <meta name='viewport' content='width=device-width, initial-scale=1.0'>" & _
"    <body style='margin:0px; padding:0px;'>" & _
"        <table bgcolor='#ac7e14' width='100%' cellspacing='0' cellpadding='0' border='0' align='center'>" & _
"            <tr>" & _
"                <td style='padding:2px 0;'>" & _
"                    <table bgcolor='#fff' width='610' cellspacing='0' cellpadding='0' border='0' align='center' style='background:#ffffff;'>" & _
"                        <tr>" & _
"                            <td align='center'>" & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>&nbsp;</p>" & _
"                                <table width='90%' cellspacing='0' cellpadding='0' align='center' border='1' style='border:1px solid #eee; border-collapse: collapse; margin:0 0 20px;'>" & _
"                                    <tr bgcolor='#eee'>" & _
"                                        <td colspan='2'>" & _
"                          <div style=""text-align: center;""><a href=""http://" & Request.Url.Host & """><img src=""http://" & Request.Url.Host & "/ui/media/dist/elements/color-logo.png"" width=""165px""  alt=''></a></div>" & _
"                                            <h3 style='font-family:arial; color:#490109; font-size:24px; text-align:center; margin:10px 0;'>Subscribe Details</h3>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Name</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtFirstName.Text & " " & txtLastName.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                    <tr>" & _
"                                        <td width='30%'>" & _
"                                            <h4 style='font-family:arial; font-size:14px; text-align:left; margin:6px;'>Email</span></h3>" & _
"                                        </td>" & _
"                                        <td width='70%'>" & _
"                                            <h5 style='font-family:arial; font-size:14px; text-align:left; font-weight:normal; margin:6px;'>" & txtEmail.Text & "</h5>" & _
"                                        </td>" & _
"                                    </tr>" & _
"                                </table>                                " & _
"                                <p style='font-family:arial; font-size:11px; text-align:center; color:#bbb; margin:5px 0 20px;'>© Copyright " & Date.Now.Year & ". All rights reserved by ~<a href='http://" & Request.Url.Host & "'>Saadiyat</a>~.</p>" & _
"                            </td>" & _
"                        </tr>  " & _
"                    </table>" & _
"                </td>" & _
"            </tr>" & _
"        </table>" & _
"    </body>" & _
"</html>"


        '"                        <tr>" & _
        '"                            <td align='center' style='padding:15px 0 7px;'>" & _
        '"                                <a href=' http://Site-URL.com '><img src='http://" & Request.Url.Host & "/ui/media/dist/email/hilton_logo2.png' width='200' alt=''></a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        '"                        <tr>" & _
        '"                            <td align='center' style='padding:0 0 10px;'>" & _
        '"                                <a style='font-family:arial; color:#490109; text-decoration:; font-size:14px;' href='http://Site-URL.com' target='_blank'>~Site Name~</a>" & _
        '"                            </td>" & _
        '"                        </tr>" & _
        Return retVal
    End Function

End Class
