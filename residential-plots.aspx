﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="residential-plots.aspx.vb" Inherits="residential_plots" %>

<%@ Register src="~/F-SEO/DynamicSEO.ascx" tagname="DynamicSEO" tagprefix="uc1" %>
<%@ Register src="~/CustomControl/RegisterNow.ascx" tagname="RegisterNow" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">

        <div class=" fadeInLeft animated home innerdetail">
            <div class="heading">
                <span>
                    <asp:Literal ID="ltrLeftPanelTitle" runat="server"></asp:Literal></span>
            </div>
            <!-- -- heading ends here -- -->
            <a href="javascript:;" class="scrollers"><img src="/ui/media/dist/elements/down.png" alt="">
            </a>
            <div class="panel-content nano">
                <div class="scroller nano-content">
                    <ul class="list-unstyled">
                        <asp:Literal ID="ltrList" runat="server"></asp:Literal>  
                    </ul>
                </div>
            </div>
            <!-- -- panel-content ends here -->

        </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">


            <!-- Main Banner Section -->
            <div class="section-slider flexslider">
                <ul class="list-unstyled slides">
                    <asp:Literal ID="ltrBanner" runat="server"></asp:Literal>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <asp:Literal ID="ltrH1" runat="server"></asp:Literal>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>">Home</a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/buy-lease-properties" %>'>Buy or lease properties</a>
                        </li>                        
                        <li class="active"><asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->

            </div>
            <!-- Main Banner Section -->
            
            <!-- Main Content Section -->
            <div class="main-content-area relativeDiv">

                <!-- About The Designs Section -->
                <div class="aboutDesignBox">
                    
                    <h2 class="maintitle">
                        <asp:Literal ID="ltrSubTitle" runat="server"></asp:Literal></h2>
                    <div class="imgHold">
                        <asp:Image ID="imgBigImage" runat="server" />
                    </div>

                
                    <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>

                    <!-- Misinary TCab Boxes -->
                    <ul class="misinaryBoxesListings csrMisinaryListing tabBoxes">
                        <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>
                        

                    </ul>
                    <!-- Misinary TCab Boxes -->

                </div>
                <!-- About The Designs Section -->

                
                <%= getSpecialFeatres() %>
                <asp:Literal ID="ltrDownloadFiles" runat="server"></asp:Literal>                            
                
                <!-- Download List Container -->

                                
                <!-- Featured Bullet Listings -->
                <asp:Literal ID="ltrFeature" runat="server"></asp:Literal>
                <!-- Featured Bullet Listings -->

                
                <!-- Enquire Now and Testimonials Section -->
               <%-- <uc3:RegisterNow ID="RegisterNow1" runat="server" />--%>
                <!-- Enquire Now and Testimonials Section -->
                <!-- Map & Gallery -->
                <div class="row">
                    
                    <!-- EnQuire Now Box -->
                    <div class="col-sm-6">
                        <h2 class="subtitle">
                            Quick <span>Contact</span>
                        </h2>
                        
                        <!-- Contact Detail Section -->
                        <div class="contactDetailsSection">
                            <ul class="contactListings">
                                <asp:Literal ID="ltrContact" runat="server"></asp:Literal>
                            </ul>
                        </div>
                        <!-- Contact Detail Section -->
                    </div>
                    <!-- EnQuire Now Box -->

                    <div class="col-sm-6">
                        <h2 class="subtitle yellow">
                            Interactive  <span>Map</span>
                        </h2>
                        <div class="imap">
                            <a href='<%= Session("domainName") & Session("lang") & "/interactive-map/residentials" %>' class="mappop" data-fancybox-type="iframe">
                                <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                            </a>
                        </div>
                    </div>

                </div>
                <!-- Map & Gallery -->


                <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
            </div>
            <!-- Main Content Section -->


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
</asp:Content>

