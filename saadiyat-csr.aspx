﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="saadiyat-csr.aspx.vb" Inherits="saadiyat_csr" %>
<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>
<%@ Register Src="~/F-SEO/DynamicSEO.ascx" TagPrefix="uc1" TagName="DynamicSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">
    <div class=" fadeInLeft animated saadiyat innerdetail">
        <div class="heading">
            <span>Your
                <br />
                <b>Saadiyat</b></span>
        </div>
        <!-- -- heading ends here -- -->
        <a href="javascript:;" class="scrollers">
            <img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt="">
        </a>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <%= getLeftThumbList() %>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->
        <asp:HiddenField ID="hdnID" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">

    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <%= styledtitle %>
                    </h1>
            <ol class="breadcrumb">
                <li><a href='<%= Session("domainName")  %>'>Home</a>
                </li>
                <li><a href='<%=  Session("domainName") & Session("lang") & "/your-saadiyat" %>'>Your Saadiyat</a>
                </li>
                <li class="active"><%= title %></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->

    </div>


    <div class="main-content-area">
        <asp:Literal ID="lblDetails" runat="server"></asp:Literal>
        <%= GetWorkerWelfare() %>

        <a href='<%= Session("domainName") & Session("lang") & "/worker-welfare" %>' class="clickHereButton" >More Information <span>
            <img alt="" src='<%= Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"%>'></span>
        </a>
         
       <ul class="misinaryBoxesListings tabBoxes">
        <%= LoadCollage("List_Saadiyat",hdnID.Value) %>
       </ul>
        <%= Utility.showEditButton(Request, Session("domainName") & "Admin/A-Collage/CollageEdit.aspx?TName=List_Saadiyat&TID=" & hdnID.Value) %>
        <h3 class="subtitle dbluebg">CSR initiatives <span>include:</span>
        </h3>

        <ul class="csrListing">

           <%= Contents("List_Saadiyat",hdnID.Value) %>
        </ul>
        <div>
        <span class="clickHereButton downLoadListTrigger" data-wow-iteration="100">Downloadable Documents <span>
            <img src='<%= Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"  %>' alt=""></span></span>
        
        <div class="downloadListContainer">

            <div class="contentSection">
                
               
                <%= getDownloadableFiles() %>
                    <%--<asp:Literal ID="lblMediafiles" runat="server"></asp:Literal>--%>
               

            </div>
        </div>
            </div>
        <uc1:DynamicSEO runat="server" ID="DynamicSEO" />
        <div class="row">

            <div class="col-sm-6">
                <div class="gallerybox">
                    <h2 class="subtitle yellow">Images and <span>Video</span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc1:UserGalleryControl runat="server" ID="UserGalleryControl" Gallery_ID="54" Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />
                        </ul>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlVgallery" runat="server">
            <div class="col-sm-6">
                <h2 class="subtitle">Video <span>Section</span>
                </h2>
                <!-- Video Slider Section -->
                <div class="detailVideoSlider flexsliders">
                    <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & galid) %>
                    <ul class="slides">
                        <asp:Literal ID="lblVideoGal" runat="server"></asp:Literal>
                      

                    </ul>

                </div>
                <!-- Video Slider Section -->
            </div>
                </asp:Panel>
            <%--<asp:Literal ID="lblMap" runat="server"></asp:Literal>--%>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
     
</asp:Content>

