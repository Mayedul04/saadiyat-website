﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="stay-details.aspx.vb" Inherits="stay_details" %>
<%@ Register Src="~/CustomControl/UserGalleryControl.ascx" TagPrefix="uc1" TagName="UserGalleryControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" Runat="Server">
      <div class=" fadeInLeft animated stay innerdetail">
            <div class="heading">
                <span>Your <br /><b>Stay</b></span>
            </div>
            <!-- -- heading ends here -- -->
            <a href="javascript:;" class="scrollers"><img src='<%= Session("domainName") & "ui/media/dist/elements/down.png" %>' alt=""/>
            </a>
            <div class="panel-content nano">
                <div class="scroller nano-content">
                    <ul class="list-unstyled">
                        <%= getResortThumbList() %>

                    </ul>
                </div>
            </div>
            <!-- -- panel-content ends here -->
<asp:HiddenField ID="hdnID" runat="server" />
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <div class="section-slider flexslider">
                <ul class="list-unstyled slides">
                   <%= getBanners() %>
                </ul>

                <!-- Title and Bread Crumb Section -->
                <div class="titleNBreadcrumbSection">
                    <h1 class="">
                        <%= styledtitle%>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href='<%= Session("domainName")  %>'>Home</a>
                        </li>
                        <li><a href='<%= Session("domainName") & Session("lang") & "/your-stay"%>'>Your Stay</a>
                        </li>
                        <li class="active"><%= title %></li>
                    </ol>
                </div>
                <!-- Title and Bread Crumb Section -->


                                                                                             
            </div>
            <!-- Main Banner Section -->

            <!-- Main Content Section -->
            <div class="main-content-area">

                <h2 class="maintitle">
                   <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                </h2>
                <span class="clearAll"></span>

                <asp:Literal ID="lblDetails" runat="server"></asp:Literal>

                
                <a class="clickHereButton" href='<%= link1 %>' target="_blank">
                    View Website <span><img src='<%= Session("domainName") & "ui/media/dist/icons/readmore-arrow.png" %>' alt=""/></span>
                </a>

              
               
                    <%= getSpecialFeatres() %>
                
                <!-- Featured Bullet Listings -->

                <div class="row checkAvailabiltyNTripadvisorSection">
                    
                    <div class="col-md-8">
                        
                        <!-- Register Section -->
                        <h2 class="subtitle">
                            Check <span> Availability </span>
                        </h2>
                        <div class="checkAvailabityWidget">
                               <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                            <div class="row formRowMargin">
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Check In
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="form3" ControlToValidate="txtCheckIn" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        </label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtCheckIn" class="form-control datepicker checkIn" AutoPostBack="true" runat="server" required></asp:TextBox>
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Check Out
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="form3" ControlToValidate="txtCheckOut" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        </label>
                                        <div class="col-sm-9">
                                            
                                            <asp:TextBox ID="txtCheckOut" class="form-control datepicker checkOut" AutoPostBack="true" runat="server" required></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row formRowMargin">
                                
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Select Hotel</label>
                                        <div class="col-sm-9">
                                             <asp:DropDownList ID="ddlHotels"  class="form-control" runat="server" DataSourceID="sdsHotelList" DataTextField="Title" DataValueField="MasterID"></asp:DropDownList>
                                            <asp:SqlDataSource ID="sdsHotelList" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [MasterID], [Title] FROM [List_Stay] WHERE (([Lang] = @Lang) and Status=1)">
                                                <SelectParameters>
                                                    
                                                    <asp:RouteParameter Name="Lang" RouteKey="lang" Type="String" />
                                                </SelectParameters>
                                             </asp:SqlDataSource>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Nights</label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtNight" class="form-control" runat="server"></asp:TextBox>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
</ContentTemplate>
                        </asp:UpdatePanel>
                            <asp:HiddenField ID="hdnBookingLinkSt" runat="server" />
                            <asp:HiddenField ID="hdnBookingLinkPk" runat="server" />
                         <asp:Button ID="btnCheckAvaibility" runat="server" class="submitButton" ValidationGroup="form3" OnClientClick = "SetTarget();" Text="check availability" />

                        </div>
                        <!-- Register Section -->

                    </div>

                    <div class="col-md-4">
                        <div class="tripAdvisorWidget">
                            <h2 class="subtitle">
                                Trip advisor
                            </h2>
                            <asp:Literal ID="lblTripadvisorcode" runat="server"></asp:Literal>
                        </div>
                    </div>

                </div>



                <!-- Map & Gallery -->
                <div class="row">

                    <div class="col-sm-6">
                        <div class="gallerybox">
                            <h2 class="subtitle yellow">
                                Images and <span>Video</span>
                                <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                            </h2>
                            <div class="flexsliders">
                                <ul class="list-unstyled gallerysection slides">
                         <uc1:UserGalleryControl runat="server" ID="UserGalleryControl1"  Gallery_Relevency="gallery" Fancybox_Class="fancybox-thumb" />
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                   <%= getfirstFooter() %>
                             
                          
                      


                </div>
                <!-- Map & Gallery -->


                <!-- contact Info & Video Section -->
                <div class="row">

                                       

                    <%= getSecondFooter() %>

                </div>
                <!-- contact Info & Video Section -->


            </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" Runat="Server">
    
   
</asp:Content>

