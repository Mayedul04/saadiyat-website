﻿
Partial Class your_home_advantage_card
    Inherits System.Web.UI.Page

    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About]  FROM  [dbo].[List_Home] where  Lang=@Lang and Status=1 order by SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()
                If reader("MasterID") = "6" Then
                    link = Session("domainName") & Session("lang") & "/your-home-advantage-card"
                ElseIf reader("MasterID") = "7" Then
                    link = Session("domainName") & Session("lang") & "/your-home-bank-partners"
                Else
                    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If
                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = "6", "javascript:;", link) & """>" & _
                        "         <div class=""" & If(reader("MasterID") = "6", "thumbnail-box activeBW", "thumbnail-box") & """>" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = "6" Then
                    currentListID = reader("ListID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()
                    ltrBigDetails.Text = reader("BigDetails").ToString & Utility.showEditButton(Request, "/Admin/A-YourHome/YourHomeEdit.aspx?lid=" & reader("ListID"))
                    'hlMap.NavigateUrl = reader("MapCode").ToString
                    'imgMap.ImageUrl = Session("domainName") & "Admin/" & reader("MapImage").ToString()
                    currentGalleryID = reader("GalleryID").ToString()



                    ltrSubTitle.Text = (reader("SubTitle").ToString())

                    imgBigImage.ImageUrl = "/Admin/" & reader("MediumImage")
                    imgBigImage.AlternateText = reader("Title").ToString()

                    With DynamicSEO1
                        .PageType = "List_Home"
                        .PageID = currentListID
                    End With

                End If

                i = i + 1
            End While
            conn.Close()

            If currentGalleryID <> "" Then
                ltrVideo.Text = GetFeaturedVideos(currentGalleryID)
            End If
            ltrAccordianContent.Text = Contents("List_Home", currentListID)
            ltrDownloadFiles.Text = getDownloadableFiles("List_Home", currentListID)
            ltrBanner.Text = getBanners("List_Home", currentListID)
            lblQC.Text = getAddresses(currentListID)
        End If
    End Sub

    Public Function getAddresses(ByVal lid As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID, Heading ,Phone, IntPhone, Email, webSite, Details from ContactDetails where TableName=@TableName and TableID=@TableID and  Lang=@Lang and Status=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Home"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = lid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"
            'If IsDBNull(reader("Phone")) = False Then
            '    retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("IntPhone")) = False Then
            '    retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("Email")) = False Then
            '    retstr += "<h5>Email : <a href=""mailto:" & reader("Email").ToString() & """ target=""_blank"">" & reader("Email").ToString() & "</h5>"
            'End If
            'If IsDBNull(reader("WebSite")) = False Then
            '    retstr += "<h5>Web : <a href=""" & reader("WebSite").ToString() & """ target=""_blank"">" & reader("WebSite").ToString() & "</h5>"
            'End If
            retstr += reader("Details").ToString() & Utility.showEditButton(Request, "/Admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID")) & "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=List_Home&TID=" & lid & "&t=" & Server.UrlEncode(Title)) & retstr
        Return retstr
    End Function
    Public Function getDownloadableFiles(TableName As String, TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName") & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & _
                    Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID") & "&TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                    "</li>"

        End While
        conn.Close()
        If retstr <> "" Then
            retstr = "<a href='javascript:;' class='clickHereButton downLoadListTrigger' data-wow-iteration='100'>Downloadable Documents <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>" & _
                           " <div class='downloadListContainer'>" & _
                           "     <div class='contentSection'>" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                           "         <ul class='listings'>" & _
                           retstr & _
                           "         </ul>" & _
                           "     </div>" & _
                           " </div>"
        End If


        Return retstr
    End Function

    Public Function GetFeaturedVideos(ByVal GalleryID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT galleryItemId,SmallImage ,BigImage,Title, VideoEmbedCode ,VideoVCode from GalleryItem where  GalleryID=@GalleryID and ItemType='Online Video' and Featured=1"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("GalleryID", Data.SqlDbType.Int, 32).Value = GalleryID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><div class=""videoHolder""><iframe title=""YouTube video player"" src=""http://www.youtube.com/embed/" & reader("VideoVCode") & "?"" allowfullscreen="""" width=""445"" frameborder=""0"" height=""250""></iframe></div>"
            retstr += "<h2>" & reader("Title").ToString() & "</h2>" & Utility.showEditButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryitemId=" & reader("GalleryItemID")) & "</li>"

        End While
        conn.Close()

        If retstr <> "" Then
            retstr = "<div class=""col-sm-6""> <h2 class=""subtitle"">Video <span>Section</span></h2><div class=""detailVideoSlider video-flexsliders"">" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & GalleryID) & "<ul class=""slides"">" & retstr & "</ul></div></div>"
        Else
            retstr = "<div class=""col-sm-6""> <h2 class=""subtitle"">Video <span>Section</span></h2><div class=""detailVideoSlider video-flexsliders"">" & Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & GalleryID) & "<ul class=""slides""></ul></div></div>"
        End If

        Return retstr
    End Function


    Private Function Contents(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText,Link from Contents where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            If counter = 0 Then
                retstr = "<li><ul class=""landingBoxListings logoList"">"
            ElseIf counter Mod 8 = 0 Then
                retstr &= "</ul></li><li><ul class=""landingBoxListings logoList"">"
            End If

            retstr &= "<li >" & _
                     "       <div class=""imgHold"">"
            If IsDBNull(reader("Link")) = False Then
                retstr += "<a href=""" & reader("Link") & """ ><img src=""/Admin/" & reader("Image") & """ alt=""" & reader("Title") & """></a>"
            Else
                retstr += "<img src=""/Admin/" & reader("Image") & """ alt=""" & reader("Title") & """>"
            End If

            retstr += "       </div>" & _
                     Utility.showEditButton(Request, "/admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=232&ImageHeight=154&Title=1&Image=1&Text=0&link=1") & _
                     "   </li>"
            counter += 1
        End While
        If counter > 0 Then
            retstr &= "</ul></li>"
        End If
        conn.Close()
        '
        retstr = Utility.showAddButton(Request, "/admin/A-Content/ContentEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text) & "&ImageWidth=232&ImageHeight=154&Title=1&Image=1&Text=0&link=1") & "<ul class=""slides"">" & retstr & "</ul>"
        Return retstr
    End Function

    


    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function


End Class
