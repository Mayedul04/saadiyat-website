﻿
Partial Class our_partners
    Inherits System.Web.UI.Page
    Public title, styledtitle, link1 As String
    Public galid As Integer
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT ListID, Title, SubTitle, BigDetails, Link, MapImage, GalleryID, SecondText from List_Saadiyat where MasterID=@MasterID and Lang=@Lang"
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("MasterID", Data.SqlDbType.Int, 32).Value = 3
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

            While reader.Read()
                hdnID.Value = reader("ListID").ToString()
                lblDetails.Text = ManipulateListing(reader("BigDetails").ToString()) & Utility.showEditButton(Request, "/Admin/A-Saadiyat/ItemEdit.aspx?lid=" & hdnID.Value)
                title = reader("Title").ToString()
                ' galid = reader("GalleryID")
                With DynamicSEO
                    .PageType = "List_Saadiyat"
                    .PageID = hdnID.Value
                End With
            End While
            conn.Close()

            styledtitle = FormateTitle(title)
        End If
    End Sub
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Floor(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Saadiyat"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = hdnID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftThumbList() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 5 ListID, Title, SmallImage, SmallDetails,MasterID from List_Saadiyat where MasterID not in (5,6,7,8) and Lang=@Lang and Status=1 order by  SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li>"

            If reader("MasterID").ToString() = "1" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-saadiyat"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "2" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/Corporate-Social-Responsibility"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "3" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/our-partners"">"
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            ElseIf reader("MasterID").ToString() = "9" Then
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-abudhabi"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            Else
                retstr += "<a href=""" & Session("domainName") & Session("lang") & "/about-tdic"">"
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName") & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("Title").ToString() & """></div>"
            End If


            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function PartnerTypes(tableName As String, tableID As String) As String
        ltrAdd.Text = Utility.showAddButton(Request, "/admin/A-Content/ContentEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=our-partners&ImageWidth=227&ImageHeight=181&Title=1&Image=1&Text=1&link=1&category=1")

        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT distinct category  from Contents where TableName=@TableName and TableID=@TableID  and Status=1 "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            If counter = 0 Then
                retstr += "<li role=""presentation"" class=""active""><a href=""#ptr" & counter & """ aria-controls=""home"" role=""tab"" data-toggle=""tab"">" & reader("category").ToString() & "<span><img alt="""" src=""" & Session("domainName").ToString() & "ui/media/dist/icons/readmore-arrow.png""></span></a></li>"
            Else
                retstr += "<li role=""presentation"" ><a href=""#ptr" & counter & """ aria-controls=""home"" role=""tab"" data-toggle=""tab"">" & reader("category").ToString() & "<span><img alt="""" src=""" & Session("domainName").ToString() & "ui/media/dist/icons/readmore-arrow.png""></span></a></li>"
            End If
            If counter = 0 Then
                lblPartners.Text += "<div role=""tabpanel"" class=""tab-pane fade in active"" id=""ptr" & counter & """><div class=""row"">" & PartnerContents(reader("Category").ToString()) & "</div></div>"
            Else
                lblPartners.Text += "<div role=""tabpanel"" class=""tab-pane fade in"" id=""ptr" & counter & """><div class=""row"">" & PartnerContents(reader("Category").ToString()) & "</div></div>"
            End If

            counter += 1
        End While

        conn.Close()

        Return retstr
    End Function
    Public Function PartnerContents(category As String) As String

        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContentID,Title ,DetailText,Image,ImageAltText,Link from Contents where  category=@category and Status=1 order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("category", Data.SqlDbType.NVarChar, 200).Value = category
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()


        While reader.Read()
            retstr += "<div class=""col-sm-3"">"
            retstr += "<div class=""partner"">"
            retstr += "<div class=""logo"">"
            retstr += "<img src=""" & Session("domainName") & "Admin/" & reader("Image").ToString() & """  width=""222"" alt="""">"
            retstr += " </div><h3>" & reader("Title").ToString() & "</h3>"
            retstr += reader("DetailText").ToString() & Utility.showEditButton(Request, Session("domainName") & "Admin/A-Content/ContentEdit.aspx?cid=" & reader("ContentID").ToString() & "&TName=List_Saadiyat&TID=" & hdnID.Value & "&t=our-partners&ImageWidth=227&ImageHeight=181&Title=1&Image=1&Text=1&link=1&category=1")
            If IsDBNull(reader("Link")) = False Then
                retstr += "<a href=""" & reader("Link") & """ class=""clickHereButton"">View Website <span><img src=""" & Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"" alt=""""></span></a>"
            End If
            retstr += "</div></div>"
        End While

        conn.Close()

        Return retstr
    End Function
    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle dbluebg"">")
        Return bigText
    End Function
End Class
