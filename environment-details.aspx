﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageInner.master" AutoEventWireup="false" CodeFile="environment-details.aspx.vb" Inherits="environment_details" %>

<%@ Register Src="F-SEO/DynamicSEO.ascx" TagName="DynamicSEO" TagPrefix="uc1" %>

<%@ Register Src="CustomControl/UserGalleryControl.ascx" TagName="UserGalleryControl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceLeftNav" runat="Server">

    <div class=" fadeInLeft animated education innerdetail">
        <div class="heading">
            <span><%= Language.Read("Your", Page.RouteData.Values("lang"))%>
                <br />
                <b><%= Language.Read("Environment", Page.RouteData.Values("lang"))%></b></span>
        </div>
        <!-- -- heading ends here -- -->
       <%-- <a href="javascript:;" class="scrollers">
            <img src="/ui/media/dist/elements/down.png" alt="">
        </a>--%>
        <div class="panel-content nano">
            <div class="scroller nano-content">
                <ul class="list-unstyled">
                    <asp:Literal ID="ltrList" runat="server"></asp:Literal>
                </ul>
            </div>
        </div>
        <!-- -- panel-content ends here -->

    </div>
    <!-- column ends here-->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceBody" runat="Server">
    <!-- Main Banner Section -->
    <div class="section-slider flexslider">
        <ul class="list-unstyled slides">
            <%= getBanners() %>
        </ul>

        <!-- Title and Bread Crumb Section -->
        <div class="titleNBreadcrumbSection">
            <h1 class="">
                <asp:Literal ID="ltrH1" runat="server"></asp:Literal>

            </h1>

            <ol class="breadcrumb">
                <li><a href="<%= Session("domainName") &  Session("lang") & "/home" %>"><%= Language.Read("Home", Page.RouteData.Values("lang"))%></a>
                </li>
                <li><a href='<%= Session("domainName") & Session("lang") & "/your-environment" %>'><%= Language.Read("Your", Page.RouteData.Values("lang"))%> <%= Language.Read("Environment", Page.RouteData.Values("lang"))%></a>
                </li>
                <li class="active">
                    <asp:Literal ID="ltrBreadcumTitle" runat="server"></asp:Literal></li>
            </ol>
        </div>
        <!-- Title and Bread Crumb Section -->


    </div>
    <!-- Main Banner Section -->

    <!-- Main Content Section -->
    <div class="main-content-area">

        <asp:Literal ID="ltrBigDetails" runat="server"></asp:Literal>
       
        <%--<span class="clickHereButton downLoadListTrigger" data-wow-iteration="100">Downloadable Documents <span>
            <img src='<%= Session("domainName") & "ui/media/dist/icons/readmore-arrow.png"  %>' alt=""></span></span>
        <div class="downloadListContainer">

            <div class="contentSection">

                <asp:Literal ID="lblAddFile" runat="server"></asp:Literal>

                <ul class="listings">

                    <asp:Literal ID="lblMediafiles" runat="server"></asp:Literal>
                </ul>

            </div>
        </div>--%>
           
        <!-- Accordian Listing Container -->
        <div class="accordianListingcontainer">
            <asp:Literal ID="ltrAccordianContent" runat="server"></asp:Literal>
        </div>
        <!-- Accordian Listing Container -->
        

        <asp:Literal ID="ltrFeature" Visible="false" runat="server"></asp:Literal>

        <!-- Featured Bullet Listings -->
        
         
        <!-- Map & Gallery -->
        <div class="row">

            <div class="col-sm-6">
                <div class="gallerybox">
                    <h2 class="subtitle yellow"><%= Language.Read("Images", Page.RouteData.Values("lang"))%> and<%= Language.Read("", Page.RouteData.Values("lang"))%> <span><%= Language.Read("Video", Page.RouteData.Values("lang"))%></span>
                        <asp:Literal ID="ltrGalleryAdd" runat="server"></asp:Literal>
                    </h2>
                    <div class="flexsliders">
                        <ul class="list-unstyled gallerysection slides">
                            <uc2:UserGalleryControl ID="UserGalleryControl1" runat="server" Gallery_Relevency="Gallery" />
                        </ul>
                    </div>
                </div>
            </div>
            <%= getfirstFooter() %>
           <%-- <asp:Panel ID="pnlVideosecond" Visible="false" runat="server">
            
                <!-- EnQuire Now Box -->
                <div class="col-sm-6">
                    <h2 class="subtitle">Video <span>Section</span>
                    </h2>
                    <!-- Video Slider Section -->
                    <div class="detailVideoSlider video-flexsliders">
                        <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & currentGalleryID) %>
                        <!-- Video Slider Section -->
                        <%= GetFeaturedVideos(currentGalleryID) %>
                        <!-- Video Slider Section -->
                    </div>
                </div>
                <!-- EnQuire Now Box -->

            
            <!-- contact Info & Video Section -->
        </asp:Panel>--%>
            


        </div>
        <!-- Map & Gallery -->
        <div class="row">
            <%= getSecondFooter() %>
            <%--<div class="col-sm-6">
                <asp:Panel ID="pnlMap" runat="server">
                    <h2 class="subtitle yellow">Interactive  <span>Map</span>
                    </h2>
                    <div class="imap">
                        <a href='<%= Session("domainName") & Session("lang") & "/interactive-map" %>' class="mappop" data-fancybox-type="iframe">
                            <img src="/ui/media/dist/home/sbr/map.jpg" alt="">
                        </a>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlVideofirst" Visible="false" runat="server">
                    <h2 class="subtitle">Video <span>Section</span>
                    </h2>
                    <!-- Video Slider Section -->
                    <div class="detailVideoSlider video-flexsliders">
                        <%= Utility.showAddButton(Request, "/admin/A-Gallery/GalleryItemEdit.aspx?galleryId=" & currentGalleryID) %>
                        <!-- Video Slider Section -->
                        <%= GetFeaturedVideos(currentGalleryID) %>
                        <!-- Video Slider Section -->
                    </div>
                </asp:Panel>
            </div>--%>
        </div>

        <!-- contact Info & Video Section -->
        

        <uc1:DynamicSEO ID="DynamicSEO1" runat="server" />
    </div>
    <!-- Main Content Section -->

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlace_script" runat="Server">
</asp:Content>

