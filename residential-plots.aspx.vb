﻿
Partial Class residential_plots
    Inherits System.Web.UI.Page

    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang  where HTML.Lang = @Lang and MasterID in(20,21,22,23) order by MasterID, Languages.SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()

                If reader("MasterID") = "23" Then

                    link = Session("domainName") & Session("lang") & "/property/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                Else
                    link = Session("domainName") & Session("lang") & "/buy-lease-property-list/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If

                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "javascript:;", link) & """>" & _
                       "         <div  class=""" & If(reader("MasterID") = "23", "thumbnail-box activeBW", "thumbnail-box") & """>" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = Page.RouteData.Values("pid") Then
                    currentListID = reader("HTMLID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()


                End If

                i = i + 1
            End While
            conn.Close()

            Contents("9")



        End If
    End Sub




    Private Function Contents(listMasterID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About],ParentHTMLMasterID  FROM  [dbo].[List_Property] where  Lang=@Lang and Status=1 and MasterID=@MasterID order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = listMasterID
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        Dim currentListID As String = "", MasterID As String = ""
        If reader.Read() Then
            currentListID = reader("ListID").ToString()
            'Dim link = Session("domainName") & Session("lang") & "/property/" & reader("ParentHTMLMasterID") & "/" & reader("MasterID") & "/" & Utility.EncodeTitle(ltrBreadcumTitle.Text, "-") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
            ltrH1.Text = FormateTitle(reader("Title").ToString())
            ltrBreadcumTitle.Text = reader("Title").ToString()
            ltrSubTitle.Text = reader("SubTitle").ToString()
            imgBigImage.ImageUrl = Session("domainName").ToString() & "Admin/" & reader("MediumImage")
            imgBigImage.AlternateText = reader("Title").ToString()
            ltrBigDetails.Text = ManipulateListing(reader("BigDetails").ToString()) & Utility.showEditButton(Request, "/admin/A-Properties/PropertyEdit.aspx?lId=" & reader("ListID") & "&ParentHTMLMasterID=" & reader("ParentHTMLMasterID"))
            MasterID = reader("MasterID").ToString()
        End If
        
        conn.Close()
        ltrBanner.Text = getBanners("List_Property", currentListID)

        ltrDownloadFiles.Text = getDownloadableFiles("List_Property", currentListID)

        ltrContact.Text = getAddresses("List_Property", currentListID)

        With DynamicSEO1
            .PageType = "List_Property"
            .PageID = currentListID
        End With

        'With RegisterNow1
        '    .TableName = "List_Property"
        '    .TableID = currentListID
        '    .MasterID = MasterID
        '    .Lang = Session("lang")
        'End With
        Return retstr
    End Function
    Public Function getSpecialFeatres() As String
        Dim total As Integer = 0
        Dim count As Integer = 0
        Dim M As String = ""
        Dim retstr As String = "<h2 class=""subtitle yellow"">Saadiyat Beach golf views at a glance :</h2><div class=""atGlanceContainer""><div class=""row"">"
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FeatureID ,Details from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim selectString1 = "SELECT COUNT(0) from SpecialFeatures where TableName=@TableName and TableID=@TableID and  Lang=@Lang"
        Dim cmdcount As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmdcount.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Property"
        cmdcount.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 17
        cmdcount.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        total = cmdcount.ExecuteScalar

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "List_Property"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = 17
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            M += "<li>" & reader("Details").ToString() & "</li>"
            count += 1
            If count = Math.Ceiling(total / 2) Then
                retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
                M = ""
            End If
        End While
        conn.Close()
        If M <> "" Then
            retstr += "<div class=""col-sm-6""><ul class=""featurelist"" style=""margin-bottom: 0;"">" & M & "</ul></div>"
            M = ""
        End If

        retstr += "</div></div>"
        Return retstr



    End Function
    Public Function getDownloadableFiles(TableName As String, TableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT FileName ,FileID, Title from MediaFiles where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = TableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = TableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><span class=""icon""><a href=""" & Session("domainName") & "Admin/" & reader("FileName").ToString() & """ target=""_blank""><img src=""" & Session("domainName") & "ui/media/dist/inner-imgs/pdf-icon.png"" alt=""""></span> " & reader("Title").ToString() & "</a>" & _
                    Utility.showEditButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?fid=" & reader("FileID") & "&TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                    "</li>"

        End While
        conn.Close()
        If retstr <> "" Then
            retstr = "<a href='javascript:;' class='clickHereButton downLoadListTrigger' data-wow-iteration='100'>Downloadable Documants <span><img src='/ui/media/dist/icons/readmore-arrow.png' alt=''></span></a>" & _
                           " <div class='downloadListContainer'>" & _
                           "     <div class='contentSection'>" & Utility.showAddButton(Request, "/admin/A-Download-Files/MediaFileEdit.aspx?TName=" & TableName & "&TID=" & TableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                           "         <ul class='listings'>" & _
                           retstr & _
                           "         </ul>" & _
                           "     </div>" & _
                           " </div>"
        End If


        Return retstr
    End Function

    Public Function getAddresses(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ContactID,Heading ,Phone, IntPhone, Email from ContactDetails where TableName=@TableName and TableID=@TableID and Status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += "<li><p>" & reader("Heading").ToString() & "</p>"
            retstr += "<h5>UAE: " & reader("Phone").ToString() & "</h5>"
            retstr += "<h5>Int’l: " & reader("IntPhone").ToString() & "</h5>" & _
                Utility.showEditButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?lid=" & reader("ContactID") & "&TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & _
                "</li>"
        End While
        conn.Close()
        retstr = Utility.showAddButton(Request, "/admin/A-Contact-Address/LocationEdit.aspx?TName=" & tableName & "&TID=" & tableID & "&t=" & Server.UrlEncode(ltrBreadcumTitle.Text)) & retstr
        Return retstr
    End Function


    Private Function ManipulateListing(ByVal bigText As String) As String
        Dim _htmlRegex As New Regex("<ul[^>]*>", RegexOptions.Compiled)
        bigText = _htmlRegex.Replace(bigText, "<ul class=""featurelist"">")
        Dim _htmlH3Regex As New Regex("<h3[^>]*>", RegexOptions.Compiled)
        bigText = _htmlH3Regex.Replace(bigText, "<h3 class=""subtitle"">")

        'Dim match = Regex.Match(MS, "<a\s+(?:[^>]*?\s+)?href=""([^""]*)", RegexOptions.IgnoreCase)
        'If match.Success Then
        '    MS = MS.Replace(match.Groups(1).Value, "http://www.nexamail.net/LTrack.aspx?ACID=&lt;:CID:&gt;&amp;EID=&lt;:EID:&gt;&amp;link=" & Server.UrlEncode(match.Groups(1).Value))
        'End If

        Return bigText
    End Function


    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function

End Class
