﻿Imports System.Data.SqlClient

Partial Class media_request_logged
    Inherits System.Web.UI.Page
    Public msg As String = "", Username As String = "", emailid As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("RegUser") Is Nothing Then
            Response.Redirect(Session("domainName") & Session("lang") & "/media-image-request")
        End If
        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML("24", HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ltrBigDetails.Text = SmallDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        LoaduserInfo()


        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function LoadMedia() As String
        Dim retstr As String = ""
        If Not Request.Cookies("SaadiyatSelections") Is Nothing Then
            Dim count As Integer = 1
            Dim sConn As String
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)


            cn.Open()
            Dim selectString1 As String = "SELECT dbo.Gallery.Title as Album, dbo.GCategory.DisplayName, dbo.GalleryItem.Title AS ImgTitle, dbo.GalleryItem.SmallImage FROM  dbo.GalleryItem INNER JOIN dbo.Gallery ON dbo.GalleryItem.GalleryID = dbo.Gallery.GalleryID INNER JOIN dbo.GCategory ON dbo.Gallery.CategoryID = dbo.GCategory.CatID where GalleryItemID in (" & Request.Cookies("SaadiyatSelections").Value & ")"
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)


            


            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read
                   
                    retstr += "<tr><td>" & reader("DisplayName") & "</td>"
                    retstr += "<td>" & reader("Album") & "</td>"
                    retstr += "<td>" & reader("ImgTitle") & "</td>"
                    retstr += "<td><div class=""imgHold""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImgTitle").ToString() & """ style=""width:10% !important;""></td></tr>"
                End While
            End If
            cn.Close()

        End If

        Return retstr
    End Function
    Public Function loadMailBody() As String
        If Not Request.Cookies("SaadiyatSelections") Is Nothing Then
            Dim count As Integer = 1
            Dim sConn As String
            msg = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)


            cn.Open()
            Dim selectString1 As String = "SELECT dbo.Gallery.Title as Album, dbo.GCategory.DisplayName, dbo.GalleryItem.Title AS ImgTitle, dbo.GalleryItem.SmallImage FROM  dbo.GalleryItem INNER JOIN dbo.Gallery ON dbo.GalleryItem.GalleryID = dbo.Gallery.GalleryID INNER JOIN dbo.GCategory ON dbo.Gallery.CategoryID = dbo.GCategory.CatID where GalleryItemID in (" & Request.Cookies("SaadiyatSelections").Value & ")"
            Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)

            msg += "The following Images have been requested from Email: " & emailid
            msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;""> Section Name</td>"
            msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Album</td>"
            msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Image Title</td>"
            msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Image Link</td></tr>"


            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                While reader.Read
                    
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & reader("DisplayName").ToString() & "</td>"
                    msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & reader("Album").ToString() & "</td>"
                    msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & reader("ImgTitle").ToString() & "</td>"
                    msg += "<td style=""padding:10px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & "</td></tr>"
                    
                End While
            End If
            cn.Close()
            msg += "</table></td></tr></table>"
        End If
        Return msg
    End Function
    Public Sub LoaduserInfo()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * from RegisteredUser where RegisterUserID=@RegisterUserID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("RegisterUserID", Data.SqlDbType.Int, 32).Value = Request.Cookies("RegUser").Value
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            Username = reader("FName").ToString() & " " & reader("LName").ToString()
            emailid = reader("Email").ToString()
        End While
        conn.Close()

    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = 24
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getHTMLContent(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang,MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li class=""col-md-4"">"
            retstr += "<div class=""swooshHolder"">"
            retstr += "<div class=""imgHold"">"
            retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """><img src=""" & Session("domainName").ToString() & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """></a>"
            retstr += "<h2><a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """>" & FormateTitle(reader("Title").ToString()) & "</a></h2>"
            retstr += "</div><span class=""swoosh"">"
            retstr += "<img src=""/ui/media/dist/saadiyat/swoosh-small.png"" alt=""""></span></div>" & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & reader("HTMLID") & "&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=0&BigDetails=0&SmallImageWidth=275&SmallImageHeight=183&BigImageWidth=504&BigImageHeight=336") & "</li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub

    Protected Sub btnRequest_Click(sender As Object, e As EventArgs) Handles btnRequest.Click
        Utility.SendMail(Username, emailid, "mayedul@digitalnexa.com", "", "mayedul.islam@wvss.net", "Media Library Request", loadMailBody())
    End Sub
End Class
