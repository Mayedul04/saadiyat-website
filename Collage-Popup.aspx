﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Collage-Popup.aspx.vb" Inherits="Collage_Popup" %>

<!DOCTYPE html>
<html lang="en">

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Saadiyat</title>

    <!-- Bootstrap -->
    <link href="/ui/stylesheets/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
         <div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=376113352542968&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- Pop Up Container -->
    <div class="popUpContainer">

        <div class="titleSection">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h2>
                        <asp:Literal ID="lblTitle" runat="server"></asp:Literal>
                    </h2>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="innerSocialIcons">
                         <li>
                                <div class="g-plusone" data-size="medium" data-href="<%= shareurl %>" data-width="150"></div>
                            </li>
                            <li>
                                
                                <a class="twitter-share-button" data-size="150"
                                    href="<%= shareurl %>">Tweet
                                </a>
                                <script type="text/javascript">
                                    window.twttr = (function (d, s, id) { var t, js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) { return } js = d.createElement(s); js.id = id; js.src = "https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); return window.twttr || (t = { _e: [], ready: function (f) { t._e.push(f) } }) }(document, "script", "twitter-wjs"));
                                </script>
                            </li>
                            <li>
                                <div class="fb-share-button" data-href="<%= shareurl %>" data-layout="button"></div>
                               
                                
                            </li>
                        
                        <li>
                            <a href="javascript:;" target="_blank" style="margin-right:30px;" onclick="window.print(); return false;">
                                <img src="/ui/media/dist/icons/print-icon.png" alt=""></a>
                        </li>
                        
                    </ul>
                </div>
            </div>
        </div>

        <div class="photohold">
            <asp:Image ID="ImgBig" Width="733" runat="server" />
            
        </div>
       
        
    </div>
    <!-- Pop Up Container -->
    </form>
   <script src="/ui/js/dist/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/ui/js/dist/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/ui/js/std/bootstrap.min.js"></script>
    <script src="/ui/js/dist/jquery.jscrollpane.min.js"></script>
    <script src="/ui/js/dist/jquery.panelslider.min.js"></script>
    <script src="/ui/js/dist/wow.min.js"></script>
    <script src="/ui/js/dist/jquery.flexslider.js"></script>
    <script src="/ui/js/dist/jquery.fancybox.js"></script>
    <script src="/ui/js/dist/masonry.pkgd.min.js"></script>
    <script src="/ui/js/dist/jquery.gray.min.js"></script>
    <script src="/ui/js/std/uniform.min.js"></script>
    <script src="/ui/js/dist/uicreep-custom.js"></script>
</body>

</html>
