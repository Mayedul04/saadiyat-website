﻿
Partial Class buy_lease_property_list
    Inherits System.Web.UI.Page

    Public Function getBanners(tableName As String, tableID As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and  TableID=@TableID and status=1 order by SortIndex "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = tableName
        cmd.Parameters.Add("TableID", Data.SqlDbType.NVarChar, 50).Value = tableID
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim selectString = "SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang  where HTML.Lang = @Lang and MasterID in(20,21,22,23) order by MasterID, Languages.SortIndex "
            Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
            Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
            Dim i = 0
            Dim link As String = ""
            Dim currentListID As String = "", currentGalleryID As String = ""

            While reader.Read()
                
                If reader("MasterID") = "23" Then
                    'property/{pid}/{ptitle}
                    link = Session("domainName") & Session("lang") & "/property/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                Else
                    link = Session("domainName") & Session("lang") & "/buy-lease-property-list/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
                End If

                Dim formatedTitle As String = FormateTitle(reader("Title").ToString())
                ltrList.Text &= "<li>" & _
                       "     <a href=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "javascript:;", link) & """>" & _
                       "         <div class=""" & If(reader("MasterID") = Page.RouteData.Values("id"), "thumbnail-box activeBW", "thumbnail-box") & """ >" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("Title").ToString() & """>" & _
                       "         </div>" & _
                       "         <h2 class=""title"">" & formatedTitle & "</h2>" & _
                       "     </a>" & _
                       " </li>"
                If reader("MasterID") = Page.RouteData.Values("id") Then
                    currentListID = reader("HTMLID").ToString()
                    ltrH1.Text = formatedTitle
                    ltrBreadcumTitle.Text = reader("Title").ToString()

                    ltrPropertyList.Text = Contents(reader("MasterID").ToString())

                    
                End If

                i = i + 1
            End While
            conn.Close()

            With DynamicSEO1
                .PageType = "HTML"
                .PageID = currentListID
            End With

            ltrBanner.Text = getBanners("HTML", Page.RouteData.Values("id")) 'for HTML only masterID is passing in banner table

        End If
    End Sub


    

    Private Function Contents(tableID As String) As String

        'Dim retstr As String = ""
        'Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        'conn.Open()
        'Dim masterids As String = ""
        'If tableID = "20" Then
        '    masterids = "2,3,5"
        'ElseIf tableID = "21" Then
        '    masterids = "4,5,9"
        'ElseIf tableID = "22" Then
        '    masterids = "2,9"
        'End If

        'Dim selectString = "SELECT ListID, Title, SmallImage, BigImage, Badge, SmallDetails,MasterID from List_Home where MasterID in (" & masterids & ") and Lang=@Lang and Status=1 order by SortIndex "
        'Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        'Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        'Dim counter As Int16 = 0
        'Dim link As String = ""
        'While reader.Read()
        '    link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
        '    retstr &= "<li>" & _
        '              "      <div class=""csrContentSection"">" & _
        '              "          <div class=""contentPart"">" & _
        '              "              <div class=""imgHold"">" & _
        '              "                  <a href=""" & link & """ ><img src=""/admin/" & reader("SmallImage") & """ alt=""" & reader("Title") & """></a>" & _
        '              "              </div>                                " & _
        '              "              <h2>" & reader("Title").ToString() & Utility.showEditButton(Request, "/Admin/A-YourHome/YourHomeEdit.aspx?lid=" & reader("ListID")) & "- <span>" & reader("Badge").ToString() & "</span></h2>" & _
        '              "              <p>" & reader("SmallDetails") & "</p>" & _
        '              "              <a class=""clickHereButton"" href=""" & link & """>" & _
        '              "                  More Information <span><img src=""/ui/media/dist/icons/readmore-arrow.png"" alt=""""></span>" & _
        '              "              </a>" & _
        '              "          </div>" & _
        '              "      </div>" & _
        '              "  </li>"
        '    counter += 1
        'End While
        'conn.Close()
        'Return retstr

        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  [ListID]      ,[Title]      ,[SubTitle]      ,[SmallDetails]      ,[BigDetails]      ,[SmallImage]      ,[MediumImage]      ,[BigImage]      ,[ImageAltText]      ,[SortIndex]      ,[Status]      ,[LastUpdated]      ,[Link]      ,[Featured]      ,[MasterID]      ,[Lang]      ,[GalleryID]      ,[MapImage]      ,[MapCode]      ,[ViewFloorPlans]      ,[ProximityMap]      ,[Brochure]      ,[LocationMap]      ,[Badge]      ,[About], ParentHTMLMasterID  FROM  [dbo].[List_Property] where  Lang=@Lang and List_Property.Status=1 and ParentHTMLMasterID=@ParentHTMLMasterID order by SortIndex  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ParentHTMLMasterID", Data.SqlDbType.Int).Value = tableID
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim counter As Int16 = 0
        While reader.Read()
            '  Dim link = Session("domainName") & Session("lang") & "/your-home-details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("Title").ToString(), "-")
            Dim link = reader("Link").ToString()
            retstr &= "<li>" & _
                      "      <div class=""csrContentSection"">" & _
                      "          <div class=""contentPart"">" & _
                      "              <div class=""imgHold"">" & _
                      "                  <a href=""" & link & """ ><img src=""/admin/" & reader("SmallImage") & """ alt=""" & reader("Title") & """></a>" & _
                      "              </div>                                " & _
                      "              <h2>" & reader("Title").ToString() & Utility.showEditButton(Request, "/admin/A-Properties/PropertyEdit.aspx?lId=" & reader("ListID") & "&ParentHTMLMasterID=" & reader("ParentHTMLMasterID")) & "- <span>" & reader("Badge").ToString() & "</span></h2>" & _
                      "              <p>" & reader("SmallDetails") & "</p>" & _
                      "              <a class=""clickHereButton"" href=""" & link & """>" & _
                      "                  More Information <span><img src=""/ui/media/dist/icons/readmore-arrow.png"" alt=""""></span>" & _
                      "              </a>" & _
                      "          </div>" & _
                      "      </div>" & _
                      "  </li>"
            counter += 1
        End While
        conn.Close()

        ' retstr = Utility.showAddButton(Request, "/admin/A-Properties/PropertyEdit.aspx?ParentHTMLMasterID=" & tableID) & retstr

        Return retstr
    End Function


    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function

End Class
