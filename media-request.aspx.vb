﻿Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Xml

Partial Class media_request
    Inherits System.Web.UI.Page
    Public Title As String = "", htmlmasterid As String = "28"
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Request.Cookies("RegUser") Is Nothing Then
            Response.Redirect(Session("domainName") & Session("lang") & "/media-image-request-logged")
        End If
        If Session("lang") = "ar" Then
            btnSend.Text = Language.Read("Sign Up", Page.RouteData.Values("lang"))
            btnSubmit.Text = Language.Read("Sign In", Page.RouteData.Values("lang"))

        End If
        Dim HTMLID As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""


        HTML(htmlmasterid, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        'ltrTitle.Text = Title
        ' ltrBigDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        hdnID.Value = HTMLID
        lblDetails.Text = BigDetails & Utility.showEditButton(Request, "/Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=1&SmallDetails=0&BigDetails=1&SmallImageWidth=0&SmallImageHeight=0&BigImageWidth=0&BigImageHeight=0")
        With DynamicSEO
            .PageType = "HTML"
            .PageID = HTMLID
        End With
    End Sub
    Public Function getBanners() As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BannerID, Title, BigImage from Banner where TableName=@TableName and TableID=@TableID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 50).Value = "HTML"
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        While reader.Read()
            retstr += " <li><img src=""" & Session("domainName") & "Admin/" & reader("BigImage").ToString() & """ alt=""" & reader("Title").ToString() & """></li>"
        End While
        conn.Close()
        Return retstr
    End Function
    Public Function getLeftNav(ByVal htmlmasterid As Integer, link As String) As String
        Dim retstr As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  HtmlID, Title,  SmallImage, BigImage, Link, LastUpdated,  ImageAltText,Lang, MasterID  FROM  HTML where MasterID=@MasterID and lang=@lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = htmlmasterid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = Session("lang")
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        Dim i = 0
        While reader.Read()
            retstr += "<li>"
            retstr += "<a href=""" & Session("domainName").ToString() & Session("lang") & "/" & link & """>"
            If Request.Url.AbsoluteUri.Contains(link) Then
                retstr += "<div class=""thumbnail-box activeBW""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            Else
                retstr += "<div class=""thumbnail-box""><img src=""" & Session("domainName").ToString() & "Admin/" & reader("SmallImage").ToString() & """ alt=""" & reader("ImageAltText").ToString() & """/></div>"
            End If

            retstr += "<h2 class=""title"">" & FormateTitle(reader("Title").ToString()) & "</h2></a></li>"

        End While
        conn.Close()
        Return retstr
    End Function
    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        If secondpart <> "" Then
            retstr = "<span>" & firstpart & "</span>" & secondpart
        Else
            retstr = firstpart
        End If

        Return retstr
    End Function
    Private Sub HTML(ByVal MasterID As String, ByRef HtmlID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and lang=@lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = MasterID
        cmd.Parameters.Add("lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            HtmlID = reader("HtmlID").ToString
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""

            ImageAltText = reader("ImageAltText") & ""

        End If
        conn.Close()
    End Sub

    Protected Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        If MyCaptcha.IsValid Then
            hdnDate.Value = Date.Now
            hdnPass.Value = Mid(System.Guid.NewGuid.ToString(), 1, 6)
            If IsExist() = False Then
                If sdsRegister.Insert() > 0 Then
                    Dim msg As String = "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    msg += "<tr><td style=""border:1px solid #ccc;""><table width=""740"" border=""0"" cellspacing=""0"" cellpadding=""0"">From the following Person, we got a media library sign up request.</td>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;""> Name</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtFName.Text & " " & txtLName.Text & "</td></tr>"
                    msg += "<tr><td style=""padding:10px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">Email Address</td>"
                    msg += "<td style=""padding:10px; border-bottom:1px solid #ccc; font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000;"">" & txtEmail.Text & "</td></tr>"

                    msg += "</table></td></tr></table>"


                    Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
                    emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
                    emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
                    emailtemplate += "<title>Sir Bani Yas</title></head>"
                    emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
                    emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
                    emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
                    emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    emailtemplate += "<tr><td>&nbsp;</td></tr>"
                    emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""" & Session("domainName") & "images/banner-img.jpg"" width=""800"" /></a>"
                    emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
                    emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
                    emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                    emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">THANK YOU FOR REQUESTING ACCESS TO THE  MEDIA LIBRARY</h1>"
                    emailtemplate += "<h2 style=""color: #333; text-align: center;"">Please use your email address and password to log in</h2></td>"
                    emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Email :</strong> " & txtEmail.Text & "</p>"
                    emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Password :</strong>  " & hdnPass.Value & "</p>"
                    emailtemplate += "</tr><tr><td></td></tr>"
                    emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
                    emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
                    emailtemplate += "800-TDIC (8342)<td></tr></table>"
                    emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
                    emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"

                    Dim postData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactClass=1&Salutation=194787&FirstName=" & HttpUtility.UrlEncode(txtFName.Text) & "&FamilyName=" & HttpUtility.UrlEncode(txtLName.Text) & "&Remarks=&MobileCountryCode=&MobileAreaCode=&MobileNumber=&TelephoneCountryCode=&TelephoneAreaCode=&Telephone=&EmailAddress1=" & HttpUtility.UrlEncode(txtEmail.Text) & "&EmailAddress2=&DirectMarketing=195054~1&NationalityID=&DateofBirth=&Gender=&MaritalStatus=&ResidenceCountryID=&PassportNumber=&PassportIssueDate=&PassportExpiryDate=&IDTypeID=&IDNumber=&"
                    Dim wData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/InsertContactWithoutLead?", "POST", postData)
                    '  MsgBox(wData)
                    If wData.Contains("Success") Then
                        Utility.SendMail(txtFName.Text & " " & txtLName.Text, txtEmail.Text, "frankie@digitalnexa.com", "", "mayedul.islam@wvss.net", "Media Library Request", msg)
                        Utility.SendMail("Admin", "noreply@saadiyat.com", txtEmail.Text, "frankie@digitalnexa.com", "mayedul.islam@wvss.net", "Media Library Request", emailtemplate)
                        Response.Redirect(Session("domainName") & Session("lang") & "/thank-you/media")
                    Else

                        Dim doc = New XmlDocument()
                        doc.LoadXml(wData)
                        '  doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText
                        Dim contactId As String = Right(doc.ChildNodes(1).ChildNodes(0).ChildNodes(1).InnerText, 6).Replace(".", "")
                        Dim secondpostData As String = "AccessCode=1261a689f6&GroupCode=2048&ContactID=" & contactId & "&ContactEmail=" & HttpUtility.UrlEncode(txtEmail.Text) & "&UpdateValues=195054~1&"

                        Dim secondwData As String = WRequest("https://crm-ws.tdic.ae/website.asmx/UpdateDirectMarketing?", "POST", secondpostData)

                        If secondwData.Contains("successfully") Then
                            lbCaptchaError.Text = "You already have an account. Please login."
                            'Response.Redirect(Session("domainName") & Session("lang") & "/thank-you/media")
                        End If
                    End If




                End If
            Else
                lbCaptchaError.Text = "You already have an account. Please login."
            End If

        End If
    End Sub
    Public Function IsExist() As Boolean
        Dim sConn As String
        Dim selectString1 As String = "Select RegisterUserID from RegisteredUser where Email=@Email"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            cn.Close()
            Return True
        Else
            cn.Close()
            Return False
        End If

    End Function

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sConn As String
        Dim selectString1 As String = "Select RegisterUserID, User1 from RegisteredUser where Email=@Email and Pass1=@Pass1 and Approve=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail1.Text
        cmd.Parameters.Add("Pass1", Data.SqlDbType.NVarChar, 50).Value = txtPass.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            Response.Cookies("RegUser").Value = reader("RegisterUserID").ToString()
            Response.Cookies("RegUser").Expires = Date.Now.AddDays(+1)
            Response.Redirect(Session("domainName") & Session("lang") & "/media-image-request-logged")
            cn.Close()
        Else
            cn.Close()

        End If
    End Sub

    Function WRequest(URL As String, method As String, POSTdata As String) As String
        Dim responseData As String = ""
        ' Try
        Dim cookieJar As New Net.CookieContainer()
        Dim hwrequest As HttpWebRequest = CType(WebRequest.Create(URL), HttpWebRequest)
        'Dim hwrequest As Net.HttpWebRequest = Net.WebRequest.Create(URL)
        hwrequest.CookieContainer = cookieJar
        hwrequest.Accept = "*/*"
        hwrequest.AllowAutoRedirect = True
        hwrequest.UserAgent = "http_requester/1.1"
        hwrequest.Timeout = 60000
        hwrequest.Method = method
        If hwrequest.Method = "POST" Then
            hwrequest.ContentType = "application/x-www-form-urlencoded"
            Dim encoding As New Text.ASCIIEncoding() 'Use UTF8Encoding for XML requests
            Dim postByteArray() As Byte = encoding.GetBytes(POSTdata)
            hwrequest.ContentLength = postByteArray.Length
            Dim postStream As Stream = hwrequest.GetRequestStream()
            postStream.Write(postByteArray, 0, postByteArray.Length)
            postStream.Close()
        End If

        Dim hwresponse As WebResponse = hwrequest.GetResponse()
        responseData = New StreamReader(hwresponse.GetResponseStream()).ReadToEnd()
        'If hwresponse.StatusCode = Net.HttpStatusCode.OK Then
        '    Dim responseStream As IO.StreamReader = _
        '      New IO.StreamReader(hwresponse.GetResponseStream())
        '    responseData = responseStream.ReadToEnd()
        'End If
        hwresponse.Close()
        'Catch e As Exception
        '    responseData = "An error occurred: " & e.Message
        'End Try
        Return responseData
    End Function
    Protected Sub btnForgotPass_Click(sender As Object, e As EventArgs) Handles btnForgotPass.Click
        Dim pass As String = ""
        Dim name As String = ""
        Dim sConn As String
        Dim selectString1 As String = "Select Pass1, FName, LName from RegisteredUser where Email=@Email and Approve=1"
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim cmd As SqlCommand = New SqlCommand(selectString1, cn)
        cmd.Parameters.Add("Email", Data.SqlDbType.NVarChar, 50).Value = txtEmail1.Text
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                name = reader("FName").ToString() & " " & reader("LName").ToString()
                pass = reader("Pass1").ToString()
            End While
        Else
            Response.Redirect(Session("domainName") & "/Thank-you/wrong-media")
        End If
        cn.Close()

        Dim emailtemplate As String = "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
        emailtemplate += "<html xmlns=""http://www.w3.org/1999/xhtml"">"
        emailtemplate += "<head><meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />"
        emailtemplate += "<title>Sir Bani Yas</title></head>"
        emailtemplate += "<body style=""margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""100%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
        emailtemplate += "<tr><td style=""background-color: #f1f1f1;"">"
        emailtemplate += "<table width=""800"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td>&nbsp;</td></tr>"
        emailtemplate += "<tr><td><a href=""http://www.sirbaniyasisland.com/"" target=""_blank""><img src=""http://sby.nexadesigns.com/images/banner-img.jpg"" width=""800"" /></a>"
        emailtemplate += "</td></tr><tr><td style=""background-color: #fff;"">"
        emailtemplate += "<table width=""712"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"">"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td><h1 style=""color: #f58023; text-align: center; text-transform: uppercase; font-weight: normal"">Forgot Password! Here is your Credentials </h1>"
        emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Email :</strong> " & name & "</p>"
        emailtemplate += "<p style=""color: #333; text-align: center;""><strong>Password :</strong>  " & pass & "</p>"
        emailtemplate += "</tr><tr><td></td></tr>"
        emailtemplate += "<tr><td style=""height: 52px;"">&nbsp;</td></tr>"
        emailtemplate += "<tr><td style=""height: 52px; text-align: center;""><a href=""mailto:info@tdic.ae"" style=""color: #f58023"">info@tdic.ae</a> or call us"
        emailtemplate += "800-TDIC (8342)<td></tr></table>"
        emailtemplate += "</td></tr><tr><td>&nbsp;</td></tr></table></td></tr><tr>"
        emailtemplate += "<td style="""">&nbsp;</td></tr></table></body></html>"






        Utility.SendMail("Admin", "noreply@sirbaniyasisland.com", txtEmail.Text, "mayedul@digitalnexa.com", "frankie@digitalnexa.com", "Media Library Password Retrieval", emailtemplate)
        Response.Redirect(Session("domainName") & "/Thank-You/media")
    End Sub
End Class
